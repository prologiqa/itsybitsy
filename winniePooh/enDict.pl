%
% Winnie-the-Pooh vocabulary - based upon the root A2 vocabulary
%

:- include('enParse/enDict'). %including the Basic English dictionary


noun(donkey,donkey).
noun(emotion,emotion).
noun(feline,feline).
noun(human,human).
noun(humanoid,humanoid).
noun(hut,hut).
noun(keeper,keeper).
noun(kangaroo,kangaroo).
noun(land,land).
noun(marsupial,marsupial).
noun(mammal,mammal).
noun(omnivore,omnivore).
noun(herbivore,herbivore).
noun(predator,predator).
noun(owl,owl).
noun(pig, pig).
%noun(rabbit,rabbit).
noun(residence,residence).
noun(rodent,rodent).
noun(weasel,weasel).
noun(elephant,elephant).
noun(sex,sex).
%noun(tiger, tiger).
%noun(honey,honey).
noun(maize,maize).
noun(maltExtract,[malt,extract]).
noun(thistle,thistle).
noun(condensedMilk,[condensed,milk]).
noun(vertebrate,vertebrate).

adj(hoofed,hoofed).
adj(shy,shy).
adj(pedant,pedant).
adj(scholastic,scholastic).
adj(touchy,touchy).
adj(male,male).


proper(chrisRobin,boy,['Christopher','Robin']).
proper(winnieThePooh,bear,['Winnie','the','Pooh']).
proper(piglet,pig,'Piglet').
proper(tigger,tiger,'Tigger').
proper(owl,owl,'Owl').
proper(kanga,kangaroo,'Kanga').
proper(roo,kangaroo,['Baby','Roo']).
proper(eeyore,donkey,'Eeyore').
proper(rabbit,rabbit,'Rabbit').
proper(hundredAcreWood,woodland,['Hundred','Acre','Wood']).
proper(poohsCorner,house,['Pooh\'s','Corner']).


proper(woozle,weasel,'Woozle').
proper(wizzle,weasel,'Wizzle').


















