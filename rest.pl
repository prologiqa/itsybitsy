:- module(rest,[server/1]).

:- use_module(library(http/http_dispatch)).
:- use_module(library(http/thread_httpd)).

:- use_module(library(http/http_json)).


:- http_handler(root(parser/english), handle_request, []).


server(Port) :-
        http_server(http_dispatch, [port(Port)]).


% Calculates a + b.
%solve(_{a:X, b:Y}, _{answer:N}) :-
%    number(X),
%    number(Y),
%    N is X + Y.

solve(_{sentence:S}, _{answer:DICT}) :-
    debug(rest,'Solving sentence: ~w\n',[S]),
    string(S), string_to_tokens(S,TOKENS),
%    T=4 .
%    T = s{np:np{n:"fox"},vp:vp{v:"jump"}}.
    parseSentence(TOKENS,[TREE|_]),
    debug(rest,'Solved tree: ~w\n',[TREE]),
    pl2dict(xDTr,TREE,DICT),
    debug(rest,'Xformed dict: ~w\n',[DICT]).


handle_request(Request):-
    debug(rest,'Handler Called ~w\n',[Request]),
    http_read_json_dict(Request, Query),
    debug(rest,'JSON Read/Query ~w\n',[Query]),
    solve(Query,Solution),
    debug(rest,'Query solved ~w\n',[Solution]),
    reply_json_dict(Solution).
