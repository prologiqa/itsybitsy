:- module(dataface, [ (^) /2, (:=) /2, ? / 1,
                      wcall/2, wcall/3, wcall/4]).

%:- use_module(meta).

:- use_module(ontology).

:- use_module('../commonsense/logics').

:- use_module(library(prolog_stack)).

:- op(200,xfy,:=).

/*
%user:exception(undefined_predicate,ontology:CLASS/2,retry):-
%    HEAD=..[CLASS,SUBW,X], BODY=..[CLASS,SUPW,X],
%    WCALL=subWorldletOf(SUBW,SUPW), assert(ontology:HEAD:-WCALL,BODY).
%user:exception(undefined_predicate,ontology:PROP/3,retry):-
%    HEAD=..[PROP,SUBW,X,Y], BODY=..[PROP,SUPW,X,Y],
%    WCALL=subWorldletOf(SUBW,SUPW), assert(ontology:HEAD:-WCALL,BODY).
user:exception(undefined_predicate,ontology:NAME/ARITY,retry):-
    ARITY1 is ARITY-1, length(ARGS,ARITY1),
    HEAD=..[NAME,SUBW|ARGS], BODY=..[NAME,SUPW|ARGS],
    WCALL=subWorldletOf(SUBW,SUPW), assert(ontology:(HEAD:-WCALL,BODY)).
*/
existenceErrorParams(ontology:CLASS/1, _, root, class, CLASS):- !.
existenceErrorParams(ontology:NAME/2, STACK, WORLDLET, TYPE, NAME):-
    PRED=..[NAME,WORLDLET,_X], once(member(frame(_,_,ontology:PRED),STACK)),
    (once(ontology:worldlet(WORLDLET)), TYPE=class, ! ;
     TYPE=property, WORLDLET=root).
existenceErrorParams(ontology:NAME/3, STACK, WORLDLET, property, NAME):-
    PRED=..[NAME,WORLDLET,_X,_Y], once(member(frame(_,_,ontology:PRED),STACK)),
    once(ontology:worldlet(WORLDLET)), !.
existenceErrorParams(ontology:NAME/ARITY, STACK,
                     WORLDLET, relation/ARITY, NAME):-
    length([WORLDLET|ARGS],ARITY), PRED=..[NAME,WORLDLET|ARGS],
    once(member(frame(_,_,ontology:PRED),STACK)),
    once(ontology:worldlet(WORLDLET)), !.


%
% a simple call
%
?(foreach(FOR,EACH)):- !,
    \+ (wcalll(winniepooh,FOR), \+ wcalll(winniepooh,EACH)).

?(CALLS):- wcalll(winniepooh,CALLS).


%
%logical quantifiers
%
VAR^CALLS:- wcalll(winniepooh,CALLS).

VAR:=VAR^CALLS:- wcalll(winniepooh,CALLS).

LIST:=each(VAR^CALL):-
    findall(VAR,wcalll(winniepooh,CALL),LIST).

VALUE:=only(VAR^CALL):-
    findall(VAR,wcalll(winniepooh,CALL),[VALUE]) .

NR:=count(VAR^CALL):-
    findall(VAR,wcalll(winniepooh,CALL),LIST),length(LIST,NR).

VALUE:=list(CALL,LIST):- maplist(wcalll(winniepooh,CALL),LIST).

%lca: least common ancestor (over a tree (maybe acyclic) relation)
%only two elements are handled here!!
VALUE:=list(VAR^CALL,RELATION):-
    findall(VAR,wcall(winniepooh,CALL),[LI,ST]),
    once(lca(LI,ST,dataface:wcall(winniepooh,RELATION),VALUE)).

wcalll(W,CALL):-
    catch_with_backtrace(wcall(W,CALL),
          error(existence_error(procedure,CULPRIT),
                CONTEXT),
          (CONTEXT=context(prolog_stack(STACK),_),
          existenceErrorParams(CULPRIT,STACK,WORLDLET,TYPE,KNOW),
          existence_error(WORLDLET/TYPE,KNOW))).


wcall(_W,true):- !.
wcall(_W,false):- !, false.
wcall(W,(CALLS1,CALLS2)):-  !,
    wcall(W,CALLS1), wcall(W,CALLS2).
wcall(W,EMOPRED):-
    EMOPRED=..[EM,X], model:subDataPropertyOf(W,EM,humanState), !,
    call(ontology:emotional(W,X,EM)).
wcall(_,M:CALL):- !, call(M:CALL).
wcall(W,CALL):- CALL=..[PRED|ARGS], CALLW=..[PRED,W|ARGS],
    call(ontology:CALLW).

wcall(_,M:CALL,X):- !, call(M:CALL,X).
wcall(W,CALL,X):- CALL=..[PRED|ARGS], CALLW=..[PRED,W|ARGS],
    call(ontology:CALLW,X).

wcall(_,M:CALL,X1,X2):- !, call(M:CALL,X1,X2).
wcall(W,CALL,X1,X2):- CALL=..[PRED|ARGS], CALLW=..[PRED,W|ARGS],
    call(ontology:CALLW,X1,X2).






















