:- use_module(library(http/http_dispatch)).
:- use_module(library(http/thread_httpd)).

:- use_module(library(http/http_json)).


:- http_handler(root(parser/english), parseEnglish, []).


server(Port) :-
        http_server(http_dispatch, [port(Port)]).


parseEnglish(REQUEST):-
        http_read_json_dict(REQUEST,QUERY),
        %solve(QUERY,SOLUTION),
        SOLUTION=QUERY,
        reply_json_dict(SOLUTION).



