:- module(parser,[parseThisSentence/2, parseThisSentence/3
                  ,parseThisSentence2Dict/2
                  ,parseTestfile//3, parseTestfile//2
                  ,initParser/4 ,start/4
                 %%%%%%%%%% ,tokens2Line//1  ---see below
                 ]).
/** <module> Parser module
 *
 * The module parses sentences,
 * and creates parse trees, as Prolog structures
 * Parser/Dict/Generator modules must be loaded previously,
 * and they will be imported like plugins dynamically
 *
 * @author Kili�n Imre 14-May-2020
 *
 * @tbd implementing other languages
 *
 */

%:- use_module(dataface).


%lazy reading tokens from a file to an open list
:- use_module(lazytokens).

%prolog to dict (json) converter
:- use_module(prolog(dictUtils/prolog/pl2dict)).

%library explicite imports
:- use_module(library(apply)).
:- use_module(library(aggregate)).

%dynamic initialization/loading of plugins
initParser(PARSER,MORPHER,DICT,GEN):-
  add_import_module(parser,DICT,end), nb_setval(dict,DICT),
  add_import_module(parser,PARSER,end), nb_setval(parser,PARSER),
  add_import_module(parser,MORPHER,end), nb_setval(morpholizer,MORPHER),
  add_import_module(parser,GEN,end), nb_setval(generator,GEN).

parserMorphDictGen(PARSER,MORPHER,DICT,GEN):-
  nb_getval(dict,DICT), nb_getval(parser,PARSER),
  nb_getval(morpholizer,MORPHER), nb_getval(generator,GEN).
/*********************************

this is something to be put to language specific
morphology module (eg. to enMorph)

%it expects lines being closed by \n
%tokens2Line(-SENTENCE,+TOKENS,-REM)
%TOKENS: list of tokens (an open/lazy list)
%REM: remaining of list
%SENTENCE: one sentence (here: a line)

tokens2Line([])--> ["\n"], !.
tokens2Line(['?'])--> ["?"], !.
tokens2Line(['.'])--> ["."], !.
tokens2Line([NUMBER|SENTENCE])--> [NUMTOKEN],
  {var(NUMBER), number_string(NUMBER,NUMTOKEN)}, !,
  tokens2Line(SENTENCE).
tokens2Line([T1,T2,T3|SENTENCE])--> [ACCEND],
  {string_length(ACCEND,TL), TL1 is TL-1,
   sub_string(ACCEND,TL1,1,0,"'")}, [NEXT], {atom_string(NX,NEXT)},
  {atom_string(ACC,ACCEND), transcribe(ACC,NX,T1,T2,T3)}, !,
     tokens2Line(SENTENCE).
tokens2Line([T1,T2|SENTENCE])--> [ACCEND],
  {string_length(ACCEND,TL), TL1 is TL-1,
   sub_string(ACCEND,TL1,1,0,"'")}, [NEXT], {atom_string(NX,NEXT)},
  ({atom_string(ACC,ACCEND), transcribe(ACC,NX,T1,T2)}->
     tokens2Line(SENTENCE);
   {downcase_atom(ACCEND,ACC), transcribe(ACC,NX,T1,T2)}->
     tokens2Line(SENTENCE)  ).
%   {sub_string(ACCEND,0,TL1,1,ACC), downcase_atom(ACC,T1),
%     NX='s', T2='is'}, tokens2Line(SENTENCE)).
tokens2Line([THISTOKEN|SENTENCE])--> [DQUOTED],
   {sub_string(DQUOTED,0,1,_,"\""), string_length(DQUOTED,TL), TL>1,
    TL2 is TL-2, sub_string(DQUOTED,1,TL2,_,THISTOKEN)}, !,
    tokens2Line(SENTENCE).
tokens2Line([THISTOKEN|SENTENCE])--> [TOKEN],
  {atom_string(THISTOKEN,TOKEN)}, !, tokens2Line(SENTENCE).
tokens2Line([])--> [].
*/

skipCommentsEmpties--> ["%"], !, tokens2Line(LINE), !,
  {atomic_list_concat(["%"|LINE],' ',LINEATOM),
   debug(parser,'COMMENT:~w',[LINEATOM])},
  skipCommentsEmpties.
skipCommentsEmpties--> ["\n"], !, skipCommentsEmpties.
skipCommentsEmpties--> [].

%parseThisSentence(+SENTENCE,-FOREST,@CALL) is semidet.
%+SENTENCE: a string to contain the sentence
%-FOREST:   a list of Prolog expressions for parse trees
%@CALL:     after parsing CALL(SENTENCE,TREE) will be called
%           for each TREE^member(TREE,FOREST)
%Should parsing fail, either fails the predicate too
%or an empty list will be returned
parseThisSentence(LINE,FOREST,CALL):- string(LINE), !,
  string_to_tokens(LINE,TOKENS), parserMorphDictGen(_,MORPH,_,_),
  MORPH:tokens2Line(SENTENCE,TOKENS,[]),
  parseThisSentence(SENTENCE,FOREST,CALL).
parseThisSentence(SENTENCE,FOREST,CALL):-
  parserMorphDictGen(PARSER,_,_,GEN),
  PARSER:parseSentence(SENTENCE,FOREST), FOREST=[_|_], !,
  toSyTree(FOREST,TEXTS), maplist(debug(parser,'~w'),TEXTS),
  convlist(GEN:toSentence,FOREST,TEXTE),
  length(TEXTE,TL), length(FOREST,L),
  (TL<L -> debug(parser,'*** ERROR GENERATING ***\n',[]); true),
  foreach(member(T,TEXTE),debug(parser,'~w',[T])),
  maplist(call(CALL,SENTENCE),FOREST).
parseThisSentence(SENTENCE,[],_):-
  debug(parser,'**** ERROR ****~w',[SENTENCE]).

%parseThisSentence(+SENTENCE,-FOREST) is semidet.
%+SENTENCE: a string to contain the sentence
%-FOREST:   a list of Prolog expressions for parse trees
%Should parsing fail, either fails the predicate too
%or an empty list will be returned
parseThisSentence(S,FOREST):-
  parseThisSentence(S,FOREST,\=).


%parseThisSentence2Dict(+SENTENCE,-DICTS) is semidet.
%+SENTENCE: a string to contain the sentence
%-DICTS:    a list of Prolog expressions for parse trees
%Should parsing fail, either fails the predicate too
%or an empty list will be returned
parseThisSentence2Dict(S,DICTS):-
  parseThisSentence(S,FOREST), nb_getval(parser,PARSER),
  maplist(pl2dict(PARSER:dict),FOREST,DICTS).


%WARNING!!!! DANGER!!!!
%extra logically/destructively increment the NTH parameter
s(S,N):- length(L,N),findall(0,member(_,L),SARG), S=..[s|SARG].
incrNth(S,NTH):- arg(NTH,S,X), Y is X+1, nb_setarg(NTH,S,Y).

wSum([],0,_).
wSum([NR|NRL],SUM,N):-
  M is N+1, wSum(NRL,S,M), SUM is S+N*NR.

%CALL: without last argument (the parse tree comes here)
parseTestfile(S,N,CALL)-->
  skipCommentsEmpties, tokens2Line(SENTENCE), {SENTENCE=[_|_]},
    {debug(parser,'SENTENCE: ~w\n',[SENTENCE])},
    {parseThisSentence(SENTENCE,FOREST,CALL),
    length(FOREST,L),LN is L+1,incrNth(S,LN)}, !,
    {debug(parser,'*** NR-OF-PARSES ***~w\n',[L])},
    parseTestfile(S,N0,CALL), {N is N0+1}.
parseTestfile(_,0,_)--> ! .

parseTestfile(S,N)--> parseTestfile(S,N,\=).

%parseTestfile(+FILE,@CALL) is det.
%The specified testFILE is parsed, and for each sentence
%CALL is called...
%At the end a statistics is displayed
%For diagnostic messages call: debug(parser)
%
parseTestFile(TESTFILE,CALL,ALL,SUCC,AVG):-
  exists_file(TESTFILE),
  open(TESTFILE, read, In, []), %OpenOptions),
  stream_to_lazy_tokens(In,TOKENS), s(S,100),
  parseTestfile(S,ALL,CALL,TOKENS,_REM),
  arg(1,S,FAIL), SUCC is ALL-FAIL,
  debug(parser,'Total: ~w, Succeeded: ~w, Failed: ~w\n',[ALL,SUCC,FAIL]),
  debug(parser,'*** NR-OF-PARSES ***~w\n',[S]), %not good!!!!!!!
  S=..[_,_|SL], wSum(SL,SUM,1),
  SUCC>0, AVG is SUM/SUCC,
  debug(parser,'Average nr of parses: ~w',[AVG]),
  close(In).

%parseTestfile(+FILE)
parseTestfile(TESTFILE):- parseTestFile(TESTFILE,\=,_,_,_).


start:-
  S=[the,quick,',',brown,fox,jumps,over,the,lazy,dog,'.'],
  start(S).

start(TESTFILE,TOTAL,SUCCEED,NRPARSES):-
  parseTestFile(TESTFILE,\=,TOTAL,SUCCEED,NRPARSES).

start(STRING,CALL):- string(STRING), !,
  parseThisSentence(STRING,[_TREE|_],CALL).
start(S,CALL):- is_list(S), !, %later the complete forest!!
  parseThisSentence(S,[_TREE|_],CALL).
start(TESTFILE,CALL):-
  parseTestFile(TESTFILE,CALL,_,_,_).

start(PARM):- start(PARM,\=).

testTokenLines-->
  skipCommentsEmpties,
  tokens2Line(SENTENCE), {SENTENCE=[_|_]}, !,
  {debug(parser,'SENTENCE: ~w',[SENTENCE])},
  testTokenLines.
testTokenLines--> [].

testTokenLines(TESTFILE):-
  exists_file(TESTFILE), !,
  open(TESTFILE, read, In, []), %OpenOptions),
  (stream_to_lazy_tokens(In,TOKENS),
    testTokenLines(TOKENS,_REM), !, close(In);
   writeln('FAILED...'), close(In)).

nSpaces(N,SPACES):-
    sub_atom('                                                          ',
             0,N,_,SPACES).

toTree(TREE,TEXT):-
    toTree(0,2,TREE,TEXT,[]).

toTree(POS,INDENT,VAR)--> {var(VAR)}, !, toTree(POS,INDENT,'_').
toTree(_POS,_INDENT,[])--> [], !.
toTree(POS,INDENT,[ITEM|LIST])--> !,
    toTree(POS,INDENT,ITEM), toTree(POS,INDENT,LIST).
toTree(POS,_,L-ARGS)--> !,
  {nSpaces(POS,SPACES), term_string(L-ARGS,LI),
   atom_string(LIN,LI), atom_concat(SPACES,LIN,LINE)},
  [LINE], !.
toTree(POS,_,L/ARGS)--> !,
  {nSpaces(POS,SPACES), term_string(L/ARGS,LI),
   atom_string(LIN,LI), atom_concat(SPACES,LIN,LINE)},
  [LINE], !.
toTree(POS,INDENT,EXPR)-->
    {EXPR =.. [NAME|ARGS],
    nSpaces(POS,SPACES), atom_concat(SPACES,NAME,LINE)},
    [LINE],
    {NEXT is POS+INDENT},
    toTree(NEXT,INDENT,ARGS).


toSyTree(TREE,TEXT):-
  xTr(TREE,TREEX), toTree(TREEX,TEXT).


xTr(VAR,VAR):- var(VAR), !.

xTr(prep(P),prep-P):- !.
xTr(adj(A,G),adj-A-G):- atom(A), !.
xTr(adjP(N,U,A),meas(N,U,A)):- atomic(N), !.
xTr(adv(A,K),adv-A-K):- atom(A), !.
xTr(connect(C,K),connect-C-K):- atom(C), !.
xTr(det(D,N),det-D-N):- atom(D), !.
xTr(det(D,N,NP),detNP([DNA|NPX])):- atom(D), term_string(det-D-N,DNS),
  atom_string(DNA,DNS), !, xTr(NP,NPX).
xTr(noun(N,NUM,C),noun-N-NUM-C):- atom(N), !.
xTr(pronoun(PN,G,C),pronoun-P/N-G-C):- PN =.. [P,N], !.
xTr(verb(V,T,N,P),verb-V-T-N/P):- atom(V), !.
xTr(verb(V,T,N,P),verb-V-T-N/P):- is_list(V), !.



xTr(LIST,XLIST):- is_list(LIST), !,
  maplist(xTr,LIST,XLIST).
xTr(EXPR,XEXPR):- compound(EXPR), !,
  EXPR =.. [NAME|ARGS], xTr(NAME,XNAME),
  xTr(ARGS,XARGS), XEXPR=..[XNAME|XARGS].
xTr(X,X).



xDTr(clause(KIND,FREE1,NP,VP,FREE2),
     clause(kind(KIND),free1(FREE1),np(NP),vp(VP),free2(FREE2))).
xDTr(vp(V,MOD,T,ARGS),vp(verb(V),mod(MOD),tense(T),args(ARGS))).
xDTr(art(ART,N,K,NP),artNP(art(ART,N,K),NP)):- atom(ART), !.
xDTr(art(ART,N,K),art(stem(ART),num(N),kind(K))):- atom(ART), !.
xDTr(prep(PREP,NP),prepNP(prep(PREP),NP)):-  !.
xDTr(noun(N,NUM,C),noun(stem(N),num(NUM),case(C))):- atom(N), !.
xDTr(common(N,NUM,C),common(stem(N),num(NUM),case(C))):- atom(N), !.
xDTr(proper(N,CL,NUM,C),proper(stem(N),
                               class(CL),num(NUM),case(C))):- atom(N), !.
xDTr(pronoun(NP,G,C),pronoun(NP,gend(G),case(C)) ):- NP =.. [_N,_P], !.
xDTr(refl(PN,G),refl(PN,gend(G)) ):- PN =.. [_P,_N], !.

xDTr(adj(A,NP),adjNP(adj(A),np(NP))):- atom(A), !.
xDTr(adj(A,NP),adjNP(adj(A),np(NP))):- is_list(A), !.
xDTr(adj(adjP(A,G),NP),adjNP(adj(stem(A),grad(G)),np(NP))):- !.

xDTr(adjP(A,G),adj(stem(A),grad(G))):- !.
xDTr(adjP(N,U,A),meas(nr(N),unit(U),adj(A))):- atomic(N), !.
xDTr(adv(A,K),adv(stem(A),K)):- atom(A), !.
xDTr(det(D,N),det(stem(D),N)):- atom(D), !.
xDTr(det(D,N,NP),np(detNP(det(D,N),[DNA|NPX]))):-
  atom(D), term_string(det-D-N,DNS),
  atom_string(DNA,DNS), !, xTr(NP,NPX).
xDTr(verb(V,T,N,P),verb(stem(V),tense(T),NP)):- atom(V), NP=.. [N,P], !.
xDTr(verb(V,T,N,P),verb(stem(V),tense(T),NP)):- is_list(V),  NP=.. [N,P], !.



:- begin_tests(parser).

%test(parseRegression):-
%  start('regression.txt',TOTAL,SUCCEED,AVG), TOTAL=:=SUCCEED, AVG<2.0 .

:- end_tests(parser).

















