:- module(deGen,[
              toSentence/2, toSentence//1]).

:- use_module(prolog(dictUtils/prolog/pl2dict)).



%replaceLast(LIST,RESULT,LAST,REPLACEMENT)
%+LIST:   original list
%-RESULT: the last item is replaced
%-LAST:   last item from LIST
%?REPLACEMENT: the item to replace LAST
%
%utility predicate for verbal argument list
%that manages syntactic discontinuity
%The last element is taken out, and replaced by a var
%German: VAUX-VARG-VINF-RELSUBCL,
%Mein Vater hat etwas gebracht, womit er arbeiten kann.
replaceLast([ITEM],[VAR],ITEM,VAR):- !.
replaceLast([HEAD|TAIL],[HEAD|VARTAIL],ITEM,VAR):-
    replaceLast(TAIL,VARTAIL,ITEM,VAR).



switchOut:- false.

/** <module> German text generator module

The module traverses the syntax tree,
and generates a German sentence
in form of a word list.

creation: 03-Feb-2021

@author Kili�n Imre
*/

%Syntax tree format
%
%Sentences:
%----------
%compound(CONN,CLAUSE1,CLAUSE2) CONN: connective, CLAUSE: clause
%CLAUSE a pure clause
%?(CLAUSE) yes/no interrogative clause (without interrogative)
%?(WH,CLAUSE) wh interrogative clause (with interrogative word)
%
%Clauses:
%--------
%clause(KIND,DIR,FREE,NPS,VP)))
% - direct clause with/out free arguments�
%
%ARGUMENTS - a Prolog list of NounPhrase arguments
%FREEARGS - a Prolog list of free arguments (NP-s or adverbs)
%
%NounPhrases(NPs)
%----------------
%... is a Prolog list of possibly cumulated noun phrases
%... with the same case
%
%np(NP,PERS,GENDER,CASE)
%PERS: sg(1..3), pl(1..3)
%GENDER: male;female;neutral
%CASE: nom, acc, gen
%
%poss(NP,OWNER) - possessive noun phrase structure, owner in front
%poss(NP,OWNER) - possessive noun phrase structure, owner at the end
% - NP and OWNER are (Numeral)NounPhrasesy
%
%numeral(KIND,NR,NP) - numeral noun phrase structure
%KIND: ord;card;indef
% NR the value of numeral, if any (otherwise the numeral
% itself)
%
%adj(ADJ,NP) - ADJ: an adjective, NP: an adjective noun phrase
%noun(N) - a pure noun
% pronoun(N,HUMAN,GENDER,CASE) a pronoun
%
%VerbalPhrases(VPs)
%------------------
%vp(VERB,ADV,MODE,TENSE,ARGUMENTS,FREE)
%
% VERB: verb construction - (with auxiliaries)
% MOOD:imperative;conditional;
%      declarative;interrogative(yn);interrogative(wh)
% TENSE:present;present(perfect); -
% past;past(perfect); -
% future;future(perfect)....
%

%%%:- use_module(parser).

%:- use_module(deDict).
%dynamic import - module should be explicitely loaded by boot
%and here only the module name is used (instead of file name)
:- add_import_module(deGen,deDict,end).

:- use_module(library(apply)).
:- use_module(library(occurs)).


ifStraight(DIR,CALL,NUMPERS,CASE,X,Y):-
  DIR=straight->call(CALL,X,Y); X=Y.

ifInverted(DIR,CALL,NUMPERS,CASE,X,Y):-
  DIR=inverted->call(CALL,X,Y); X=Y.


interj2German(WORD)--> {interj(_IJ,WORD)}, [WORD].

interr2German(INTERR,KIND)--> {interr(_SEM,INTERR,KIND)},
    ({atom(INTERR)}-> [INTERR];
    {is_list(INTERR)}-> INTERR).

art2German(ART,KIND,NUM/3,GENDER,CASE)-->
  {gendnum2GenderNumber(GENDNUM,GENDER,NUM)},
  {art_(ART,KIND,GENDNUM,CASE,WORD)}, [WORD].

det2German(DET,NUMPERS,GENDER,CASE)-->
  art2German(DET,def,NUMPERS,GENDER,CASE), !.
det2German(DET,sg/_PERS,GENDER,CASE)--> demPronoun2German(DET,GENDER,CASE), !.
det2German(DET,pl/_PERS,_GENDER,CASE)--> demPronoun2German(DET,pl,CASE), !.
det2German(DET,NUMPERS,GENDER,CASE)--> {atom(DET)}, !, [DET].
det2German(DET,NUMPERS,GENDER,CASE)--> {is_list(DET)}, !, DET.

conn2German(connect(CONJ,KIND))-->
   {conj(_,CONJ,KIND)}, !, [CONJ].
conn2German(connect(CONJ,KIND/SUB))-->
   {conj(_,CONJ,SUB,KIND)}, !, [CONJ].
conn2German(PUNCT)-->
   {punct(PUNCT)}, !, [PUNCT].
conn2German(CONN)--> {conj(_,CONN,_)}, [CONN].


adv2German([])--> ! .
adv2German([ADV|LIST])--> !, adv2German(ADV), adv2German(LIST).

adv2German(adv(ADV,_,GRADE))--> adv2German(ADV,GRADE).
adv2German(adverb(ADV,_,GRADE))--> adv2German(ADV,GRADE), ! .
adv2German(ADV)--> {atom(ADV), adv_(_,ADV,_,base,ADVERB)}, !, [ADVERB].

adv2German(ADV,GRADE)--> {atom(ADV), adv_(_,ADV,_,GRADE,ADVERB)}, !, [ADVERB].

adjGrade(base,base).
adjGrade(more,comp).
adjGrade(comp,comp).
adjGrade(most,super).
adjGrade(most,super).
adjGrade(super,super).


adj2German(ADJ,GRADE,DECL,GN,CASE)-->
  {atom(ADJ), !, adj_(_,ADJ,GRADE,DECL,GN,CASE,ADJECT)}, [ADJECT].
adj2German(VERB-end,GRADE,DECL,GN,CASE)--> !,
  {adj_(_,VERB-end,GRADE,DECL,GN,CASE,ADJECT)}, [ADJECT].
adj2German(VERB-et,GRADE,DECL,GN,CASE)-->
  {adj_(_,VERB-et,GRADE,DECL,GN,CASE,ADJECT)}, [ADJECT].


adj2German(adj(ADJ,GRADE,DECL,GN,CASE))-->
  !, adj2German(ADJ,GRADE,DECL,GN,CASE).
adj2German(ADJ)--> {atom(ADJ)}, !,
  adj2German(ADJ,base,uninflected,no,nom).
adj2German(adj(ADJ))--> adj2German(ADJ), !.
adj2German(passpart(VP,GRADE,DECL,GENDNUM,CASE))-->
   {gendnum2GenderNumber(GENDNUM,GENDER,NUM)},
   !, vpPassive2German(VP,DECL,GENDER,NUM/3,CASE).
adj2German(adjP(ADV,_GRADE,ADJ))-->
  adv2German(ADV), !, adj2German(ADJ).
adj2German(adjP(KIND,NR,UNIT,ADJ))--> !, {NR=1->NUM=sg;NUM=pl},
  np2German(numeral(KIND,NR,NUM/3,common(UNIT,GENDER,NUM/3,CASE)),
            _DECL,NUM/3,_,CASE),
  adj2German(ADJ).

adj2German([],X,X):- ! .
adj2German([ADJ])-->  adj2German(ADJ), !.
adj2German([ADJ|ADJLIST])-->  adj2German(ADJ), !, [','],
   adj2German(ADJLIST).


num2ord(N,NUMBER):- LAST is N rem 10,
    (LAST=:=1->atom_concat(N,'-st',NUMBER);
     LAST=:=2->atom_concat(N,'-nd',NUMBER);
     LAST=:=3->atom_concat(N,'-rd',NUMBER);
     atomic_concat(N,'-th',NUMBER)).

%@todo num2German<-deDict:card/6 should take DECL into account
num2German(card,NUMR,DECL,NUM,GENDER,CASE)--> {atom(NUMR)}, !,
   {card(_NR,NUMR,NUM,GENDER,CASE,WORD)}, [WORD].
num2German(card,NUMR,DECL,_NUM,_G,_C)--> {atom_number(NR,NUMR)}, !, [NR].
num2German(ord,NUMR,DECL,sg,GENDER,CASE)-->
   {gendnum2GenderNumber(GENDNUM,GENDER,sg), atom(NUMR), !,
    ord(_NR,NUMR,GENDNUM,CASE,ORD)}, [ORD].
num2German(ord,NUMR,DECL,sg,GENDER,CASE)-->
   {integer(NUMR)}, !,
   num2German(card,NUMR,DECL,sg,GENDER,CASE), ['.'].
%I had 'many' children.
num2German(indef,INDEF,DECL,NUM,GENDER,CASE)--> {atomic(INDEF)},
   {gendnum2GenderNumber(GENDNUM,GENDER,NUM)},
   ({indefNum(_SEM,INDEF,unflected,GENDNUM,CASE,WORD)}->[WORD];
    {indefNum(_SEM,INDEF,DECL,GENDNUM,CASE,WORD)},[WORD]).


%%     wrappers for verb(V) structure
%verb2German(+VERB,+MODE,+TENSE,+NUMPERS,-FRAME)
%+VERB: infinite - verbstructure (prefixes, lists)
%+MODE: infinte;conjunctive;indicative;imperative
%+TENSE:infinite;present;past;perfect;participle
%+PERSNUM: sg/1..pl/3
%-FRAME:   frame definition
verb2German('',_MODE,_TENSE,_NUMPERS,[])--> !, [].
verb2German(?(PREF)-V,MODE,TENSE,NUMPERS,FRAME)--> {atom(V)}, !,
  {verb_(_,V,MODE,TENSE,NUMPERS,FORM)}, !, [FORM],
  {verbAPrefixFrame(PREF-V,FRAME)}.
verb2German(PREF-V,MODE,TENSE,NUMPERS,FRAME)--> {atom(V)},
  {\+ presentpast(TENSE)},
  {verb_(_,V,MODE,TENSE,NUMPERS,FORM)}, !, {atom_concat(PREF,FORM,VFORM)},
  [VFORM], {verbAPrefixFrame(PREF-V,FRAME)}.
verb2German(PREF-V,MODE,TENSE,NUMPERS,FRAME)--> {atom(V)},
  {presentpast(TENSE)}, {verb_(_,V,MODE,TENSE,NUMPERS,FORM)}, !,[FORM],
  {verbAPrefixFrame(PREF-V,FRAME)}.
verb2German(VERB,MODE,TENSE,NUMPERS,FRAME)-->
 {verb_(_,VERB,MODE,TENSE,NUMPERS,FORM)}, [FORM],
 {verbAPrefixFrame(VERB,FRAME)}.
verb2German([V],TENSE,MODE,NUMPERS,FRAME)--> {atom(V)},
  {verb_(_,V,MODE,TENSE,NUMPERS,FORM)}, !, [FORM], {verbAPrefixFrame(V,FRAME)}.
verb2German([A,V],MODE,TENSE,NUMPERS,FRAME)--> {atom(A), atom(V)},
  {verb_(_,V,MODE,TENSE,NUMPERS,FORM)}, !, [FORM], {verbAPrefixFrame(V,FRAME)}.

%verb2German(+VERB,+TENSE,+NUMPERS,-FRAME)--> !, [].
verb2German([PREF-V],TENSE,NUMPERS,FRAME)-->
  !, verb2German(PREF-V,TENSE,NUMPERS,FRAME).
verb2German(verb(VERB,infinite,infinite,_NUMPERS),infinite,infinite,FRAME)-->
  !, verb2German(VERB,infinite,infinite,FRAME).
verb2German(verb(VERB,MODE,TENSE,NUMPERS),TENSE,NUMPERS,FRAME)-->
  !, verb2German(VERB,MODE,TENSE,NUMPERS,FRAME).


%nondeterministically evaluates possessive pronouns
%OGEND: gender of the owner/pronoun
%NUM:   number of the expression/owned object
%GENDER:gender of the expression/owned object
possPronoun2German(PRONOUN,_DECL,OGEND,GENDER,NUM/_,CASE)-->
  {possPronoun(PRONOUN,OGEND,GENDNUM,CASE,WORD)},
    {gendnum2GenderNumber(GENDNUM,GENDER,NUM)}, [WORD].


demPronoun2German(DEM,GN,CASE)--> {atomic(DEM)},
  {demPronoun(_,DEM,_,GN,CASE,FORM)}, !, [FORM].


indefPronoun2German(INDEF,GENDER,NUM,CASE)-->
   {deDict:indefPronoun(_,INDEF,strong,GENDER,NUM,CASE,WORD)}, !, [WORD].
indefPronoun2German([INDEF],GENDER,NUM,CASE)-->
  indefPronoun2German(INDEF,GENDER,NUM,CASE).

proper2German(PROPER,_CLASS,_GENDER,_NUM,_CASE)-->
  {string(PROPER), atom_string(ATOM,PROPER)}, !, [ATOM].
proper2German([PRO|PER],CLASS,GENDER,NUM,CASE)-->
  {proper_(_SEM,[PRO|PER],GENDER,NUM,CASE,CLASS,PROPER)}, !, PROPER.
proper2German([PRO|PER],_CLASS,_GENDER,_NUM,_CASE)-->
  !, [PRO|PER].
proper2German(PROP,CLASS,GENDER,NUM,CASE)-->
  {proper_(_SEM,PROP,GENDER,NUM,CASE,CLASS,PROPER)}, !, [PROPER].
proper2German(PROPER,_CLASS,_GENDER,_NUM,_CASE)-->
  {atom(PROPER)}, !, [PROPER].


np2German(VAR,_DECL,_NUMP,_GENDER,_CASE)--> {var(VAR)}, [], !.
np2German('',_DECL,_NUMP,_GENDER,_CASE)--> [], !.
np2German(ref(_),_DECL,_NUMP,_GENDER,_CASE)--> [], !.
np2German(interr(WH,KIND,GENDNUM,CASE),_DECL,_NUMP,_GENDER,_CASE)-->
  ip2German(interr(WH,KIND,GENDNUM,CASE),CASE).
np2German(actpart(NP,VP),DECL,NUMP,GENDER,CASE)-->
  !, np2German(NP,DECL,NUMP,GENDER,CASE),
  vp2German(VP,'',straight,NUMP).
np2German(passpart(NP,VP),DECL,NUMP,GENDER,CASE)-->
  !, np2German(NP,DECL,NUMP,GENDER,CASE),
  vp2German(VP,'',straight,NUMP).
np2German(that(KIND,NP,DECLT,CL),DECL,NUMP,GENDER,CASE)-->
  {once(KIND=subject;KIND=pendent)},
  !, np2German(NP,DECL,NUMP,GENDER,CASE),
  relClause2German(that(KIND,NP,DECLT,CL)).
np2German(infinite(NP,VP),DECL,NUMP,GENDER,CASE)-->
  !, np2German(NP,DECL,NUMP,GENDER,CASE), [zu],
  vp2German(VP,'',straight,NUMP).
np2German(refl(PERSNUM,GENDER),_DECL,PERSNUM,GENDER,_)-->
     {reflPronoun(PERSNUM,ENGLISH)}, !, [ENGLISH].
np2German(refl(REFL,GENDER,CASE,NP),DECL,NUMPERS,GENDER,CASE)-->
  np2German(NP,DECL,NUMPERS,GENDER,CASE),
  {reflPronoun(REFL,ENGLISH)}, !, [ENGLISH].
np2German(wh(WH),_DECL,_NUMP,_GENDER,_CASE)--> [WH], !.
np2German(both(NP1,NP2),_DECL,pl/3,_GENDER,CASE)--> !,
  np2German(NP1,_DECL1,_NUMPERS1,_GENDER1,CASE), [und],
  np2German(NP2,_DECL2,_NUMPERS2,_GENDER2,CASE), [beide].
np2German(coordinating(CONN,[NP1,NP2]),DECL,pl/3,GENDER,CASE)--> !,
  np2German(coordinating(CONN,NP1,NP2),DECL,_NUMPERS,GENDER,CASE).
np2German(coordinating(CONN,[NP1,NP2|NPS]),_DECL,pl/3,GENDER,CASE)--> !,
  np2German(NP1,_DECL1,sg/3,_GENDER1,CASE), [','],
  np2German(coordinating(CONN,[NP2|NPS]),_DECL2,pl/3,GENDER,CASE).
np2German(coordinating(CONN,NP1,NP2),_DECL,_NUMPERS,_GENDER,CASE)--> !,
  np2German(NP1,_DECL1,_NUMPERS1,_GENDER1,CASE), conn2German(CONN),
  np2German(NP2,_DECL2,_NUMPERS2,_GENDER2,_CASE2).
np2German(correlative(CONN,NP1,NP2),_DECL,_NUMPERS,_GENDER,CASE)-->
  {correlative(CONN,C1,C2,nounPhrase)}, !, conn2German(C1),
  np2German(NP1,_DECL1,_NUMPERS1,_GENDER1,CASE), conn2German(C2),
  np2German(NP2,_DECL2,_NUMPERS2,_GENDER2,CASE).
np2German(correlative(CONN,NP1,NP2),_DECL,_NUMPERS,_GENDER,CASE)-->
  {correlative(CONN,C1,C2,C3, nounPhrase)}, !, conn2German(C1),
  np2German(NP1,_DECL1,_NUMPERS1,_GENDER1,CASE),
  conn2German(C2), conn2German(C3),
  np2German(NP2,_DECL2,_NUMPERS2,_GENDER2,CASE).
np2German(correlative(CONN,NP1,NP2),_DECL,_NUMPERS,_GENDER,CASE)-->
  {correlative(CONN,C1,C2,C3,C4, nounPhrase)}, !,
  conn2German(C1), conn2German(C2),
  np2German(NP1,_DECL1,_NUMPERS1,_GENDER1,CASE),
  conn2German(C3), conn2German(C4),
  np2German(NP2,_DECL2,_NUMPERS2,_GENDER2,CASE).
np2German(post(NP,POST),DECL,NUMPERS,GENDER,CASE)--> !,
  np2German(NP,DECL,NUMPERS,GENDER,CASE), np2German(POST,_,_,_,_).
np2German(dem(DEM,GENDER,NUM/3,CASE),_DECL,NUM/3,GENDER,CASE)-->
  {gendnum2GenderNumber(GN,GENDER,NUM)}, demPronoun2German(DEM,GN,CASE).
np2German(poss(OWNED,NUMPERS,OWNER),DECL,NUMPERS,GENDER,CASE)-->
  np2German(OWNER,_,_NUM,_,gen), np2German(OWNED,DECL,NUMPERS,GENDER,CASE), ! .
%NUMPERS: of the owned object=of the expression...
%if the owner is a possessive pronoun, it must also be passed
%eg. 'unsere Eltern'+ACC
np2German(poss(OWNED,NUMPERS,OWNER,OWNERG),DECL,NUMPERS,G,CASE)--> !,
  np2German(OWNER,_,NUMPERS,OWNERG,_OCASE),
  ({OWNER=possPronoun(_,_,_,_)}->np2German(OWNED,mixed,NUMPERS,G,CASE);
  np2German(OWNED,DECL,NUMPERS,G,CASE)).
np2German(postposs(OWNED,NUM/3,OWNER),DECL,NUM/3,GENDER,CASE)--> !,
  np2German(OWNED,DECL,NUM/3,GENDER,CASE),
  np2German(OWNER,_,_PERS,_OGENDER,gen).
np2German(numcomp(NP1,<,NP2),_DECL,NUMPERS,GENDER,CASE)-->
  [weniger], np2German(NP1,_DECL1,NUMPERS,GENDER,CASE), !, [als],
  np2German(NP2,_DECL2,_NUMP,_G2,CASE).
np2German(numcomp(NP1,>,NP2),DECL,NUMPERS,GENDER,CASE)-->
  [mehr], np2German(NP1,DECL,NUMPERS,GENDER,CASE), !, [als],
  np2German(NP2,DECL,_NUMP,_G2,CASE).
np2German(prep(PREP,ARG),DECL,NUMPERS,GENDER,CASE)-->
  !, [PREP], {ignore(once(CASE=..[PREP,CAS];CAS=CASE))},
  (np2German(ARG,DECL,NUMPERS,GENDER,CAS)->{true};
  adv2German(ARG)).
np2German(postp(POST,NP),DECL,NUMPERS,GENDER,CASE)--> !,
  np2German(NP,DECL,NUMPERS,GENDER,CASE), [POST].
np2German(NP#NR,DECL,NUMPERS,GENDER,CASE)--> !,
  np2German(NP,DECL,NUMPERS,GENDER,CASE), {atom_number(NUMR,NR)}, [NUMR].
np2German(adj(A,_GRADE,NP),DECL,NUMPERS,GENDER,CASE)-->
  adj2German(A), !, np2German(NP,DECL,NUMPERS,GENDER,CASE).
%eg. "all of" demands pl, but "his money" is sg
%eg. "either of" "my parents"
np2German(det(D,GENDER,NUMPERS,CASE,NP),DECL,NUMPERS,GENDER,CASE)-->
  det2German(D,NUMPERS,GENDER,CASE), np2German(NP,DECL,NUMPERS,GENDER,CASE).
np2German(art(ART,NUM/3,KIND,NP),_DECLT,NUM/3,GENDER,CASE)--> !,
  art2German(ART,KIND,NUM/3,GENDER,CASE),
    ({KIND==def}->np2German(NP,weak,NUM/3,GENDER,CASE);
    np2German(NP,mixed,NUM/3,GENDER,CASE)).
np2German(art(ART,rel,GENDNUM,CASE),DECL,NUM/3,GENDER,CASE)--> {atom(CASE)}, !,
  {gendnum2GenderNumber(GENDNUM,GENDER,NUM)},
  art2German(ART,rel,NUM/3,GENDER,CASE).
np2German(art(ART,rel,GENDNUM,CASE),DECL,NUM/3,GENDER,CASE)-->
  {compound(CASE)}, !,
  {CASE=..[PREP,CASE0]}, [PREP], {gendnum2GenderNumber(GENDNUM,GENDER,NUM)},
  art2German(ART,rel,NUM/3,GENDER,CASE0).
%nur wenige Schlangen: mixed->strong+mixed
np2German(numeral(KIND,NUMR,adverb(ADV,AK,base),_NUM,NP),
           DECL,NUM/3,GENDER,CASE)--> !, {ignore(DECL=mixed)},
  ({AK=comp}->num2German(KIND,NUMR,no,NUM,GENDER,CASE), {atom(NUMR)}, !,
    adv2German(adv(ADV,AK,base)), np2German(NP,no,NUM/3,GENDER,CASE);
  adv2German(adv(ADV,AK,base)), num2German(KIND,NUMR,strong,NUM,GENDER,CASE),
    np2German(NP,DECL,NUM/3,GENDER,CASE)).
%@todo "ein von meinen Eltern" - "ein von dem zw�lf???"
%      - a new parser construction is necessary for this
np2German(numeral(KIND,1,sg/3,NP),_DECL,_/3,GENDER,CASE)-->
  !, num2German(KIND,1,mixed,sg,GENDER,CASE),
  np2German(NP,mixed,_NUM/3,GENDER,CASE).
np2German(numeral(ord,N,NUM,NP),_DECL,NUM/3,GENDER,CASE)-->
  !, num2German(ord,N,weak,NUM,GENDER,CASE),
  np2German(NP,weak,NUM/3,GENDER,CASE).
np2German(numeral(KIND,N,NNUM/3,NP),DECL,_/3,GENDER,CASE)-->
  !, num2German(KIND,N,DECL,NNUM,GENDER,CASE),
  np2German(NP,DECL,_NUM/3,GENDER,_).
np2German(#(NP,NR),DECL,NUM/3,GENDER,CASE)-->
  !, np2German(NP,DECL,NUM/3,GENDER,CASE),
  num2German(card,NR,unflected,NUM,GENDER,CASE).
np2German(quantity(NR,UNIT,abbr,NUM/3,CASE),DECL,NUM/3,_GENDER,CASE)-->
  num2German(card,NR,DECL,NUME,GENDER,CASE), {ignore(NUME=NUM)},
  {once(measure(_,UNIT,_,_))}, !,
  ({atom(UNIT)}->{capitalize(UNIT,CAPUNIT)},[CAPUNIT];
  {is_list(UNIT)}, UNIT).
np2German(quantity(NR,UNIT0,full,NUM/3,CASE),DECL,NUM/3,_GENDER,CASE)-->
  num2German(card,NR,DECL,NUME,GENDER,CASE),
  {ignore(NUME=NUM)}, {once(measure(UNIT0,_,_,_))},
  {noun_(_SEM,UNIT0,GENDER,NUM,CASE,UNIT)}, !,
  ({atom(UNIT)}->{capitalize(UNIT,CAPUNIT)},[CAPUNIT];
  {is_list(UNIT)}, UNIT).
np2German(common(NOUN,GENDER,NUM/3,CASE),_DECL,NUM/3,GENDER,CASE)-->
  {once(atom(NOUN);NOUN=_+_)},
  {noun_(_SEM,NOUN,GENDER,NUM,CASE,WORD)}, !,
  {capitalize(WORD,W)},[W].
np2German(common([NOUN1,NOUN2],GENDER,NUM/3,CASE),_DECL,NUM/3,GENDER,CASE)-->
  {noun_(_SEM1,NOUN1,_G1,sg,nom,WORD1),
   noun_(_SEM2,NOUN2,GENDER,NUM,CASE,WORD2)}, !, [WORD1,WORD2].
np2German(common(NOUN1-NOUN2,GENDER,NUM/3,CASE),_DECL,NUM/3,GENDER,CASE)-->
  {noun_(_SEM1,NOUN1,_G1,sg,nom,WORD1),
   noun_(_SEM2,NOUN2,GENDER,NUM,CASE,WORD2)}, !,
  {concat_atom([WORD1,"-",WORD2],WORD12)}, [WORD12].
np2German(common([NOUN|NOUNS],GENDER,NUM/3,CASE),_DECL,NUM/3,GENDER,CASE)-->
  {noun_(_SEM,[NOUN|NOUNS],GENDER,NUM,CASE,WORDS)}, !, WORDS.
np2German(common(NOUN1-NOUN2,GENDER,NUM/3,CASE),_DECL,NUM/3,GENDER,CASE)-->
  {noun_(_SEM1,NOUN1-NOUN2,GENDER,NUM,CASE,F1-F2)}, !, [F1,-,F2].
np2German(proper(PROPER,CLASS,GENDER,NUM0/3,CASE0),_DECL,NUM/3,GENDER,CASE)-->
  {ignore(CASE0=CASE), ignore(NUM0=NUM)},
  proper2German(PROPER,CLASS,GENDER,NUM,CASE0).
np2German(noun(LIST,_NUM,_CASE),_,_,_,_)--> {is_list(LIST)}, !, LIST .
%PRONOUN: sg..pl/1..3
%OWNGEND: gender of the owner (if PRONOUN=sg/3)
%OGENDNUM:gender/number of the owned object
%OCASE:   case of the owned object
np2German(possPronoun(PRONOUN,OWNGEND,OGENDNUM,OCASE),
          DECL,ONUM/3,_OGEND,OCASE)-->
  {gendnum2GenderNumber(OGENDNUM,OGEND,ONUM)},
  possPronoun2German(PRONOUN,DECL,OWNGEND,OGEND,ONUM/3,OCASE), !.
np2German(pronoun(NUM/PERS,GENDER,CASE),_DECL,NUM/PERS,GENDER,_CASE)-->
  {persPronoun(NUM/PERS,GENDER,CASE,FORM)}, !, [FORM].
np2German(pronoun(PRONOUN,GENDER),_DECL,_NUMP,_GENDER,_CASE)-->
%if OWNER is a person???????
  {persPronoun(PRONOUN,GENDER,poss,FORM)}, !, [FORM].

np2German(indef(INDEF,GENDER,NUM/3,CASE),DECL,NUM/3,GENDER,CASE)--> !,
  indefPronoun2German(INDEF,GENDER,NUM,CASE).

np2German(NP,_DECL,_NUM,_,_CASE)--> {atom(NP)}, !, [NP].

case(nom).
case(acc).
case(gen).
case(dat).

nps2German([],_,_)--> !, {true}.
nps2German(coordinating(CONN,NP,NPS),NUMPERS,CASE)--> !,
   np2German(NP,_,NUMPERS,_,CASE), [CONN], nps2German(NPS,_NUMPERS2,CASE).
nps2German([NP|NPS],NUMPERS,CASE)--> !,
  np2German(NP,_,NUMPERS,_,CASE), nps2German(NPS,NUMPERS,CASE).
nps2German(np(NP,NUMPERS,GENDER,CASE),NUMPERS,CASE)-->
  np2German(NP,_,NUMPERS,GENDER,CASE), !.
nps2German(NP,NUMPERS,CASE)-->
  np2German(NP,_,NUMPERS,_GENDER,CASE), !.

ref2German(ADV)--> adv2German(ADV), !.
ref2German(ADJ)--> adj2German(ADJ), !.
ref2German(NP)--> np2German(NP,_,_,_,_), !.
ref2German(CL)--> clause2German(CL,declarative).

arg2German(CASE,VAR)--> {var(VAR)}, !, {CASE=clause(_)}, [].
arg2German(_,INTERR)--> {INTERR =..[interr|_]}, !.
arg2German(_,interj(IJ))--> interj2German(IJ), !.
arg2German(CASE,adverb(ADV,_NUMP,NP))-->
   {compound(NP)}, !, adv2German(adv(ADV,_,base)), arg2German(CASE,NP).
arg2German(_,ADV)--> adv2German(ADV), !.
arg2German(_,ADJ)--> adj2German(ADJ), !.
arg2German(_,CONJ)--> conn2German(CONJ), !.
arg2German(FRAME,art(ART,KIND,NP))--> art2German(ART,KIND,NUM/3,GENDER,CASE),
       ({KIND==nodef}-> np2German(NP,mixed,NUM/3,GENDER,FRAME), !;
       np2German(NP,KIND,NUM/3,GENDER,FRAME), !).
arg2German(_CASE,ord(N))--> num2German(ord,N,DECL,NUM,GENDER,CASE).
arg2German(CASE,connected(CONN,NPP1,NPP2))-->
    arg2German(CASE,NPP1), conn2German(CONN), arg2German(CASE,NPP2).
arg2German(CASE,connected(CONN,NPP1,punct(P),NPP2))-->
    arg2German(CASE,NPP1), [P], conn2German(CONN), arg2German(CASE,NPP2).
arg2German(ADVPLACE,adverb(ADV,ADVPLACE,base,
                  interr(WH,ADVPLACE,_,ADVPLACE),
                  CLAUSE))-->
  {ADVPLACE=adv(place)}, adv2German(adverb(ADV,ADVPLACE,base)),[','],
    ip2German(interr(WH,ADVPLACE,_,ADVPLACE),ADVPLACE),
    clause2German(CLAUSE,declarative).
arg2German(_FRAMES,comp(REF1,<,REF2))-->
       ref2German(REF1), !, [als], ref2German(REF2).
arg2German(_FRAMES,comp(REF1,>,REF2))-->
       ref2German(REF1), !, [als], ref2German(REF2).
arg2German(_FRAMES,comp(REF1,=,REF2))-->
       [so], ref2German(REF1), !, [wie], ref2German(REF2).
arg2German(clause(wh),connect(IP,CLAUSE))-->
       !, ip2German(IP,_KIND),
       arg2German(clause,CLAUSE).
arg2German(clause(C),connect(CONN,CLAUSE))-->
       {C \= wh}, !,
       {conj(CONN,CONNECT,subclause(_))},[','], conn2German(CONNECT),
       arg2German(clause,CLAUSE).
arg2German(clause,clause(KIND,DIR,FREE,NP,VP,NUMPERS))--> !,
     clause2German(clause(KIND,DIR,FREE,NP,VP,NUMPERS),declarative).
arg2German(verbal,verbal(VP))--> !,
   vp2German(VP,'',straight,sg/_P).
arg2German(verbal(to),verbal(to,VP))--> !,
   vp2German(VP,'',infinite,inverted,sg/_P).
%arg2German(verbal(ing),verbal(ing,VP))--> !,
%   vp2German(VP,'',straight,sg/_P).
arg2German(verbal(wh),verbal(IP,VP))--> !,
   ip2German(IP,_KIND), [zu],
   vp2German(VP,'',straight,sg/_P).
arg2German(CASE,NP)-->
    {NP = prep(PRE,_), nonvar(CASE)}, !,
    ({CASE=..[PRE,PURECASE]}->np2German(NP,_DECL,_NUMPERS,_GENDER,PURECASE);
    {once(CASE=nom;CASE=acc)},np2German(NP,_DECL,_NUMPERS,_GENDER,PURECASE)).
arg2German(_,NP)-->
    np2German(NP,_DECL,_NUMPERS,_GENDER,_CASE).

%invoked by args2German//1
arg2German(ARG)--> arg2German(_,ARG).


%args2German(FRAMES,ARGS)
%+ARGS: argument values
%+FRAMES: frame descriptions
args2German(_,VAR)--> {var(VAR)}, !, {fail}.
args2German(CASES,ARGS)-->foldl(arg2German,CASES,ARGS).


%args2German(FRAMES,ARGS,RELCLAUSE)
%+ARGS:      argument values
%+FRAMES:    frame descriptions
%-RELCLAUSE: relative clause, attached to the last argument
args2German(_,VAR,_)--> {var(VAR)}, !, {fail}.
args2German(CASES,ARGS0,that(back,LAST,RELATIVE,RELCLAUSE))-->
  {ground(ARGS0)},
  {replaceLast(ARGS0,ARGS,that(back,LAST,RELATIVE,RELCLAUSE),LAST)}, !,
  foldl(arg2German,CASES,ARGS).
args2German(CASES,ARGS,_)-->
  args2German(CASES,ARGS).


opt1Args2German([?(FRAME)|FRAMES],ARGUMENTS)-->
   args2German([FRAME|FRAMES],ARGUMENTS), !.
opt1Args2German([?(_)|FRAMES],ARGUMENTS)-->
   args2German(FRAMES,ARGUMENTS).

args2German(ARGS)-->foldl(arg2German,ARGS).

frees2German(FREES)-->args2German(FREES).

callNT(NT,X0,X):- call(NT,X0,X).

ifStraightNPS(straight,NPS,NUMPERS,CASE)--> {NPS\=''}, !,
    callNT(nps2German(NPS,NUMPERS,CASE)).
ifStraightNPS(_,_,_,_,X,X).

ifInvertedNPS(inverted,NPS,NUMPERS,CASE)--> {NPS\=''}, !,
    callNT(nps2German(NPS,NUMPERS,CASE)).
ifInvertedNPS(_,_,_,_,X,X).

ifOppositeNPS(opposite,NPS,NUMPERS,CASE)--> {NPS\=''}, !,
    callNT(nps2German(NPS,NUMPERS,CASE)).
ifOppositeNPS(_,_,_,_,X,X).

/*
%kindOfMood(imperative).
kindOfMood(declarative).
kindOfMood(interrogative(yn)).
kindOfMood(interrogative(wh)).
%kindOfMood(conditional).
*/

kindOfDir(straight).
kindOfDir(inverted).
kindOfDir(opposite).

%auxVerb2German(AUX,MODE,TENSE,PERS)
%+AUX: auxiliary verb, optionally empty ('')
%+MODE: of the verb: indicative;conjunctive;imperative
%+TENSE: - tense of the auxiliary verb
%+PERS: person of verb
%if AUX is
auxVerb2German('',_MODE,_TENSE,_PERS)--> [] .
%'w�rde'
auxVerb2German(werd,conjunctive,past,PERS)-->
 {verb_(_,werd,conjunctive,past,PERS,FORM)}, [FORM], !.
auxVerb2German(AUX,MODE,TENSE,PERS)-->
 {aux(AUX)},{verb_(_,AUX,MODE,TENSE,PERS,FORM)}, [FORM], !.
auxVerb2German(MODAL,MODE,TENSE,PERS)-->
 {modal(MODAL,_,_,verb)}, {verb_(_,MODAL,MODE,TENSE,PERS,FORM)}, !,
 [FORM].
auxVerb2German(MODAL,_MODE,_TENSE,_PERS)-->
 {modal(MODAL,_,_,verbal(to))}, !, [MODAL],[zu].


%vp(VERB,MADV,MODE,ARGUMENTS,TENSE,FREE)
%+VERB: a verb structure (evtl with auxiliaries)
%+ADV: if the verbal structure had a middle adverb
%?MODE=imperative;conditional;
%      declarative;interrogative(wh);interrogative(yn)
%+ARGUMENTS: verbal arguments
%?MODAL= ????....can, should, would????????
%?TENSE=present;present(perfect);present(participle)...
%?TENSE=past...;future
%+FREE: list of free arguments
%

optGenMiddleAdv('')-->[], !.
%optGenMiddleAdv(adverb(_,neg,base))-->[], !.
optGenMiddleAdv(ADV)-->adv2German(ADV).

optGenCommaAfterRelclause(NP)-->
  {functor(NP,that,4)}, !, [,]; [].


% vp2German//10
% vp2German(+VERB,+NPS,+FRAMES,-REM,+ARGS,+AUX,
%            +ADV,+MODE,+TENSE,+PERS)
% +VERB: atom
% +NPS: Noun phrase structure
%       (evtly a list of them->NOT IMPLEMENTED)
% +FRAMES: frame specification forced from external
%          (eg. from macro)
% -REM: not processed tail of frame specification list
%           (FRAMES-REM is an accumulator pair!!)
% +ARGS: verbal argument list
% +AUX: auxiliary verb
% +ADV: adverb between auxiliary and verb or ''
% +MODE: (sentence) VP mode...
%        declarative;interrogative(yn);interrogative(wh)
% +TENSE: verbal tense structure (see above)
% +PERS: NUMBER(PERSON) structure (sg(1)..pl(3))
%
% The predicate is the working horse of VP generation
%
% straight word order without sentence bound free arguments.
% "Mein Vater faehrt sein Auto."
% inverted word order: with sentence bound free arguments
% "Jetzt faehrt mein Vater sein Auto."
% interrogative(yn) wrapper: inverted order of words
% "Faehrt mein Vater sein Auto?"
% interrogative(subj) wrapper: straight order of words
% "Wer faehrt sein Auto?"
% interrogative(wh) wrapper: inverted order of words
% "Was faehrt mein Vater?"

:- discontiguous vp2German//10.

%repeated verb wrapper (4verbs)
vp2German(verb([AUX,_AU,_A,VERB],MODE,TENSE,NUMPERS),
           NP,ARGUMENTS,_AUX,ADV,MODE,TENSE,NUMPERS)-->
  \+ {modal(AUX)}, !,
  vp2German(VERB,MODE,NP,ARGUMENTS,'',ADV,MODE,TENSE,NUMPERS).
vp2German(verb([AUX,_AU,_A,VERB],MODE,TENSE,NUMPERS),
           NP,ARGUMENTS,_AUX,ADV,MODE,TENSE,NUMPERS)-->
  {modal(AUX)}, !,
  vp2German(VERB,MODE,NP,ARGUMENTS,AUX,ADV,MODE,TENSE,NUMPERS).
%repeated verb wrapper (3verbs)
vp2German(verb([AUX,AU-ADV,VERB],MODE,TENSE,NUMPERS),
           NP,ARGUMENTS,AUXX,'',MODE,TENSE,NUMPERS)--> !,
  vp2German(verb([AUX,AU-ADV,VERB],MODE,TENSE,NUMPERS),
            NP,ARGUMENTS,AUXX,ADV,MODE,TENSE,NUMPERS).
%repeated verb wrapper
vp2German(verb([AUX,_AU,VERB],MODE,TENSE,NUMPERS),
           NP,ARGUMENTS,_AUX,ADV,MODE,TENSE,NUMPERS)-->
  \+ {modal(AUX)}, !,
  vp2German(VERB,MODE,NP,ARGUMENTS,'',ADV,MODE,TENSE,NUMPERS).
%repeated verb wrapper
vp2German(verb([AUX,_AU,VERB],MODE,TENSE,NUMPERS),
           NP,ARGUMENTS,_AUX,ADV,MODE,TENSE,NUMPERS)-->
  {modal(AUX)}, !,
  vp2German(VERB,MODE,NP,ARGUMENTS,AUX,ADV,MODE,TENSE,NUMPERS).
vp2German(verb([AUX,VERB],MODE,TENSE,NUMPERS),
           NP,ARGUMENTS,_,ADV,MODE,TENSE,NUMPERS)-->
  {\+ modal(AUX), VERB\=''}, !,
  vp2German(VERB,MODE,NP,ARGUMENTS,'',ADV,MODE,TENSE,NUMPERS).
vp2German(verb([AUX,VERB],MODE,TENSE,NUMPERS),
           NP,ARGUMENTS,_AUX,ADV,MODE,TENSE,NUMPERS)-->
  {once(modal(AUX);VERB='')}, !,
  vp2German(VERB,MODE,NP,ARGUMENTS,AUX,ADV,MODE,TENSE,NUMPERS).
vp2German(verb([VERB],MODE,TENSE,NUMPERS),
           NP,ARGUMENTS,AUX,ADV,MODE,TENSE,NUMPERS)--> !,
  vp2German(VERB,MODE,NP,ARGUMENTS,AUX,ADV,MODE,TENSE,NUMPERS).
%general case
vp2German(verb(VERB,MODE,TENSE,NUMPERS),
           NP,ARGUMENTS,AUX,ADV,MODE,TENSE,NUMPERS)-->
  {atom(VERB)}, !,
  vp2German(VERB,MODE,NP,ARGUMENTS,AUX,ADV,MODE,TENSE,NUMPERS).


%passive VP in an adjective role
%(die) gestern von dir gesehene Frau (ist wirklich sch�n)
vpPassive2German(vp(verb(VERB,MODE,TENSE,NUM/3),
                    ADV,MODE,TENSE,ARGUMENTS,FREE),
                 DECL,GENDER,NUM/3,CASE)-->
  {TENSE=past(participle)},
  opt1Args2German([?(von(dat))|FRAMES],ARGUMENTS),
  optGenMiddleAdv(ADV),
%  verb2German(VERB,past(participle),NUM/3,VFRAMES),
  {verbAPrefixFrame(VERB,VFRAMES), adj_(_,VERB-et,base,DECL,GN,CASE,WORD)},
  {gendnum2GenderNumber(GN,GENDER,NUM)}, [WORD],
  {ignore([nom,acc|FRAMES]=VFRAMES), ignore(FRAMES=[])}.

%The working horse that pulls the big load
%vp2German(VERB,MODE,NP,ARGS,AUX,ADV,FREES,DIR,TENSE,NUMPERS).
%+VERB: pure main verb
%+MODE: mode of the verb (declarative,conjunctive,imperative)
%+NP:   noun phrase structure, the subject
%+ARGS: list of verbal arguments
%+AUX:  auxiliary word (that comes to the begin of the sentence)
%+ADV:  adverb
%+FREES:list of free arguments
%+DIR:  direction of the sentence (straight,inverted,opposite)
%+TENSE:tense structure
%+NUMPERS: sg..pl/1..3
%
%%%%%%%%% INFINITE we dont allow free arguments here
vp2German(VERB,_MODE,_NP,ARGUMENTS,'',ADV,[],straight,infinite,_NUMPERS)--> !,
  optGenMiddleAdv(ADV),
  verb2German(VERB,_,infinite,infinite,VFRAMES),
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  args2German(FRAMES,ARGUMENTS).
vp2German(VERB,_MODE,_NP,ARGUMENTS,'',ADV,[],inverted,infinite,_NUMPERS)--> !,
  {verbAPrefixFrame(VERB,VFRAMES)},
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  args2German(FRAMES,ARGUMENTS), optGenMiddleAdv(ADV), [zu],
  verb2German(VERB,_,infinite,infinite,VFRAMES).

vp2German(VERB,MODE,NP,ARGUMENTS,AUX,ADV,straight,infinite,_PERS)--> !,
  vp2German(VERB,MODE,NP,ARGUMENTS,AUX,ADV,straight,present,pl/3).

%%%%%%%%% present participle (active continuous)
vp2German(VERB,_MODE,'',ARGUMENTS,'',ADV,FREES,
           _DIR,present(participle),_NUMPERS)-->
  optGenMiddleAdv(ADV),
  verb2German(VERB,_,present(participle),_,VFRAMES),
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  args2German(FRAMES,ARGUMENTS), frees2German(FREES).

%%%%%%%%% past participle
vp2German(VERB,_MODE,NP,ARGUMENTS,'',ADV,FREES,
           DIR,past(participle),NUMPERS)-->
  {kindOfDir(DIR)},
  ifStraightNPS(DIR,NP,NUMPERS,nom),
  optGenMiddleAdv(ADV),
  ifInverted(DIR,opt1Args2German([?(von(dat))|FRAMES],ARGUMENTS),NUMPERS,nom),
%  verb2German(VERB,_,past(participle),_,VFRAMES),
  {verbAPrefixFrame(VERB,VFRAMES), adj_(_,VERB-et,base,DECL,GN,nom,WORD)},
  [WORD],
  {ignore([nom,acc|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  ifStraight(DIR,opt1Args2German([?(von(dat))|FRAMES],ARGUMENTS),NUMPERS,nom),
  frees2German(FREES),
  ifInvertedNPS(DIR,NP,NUMPERS,nom).


%%%%%%%%% ACTIVE PRESENT AND PAST
presentpast(present).
presentpast(past).

%simple present/past with/out auxiliary verb
vp2German(VERB,MODE,NP,ARGUMENTS,AUX,ADV,FREES,
           DIR,TENSE,NUMPERS)-->
%'Er schreibt ein Buch'
%'Er kann ein Buch schreiben'
  {presentpast(TENSE), verbAPrefixFrame(VERB,VFRAMES)},
  ({DIR==straight}->nps2German(NP,NUMPERS,nom),
    optGenCommaAfterRelclause(NP),
    auxVerb2German(AUX,MODE,TENSE,NUMPERS),
    optGenMiddleAdv(ADV),
    ({AUX==''}->verb2German(VERB,MODE,TENSE,NUMPERS,VFRAMES);[]),
    {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
    args2German(FRAMES,ARGUMENTS,RELCLAUSE), frees2German(FREES),
    ({AUX\==''}->verb2German(VERB,_,infinite,infinite,VFRAMES);[]),
    relClause2German(RELCLAUSE);
%'Schreibt er ein Buch zu Hause'?
%'Kann er nicht ein Buch zu Hause schreiben'?
%'H�rst du auf schlimme Geschichten zu erz�hlen'?
  {DIR==inverted}->
    ({AUX\==''}->auxVerb2German(AUX,MODE,TENSE,NUMPERS);
    verb2German(VERB,MODE,TENSE,NUMPERS,VFRAMES)),
    nps2German(NP,NUMPERS,nom),
    optGenMiddleAdv(ADV),
    ({AUX\==''}->verb2German(VERB,_,infinite,infinite,VFRAMES);[]),
    optGenCommaAfterRelclause(NP),
    {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
    ({FRAMES=[verbal(to)]}-> ({VERB= ?(PRE)-_V} -> [PRE];[]),
      args2German(FRAMES,ARGUMENTS), frees2German(FREES);
    args2German(FRAMES,ARGUMENTS), frees2German(FREES),
    ({AUX\==''}->verb2German(VERB,_,infinite,infinite,VFRAMES);[]),
    ({VERB= ?(PRE)-_V} -> [PRE];[]));
%(...ob) 'er ein Buch zu Hause scrieb'
%(...ob) 'er ein Buch zu Hause schreiben kann'
  {DIR==opposite}, nps2German(NP,NUMPERS,nom),
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  args2German(FRAMES,ARGUMENTS), frees2German(FREES),
  optGenMiddleAdv(ADV),
  ({AUX=''}->verb2German(VERB,MODE,TENSE,NUMPERS,VFRAMES);
  verb2German(VERB,_,infinite,infinite,VFRAMES),
    auxVerb2German(AUX,MODE,TENSE,NUMPERS))).

presentpastPerfect(present(perfect),present).
presentpastPerfect(past(perfect),past).
%present/past perfect without modal auxiliary
vp2German(VERB,MODE,NP,ARGUMENTS,AUX,ADV,FREES,
           DIR,TENSE,NUMPERS)-->
  {presentpastPerfect(TENSE,MTENSE)}, {kindOfDir(DIR)},
  {verbParticipleAux(VERB,AUX),verbAPrefixFrame(VERB,VFRAMES)},
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  %'er hat(te) gestern zu Hause ein Buch geschrieben'.
  ({DIR==straight}->nps2German(NP,NUMPERS,nom),
    optGenCommaAfterRelclause(NP),
    auxVerb2German(AUX,MODE,MTENSE,NUMPERS),
    optGenMiddleAdv(ADV),
    args2German(FRAMES,ARGUMENTS,RELCLAUSE), frees2German(FREES),
    verb2German(VERB,MODE,past(participle),infinite,_),
    relClause2German(RELCLAUSE);
  {DIR==inverted}->auxVerb2German(AUX,MODE,MTENSE,NUMPERS),
    nps2German(NP,NUMPERS,nom), optGenCommaAfterRelclause(NP),
    optGenMiddleAdv(ADV),
    args2German(FRAMES,ARGUMENTS), frees2German(FREES),
    verb2German(VERB,MODE,past(participle),infinite,_);
  {DIR==opposite}->nps2German(NP,NUMPERS,nom),
    optGenMiddleAdv(ADV),
    args2German(FRAMES,ARGUMENTS), frees2German(FREES),
    verb2German(VERB,MODE,past(participle),infinite,_),
    auxVerb2German(AUX,MODE,MTENSE,NUMPERS)).

%present/past perfect with modal auxiliary
vp2German(VERB,MODE,SUBJ,ARGUMENTS,MOD,ADV,FREES,
           DIR,mod(TENSE),NUMPERS)-->
  {presentpastPerfect(TENSE,MTENSE)}, {kindOfDir(DIR)},
  {verbParticipleAux(VERB,AUX),verbAPrefixFrame(VERB,VFRAMES)},
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  %'er hat(te) gestern zu Hause ein Buch schreiben m�ssen'.
  ({DIR==straight}->nps2German(SUBJ,NUMPERS,nom),
    optGenCommaAfterRelclause(SUBJ),
    auxVerb2German(AUX,MODE,MTENSE,NUMPERS),
    optGenMiddleAdv(ADV),
    args2German(FRAMES,ARGUMENTS,RELCLAUSE), frees2German(FREES),
    verb2German(VERB,_,infinite,infinite,_),
    verb2German(MOD,_,infinite,infinite,_),
    relClause2German(RELCLAUSE);
  {DIR==inverted}->auxVerb2German(AUX,MODE,MTENSE,NUMPERS),
    nps2German(SUBJ,NUMPERS,nom), optGenCommaAfterRelclause(SUBJ),
    optGenMiddleAdv(ADV),
    args2German(FRAMES,ARGUMENTS), frees2German(FREES),
    verb2German(VERB,_,infinite,infinite,_),
    verb2German(MOD,_,infinite,infinite,_);
  {DIR==opposite}->nps2German(SUBJ,NUMPERS,nom),
    optGenMiddleAdv(ADV),
    args2German(FRAMES,ARGUMENTS), frees2German(FREES),
    verb2German(VERB,_,infinite,infinite,_),
    verb2German(MOD,_,infinite,infinite,_),
    auxVerb2German(AUX,MODE,MTENSE,NUMPERS)).

%%%%%%%%%% ACTIVE FUTURE
%declarative/interrogative simple future
vp2German(VERB,MODE,NP,ARGUMENTS,AUX,ADV,FREES,
           DIR,future,NUMPERS)-->
  {kindOfDir(DIR)}, ifStraightNPS(DIR,NP,NUMPERS,nom),
  ifOppositeNPS(DIR,NP,NUMPERS,nom),
  verb2German(sei,MODE,future,NUMPERS,_),
  ifInvertedNPS(DIR,NP,NUMPERS,nom), optGenMiddleAdv(ADV),
  {verbAPrefixFrame(VERB,VFRAMES)},
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  args2German(FRAMES,ARGUMENTS), frees2German(FREES),
  verb2German(VERB,_,infinite,infinite,VFRAMES).
%  auxVerb2German(AUX,_,_,infinite). %maybe for modal auxiliaries?

%declarative/interrogative future perfect
vp2German(VERB,MODE,NP,ARGUMENTS,_AUX,ADV,FREES,
           DIR,future(perfect),NUMPERS)-->
  {kindOfDir(DIR)}, ifStraightNPS(DIR,NP,NUMPERS,nom),
  verb2German(sei,MODE,future,NUMPERS,_),
  ifInvertedNPS(DIR,NP,NUMPERS,nom), optGenMiddleAdv(ADV),
  verb2German(hab,_,infinite,infinite,_),
  verb2German(VERB,_,past(participle),_,VFRAMES),
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  args2German(FRAMES,ARGUMENTS), frees2German(FREES).

%
% PASSIVE SIMPLE PRESENT/PAST
%
%declarative/straight - interrogative/inverted
%Wie viele
vp2German(V,MODE,NP,ARGUMENTS,werd,ADV,FREES,
           DIR,passive(TENSE),NUMPERS)-->
  {presentpast(TENSE)}, !, {kindOfDir(DIR)},
  ifStraightNPS(DIR,NP,NUMPERS,nom),
  verb2German(werd,MODE,TENSE,NUMPERS,_),
  ifInvertedNPS(DIR,NP,NUMPERS,nom), optGenMiddleAdv(ADV),
  frees2German(FREES),
  verb2German(V,_,past(participle),_,[nom,acc|FRAMES]),
  opt1Args2German([?(von(dat))|FRAMES],ARGUMENTS).

%declarative/straight - interrogative/inverted with modal verb
vp2German(V,MODE,NP,ARGUMENTS,AUX,ADV,FREES,
           DIR,passive(mod,TENSE),NUMPERS)-->
  {presentpast(TENSE)}, !, {kindOfDir(DIR)},
  ifStraightNPS(DIR,NP,NUMPERS,nom),
  auxVerb2German(AUX,MODE,TENSE,NUMPERS),
  ifInvertedNPS(DIR,NP,NUMPERS,nom),
  optGenMiddleAdv(ADV),
  verb2German(V,_,past(participle),_,[nom,acc|FRAMES]),
  opt1Args2German([?(von(dat))|FRAMES],ARGUMENTS),
  frees2German(FREES),
  verb2German(werd,_,infinite,infinite,_).


%
% PASSIVE SIMPLE PRESENT/PAST PERFECT
%
%declarative/straight
vp2German(V,MODE,NP,ARGUMENTS,MODAUX,ADV,FREES,
           straight,passive(TENSE),NUMPERS)-->
  {presentpastPerfect(TENSE,HATENSE)}, !,
  callNT(nps2German(NP,NUMPERS,nom)),
  ({MODAUX==''}->
    verb2German(hab,MODE,HATENSE,NUMPERS,_),
    optGenMiddleAdv(ADV),
    opt1Args2German([?(von(dat))|FRAMES],ARGUMENTS),
    frees2German(FREES),
    verb2German(V,_,past(participle),_,[nom,acc|FRAMES]),
    verb2German(werd,_,past(participle),_,_);
  verb2German(MODAUX,MODE,HATENSE,NUMPERS,_),
    optGenMiddleAdv(ADV),
    opt1Args2German([?(von(dat))|FRAMES],ARGUMENTS),
    frees2German(FREES),
    verb2German(V,_,past(participle),_,[nom,acc|FRAMES]),
    verb2German(werd,_,past(part),_,_)
  ).

%interrogative/inverted
vp2German(V,MODE,NP,ARGUMENTS,'',ADV,FREES,
           inverted,passive(TENSE),NUMPERS)-->
  {presentpastPerfect(TENSE,HATENSE)}, !,
  verb2German(hab,MODE,HATENSE,NUMPERS,_),
  callNT(nps2German(NP,NUMPERS,nom)),
  optGenMiddleAdv(ADV),
  opt1Args2German([?(von(dat))|FRAMES],ARGUMENTS),
  frees2German(FREES),
  verb2German(werd,indicative,past(participle),_,_),
  verb2German(V,indicative,past(participle),_,[nom,acc|FRAMES]).

%
% PASSIVE SIMPLE FUTURE
%
%declarative/straight
vp2German(V,MODE,NP,ARGUMENTS,'',ADV,FREES,
           straight,passive(future),NUMPERS)-->
  !,
  callNT(nps2German(NP,NUMPERS,nom)), verb2German(sei,future,NUMPERS,_),
  optGenMiddleAdv(ADV),
  opt1Args2German([?(von(dat))|FRAMES],ARGUMENTS),
  frees2German(FREES),
  verb2German(V,_,past(participle),_,[nom,acc|FRAMES]),
  verb2German(werd,_,infinite,infinite,_).

%interrogative/inverted
vp2German(V,MODE,NP,ARGUMENTS,'',ADV,FREES,
           inverted,passive(future),NUMPERS)-->
  !,
  verb2German(werd,MODE,future,NUMPERS,_),
  callNT(nps2German(NP,NUMPERS,nom)), optGenMiddleAdv(ADV),
  opt1Args2German([?(von(dat))|FRAMES],ARGUMENTS),
  frees2German(FREES),
  verb2German(werd,_,infinite,infinite,_),
  verb2German(V,_,past(participle),_,[nom,acc|FRAMES]).


%
%VP... wrappers
%
%vp2German//4
%vp2German(VP,NPS,DIR,PERSNUM)
%VP:  verbal phrase
%NP:  noun phrase (subject)
%DIR: straight;inverted;opposite
%PERSNUM sg;pl/1..3
%
%coordinated list of noun phrases
%general case and renaming to vp2German//11
vp2German(coordinating(_CONJ,[VP]),NPS,DIR,PERSNUM)-->
  !, vp2German(VP,NPS,DIR,PERSNUM).
vp2German(coordinating(CONJ,[VP|VPS]),
           NPS,DIR,PERSNUM)--> !,
 vp2German(VP,NPS,DIR,PERSNUM), [CONJ],
   vp2German(coordinating(CONJ,VPS),'',DIR,PERSNUM).

%wrapper to generate verb particle at the end
%change to a special particle - frames are particle dependent
%V-?(PART) dummy particle wrt frames must be fetched...???????
vp2German(vp(verb(VERB^PREF,MODE,TENSE,PERSNUM),ADV,VMODE,TENSE,ARGS,FREES),
           NPS,DIR,PERSNUM)-->  !,
    vp2German(vp(verb(PREF-VERB,MODE,TENSE,PERSNUM),ADV,VMODE,TENSE,ARGS,FREES),
           NPS,DIR,PERSNUM), [PREF].

%general case without aux (in list) and renaming to vp2German//10
vp2German(vp(verb([VERB],MODE,TENSE,_),ADV,VMODE,TENSE,ARGUMENTS,FREES),
           NPS,DIR,PERSNUM)--> !,
  vp2German(vp(verb(VERB,MODE,TENSE,_),ADV,VMODE,TENSE,ARGUMENTS,FREES),
           NPS,DIR,PERSNUM).
%general case with two auxes and renaming to vp2German//10
vp2German(vp(verb([AUX1,V,AUX2],MODE,TENSE,PERSNUM),ADV,MODE,TENSE,ARGS,FREES),
           NPS,DIR,PERSNUM)-->
    {AUX2=werd}, !,
     vp2German(V,MODE,NPS,ARGS,AUX1,ADV,FREES,DIR,TENSE,PERSNUM);
    {modal(AUX2)}, !,
     vp2German(V,MODE,NPS,ARGS,AUX2,ADV,FREES,DIR,TENSE,PERSNUM).
%general case with one aux and renaming to vp2German//10
vp2German(vp(verb([AUX,V],MODE,TENSE,PERSNUM),ADV,MODE,TENSE,ARGS,FREES),
           NPS,DIR,PERSNUM)--> !,
     vp2German(V,MODE,NPS,ARGS,AUX,ADV,FREES,DIR,TENSE,PERSNUM).
%general case without aux and renaming to vp2German//10
vp2German(vp(verb(VERB,MODE,TENSE,PERSNUM),ADV,MODE,TENSE,ARGUMENTS,FREES),
           NPS,DIR,PERSNUM)--> {\+ is_list(VERB)}, !,
  vp2German(VERB,MODE,NPS,ARGUMENTS,'',ADV,FREES,DIR,TENSE,PERSNUM).

%to generate zu infinitive clauses (without subject)
vp2German(vp(verb(VERB,MODE,infinite,PERSNUM),ADV,MODE,infinite,ARGUMENTS,FREES),
           '',infinite,DIR,PERSNUM)-->
  vp2German(VERB,MODE,'',ARGUMENTS,'',ADV,FREES,DIR,infinite,PERSNUM).


relClause2German(VAR)--> {var(VAR)}, !, [] .
relClause2German(that(_,_,RELATIVE,RELCL))-->
  [,], np2German(RELATIVE,_,_,_,_), clause2German(RELCL,declarative).

simpleWhKind(wie,adv(manner)).
simpleWhKind(was,acc).
simpleWhKind(was,nom).
simpleWhKind(warum,adv(reason)).
simpleWhKind(wann,adv(time)).
simpleWhKind(wo,adv(place)).
simpleWhKind(wer,nom).
simpleWhKind(woher,adv(place)).
simpleWhKind(wohin,adv(place)).
simpleWhKind(wessen,gen).

%wie... warum ... wann ... wo ... woher ... wohin
ip2ipp(interr(WH,KIND,NP,NUMPERS),'',interr(WH,KIND,NP,NUMPERS)):-
   simpleWhKind(WH,KIND), !.
% welches nette M�dchen, welches rote Auto
% wie sch�ne M�dchen, wie schnelles Auto
% wie viel rote �pfel
% %wie lang (an adverb)
ip2ipp(interr(WH,ADJADVKIND,ADJADV),'', %interr(WH,num,GEND,NUM/3,NP)
       interr(WH,ADJADVKIND,ADJADV)):- ! .
%wie schnellAFF (as an adjective)
%wie unglaublich schnellAFF
ip2ipp(interr(WH,GENDER,NUMP,CASE,NP),'', %interr(WH,num,GEND,NUM/3,NP)
       interr(WH,GENDER,NUMP,CASE,NP)):- ! .
%an wen (hast du einen Brief geschrieben)
%unter was (lebt)
ip2ipp(interr(WH,G,NUMP,CASE),PRE,interr(WH,G,NUMP,CASEX)):- CASEX =..[PRE,CASE],
   once(CASE=nom;CASE=acc), !.
ip2ipp(interr(WH,CASE),'',interr(WH,CASE)):-
   once(CASE=nom;CASE=acc), !.
% an welches M�dchen (hast du einen Brief geschrieben)
% unter welchem Namen (lebt er)
% unter welchem Baum (sitzt er)
ip2ipp(interr(WH,G,NUM/3,CASE,NP),PRE,
       interr(WH,G,NUM/3,CASEX,NP)):- CASEX=..[PRE,CASE], ! .




%interrogative phrase to German
%
%wie viel Sch�ler (sind here?)
ip2German(interr(WH,num,GENDER,NUM,NP),CASE)--> {is_list(WH)}, !,
    interr2German(WH,num), np2German(NP,no,NUM,GENDER,CASE).
%how quick car (do you have?)
ip2German(interr(WH,GENDER,NUM/3,CASE,NP),CASE)--> {atom(CASE)}, !,
  interr2German(WH,_), np2German(NP,_,NUM/3,GENDER,CASE).
%under which tree (did you sit)?
ip2German(interr(WH,GENDER,NUM,CASE,NP),CASE)--> {CASE=..[PREP,NPCASE]}, !,
    [PREP], interr2German(WH,_), np2German(NP,_DECL,NUM,GENDER,NPCASE).
%wie schwer (ist das Last?)
ip2German(interr(wie,adj,ADJP),adj)--> !,
    interr2German(wie,adj), adj2German(ADJP).
%wie lang (habe ich meinen Stift gesucht?)
ip2German(interr(WH,adv(KIND),adverb(ADV,KIND,base)),adv(KIND))-->
    {atom(ADV)}, !, interr2German(WH,adv(KIND)), [ADV].
ip2German(interr(WH,adv(KIND)),adv(KIND))--> {atom(KIND)}, !,
    interr2German(WH,adv(KIND)).
%an wen (hast du einen Brief geschickt?)
%(B�cher,) in welchen (du sch�ne Bilder gesehen hast)
ip2German(interr(WH,KIND,NUM/3,CASE),CASE)-->
    {nonvar(CASE), CASE=..[PREP,_CASE0], deDict:prep(_,PREP,_)}, !,
    [PREP], interr2German(WH,KIND).
%interr(wo,adv(place),_,adv(place))
ip2German(interr(WH,adv(KIND),_NUM,adv(KIND)),adv(KIND))-->
    {nonvar(KIND)}, !, interr2German(WH,adv(KIND)).
ip2German(interr(WH,_GN,KIND),KIND)--> {nonvar(KIND), functor(KIND,PREP,1)},
   !, [PREP], interr2German(WH,KIND).
%wem (hast du gesehen?)
ip2German(interr(WH,_G,_NUM,CASE),CASE)-->  !, interr2German(WH,_).
ip2German(interr(WH,KIND,_GN),KIND)--> interr2German(WH,_), !.


clause2German(wh(WH),interrogative(wh))--> {atom(WH)}, !, [WH].
clause2German(wh(WH),interrogative(wh))--> {is_list(WH)}, !, WH.
clause2German(?(wh(what),clause(interrogative(wh),DIR,FREE,_,VP,NUM/3)),
               interrogative(wh))--> !,
 vp2German(VP,wh(what),DIR,NUM/3), frees2German(FREE).
clause2German(?(wh(who),clause(_/interrogative(wh),DIR,FREE,_,VP,NUM/3)),
               interrogative(wh))--> !,
  vp2German(VP,wh(who),DIR,NUM/3), frees2German(FREE).
clause2German(?(IPP,clause(_/interrogative(about),
                            inverted,FREE,NPS,_VP,NUMPERS)),
   interrogative(about))-->
  !, ip2German(IPP,_CASE),  np2German(NPS,_DECL,NUMPERS,_G,acc),
  frees2German(FREE).

%when verbal argument or subject is questioned
%'Wer hat dieses Buch geschrieben'?
%'Whom did you write a letter to'? (preposition at the end)
%'Wer bist du'? / 'Was ist das' (wer: predicate, pendent argument)
clause2German(?(IPP,CL),interrogative(KIND))-->
  {sub_var(IPP,CL), CL=clause(_/interrogative(KIND),DIR,FREE,NPS,VP,NUMPERS)},
  {once(KIND=wh;KIND=subj)},
  {ip2ipp(IP,PRE,IPP)}, !,
  ip2German(IP,_), frees2German(FREE),
  vp2German(VP,NPS,DIR,NUMPERS),
  ({PRE == ''} -> []; [PRE]).
%'Who wrote this book?' - the subject is questioned
clause2German(?(IP,clause(_/interrogative(subj),DIR,FREE,NPS,VP,NUMPERS)),
               interrogative(subj))-->
  ip2German(IP,CASE), {ignore(CASE=nom)}, !, frees2German(FREE),
  vp2German(VP,NPS,DIR,NUMPERS).
%'Who is Baby Roo's mother?' - the predicate is questioned
clause2German(?(IP,clause(_/interrogative(pred),inverted,[],NPS,VP,NUMPERS)),
               interrogative(pred))-->
  ip2German(IP,CASE), {ignore(CASE=nom)}, !,
  vp2German(VP,NPS,inverted,NUMPERS).
%'An wen hast du den Brief gegeben?'
clause2German(?(IP,clause(_/interrogative(wh),DIR,FREE,NPS,VP,NUMPERS)),
               interrogative(wh))--> !,
  ip2German(IP,_), frees2German(FREE),
  vp2German(VP,NPS,DIR,NUMPERS).
clause2German(?(WH,clause(_/interrogative(wh),
                           DIR,FREE,'',VP,NUMPERS)),interrogative(wh))-->
  !, vp2German(VP,wh(WH),DIR,NUMPERS),
  frees2German(FREE).
clause2German(?(clause(_/interrogative(yn),
                        inverted,FREE,NPS,VP,NUMPERS)),interrogative(yn))-->
  !, vp2German(VP,NPS,inverted,NUMPERS),
  frees2German(FREE).
clause2German(?(CLAUSE),interrogative(INTER))--> !,
 clause2German(CLAUSE,interrogative(INTER)).
clause2German(clause(_/KIND,inverted,FREES,NPS,PRED,NUMPERS),KIND)-->
  frees2German(FREES), vp2German(PRED,NPS,inverted,NUMPERS), !.
clause2German(clause(_/KIND,straight,FREES,NPS,PRED,NUMPERS),KIND)-->
  vp2German(PRED,NPS,straight,NUMPERS), frees2German(FREES), !.
clause2German(clause(_/KIND,opposite,FREES,NPS,PRED,NUMPERS),KIND)-->
  vp2German(PRED,NPS,opposite,NUMPERS), frees2German(FREES), !.
clause2German(interj(IJ),declarative)--> !,
  interj2German(IJ).

addr2German(proper(PROPER,CLASS,GENDER,NUM,CASE))-->
   proper2German(PROPER,CLASS,NUM,CASE,GENDER).


:- op(1050,yfx,<-).

sentence2German(preclause(ADDR,CL))-->
  !, addr2German(ADDR), [','],
  clause2German(CL,KIND), {senType2Punct(KIND,PUNCT)}, [PUNCT].
sentence2German(postclause(CL,POSTCL))-->
  !, clause2German(CL,KIND), [','],
  (addr2German(POSTCL)-> {senType2Punct(KIND,PUNCT)}, [PUNCT];
  {POSTCL=interj(IJ)}, interj2German(IJ), {senType2Punct(KIND,PUNCT)}, [PUNCT];
  clause2German(POSTCL,_)).
sentence2German(questag(',',CL1,CL2))-->
  !, clause2German(CL1,declarative), [','],
  clause2German(CL2,declarative), ['?'].
sentence2German(connected(ADV,CL))-->
  adv2German(ADV), !, [','],
  clause2German(CL,KIND), {senType2Punct(KIND,PUNCT)}, [PUNCT].
sentence2German(correlated(CONN,CL1,CL2))-->
  {correlative(CONN,C1,DIR1,C2,DIR2)},
  [C1], clause2German(CL1,declarative),
  {listConnective(C2)},[C2], !,
  clause2German(CL2,declarative), ['.'].
sentence2German(correlated(CONN,CL1,CL2))-->
  {correlative(CONN,C1,DIR1,C2,DIR2)},
  [C1], clause2German(CL1,declarative),
  {\+ listConnective(C2)}, !, [','], [C2],
  clause2German(CL2,declarative), ['.'].
%sentence2German(correlated(CONN,CL1,CL2))-->
%  {correlative(CONN,C1,C2,inverse,C3,straight)}, !,
%  [C1,C2], clause2German(CL1,declarative), [C3],
%  clause2German(CL2,declarative), ['.'].

%CONN can be a connective or a punctuation mark (,)
sentence2German(compound(CONN,CL1,CL2))-->
  clause2German(CL1,declarative), !,
  conn2German(CONN), sentence2German(CL2).
%main clause comes first...
%Wir sind zu Hause geblieben, bis er wieder gekommen ist.
sentence2German(complex(CONN,MAIN->SUB))-->
  !, clause2German(MAIN,KIND), [','],
  conn2German(CONN), clause2German(SUB,declarative),
  {once(senType2Punct(KIND,PUNCT))}, [PUNCT].
%Als ich Kind war, ich a� viel �pfel.
sentence2German(complex(CONN,SUB<-MAIN))-->
  !, conn2German(CONN), clause2German(SUB,declarative), [','],
  clause2German(MAIN,KIND),
  {once(senType2Punct(KIND,PUNCT))}, [PUNCT].
sentence2German(incomplete(INTERR,KIND))--> !, ip2German(INTERR,_),
    {once(senType2Punct(KIND,PUNCT))}, [PUNCT].
sentence2German(CLAUSE)-->
  clause2German(CLAUSE,KIND), {once(senType2Punct(KIND,PUNCT))}, [PUNCT].

senType2Punct(interrogative(_),'?').
senType2Punct(declarative,'.').
senType2Punct(exclamatory,'!').
senType2Punct(imperative(strong),'!').
senType2Punct(imperative(weak),'.').

%!  toSentence(+TREE:expr)// is nondet.
% The DCG procedure generates an German text (list of words) from the
% parameter syntax tree. It must be called through the phrase/2..3
% wrapper
%
toSentence(TREE)-->
  sentence2German(TREE), ! .

%!  toSentence(+TREE:expr,ENGLISH:list) is nondet.
% The Prolog procedure generates an German text (list of words) from
% the parameter syntax tree. It can be called directly from
% Prolog
%
% @arg TREE the syntax tree/dict of syntax tree
% @arg ENGLISH a list of German words
toSentence(DICT,ENGLISH):- is_dict(DICT), dict2pl(deParser:dict,DICT,TREE),
  toSentence(TREE,ENGLISH), ! .
toSentence(TREE,ENGLISH):-
   toSentence(TREE,ENGLIST0,[]),
%   tokenTrans(ENGLIST0,ENGLIST),
   tokens2Line(ENGLIST,ENGLIST0,[]),
   capitalize(ENGLIST,ENGLIST00),
   atomics_to_string(ENGLIST00,' ',ENGLISH),
   debug(genGerman,'GermanSentence:~w\n',[ENGLISH]).



vowel('a').
vowel('e').
vowel('o').
vowel('i').

capitalize([T|TAIL],[TC|TAIL]):- !, capitalize(T,TC).
capitalize(T,TCAP):- atomic(T),
   sub_string(T,0,1,_,FIRST), atom_concat(FIRST,REST,T),
   string_upper(FIRST,FIRSTUP), atom_concat(FIRSTUP,REST,TCAP).

tokenTrans([T00,T10|TOKENS],[T0CAP,T1,' '|TOKENLIST]):-
   transcribe(T0,T1,T00,T10), !, capitalize(T0,T0CAP),
   tokenTran(TOKENS,TOKENLIST).
tokenTrans([T0|TOKENS],TOKENLIST):-
   capitalize(T0,T0CAP),
   tokenTran([T0CAP|TOKENS],TOKENLIST).


start:-
  S=[the,quick,',',brown,fox,jumps,over,the,lazy,dog,'.'],
  start(S).

start(STRING,CALL):- string(STRING), !,
  parser:string_to_tokens(STRING,TOKENS),
  parser:tokens2Line(SENTENCE,TOKENS,[]),
  start(SENTENCE,CALL).
start(S,CALL):- is_list(S), !, %later the complete forest!!
  parseThisSentence(S,_TREES,CALL).
start(TESTFILE,CALL):-
  deParser:enParseTestfile(TESTFILE,CALL).

start(PARM):- start(PARM,\=).

%'ein unserer Eltern'+NOM

%'unsere Eltern'+ACC ---- friss�teni, possPronoun/5
test(1,poss(common(eltern,_,pl/3,acc),pl/3,possPronoun(pl/1,A,pl,acc),A)).
test(2,numeral(card,ein,sg/3,
               poss(common(eltern, _, pl/3, gen),pl/3,
               possPronoun(pl/1, A, pl, gen),A))).



test(N)--> {test(N,NP)}, np2German(NP,DECL,NUMP,GENDER,CASE).





















