:- encoding(iso_latin_1).

%
% pre and postpositionen
%
%prepositions with genitive case
%anstatt, statt, anl�sslich, anstelle, aufgrund, au�erhalb
%bez�glich, innerhalb, jenseits, kraft, laut, seitens
%trotz, w�hrend, wegen


%prep(about,about).
%prep(above,above).
%prep(across,across).
prep(after,nach,dat).              %DE
prep(against,gegen,acc).           %DE
%prep(ago,ago).
prep(along,entlang,acc).           %DE
%prep(among,among).
prep(around,um,acc).               %DE
prep(as,als,nom).                  %DE
prep(atTo,an,acc).                 %DE
prep(at,an,dat).                   %DE
prep(becauseOf,wegen,gen).         %DE
%prep(before,bevor).
prep(behind,hinter,acc).           %DE
prep(behind,hinter,dat).           %DE
%prep(below,below).
prep(between,zwischen,acc).        %DE
prep(between,zwischen,dat).        %DE
prep(beside,neben,acc).            %DE
prep(beside,neben,dat).            %DE
prep(by,bei,dat).                  %DE
%prep(down,down).
prep(during,w�hrend,gen).          %DE
prep(except,au�er,dat).            %DE
prep(for,f�r,acc).                 %DE
prep(from,von,dat).                %DE
prep(fromTime,ab,dat).             %DE
prep(into,in,acc).                 %DE
prep(in,in,dat).                   %DE
prep(toFront,vor,acc).             %DE
prep(inFront,vor,dat).             %DE
prep(inSpiteOf,trotz,gen).         %DE
prep(insteadOf,statt,gen).         %DE
%prep(including,including).
%prep(inside,inside).
%prep(into,into).
%prep(like,like).
%prep(minus,minus).
%prep(near,near).
%prep(of,of).
prep(on,auf,acc).                  %DE
prep(on,auf,dat).                  %DE
prep(opposite,gegen�ber,dat).      %DE
prep(out,aus,dat).                 %DE
%prep(outside,outside).
prep(over,�ber,acc).               %DE
prep(over,�ber,dat).               %DE
%prep(past,past).
%prep(per,pro).          %DE
%prep(plus,plus).
%prep(round,round).
prep(resulted,infolge,gen).        %DE
prep(since,seit,dat).              %DE
prep(through,durch,acc).           %DE
prep(till,bis,acc).                %DE
prep(to,zu,dat).                   %DE
prep(under,unter,acc).             %DE
prep(under,unter,dat).             %DE
%prep(until,until).
%prep(up,up).
%prep(versus,versus).
prep(with,mit,dat).                %DE
prep(without,ohne,acc).            %DE

% halber+gen, zuwider, gem��, entlang+acc, gegen�ber+dat, �ber+acc,
% durch+acc, meiner Meinung nach + dat
postp(accordingTo,zufolge,dat).
postp(becauseOf,wegen,gen).


infPart(zu).

%
% pronouns (personal, reflexive, demonstrative, indefinite)
%
%personal pronouns
persPron(sg/1,_,ich,mich,mein,mir).
persPron(sg/2,_,du,dich,dein,dir).
persPron(sg/3,feminin,sie,sie,ihr,ihr).
persPron(sg/3,masculin,er,ihn,sein,ihm).
persPron(sg/3,neutral,es,es,sein,ihm).
persPron(pl/1,_,wir,uns,unser,uns).
persPron(pl/2,_,ihr,euch,euer,euch).
persPron(pl/3,_,sie,sie,ihr,ihnen).

reflPron(sg/1,mich).
reflPron(sg/2,dich).
reflPron(sg/3,sich).
reflPron(pl/1,uns).
reflPron(pl/2,euch).
reflPron(pl/3,sich).


demAffix(masculin,nom,er).
demAffix(masculin,acc,en).
demAffix(masculin,gen,es).
demAffix(masculin,dat,em).

demAffix(feminin,nom,e).
demAffix(feminin,acc,e).
demAffix(feminin,gen,er).
demAffix(feminin,dat,er).

demAffix(neutral,nom,es).
demAffix(neutral,acc,es).
demAffix(neutral,gen,es).
demAffix(neutral,dat,em).

demAffix(pl,nom,e).
demAffix(pl,acc,e).
demAffix(pl,gen,er).
demAffix(pl,dat,en).


%demonstrative pronouns
%dem(all,all,pl,numNP).              %DE
dem(this,dies,sg,numNP).            %DE
%dem(that,das,sg,numNP).             %DE - hard coded exception
dem(thatThere,jen,sg,numNP).        %DE
dem(other,ander,sg,numNP).          %DE
dem(last,letzter,sg,numNP).         %DE
dem(first,erster,sg,numNP).         %DE


% etwas, irgendetwas, nichts - no declension
% jemand, irgendjemand, niemand - "mand" declension
% jeder - like it were an adjective
% man - only NOM case!!
% indefinite pronouns

mandAffix(nom,'').
mandAffix(acc,'').
mandAffix(acc,en).
mandAffix(gen,s).
mandAffix(gen,es).
mandAffix(dat,'').
mandAffix(dat,em).



%jemand:  NOM:'',(ACC:'';-en),(GEN: -s; -es),(DAT:-em;'')
indefPronoun(somebody,jemand,C^A^mandAffix(C,A),sg). %DE
indefPronoun(something,etwas,'',sg).                 %DE - no declension
%indefPronoun(anyone,anyone,sg).
%indefPronoun(anybody,anybody,sg).
%indefPronoun(anything,anything,sg).
%indefPronoun(everyone,everyone,pl).
%indefPronoun(everybody,everybody,pl).
%indefPronoun(everything,everything,pl).
%niemand:  NOM:'',(ACC:'';-en),(GEN: -s; -es),(DAT:-em;'')
indefPronoun(nobody,niemand,C^A^mandAffix(C,A),sg).  %DE
indefPronoun(nothing,nichts,'',sg).    %DE - no declension
%indefPronoun(many,many,pl).
%indefPronoun(some,some,pl). %manche
%indefPronoun(each,each,pl). %jeder
%indefPronoun(few,few,pl).
%indefPronoun(aFew,[a,few],pl).
%indefPronoun(any,any,pl).
%indefPronoun(all,all,pl).
%indefPronoun(lot,lot,sg).
%indefPronoun(aLot,[a,lot],sg).
indefPronoun(one,man,nom,sg).           %DE - only NOM
%indefPronoun(this,this,sg). this is rather a demonstrative pronoun!
%indefPronoun(that,that,sg). this is rather a demonstrative pronoun!
%indefPronoun(theSame,[the,same],sg).

indefPronounNumeral(each,jed).
indefPronounNumeral(muchmany,viel).
indefPronounNumeral(someCountable,manch).

%
% articles and determiners
%
relArt(der,masculin,gen,dessen).
relArt(die,feminin,gen,deren).
relArt(das,neutral,gen,dessen).
relArt(die,pl,gen,deren).
relArt(die,pl,dat,denen).

defArt(der,masculin,der,nom).
defArt(der,masculin,den,acc).
defArt(der,masculin,des,gen).
defArt(der,masculin,dem,dat).
defArt(die,feminin,die,nom).
defArt(die,feminin,die,acc).
defArt(die,feminin,der,gen).
defArt(die,feminin,der,dat).
defArt(das,neutral,das,nom).
defArt(das,neutral,das,acc).
defArt(das,neutral,des,gen).
defArt(das,neutral,dem,dat).
defArt(die,pl,die,nom).
defArt(die,pl,die,acc).
defArt(die,pl,der,gen).
defArt(die,pl,den,dat).

indefArtAffix(masculin,'',nom).
indefArtAffix(masculin,en,acc).
indefArtAffix(masculin,es,gen).
indefArtAffix(masculin,em,dat).

indefArtAffix(feminin,e,nom).
indefArtAffix(feminin,e,acc).
indefArtAffix(feminin,er,gen).
indefArtAffix(feminin,er,dat).

indefArtAffix(neutral,'',nom).
indefArtAffix(neutral,'',acc).
indefArtAffix(neutral,es,gen).
indefArtAffix(neutral,em,dat).

indefArtAffix(pl,e,nom).
indefArtAffix(pl,e,acc).
indefArtAffix(pl,er,gen).
indefArtAffix(pl,en,dat).

art(def,der,masculin).
art(def,die,feminin).
art(def,das,neutral).
art(def,die,pl).
art(indef,ein,_).
art(nodef,kein,_).



%det(allThe,sg,[all,the],_,possNP).
%det(alotOf,pl,[a,lot,of],_,possNP).
%det(anyOf,sg,[any,of],pl,possNP).
%det(bitOf,sg,[a,bit,of],sg,possNP).
det(coupleOf,pl,[ein,paar],pl,possNP).
det(ca,pl,[ca,.],pl,numNP). %==>adv
%det(eitherOf,sg,[either,of],pl,possNP).
det(fewOf,NUM,[ein,bisschen],NUM,possNP).
%det(greatDealOf,sg,[a,great,deal,of],sg,possNP).
%det(lotsOf,pl,[lots,of],pl,possNP).
% det(neitherOf,pl,[neither,of],pl,possNP). %neither of my friends does
% det(noneOf,pl,[none,of],pl,possNP). % keiner meiner Freunde kommt
% det(numberOf,pl,[a,number,of],pl,possNP). %eine Reihe von Freunden
% det(pairOf,pl,[a,pair,of],pl,possNP).
% det(plentyOf,pl,[plenty,of],sg,possNP). %ich habe plenty of time
% det(quiteA,sg,[quite,a],sg,possNP). %ein ziemlicher Erfolg
% det(ratherA,sg,[rather,a],sg,possNP). %ein ziemlicher Erfolg
% det(suchA,sg,[such,a],sg,possNP). %solch ein Tag
% det(theWholeOf,sg,[the,whole,of],_,possNP).
% det(tonsOf,pl,[tons,of],pl,possNP).
det(whatA,sg,[was,f�r,ein],sg,possNP). %was f�r ein Tag

%
% numerals and measures
%
ord(1,erst).
ord(2,zweit).
ord(3,dritt).
ord(4,viert).
ord(5,f�nft).
ord(6,sechst).
ord(7,siebt).
ord(8,acht).
ord(9,neunt).
ord(10,zehnt).
ord(11,elft).
ord(12,zw�lft).



card(0,null).
card(1,ein).
card(2,zwei).
card(3,drei).
card(4,vier).
card(5,f�nf).
card(6,sechs).
card(7,sieben).
card(8,acht).
card(9,neun).
card(10,zehn).
card(11,elf).
card(12,zw�lf).
card(13,dreizehn).
card(14,vierzehn).
card(15,f�nfzehn).
card(16,sechzehn).
card(17,siebzehn).
card(18,achtzehn).
card(19,neunzehn).
card(20,zwanzig).
card(30,drei�ig).
card(40,vierzig).
card(50,f�nfzig).
card(60,sechzig).
card(70,siebzig).
card(80,achtzig).
card(90,neunzig).
card(100,hundert).
card(1000,tausend).
card(1000000,million).
card(1000000000,milliarde).


%indefinite numerals
%indef(SEM,STEM,DECL)
%DECL: weak;mixed;strong ==> declination demanded from adj
%indef(afew,[ein,paar]).            %DE
indef(all,all,strong).              %DE
%indef(each,jed).                   %DE
indef(more,mehr).                   %DE !!!!!!!
indef(most,meist,weak).             %DE
indef(most,meist,strong).           %DE
% *viel, *wenig, *einige, manche, etliche, gering, zahlreich, *ein paar,
% einzeln, vereinzelte, gesamt, verschieden, zahllos, ungez�hlt, ganz (�
% Besonderheit siehe Erkl�rung unten), unz�hlig, etwas, ein
% bisschen, nichts usw.
% weitere, sonstige, �brige usw.
%
%'beide Geschaefte': strong->mixed
%'meine beiden Eltern': mixed
indef(both,beid,_).                 %DE
indef(each,jed,strong).             %DE
%'eine halbe Stunde': mixed
indef(half,halb,_).                 %DE
%'wenige Schlangen': strong->mixed
indef(little,wenig,_).              %DE
indef(more,mehr,unflected).         %DE
indef(muchmany,viel,strong).        %DE
indef(some,etwas,unflected).        %DE
indef(someCountable,einig,strong).  %DE
indef(someCountable,manch,strong).  %DE

indef(SEM,STEM):- indef(SEM,STEM,_).


%measure(NAME,ABBR,QUANTITY,ADJS)
%NAME:atom full name of the measure
%ABBR:abbreviation
%QUANTITY: semantic label of the quantity (eg. "time" for "hours")
%ADJS: a list of adjectives to use with the measurement
measure(grad,'C',temperature,[warm,heiss,kalt]).
measure(dollar,$,money,[billig,teuer]).
measure(euro,'EUR',money,[billig,teuer]).
measure(cent,c,money,[billig,teuer]).
measure(franken,[sFr,.],money,[billig,teuer]).
measure(meter,m,length,[lang,kurz]).
measure(zentimeter,m,length,[lang,kurz]).
measure(kilometer,km,length,[lang,kurz,weit]).
measure(quadratmeter,m2,area,[large]).
measure(prozent,'%',rate,[]).
measure(liter,l,volume,[]).
measure(gramm,g,mass,[schwer]).
measure(kilogramm,kg,mass,[schwer]).
measure(kilo,kg,mass,[heavy]).
measure(dekagramm,dkg,mass,[heavy]).
measure(minute,min,time,[lang,kurz]).
measure(jahr,'',time,[alt,lang]).
measure(uhr,'',time,[sp�t,fr�h]).
measure(gigabyte,'GB',memory,[]).



% both...and...
% either...or... sentence either you will eat your dinner of you will go
% to bed
% just as...so...
% neither...nor... INVERTED
%
% not only...INVERTED... but also...
% Not only do I love this band, but I have also seen the concert twice.
% rather...or... whether...or...

%if...then...  wo 'then' is applicable too...
%If that is true, then what happened is not surprising.


%correlative conjunctions
correlative(eitherOr,entweder,straight,oder,straight).           %DE
correlative(eitherOr,entweder,inverted,oder,inverted).           %DE
correlative(onOneOtherHand,einerseits,inverted,
                           andererseits,inverted).               %DE
correlative(oneOtherTime,mal,inverted,mal,inverted).             %DE


correlative(neitherNor,weder,straight,noch,inverse).             %DE


correlative(eitherOr,entweder,oder,nounPhrase).                  %DE
correlative(neitherNor,weder,noch,nounPhrase).                   %DE
correlative(onOneOtherHand,einerseits,andererseits,nounPhrase).  %DE

correlative(asWellToo,sowohl,als,auch,nounPhrase).               %DE

correlative(notOnlyButAlso,nicht,nur,sondern,auch,nounPhrase).   %DE

%correlative(ifThen,if,straight,then,straight).
%correlative(scarcelyWhen,scarcely,inverse,when,straight).


%
% conjunctives
%
listConnective(oder).
listConnective(und).
listConnective(beziehungsweise).

conj(bothAnd,both,and,coordinating,phrase).

conj(that,dass,subclause(that)).        %DE
conj(if,wenn,subclause(if)).            %DE
conj(whether,ob,subclause(if)).         %DE


%coordinating conjunctives - for
conj(and,und,coordinating).            %(,)DE the combination of two thoughts
conj(but,aber,coordinating).           %,DE contrasting between two thoughts
% conj(so,so,coordinating).      %logical implication of two thoughts
conj(yet,doch,coordinating).           %(,)contrasting between two thoughts
conj(or,oder,coordinating).            %(,)DE indicating alternative thoughts
conj(ifNot,au�er,coordinating).        %,DEwhen part2 is false
conj(because,denn,coordinating).       %,DE
conj(butRather,sondern,coordinating).  %,DE

%subordinating conjunctives
conj(afterThis,nachdem,subordinating).  %DE
conj(although,obwohl,subordinating).    %DE
conj(alsoIf,obschon,subordinating).     %DE
conj(as,als,subordinating).             %DE
conj(asLongAs,sofern,subordinating).    %DE
conj(asMuchAs,sowie,subordinating).     %DE
conj(asSoonAs,sobald,subordinating).    %DE
conj(becauseThen,weil,subordinating).   %DE
conj(before,bevor,subordinating).       %DE
conj(beforeAs,ehe,subordinating).       %DE
conj(evenIf,obgleich,subordinating).    %DE
conj(how,wie,subordinating).            %DE
conj(inCaseOf,falls,subordinating).     %DE
conj(ifWhen,wenn,subordinating).        %DE
conj(inOrder,um,subordinating).         %DE
conj(inOrderTo,damit,subordinating).    %DE
conj(inSoFar,insofern,subordinating).   %DE
conj(inSoFarAs,indem,subordinating).    %DE
conj(if,ob,subordinating).              %DE
conj(since,seit,subordinating).         %DE
conj(sinceThen,seitdem,subordinating).  %DE
conj(sinceAs,da,subordinating).         %DE
conj(so,so,subordinating).              %DE
conj(soFar,sofern,subordinating).       %DE
conj(soFarAs,soweit,subordinating).     %DE
conj(soMuch,soviel,subordinating).      %DE
conj(soThat,damit,subordinating).       %DE
conj(that,dass,subordinating).          %DE
conj(thereby,dadurch,subordinating).    %DE
conj(therewith,damit,subordinating).    %DE
conj(until,bis,subordinating).          %DE
conj(while,w�hrend,subordinating).      %DE
conj(when,wenn,subordinating).          %DE
conj(where,wo,subordinating).           %DE
conj(whereas,wohingegen,subordinating). %DE
conj(whether,ob,subordinating).         %DE

%**causal (diese leiten eine Begr�ndung ein): daher, n�mlich, deswegen,
% deshalb, darum
%**concessive (diese leiten Nebens�tze ein, die erkl�ren, dass etwas zum
% Trotz geschieht): allerdings, indessen, immerhin, dennoch, trotzdem,
% nichtsdestotrotz, nichtsdestoweniger
%**copulative (diese verbinden zwei gleichwertige S�tze): au�erdem,
% dar�ber hinaus, auch, ferner, zudem, dazu, zus�tzlich, ebenfalls
%**consecutive (diese leiten die Folgen einer Handlung ein): also,
% demzufolge, folglich, infolgedessen, demnach, insofern, so, somit
%**adversative (diese leiten einen Gegensatz ein): jedoch, doch,
% dagegen, stattdessen, hingegen, allerdings, dennoch, indes, indessen,
% vielmehr, trotzdem
%**temporal (leiten eine Erkl�rung zur zeitlichen Folge ein): danach,
% davor, anschlie�end, w�hrenddessen, zuerst, dann, schlie�lich, zuletzt
%
% conjunctional adverbs
conj(againstThat,dagegen,adversative,conjunctional).         %DE
conj(afterAll,immerhin,concessive,conjunctional).            %DE
conj(beforeThis,davor,temporal,conjunctional).               %DE
conj(beforeThat,zuvor,temporal,conjunctional).               %DE
conj(besideThat,dabei,copulative,conjunctional).             %DE
conj(becauseOfThis,deswegen,causal,conjunctional).           %DE
conj(consequently,folglich,consecutive,conjunctional).       %DE
conj(inbetween,inzwischen,temporal,conjunctional).           %DE
conj(indeed,allerdings,concessive,conjunctional).            %DE
conj(inAddition,au�erdem,copulative,conjunctional).          %DE
conj(earlier,vorher,temporal,conjunctional).                 %DE
conj(either,entweder,conjunctional).                         %DE
conj(finally,schlie�lich,temporal,conjunctional).            %DE
conj(firstly,zuerst,temporal,conjunctional).                 %DE
conj(forThis,daf�r,copulative,conjunctional).                %DE
conj(forThisReason,deshalb,causal,conjunctional).            %DE
conj(further,ferner,copulative,conjunctional).               %DE
conj(justLikeThat,genauso,concessive,conjunctional).         %DE
conj(later,sp�ter,temporal,conjunctional).                   %DE
conj(namely,n�mlich,causal,conjunctional).                   %DE
conj(nevertheless,trotzdem,concessive,conjunctional).        %DE
conj(oneHand,einerseits,conjunctional).                      %DE
conj(otherHand,andererseits,conjunctional).                  %DE
conj(otherwise,sonst,copulative,conjunctional).              %DE
conj(respectively,beziehungsweise,conjunctional).            %DE
conj(sinceThat,seitdem,temporal,conjunctional).              %DE
conj(so,also,consecutive,conjunctional).                     %DE
conj(subsequently,anschlie�end,temporal,conjunctional).      %DE
conj(throughThis,dadurch,causal,conjunctional).              %DE
conj(thatIs,zwar,conjunctional).                             %DE
conj(thereAfter,danach,temporal,conjunctional).              %DE
conj(therefore,darum,causal,conjunctional).                  %DE
conj(thereon,darauf,copulative,conjunctional).               %DE
conj(therewith,damit,copulative,conjunctional).              %DE
conj(than,dann,temporal,conjunctional).                      %DE
conj(though,jedoch,adversative,conjunctional).               %DE
conj(thus,somit,causal,conjunctional).                       %DE
conj(toThat,dazu,copulative,conjunctional).                  %DE
conj(yet,dennoch,concessive,conjunctional).                  %DE




%comparison conjunctive
conj(than,als,comp).                    %DE
conj(as,so,comp).                       %DE


conj(and,und,corr).                     %DE
conj(besides,au�erdem,corr).            %DE
%conj(both,both,corr).
conj(but,doch,corr).                    %DE
conj(either,entweder,corr).             %DE
conj(time,mal,corr).                    %DE
conj(asWell,sowohl,corr).               %DE
conj(as,als,corr).                      %DE
conj(too,auch,corr).                    %DE
conj(if,wenn,corr).                     %DE
conj(or,oder,corr).                     %DE
conj(nor,noch,corr).                    %DE
conj(neither,weder,corr).               %DE
conj(not,nicht,corr).                   %DE
conj(only,nur,corr).                    %DE
conj(but,sondern,corr).                 %DE
conj(pity,schade,corr).                 %DE
conj(indeed,allerdings,corr).           %DE
conj(regardlessWhether,[egal,ob],corr). %DE
conj(respectively,[bzw,.],corr).        %DE
conj(then,dann,corr).   %indicating alternative thoughts
conj(when,wann,corr).                   %DE

%
% interjectives
%
interj(attention,achtung).          %DE
interj(attention,vorsicht).         %DE
interj(bye,tsch�ss).                %DE
interj(dh,[d,., h,.] ).             %DE
interj(hello,hallo).                %DE
interj(hey,hey).                    %DE
interj(no,nein).                    %DE
interj(oh,ach).                     %DE
interj(oh,oh).                      %DE
interj(ok,[o,.,k,.]).               %DE
interj(ok,['O',.,'K',.]).           %DE
interj(ok,okay).                    %DE
interj(once,mal).                   %DE
interj(ouch,aua).                   %DE
interj(please,bitte).               %DE
interj(whoa,whoa).
interj(wow,wow).
interj(yes,ja).                     %DE

interj(etc,[etc,.]).                %DE
interj(etc,[usw,.]).                %DE

%
% interrogatives
%
interr(how,wie,adv(manner)).        %DE
interr(what,was,acc).               %DE
interr(what,was,nom).               %DE
interr(when,wann,adv(time)).        %DE
interr(where,wo,adv(place)).        %DE
interr(whereFrom,woher,adv(place)). %DE
interr(whereTo,wohin,adv(place)).   %DE
interr(which,welch,adj).            %DE
interr(whom,wen,acc).               %DE
interr(whomTo,wem,dat).             %DE
interr(who,wer,nom).                %DE
interr(whose,wessen,gen).           %DE
interr(why,warum,adv(reason)).      %DE

interr(atWhat,woran,an(dat)).       %DE
interr(forWhat,wof�r,for(dat)).     %DE
interr(withWhat,womit,mit(dat)).    %DE
interr(overWhat,wor�ber,�ber(dat)). %DE


interr(howMany,[wie,viel],num).     %DE
interr(whatKind,[was,f�r],adj).     %DE

interrPrefixable(wen).   %'An wen (schreibst du den Brief?)'
interrPrefixable(wem).   %'Mit wem (warst du im Theatre?)'
interrPrefixable(wann).  %'Ab wann (wohnst du here?')
interrPrefixable(welch). %'Unter welchem (Baum bist du gesessen?')
interrPrefixable([was,f�r]). %'In was f�r einer Stadt wohnen wir?')
%
% adjectives
%
% adjectives requiring zu+infinitive subclause
%
% bem�ht, bestrebt, beunruhigt, entschlossen, entt�uscht, erfreut,
% erleichtert, froh, traurig, �berrascht

%adjKind(ADJSEM,KIND).
adjKind(white,color).
adjKind(black,color).
adjKind(green,color).
adjKind(blue,color).
adjKind(red,color).
adjKind(brown,color).
adjKind(yellow,color).
adjKind(grey,color).



adjAffix(weak,masculin,nom,e).
adjAffix(weak,masculin,gen,en).
adjAffix(weak,masculin,dat,en).
adjAffix(weak,masculin,acc,en).
adjAffix(weak,feminin,nom,e).
adjAffix(weak,feminin,gen,en).
adjAffix(weak,feminin,dat,en).
adjAffix(weak,feminin,acc,e).
adjAffix(weak,neutral,nom,e).
adjAffix(weak,neutral,gen,en).
adjAffix(weak,neutral,dat,en).
adjAffix(weak,neutral,acc,e).
adjAffix(weak,pl,nom,en).
adjAffix(weak,pl,gen,en).
adjAffix(weak,pl,dat,en).
adjAffix(weak,pl,acc,en).

adjAffix(mixed,masculin,nom,er).
adjAffix(mixed,masculin,gen,en).
adjAffix(mixed,masculin,dat,en).
adjAffix(mixed,masculin,acc,en).
adjAffix(mixed,feminin,nom,e).
adjAffix(mixed,feminin,gen,en).
adjAffix(mixed,feminin,dat,en).
adjAffix(mixed,feminin,acc,e).
adjAffix(mixed,neutral,nom,es).
adjAffix(mixed,neutral,gen,en).
adjAffix(mixed,neutral,dat,en).
adjAffix(mixed,neutral,acc,es).
adjAffix(mixed,pl,nom,en).
adjAffix(mixed,pl,gen,en).
adjAffix(mixed,pl,dat,en).
adjAffix(mixed,pl,acc,en).

adjAffix(strong,masculin,nom,er).
adjAffix(strong,masculin,gen,en).
adjAffix(strong,masculin,dat,em).
adjAffix(strong,masculin,acc,en).
adjAffix(strong,feminin,nom,e).
adjAffix(strong,feminin,gen,er).
adjAffix(strong,feminin,dat,er).
adjAffix(strong,feminin,acc,e).
adjAffix(strong,neutral,nom,es).
adjAffix(strong,neutral,gen,en).
adjAffix(strong,neutral,dat,em).
adjAffix(strong,neutral,acc,es).
adjAffix(strong,pl,nom,e).
adjAffix(strong,pl,gen,er).
adjAffix(strong,pl,dat,en).
adjAffix(strong,pl,acc,e).

%
% colors
%
adj(black,schwarz).               %DE
adj(blue,blau).                   %DE
adj(brown,braun).                 %DE
adj(colorful,bunt).               %DE
adj(darkBlue,dunkelblau).         %DE
adj(green,gr�n).                  %DE
adj(grey,grau).                   %DE
adj(lightBlue,hellblau).          %DE
adj(red,rot).                     %DE
adj(rosa,rosa).                   %DE
adj(white,wei�).                  %DE
adj(young,jung).                  %DE
%
% nationalities
%
adj(spanish,spanische).           %DE

%adj(able,able).
%adj(acid,acid).
%adj(academic,academic).
%adj(accidental,accidental).
%adj(accurate,accurate).
adj(active,aktiv).                %DE
adj(actual,aktuell).              %DE
%adj(adult,adult).
%adj(advanced,advanced).
%adj(afraid,afraid).
%adj(aged,aged).
adj(alone,allein).                %DE
%adj(allright,allright).
adj(alternative,alternativ).      %DE
%adj(amazing,amazing).
%adj(american,'American').
%adj(angry,angry).
%adj(apparent,apparent).
%adj(applicable,applicable).
%adj(approved,approved).
%adj(approximate,approximate).
%adj(asian,'Asian').
%adj(attractive,attractive).
adj(austrian,�sterreichisch).     %DE
adj(automatic,automatisch).       %DE
%adj(auxiliary,auxiliary).
%adj(available,available).
%adj(average,average).
adj(awake,wach).                  %DE
%adj(awesome,awesome).
adj(awful,furchtbar).             %DE


%adj(back,back).
adj(bad,schlecht).                %DE
adj(bad,schlimm).                 %DE
%adj(basic,basic).
adj(beautiful,sch�n).             %DE
adjGr(sch�n,sch�ner,sch�nest).    %DE
adj(big,gro�).                    %DE
adjGr(gro�,gr��er,gr��t).         %DE
adj(bitter,bitter).               %DE
adj(blond,blond).                 %DE
%adj(blocked,blocked).
%adj(blunt,blunt).
%%adj(boiled,boiled)... past-participle
%adj(bored,bored).
adj(boring,langweilig).           %DE
adj(born,geboren).                %DE
%adj(brave,brave).
adj(bright,hell).                 %DE
%adj(brilliant,brilliant).
adj(broken,kaputt).               %DE
%adj(busy,busy).


adj(careful,vorsichtig).          %DE
%adj(certain,certain).
adj(cheap,billig).                %DE
adj(cheap,g�nstig).               %DE
%adj(chemical,chemical).
%adj(chief,chief).
%adj(circular,circular).
adj(clean,sauber).                %DE
adj(clear,deutlich).              %DE
adj(clear,klar).                  %DE
%adj(clever,clever).
%adj(close,close).
%%adj(closed,closed). PP
adj(cloudy,bew�lkt).              %DE
%adj(clueless,clueless).
adj(cold,kalt).                   %DE
adj(comfortable,bequem).          %DE
%adj(common,common).
%adj(comparable,comparable).
%adj(complete,complete).
%adj(complex,complex).
%adj(confined,confined).
%adj(conscious,conscious).
adj(conservative,konservativ).    %DE
%adj(constant,constant).
%adj(continuous,continuous).
adj(cool,k�hl).                   %DE
adj(cordial,herzlich).            %DE
%adj(correct,correct).
%adj(cruel,cruel).
%adj(curious,curious).
%adj(crazy,crazy).
%adj(cream,cream).
%adj(crowded,crowded).
%adj(current,current).


adj(daily,t�glich).               %DE
%adj(damaged,damaged).
adj(dangerous,gef�hrlich).        %DE
adj(dark,dunkel).                 %DE
adj(dead,tot).                    %DE
%adj(deaf,deaf).
adj(dear,lieb).                   %DE
adj(deep,tief).                   %DE
%adj(defective,defective).
%adj(delicate,delicate).
%adj(delicious,delicious).
adj(democratic,demokratisch).     %DE
%adj(dependent,dependent).
%adj(diagonal,diagonal).
adj(different,verschieden).       %DE
adj(difficult,schwierig).         %DE
%adj(digital,digital).
adj(diligent,flei�ig).            %DE
%adj(dim,dim).
adj(direct,direkt).               %DE
adj(dirty,schmutzig).             %DE
%adj(diving,diving).
%adj(divorced,divorced).
%adj(dizzy,dizzy).
adj(double,doppel).               %DE
%%adj(dressed,dressed). PP
adj(dry,trocken).                 %DE


adj(early,fr�h).                  %DE
adjGr(fr�h,fr�her,fr�hest).       %DE
adj(east,�stlich).                %DE
%adj(easy,easy).
%adj(elastic,elastic).
adj(electric,electric).
%adj(electronic,electronic).
adj(empty,leer).                  %DE
adj(english,'Englisch').          %DE
adj(entire,ganz).                 %DE
%adj(equal,equal).
adj(evil,b�se).                   %DE
adj(everyday,allt�glich).         %DE
adj(european,europ�isch).         %DE
adj(exact,genau).                 %DE
%adj(excellent,excellent).
%adj(excited,excited).
adj(exciting,spannend).           %DE
%adj(expanded,expanded).
adj(expensive,teuer).             %DE
%adj(experienced,experienced).
%adj(expert,expert).
%adj(expired,expired).
%adj(explosive,explosive).
%adj(external,external).
%adj(extra,extra).


%adj(fair,fair).
adj(false,falsch).                %DE
%adj(familiar,familiar).
adj(famous,ber�hmt).              %DE
adj(fantastic,fantastisch).       %DE
%adj(fancy,fancy).
adj(far,weit).                    %DE
adj(fascinating,aufregend).       %DE
adj(fast,schnell).                %DE
adj(fat,fett).                    %DE
adj(favorite,liebling).           %DE
%adj(feeble,feeble). %HU: gyenge
adj(female,weiblich).             %DE
%adj(fertile,fertile).
%adj(few,few).
%adj(final,final).
%adj(fine,fine).
%adj(firstClass,first-class).
%adj(fixed,fixed).
adj(fit,fit).                     %DE
%adj(flat,flat).
%adj(flexible,flexible).
adj(foggy,neblig).                %DE
%adj(foolish,foolish).
%adj(fond,[fond,of]).
%adj(foreign,foreign).
adj(free,frei).                   %DE
adj(freeNoConst,kostenlos).       %DE
adj(fresh,frisch).                %DE
%adj(frequent,frequent).
%%adj(fried,fried). PP
adj(friendly,freundlich).         %DE
%adj(frightened,frightened).
%adj(front,front).
%%adj(frozen,frozen). PP
adj(full,voll).                   %DE
adj(funny,komisch).               %DE
adj(funny,lustig).                %DE
adj(funny,witzig).                %DE
%adj(further,further).
%adj(future,future).


adj(general,allgemein).           %DE
adj(german,deutsche).             %DE
%adj(glad,glad).
%adj(glass,glass).
adj(global,global).               %DE
%adj(gold,gold).
%adj(golden,golden).
adj(good,gut).                    %DE
adjGr(gut,besser,best).           %DE
%adj(goodlooking,[good,looking]).
adj(great,super).                 %DE
adj(greek,griechisch).            %DE
%adj(grilled,grilled).
%adj(gross,gross).


%adj(halfprice,[half,price]).
adj(happy,froh).                  %DE
adj(hard,hart).                   %DE
adj(harmful,sch�dlich).           %DE
adj(healthy,gesund).              %DE
adj(heavy,schwer).                %DE
adj(high,hoch).                   %DE
adjGr(hoch,h�her,h�chst).         %DE
%adj(hollow,hollow). %HU:�reges
%adj(honest,honest).
adj(honoured,geehrte).            %DE
%adj(horrible,horrible).
%adj(horror,horror).
adj(hot,hei�).                    %DE
adj(hungry,hungrig).              %DE
adj(hurried,eilig).               %DE
%adj(hurt,hurt).


adj(ill,krank).                   %DE
adj(important,wichtig).           %DE
%adj(impossible,impossible).
%adj(indoor,indoor).
%adj(included,included).
%adj(incorrect,incorrect).
%adj(independent,independent).
adj(inexpensive,preiswert).       %DE
%adj(initial,initial).
adj(intelligent,intelligent).     %DE
%adj(interested,interested).
adj(interesting,interessant).     %DE
%adj(internal,internal).
adj(international,international). %DE
adj(inTime,p�nktlich).            %DE
%adj(irregular,irregular).


adj(jazzy,toll).                  %DE


adj(kind,nett).                   %DE
adj(known,bekannt).               %DE %verb..PP???


%adj(large,large).
adj(last,letzt).                  %DE
adj(last,zuletzt).                %DE
adj(late,sp�t).                   %DE
adj(lazy,faul).                   %DE
%adj(leather,leather).
adj(left,link).                   %DE
%adj(leftHand,left-hand).
adj(light,leicht).                %DE
adj(liberal,liberal).             %DE
adj(lila,lila).                   %DE
%adj(liquid,liquid).
adj(little,bisschen).             %DE
adj(live,live).                   %DE
%adj(local,local).
%adj(lonely,lonely).
adj(long,lang).                   %DE
%adj(loose,loose).
%adj(lost,lost).
adj(loud,laut).                   %DE
%adj(lousy,lousy).
%adj(lovely,lovely).
%adj(low,low).
adj(lucky,gl�cklich).             %DE


%adj(mad,mad).
adj(main,haupt).                  %DE
adj(male,m�nnlich).               %DE
%adj(mandatory,mandatory).
%adj(manual,manual).
adj(married,verheiratet).         %DE
%adj(material,material).
%adj(medical,medical).
%adj(metal,metal).
%adj(middle,middle).
%adj(military,military).
adj(mineral,mineral).             %DE
%adj(missing,missing).
%adj(mixed,mixed).
%adj(modern,modern).
%adj(monthly,monthly).
%adj(musical,musical).


%adj(narrow,narrow).
%adj(national,national).
adj(natural,nat�rlich).           %DE
adj(near,nah).                    %DE
adjGr(nah,n�her,n�chst).          %DE
%adj(nearby,nearby).
adj(necessary,notwendig).         %DE
%adj(negative,negative).
adj(nervous,nerv�s).              %DE
%adj(net,net).
adj(new,neu).                     %DE
adj(next,n�chst).                 %DE
%adj(nice,nice).
%adj(noisy,noisy).
adj(normal,normal).               %DE
adj(north,nordlich).              %DE


adj(occupied,besetzt).            %DE ??? verb-PP
%adj(odd,odd).
adj(offline,offline).             %DE
adj(old,alt).                     %DE
%adj(oneWay,one-way).
adj(online,online).               %DE
adj(only,nur).                    %DE
adj(onWay,unterwegs).             %DE
adj(open,offen).                  %DE
%adj(opposite,opposite).
%adj(optional,optional).
adj(orange,orange).               %DE
adj(original,echt).               %DE
%adj(outdoor,outdoor).
%adj(outer,outer).
%adj(overall,overall).
adj(own,eigen).                   %DE


%adj(pale,pale).
%adj(paper,paper).
%adj(parallel,parallel).
%adj(past,past).
%adj(perfect,perfect).
%adj(permanent,permanent).
%adj(personal,personal).
%adj(physical,physical).
%adj(pink,pink).
%adj(plastic,plastic).
adj(pleasant,angenehm).           %DE
%adj(pleased,pleased).
%adj(poisonous,poisonous).
%adj(polite,polite).
%adj(political,political).
adj(poor,arm).                    %DE
adj(popular,beliebt).             %DE
%adj(positive,positive).
adj(possible,m�glich).            %DE
%adj(positive,positive).
adj(practical,praktisch).         %DE
adj(pregnant,schwanger).          %DE
%adj(present,present).
%adj(preserved,preserved).
adj(pretty,h�bsch).               %DE
%adj(primary,primary).
adj(private,privat).              %DE
adj(probable,wahrscheinlich).     %DE
%adj(professional,professional).
%adj(public,public).
%adj(purple,purple).


%adj(quick,quick).
adj(quiet,leise).                 %DE


%adj(rare,rare).
adj(ready,fertig).                %DE
%adj(real,real).
%adj(rear,rear).
%adj(recent,recent).
%adj(regular,regular).
%adj(related,related).
%adj(remaining,remaining).
%adj(rescued,rescued).
%adj(resistant,resistant).
%adj(responsible,responsible).
adj(restful,ruhig).               %DE
adj(rich,reich).                  %DE
adj(right,recht).                 %DE
%adj(rightHand,right-hand).
%adj(roast,roast).
adj(romantic,romantisch).         %DE
%adj(rough,rough). %HU:durva
adj(round,rund).                  %DE
%adj(rusty,rusty).
%
%
adj(sad,traurig).                 %DE
%adj(safe,safe).
%adj(same,same).
%adj(satisfactory,satisfactory).
adj(satisfied,zufrieden).         %DE
%%adj(saved,saved). PP
adj(scared,angstlich).            %DE
%adj(scary,scary).
%adj(secondary,secondary).
%adj(secret,secret).
%adj(senior,senior).
%adj(sensitive,sensitive).
%adj(separate,separate).
%adj(serious,serious).
adj(sharp,scharf).                %DE
adj(short,kurz).                  %DE
%adj(shiny,shiny).
%adj(shut,shut).
%adj(sick,sick).
%adj(silver,silver).
adj(similar,�hnlich).             %DE
adj(simple,einfach).              %DE
adj(single,einzel).               %DE
%adj(slim,slim).
adj(slow,langsam).                %DE
adj(small,klein).                 %DE
adj(smart,klug).                  %DE
%adj(smooth,smooth).
%adj(social,social).
adj(soft,weich).                  %DE
%adj(solid,solid).
%adj(sorry,sorry).
adj(sour,sauer).                  %DE
adj(south,s�dlich).               %DE
%adj(spare,spare).
%adj(special,special).
%adj(specific,specific).
%adj(specified,specified).
%adj(spherical,spherical).
adj(sportly,sportlich).           %DE
%adj(square,square).
%adj(sticky,sticky). %HU:ragad�s
%adj(stable,stable).
adj(state,staatlich).             %DE
%adj(straight,straight). %HU: egyenes
adj(strange,fremd).               %DE
adj(stressed,stressig).           %DE
adj(strict,streng).               %DE
%adj(striped,striped).
adj(strong,stark).                %DE
%adj(structural,structural).
adj(stupid,bl�d).                 %DE
adj(stupid,dumm).                 %DE
%adj(successful,successful).
%adj(sudden,sudden).
adj(sunny,sonnig).                %DE
%adj(sure,sure).
%adj(surprised,surprised).
%adj(symmetrical,symmetrical).
adj(sympathetic,sympathisch).     %DE
%adj(synchronzed,synchronized).
adj(sweet,s��).                   %DE
adj(swiss,schweizerisch).         %DE


%adj(tall,tall).
%adj(temporary,temporary).
adj(terrible,schrecklich).        %DE
%adj(tertiary,tertiary).
adj(thick,dick).                  %DE
adj(thin,d�nn).                   %DE
%adj(thirsty,thirsty).
%adj(tidy,tidy).
adj(tight,eng).                   %DE
adj(tired,m�de).                  %DE
adj(total,total).                 %DE
%adj(transparent,transparent).
adj(true,wahr).                   %DE
adj(turkish,t�rkisch).            %DE
adj(typical,typisch).             %DE


adj(ugly,h�sslich).                       %DE
%adj(underground,underground).
adj(unemployed,arbeitslos).               %DE
adj(unfriendly,unfreundlich).             %DE
%adj(unsatisfactory,unsatisfactory).
%adj(unservicable,unservicable).
%adj(unknown,unknown).
%adj(unusual,unusual).
%adj(unwanted,unwanted).
%adj(upset,upset).
adj(urgent,dringend).                     %DE
adj(useful,n�tzlich).                     %DE
%adj(usual,usual).


adj(valid,g�ltig).                        %DE
%adj(various,various).
adj(verabredet,arranged).                 %DE
%adj(vertical,vertical).
%adj(visual,visual).
%adj(violent,violent).
adj(violet,violett).                      %DE
adj(voluntary,freiwillig).                %DE


adj(warm,warm).                           %DE
adj(weak,schwach).                        %DE
%adj(weird,weird).
%adj(weekly,weekly).
adj(welcome,willkommen).                  %DE
adj(west,westlich).                       %DE
adj(wet,nass).                            %DE
%adj(whole,whole).
%adj(wide,wide). %HU:sz�les
%adj(wise,wise). %HU:b�lcs
adj(wild,wild).
adj(windy,windig).                        %DE
adj(wonderful,wunderbar).                 %DE
adj(wooden,wooden).
%adj(worried,worried).
adj(written,schriftlich).                 %DE
%adj(wrong,wrong).

adj(yellow,gelb).                         %DE


%adv(east,place,east).
%adv(north,place,north).
%adv(south,place,south).
%adv(west,place,west).

%in german no other adverbs are graded
%advgr(BASE,COMP,SUPER)
%COMP, SUPER are an adv-stems


adv(oneTime,time,einmal).                 %DE
adv(twoTimes,time,zweimal).               %DE
adv(threeTimes,time,dreimal).             %DE

adv(mondays,time,montags).                %DE
adv(tuesdays,time,dienstags).             %DE
adv(fridays,time,freitags).               %DE
adv(thursdays,time,donnerstags).          %DE
adv(saturdays,time,samstags).             %DE
adv(sundays,time,sonntags).               %DE

adv(thereby,place,dabei).                 %DE
adv(besides,place,daneben).               %DE
adv(therefor,place,daf�r).                %DE
adv(thereon,place,darauf).                %DE
adv(thereto,manner,dazu).                 %DE
adv(therewith,manner,damit).              %DE
adv(therethrough,manner,dadurch).         %DE
adv(onIt,place,daran).                    %DE
adv(aboutThat,manner,dar�ber).            %DE
adv(vonThat,manner,davon).                %DE


%adv(about,degree,about).
adv(above,place,oben).                    %DE
%adv(abroad,place,abroad).
adv(absolutely,degree,absolut).           %DE
%adv(accidentally,manner,accidentally).
%adv(accurately,manner,accurately).
%adv(across,place,across).  quer??
adv(actually,freq,eigentlich).            %DE
%adv(after,time,after). danach??
%adv(afterwards,time,afterwards).
adv(again,time,wieder).                   %DE
%adv(ago,time,ago).
%adv(all,degree,all).
%adv(allDay,time,[all,day]).
%adv(allRight,manner,[all,right]).
%adv(almost,freq,almost).
%adv(almost,degree,almost).  fast
%adv(alone,manner,alone).    alain
adv(already,time,schon).                  %DE
%adv(allRight,manner,alright).
adv(also,degree,auch).                    %DE
%adv(alternatively,manner,alternatively).
adv(always,freq,immer).                   %DE
%adv(amazingly,manner,amazingly).
%adv(angrily,manner,angrily).
%adv(anymore,time,anymore).
adv(anyway,focus,gar).                    %DE
%adv(anywhere,place,anywhere). irgendwo
%adv(apart,place,apart).
%adv(apparently,manner,apparently).
adv(approximately,degree,circa).          %DE
%adv(around,degree,around).
%adv(asWell,link,[as,well]).
adv(atLast,time,endlich).                 %DE
adv(atLeast,degree,wenigstens).           %DE
adv(atLeast,degree,mindestens).           %DE
adv(atNoon,time,mittags).                 %DE
adv(atNight,time,nachts).                 %DE
adv(atMidnight,time,[um,'Mitternacht']).  %DE
adv(automatically,manner,automatisch).    %DE
%adv(averagely,manner,averagely).
adv(away,place,vorbei).                   %DE
adv(away,place,weg).                      %DE
%adv(awefully,degree,awefully).
%adv(awesomely,manner,awesomely).


adv(back,place,hinten).                   %DE
%adv(badly,manner,badly).
%adv(basically,manner,basically).
adv(before,time,bevor).                  %DE
%adv(behind,place,behind).
%adv(below,place,below).  unten
%adv(brightly,manner,brightly).
%
%
%adv(carefully,manner,carefully). sorgf�ltig
%adv(certainly,prob,certainly).
adv(clearly,manner,deutlich).             %DE
%adv(clockwise,place,clockwise).
%adv(comfortably,manner,comfortably).
adv(completely,degree,ganz).              %DE
%adv(consequently,link,consequently).
%adv(constantly,degree,constantly).
%adv(continuously,manner,continuously).
adv(cordially,manner,herzlich).           %DE
%adv(correctly,manner,correctly).
%adv(counterclockwise,place,counterclockwise).


%adv(dangerously,manner,dangerously).
adv(daily,time,t�glich).                  %DE
adv(daytime,time,tags�ber).               %DE
adv(dayAfterTomorrow,time,�bermorgen).    %DE
adv(dayBeforeYesterday,time,vorgestern).  %DE
adv(definitely,prob,[auf,jeden,'Fall']).  %DE
%adv(diagonally,place,diagonally).
adv(differently,degree,anders).           %DE
%adv(dimly,manner,dimly).
adv(directly,manner,direkt).              %DE
adv(double,degree,doppelt).               %DE
adv(down,place,abw�rts).                  %DE
%adv(downstairs,place,downstairs).


adv(early,time,fr�her).                   %DE
%adv(easily,manner,easily).
%adv(east,place,east).
%adv(electrically,manner,electrically).
%adv(else,manner,else).
%adv(emptily,manner,emptily).
adv(enough,degree,genug).                 %DE
%adv(entirely,degree,entirely).
%adv(equally,degree,equally).
adv(especially,focus,besonders).          %DE
adv(even,focus,sogar).                    %DE
adv(ever,freq,jemals).                    %DE
adv(everywhere,place,�berall).            %DE
adv(exactly,manner,genau).                %DE
%adv(extremely,degree,extremely).
%adv(externally,manner,externally).


%adv(fairly,degree,fairly).
adv(far,place,fern).                      %DE
adv(fast,manner,schnell).                 %DE
adv(finally,time,schliesslich).           %DE
%adv(finely,manner,finely).
adv(first,time,anfangs).                  %DE
adv(first,time,erst).                     %DE
adv(first,time,zuerst).                   %DE
adv(forward,place,vorw�rts).              %DE
%adv(freely,manner,freely).
adv(friendly,manner,freundlich).          %DE
%adv(fully,manner,fully).
%adv(funnily,manner,funnily).
adv(further,place,weiter).                %DE


%adv(generally,freq,generally).
adv(gladly,manner,gern).                  %DE
advGr(gern,lieber,liebsten).              %DE

%adv(happily,manner,happily).
adv(hard,manner,hart).                    %DE
adv(hardly,freq,kaum).                    %DE
%adv(heavily,manner,heavily).
adv(here,place,hier).                     %DE
adv(underWard,place,herunter).            %DE
adv(inWard,place,herein).                 %DE
%adv(highly,degree,highly).
adv(home,place,heim).                     %DE
adv(hopefully,manner,hoffentlich).        %DE
adv(hourly,time,st�ndlich).               %DE
adv(how,manner,wie).                      %DE
%adv(however,link,however).


adv(immediately,manner,gleich).           %DE
adv(immediately,manner,sofort).           %DE
adv(in,place,rein).                       %DE
%adv(importantly,manner,importantly).
adv(inCash,manner,bar).                   %DE
%adv(independently,manner,independently).
adv(individually,manner,einzeln).         %DE
%adv(indoors,place,indoors).
adv(inAfternoon,time,nachmittags).        %DE
adv(inEvening,time,abends).               %DE
adv(inFront,place,vorne).                 %DE
adv(inFront,place,vorn).                  %DE
adv(inMornings,time,morgens).             %DE
adv(inMorning,time,vormittags).           %DE
adv(inTime,time,p�nktlich).               %DE
adv(inside,place,drinnen).                %DE
%adv(instead,manner,instead).
%adv(initially,manner,initially).


adv(just,focus,gerade).                   %DE


%adv(largely,focus,largely).
%adv(lastpast,time,last).
%adv(lastYear,time,[last,year]).
adv(late,time,sp�t).                      %DE
adv(later,time,sp�ter).                   %DE
%adv(least,super,least).
adv(left,place,links).                    %DE
adv(less,comp,weniger).                   %DE
%adv(like,manner,like).
adv(likewise,manner,gleichfalls).         %DE
adv(little,degree,wenig).                 %DE
adv(long,time,lange).                     %DE
%adv(loosely,manner,loosely).
adv(loudly,manner,laut).                  %DE
%adv(lots,degree,lots).
adv(luckily,manner,gl�cklich).            %DE


%adv(mainly,focus,mainly).
%adv(manually,manner,manually).
adv(maybe,prob,vielleicht).               %DE
%adv(moderately,manner,moderately).
%adv(momentarily,time,momentarily).
adv(monthly,time,monatlich).              %DE
adv(more,degree,mehr).                    %DE
%adv(more,comp,more).
adv(most,super,meist).                    %DE
%adv(mostOfTime,time,meist).              %DE
adv(mostly,time,meistens).                %DE
adv(much,degree,viel).                    %DE
adjGr(viel,mehr,meist).                   %DE


adv(naturally,manner,nat�rlich).          %DE
adv(near,place,[in,der,n�he]).            %DE
adv(nearly,degree,fast).                  %DE
%adv(nearly,freq,nearly).
adv(never,freq,nie).                      %DE
adv(nowhere,place,nirgends).              %DE
adv(necessarily,manner,unbedingt).        %DE
adv(nextDoor,place,nebenan).              %DE
%adv(newly,manner,newly).
%adv(nicely,manner,nicely).
%adv(no,neg,no).
adv(noMatter,manner,egal).                %DE
%adv(north,place,north).
adv(not,neg,nicht).                       %DE
adv(now,time,jetzt).                      %DE


%adv(occasionally,freq,occasionally).
adv(off,manner,los).                      %DE
adv(offline,manner,offline).              %DE
%adv(ofcourse,focus,[of,course]).
adv(often,freq,oft).                      %DE
advGr(oft,h�ufiger,h�ufigsten).           %DE
advGr(oft,�fter,�ftesten).                %DE
%adv(oldly,manner,oldly).
%adv(on,manner,on).
adv(once,freq,einmal).                    %DE
adv(oncemore,freq,nochmals).              %DE
adv(online,manner,online).                %DE
adv(only,focus,nur).                      %DE
%adv(open,manner,open).
adv(opposite,place,gegen�ber).            %DE +DAT
adv(out,place,raus).                      %DE
adv(out,place,aus).                       %DE
%adv(outdoors,place,outdoors).
adv(outside,place,au�erhalb).             %DE
adv(outside,place,drau�en).               %DE
%adv(over,time,over).
%adv(over,degree,over).
%adv(overnight,time,overnight).
adv(overThere,space,dr�ben).              %DE


%adv(particularly,focus,particularly).
%adv(past,time,past).
%adv(perfectly,degree,perfectly).
%adv(permanently,manner,permanently).
%adv(perhaps,prob,perhaps).
%adv(positively,manner,positively).
%adv(possibly,prob,possibly).
%adv(pretty,degree,pretty).
adv(previously,time,vorher).              %DE
adv(practically,manner,praktisch).        %DE
adv(probably,prob,wahrscheinlich).        %DE
adv(properly,manner,richtig).             %DE


%adv(quickly,manner,quickly).
%adv(quite,degree,quite).


%adv(rarely,freq,rarely).
%adv(rather,degree,rather).
adv(really,degree,echt).                  %DE
adv(really,degree,wirklich).              %DE
%adv(rearwards,place,rearwards).
%adv(recently,time,recently).
%adv(regularly,manner,regularly).
%adv(remarkably,degree,remarkably).
adv(return,place,zur�ck).                 %DE
adv(right,place,rechts).                  %DE


%adv(sadly,manner,sadly).
%adv(satisfactorily,manner,satisfactorily).
%adv(secret,manner,secretly).
adv(nicely,manner,sch�n).                 %DE
adv(self,manner,selbst).                  %DE
%adv(seldom,freq,seldom).
adv(separated,manner,getrennt).           %DE
%adv(seriously,manner,seriously).
%adv(sickly,manner,sickly).
%adv(simply,focus,simply).
%adv(since,time,since).
%adv(slightly,degree,slightly).
adv(slowly,manner,langsam).               %DE
adv(so,degree,so).                        %DE
adv(sometimes,freq,manchmal).             %DE
%adv(somewhat,degree,somewhat).
%adv(somewhere,place,somewhere).
adv(soon,time,bald).                      %DE
advGr(bald,eher,ehesten).                 %DE
%adv(sooner,time,sooner).
adv(sore,manner,weh).                     %DE
adv(sorry,manner,leid).                   %DE
%adv(south,place,south).
%adv(specially,manner,specially).
adv(still,focus,noch).                    %DE
adv(straigth,place,geradeaus).            %DE
%adv(structurally,manner,structurally).
%adv(subsequently,manner,subsequently).
adv(suddenly,time,pl�tzlich).             %DE
adv(sure,manner,sicher).                  %DE
%adv(symmetrically,manner,symmetrically).
%adv(systematically,manner,systematically).


%adv(temporarily,time,temporarily).
%adv(terribly,degree,terribly).
adv(thatTime,time,damals).                %DE
adv(thanNext,time,dann).                  %DE
%adv(then,link,then).
adv(there,place,da).                      %DE
adv(there,place,dort).                    %DE
adv(toThere,place,hin).                   %DE
adv(toThere,place,dorthin).               %DE
adv(thus,link,also).                      %DE
%adv(tightly,manner,tightly).
%adv(tiredly,manner,tiredly).
adv(today,time,heute).                    %DE
adv(together,manner,zusammen).            %DE
adv(tomorrow,time,morgen).                %DE
%adv(tonight,time,tonight).
adv(too,degree,zu).                       %DE
%adv(totally,degree,totally).
%adv(twice,freq,twice).


%adv(up,place,up).
%adv(upstairs,place,upstairs).
adv(under,place,unten).                   %DE
adv(unfortunately,manner,leider).         %DE
%adv(unsatisfactorily,manner,unsatisfactorily).
%adv(unusually,freq,unusually).
%adv(usually,freq,usually).


%adv(vertically,place,vertically).
adv(very,degree,sehr).                    %DE
advGr(sehr,mehr,meisten).                 %DE
%adv(visually,manner,visually).


adv(weekly,time,w�chentlich).             %DE
%adv(west,place,west).
adv(well,manner,wohl).                    %DE
advGr(wohl,besser,besten).                %DE
advGr(wohl,wohler,wohlsten).              %DE
%adv(wrongly,manner,badly).


adv(yesterday,time,gestern).              %DE
%adv(yet,degree,yet).
%adv(youngly,manner,youngly).


%Per comma connecting adverbs may come in the front of sentences
%following by a comma. Eg: "However, we were sleeping."
adv(accordingly,connecting(comma),accordingly).
adv(atLast,connecting(comma),endlich).                 %DE
adv(firstOfAll,connecting(comma),[first,of,all]).
adv(besides,connecting(comma),ausserdem).              %DE
adv(byTheWay,connecting(comma),�brigens).              %DE
adv(consequently,connecting(comma),consequently).
adv(furthermore,connecting(comma),furthermore).
adv(hence,connecting(comma),hence).
adv(however,connecting(comma),however).
adv(likewise,connecting(comma),likewise).
adv(moreover,connecting(comma),moreover).
adv(nevertheless,connecting(comma),nevertheless).
adv(nonetheless,connecting(comma),andererseits).       %DE
adv(otherwise,connecting(comma),sonst).                %DE
adv(quickly,connecting(comma),quickly).
adv(sorry,connecting(comma),sorry).
adv(still,connecting(comma),still).
adv(therefore,connecting(comma),deshalb). %DE
adv(thus,connecting(comma),thus).
adv(so,connecting(comma),so).


adv(after,part,after).
adv(at,part,at).
adv(down,part,down).
adv(for,part,for).
adv(off,part,off).
adv(on,part,on).
adv(out,part,out).
adv(up,part,up).



%these are rather relative pronouns than adverbs
adv(when,time,wann).         %DE
adv(where,place,wo).         %DE
adv(why,reason,warum).       %DE

