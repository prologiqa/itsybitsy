/*
%prep(about,about).
%prep(abroad,abroad).
%prep(above,above).
%prep(across,across).
prep(accordingTo,[according,to]).
%prep(after,after).
%prep(against,against).
%prep(ago,ago).
%prep(along,along).
%prep(among,among).
prep(amongst,amongst).
%prep(around,around).
%prep(as,as).
prep(asWell,[as,well]).
prep(asWellAs,[as,well,as]).
%prep(at,at).
prep(becauseOf,[because,of]).
%prep(before,before).
%prep(behind,behind).
%prep(below,below).
prep(beneath,beneath).
%prep(beside,beside).
prep(besides,besides).
prep(between,between).
prep(beyond,beyond).
prep(but,but).
%prep(by,by).
%prep(down,down).
prep(despite,despite).
prep(down,down).
prep(dueTo,[due,to]).
%prep(during,during).
%prep(except,except).
%prep(for,for).
%prep(from,from).
%prep(in,in).
%prep(including,including).
%prep(inside,inside).
%prep(into,into).
%prep(like,like).
%prep(minus,minus).
%prep(near,near).
prep(nextTo,[next,to]).
%prep(of,of).
prep(off,off).
%prep(on,on).
prep(onto,onto).
%prep(opposite,opposite).
prep(outOf,[out,of]).
%prep(outside,outside).
%prep(over,over).
%prep(past,past).
%prep(per,per).
%prep(plus,plus).
prep(regarding,regarding).
prep(round,round).
%prep(since,since).
%prep(through,through).
%prep(till,till).
%prep(to,to).
prep(toward,toward).
prep(toward,towards).
%prep(under,under).
%prep(until,until).
%prep(up,up).
prep(upon,upon).
prep(upto,[up,to]).
%prep(versus,versus).
prep(via,via).
%prep(with,with).
prep(within,within).
%prep(without,without).


/*
art(def,the,_).
art(indef,a,sg).
art(indef,an,sg).

%independent/coordinating conjunctions
conj(and,and,coordinating). %the combination of two similar thoughts
conj(but,but,coordinating). %contrasting between two thoughts
conj(yet,yet,coordinating). %contrasting between two thoughts
conj(or,or,coordinating).   %indicating alternative thoughts
conj(nor,nor,coordinating). %when neither thought is true


% both...and...
% either...or... sentence either you will eat your dinner of you will go
% to bed
% hardly...when... He had hardly begun to work when he was interrupted
% just as...so...
% neither...nor... INVERTED
% no sooner...INVERTED than...
% No sooner had I received the corner than the bus came
% Scarcely... INVERTED... than...
%
% not only...INVERTED... but also...
% Not only do I love this band, but I have also seen the concert twice.
% rather...or... whether...or...

%if...then...  wo 'then' is applicable too...
%If that is true, then what happened is not surprising.


%correlative conjunctions
correlative(noSoonerThan,no,sooner,inverse,than,straight).

correlative(eitherOr,either,straight,or,straight).
correlative(neitherNor,neither,straight,nor,inverse).
correlative(ifThen,if,straight,then,straight).
correlative(scarcelyWhen,scarcely,inverse,when,straight).

correlative(eitherOr,either,or,nounPhrase).
correlative(neitherNor,neither,nor,nounPhrase).
correlative(bothAnd,both,and,nounPhrase).

%correlative(ifThen,if,straight,then,straight).
%correlative(scarcelyWhen,scarcely,inverse,when,straight).

conj(bothAnd,both,and,coordinating,phrase).

conj(that,that,subclause(that)).
conj(if,if,subclause(if)).
conj(whether,whether,subclause(if)).


%subordinating conjunction
conj(so,so,subordinating).   %the second follows from the first
conj(because,because,subordinating). %the first follows from the second
conj(for,for,subordinating). %the first follows from the second
conj(if,if,subordinating).
conj(when,when,subordinating).
conj(while,while,subordinating).
conj(although,although,subordinating).
conj(as,as,subordinating).



%comparison conjunction
conj(than,than,comp).
conj(as,as,comp).


conj(or,or,corr).   %indicating alternative thoughts
conj(nor,nor,corr). %when neither thought is true
conj(either,either,corr).
conj(neither,neither,corr).
conj(if,if,corr).   %indicating alternative thoughts
conj(then,then,corr).   %indicating alternative thoughts
conj(scarcely,scarcely,corr).
conj(when,when,corr).
conj(both,both,corr).
conj(and,and,corr).


%conj(please,please).

%demonstratives
det(this,this,sg,numNP).
det(that,that,sg,numNP).
det(these,these,pl,numNP).
det(those,those,pl,numNP).
det(enough,enough,sg,numNP).
det(certain,certain,pl,numNP).
det(other,other,pl,numNP).
det(another,another,sg,numNP).


det(allOf,pl,[all,of],_,possNP).
det(fewOf,pl,[few,of],pl,possNP).
det(alotOf,pl,[a,lot,of],_,possNP).
det(lotsOf,pl,[lots,of],pl,possNP).
det(neitherOf,pl,[neither,of],pl,possNP).
det(noneOf,pl,[none,of],pl,possNP).
det(eitherOf,sg,[either,of],pl,possNP).
det(anyOf,sg,[any,of],pl,possNP).
det(coupleOf,pl,[a,couple,of],pl,possNP).
det(tonsOf,pl,[tons,of],pl,possNP).
det(plentyOf,pl,[plenty,of],sg,possNP).
det(bitOf,sg,[a,bit,of],sg,possNP).
det(numberOf,pl,[a,number,of],pl,possNP).
det(pairOf,pl,[a,pair,of],pl,possNP).
det(greatDealOf,sg,[a,great,deal,of],sg,possNP).
det(suchA,sg,[such,a],sg,possNP).
det(whatA,sg,[what,a],sg,possNP).
det(quiteA,sg,[quite,a],sg,possNP).
det(ratherA,sg,[rather,a],sg,possNP).


indef(all,all,pl,count,adjNP).
indef(each,each,_,count,adjNP).
indef(some,some,_,count,adjNP).
indef(many,many,pl,count,adjNP).
indef(much,much,sg,uncount,adjNP).
indef(more,more,sg,_,adjNP).
indef(most,most,pl,_,adjNP).
indef(any,any,_,_,adjNP).
indef(few,few,_,_,adjNP).
indef(fewer,fewer,_,_,adjNP).
indef(fewest,fewest,sg,_,adjNP).
indef(little,little,sg,uncount,adjNP).
indef(less,less,_,_,adjNP).
indef(least,least,sg,_,adjNP).
indef(every,every,sg,count,adjNP).
indef(several,several,pl,count,adjNP).
indef(no,no,_,_,adjNP).

indef(both,both,pl,count,possNP).
indef(half,half,sg,count,possNP).


indefPronoun(someone,someone,sg).
indefPronoun(somebody,somebody,sg).
indefPronoun(something,something,sg).
indefPronoun(anyone,anyone,sg).
indefPronoun(anybody,anybody,sg).
indefPronoun(anything,anything,sg).
indefPronoun(everyone,everyone,pl).
indefPronoun(everybody,everybody,pl).
indefPronoun(everything,everything,pl).
indefPronoun(nobody,nobody,sg).
indefPronoun(nothing,nothing,sg).
indefPronoun(many,many,pl).
indefPronoun(some,some,pl).
indefPronoun(each,each,pl).
indefPronoun(any,any,pl).
indefPronoun(one,one,sg).
indefPronoun(this,this,sg).
indefPronoun(that,that,sg).


interj(hello,hello).
interj(no,no).
interj(ok,ok).
interj(whoa,whoa).
interj(wow,wow).
interj(yes,yes).

interr(how,how,adv(manner)).
interr(what,what,_). %it can be nom or acc as well
interr(when,when,adv(time)).
interr(where,where,adv(place)).
interr(which,which,adj). %which shoes
interr(whom,whom,acc).
interr(who,who,nom).
interr(why,why,adv(reason)).

interr(howmany,[how,many],num).
interr(howmuch,[how,much],num).


ord(first,1).
ord(second,2).
ord(third,3).
ord(fourth,4).
ord(fifth,5).

card(one,1).
card(two,2).
card(three,3).
card(four,4).
card(five,5).
card(six,6).
card(seven,7).
card(eight,8).
card(nine,9).
card(ten,10).
card(eleven,11).
card(twelve,12).
card(twenty,20).
card(thirty,30).
card(fourty,40).
card(fifty,50).
card(sixty,60).
card(seventy,70).
card(eighty,80).
card(ninety,90).
card(houndred,100).
card(thousand,1000).
card(million,1000000).
card(billion,1000000000).

unitAdj(meter,long).
unitAdj(meter,high).
unitAdj(hour,long).
unitAdj(year,old).
unitAdj(year,long).
*/


%adj(able,able).
adj(above,above).
adj(absent,absent).
%adj(acid,acid).
%adj(academic,academic).
%adj(accidental,accidental).
%adj(accurate,accurate).
adj(active,active).
%adj(adult,adult).
%adj(advanced,advanced).
%adj(afraid,afraid).
%adj(aged,aged).
adj(alive,alive).
%adj(alone,alone).
%adj(alright,alright).
adj(amazed,amazed).
%adj(amazing,amazing).
%adj(american,'American').
adj(amusing,amusing).
adj(ancient,ancient).
%adj(angry,angry).
adj(annual,annual).
adj(antique,antique).
adj(anxious,anxious).
adj(apparent,apparent).
adj(applicable,applicable).
adj(approved,approved).
adj(approximate,approximate).
%adj(asian,'Asian').
adj(ashamed,ashamed).
adj(asleep,asleep).
%adj(attractive,attractive).
adj(automatic,automatic).
adj(auxiliary,auxiliary).
%adj(available,available).
adj(average,average).
adj(awake,awake).
%adj(awesome,awesome).
%adj(awful,awful).


%adj(back,back).
%adj(bad,bad).
adj(bald,bald).
adj(basic,basic).
%adj(beautiful,beautiful).
%adj(big,big).
adj(bitter,bitter).
%adj(black,black).
adj(blank,blank).
adj(blind,blind).
%adj(blond,blond).
adj(blond,blonde).
adj(blocked,blocked).
%adj(blue,blue).
%adj(blunt,blunt).
%%adj(boiled,boiled)... PP
adj(bold,bold).
%adj(bored,bored).
%adj(boring,boring).
adj(born,born).
adj(bottom,bottom).
adj(brandNew,[brand,new]).
%adj(brave,brave).
adj(brief,brief).
%adj(bright,bright).
%adj(brilliant,brilliant).
adj(broad,broad).
%%adj(broken,broken). PP
%adj(brown,brown).
%adj(burning,burning). PP
%adj(busy,busy).


adj(calm,calm).
adj(capital,capital).
adj(cardboard,cardboard).
%adj(careful,careful).
adj(careless,careless).
adj(casual,casual).
adj(central,central).
adj(certain,certain).
adj(challenging,challenging).
adj(charming,charming).
%adj(cheap,cheap).
adj(cheerful,cheerful).
%adj(chemical,chemical).
%adj(chief,chief).
%adj(circular,circular).
adj(classical,classical).
%adj(clean,clean).
%adj(clear,clear).
%adj(clever,clever).
%adj(close,close).
%%adj(closed,closed). PP
%adj(cloudy,cloudy).
adj(clueless,clueless).
%adj(cold,cold).
adj(colorful,colorful).
adj(colorful,colourful).
%adj(comfortable,comfortable).
adj(comic,comic).
adj(common,common).
adj(comparable,comparable).
adj(complete,complete).
adj(complicated,complicated).
adj(complex,complex).
adj(confident,confident).
adj(confined,confined).
adj(confused,confused).
adj(confusing,confusing).
adj(conscious,conscious).
adj(constant,constant).
adj(continuous,continuous).
adj(convenient,convenient).
%adj(cool,cool).
%adj(correct,correct).
adj(cosy,cosy).
%adj(crazy,crazy).
%adj(cream,cream).
adj(creative,creative).
%adj(crowded,crowded).
adj(cruel,cruel).
adj(cultural,cultural).
adj(curious,curious).
adj(current,current).
adj(curly,curly).
adj(cute,cute).


%adj(daily,daily).
%%adj(damaged,damaged). PP
%adj(dangerous,dangerous).
%adj(dark,dark).
%adj(dead,dead).
adj(deaf,deaf).
%adj(dear,dear).
%adj(deep,deep).
adj(defective,defective).
adj(delicate,delicate).
%adj(delicious,delicious).
adj(delighted,delighted).
adj(dependent,dependent).
adj(depressed,depressed).
adj(diagonal,diagonal).
%adj(different,different).
%adj(difficult,difficult).
%adj(digital,digital).
adj(dim,dim).
adj(direct,direct).
%adj(dirty,dirty).
adj(disabled,disabled).
adj(disappointed,disappointed).
adj(disappointing,disappointing).
adj(disgusting,disgusting).
%adj(diving,diving).
adj(divorced,divorced).
adj(dizzy,dizzy).
%adj(double,double).
adj(downstairs,downstairs).
%%adj(dressed,dressed). PP
%adj(dry,dry).
adj(due,due). %to
adj(dull,dull).
adj(dusty,dusty).
adj(dutyFree,[duty,free]).


%adj(early,early).
%adj(east,east).
adj(eastern,eastern).
%adj(easy,easy).
adj(easygoing,easygoing).
adj(efficient,efficient).
adj(elastic,elastic).
adj(elder,elder).
adj(elderly,elderly).
%adj(electric,electric).
adj(electrical,electrical).
adj(electronic,electronic).
adj(elementary,elementary).
adj(embarrassed,embarrassed).
adj(embarrassing,embarrassing).
%adj(empty,empty).
adj(engaged,engaged).
%adj(english,'English').
adj(enormous,enormous).
adj(environmental,environmental).
adj(equal,equal).
adj(essential,essential).
%adj(european,'European').
adj(exact,exact).
%adj(excellent,excellent).
%adj(excited,excited).
%adj(exciting,exciting).
adj(exhausted,exhausted).
adj(expanded,expanded).
%adj(expensive,expensive).
adj(experienced,experienced).
adj(expert,expert).
adj(expired,expired).
adj(explosive,explosive).
adj(external,external).
%adj(extra,extra).
adj(extraordinary,extraordinary).


adj(faceToFace,face-to-face).
%adj(fair,fair).
adj(false,false).
adj(familiar,familiar).
%adj(famous,famous).
%adj(fantastic,fantastic).
adj(fancy,fancy).
%adj(far,far).
adj(fashionable,fashionable).
%adj(fast,fast).
%adj(fat,fat).
%adj(favorite,favorite).
adj(feeble,feeble). %HU: gyenge
%adj(female,female).
%adj(fertile,fertile).
%adj(few,few).
%adj(final,final).
adj(financial,financial).
%adj(fine,fine).
%adj(fixed,fixed).
%adj(fit,fit).
adj(flat,flat).
adj(flexible,flexible).
%adj(foggy,foggy).
adj(following,following).
adj(foolish,foolish).
adj(fond,[fond,of]). %of
adj(forbidden,forbidden).
%adj(foreign,foreign).
adj(former,former).
%adj(free,free).
adj(freezing,freezing).
adj(frequent,frequent).
%adj(fresh,fresh).
%%adj(fried,fried). PP
%adj(friendly,friendly).
%adj(frightened,frightened).
adj(frightening,frightening).
adj(front,front).
%%adj(frozen,frozen). PP
%adj(full,full).
%adj(funny,funny).
%adj(further,further).
adj(furthest,furthest).
adj(future,future).


adj(general,general).
adj(generous,generous).
adj(gentle,gentle).
adj(giant,giant).
%adj(glad,glad).
%adj(glass,glass).
%adj(gold,gold).
%adj(golden,golden).
%adj(good,good).
%adj(goodlooking,[good,looking]).
adj(gorgeous,gorgeous).
adj(grateful,grateful).
%adj(great,great).
%adj(green,green).
%adj(grey,grey).
%adj(grilled,grilled).
adj(gross,gross).
adj(guilty,guilty).


%adj(halfprice,[half,price]).
adj(handHeld,hand-held).
adj(handsome,handsome).
%adj(happy,happy).
%adj(hard,hard).
%adj(healthy,healthy).
%adj(heavy,heavy).
adj(hidden,hidden).
%adj(high,high).
adj(historic,historic).
adj(historical,historical).
adj(hollow,hollow). %HU:�reges
%adj(honest,honest).
adj(hopeful,hopeful).
adj(hopeless,hopeless).
%adj(horrible,horrible).
%adj(horror,horror).
%adj(hot,hot).
adj(huge,huge).
adj(human,human).
adj(humid,humid).
%adj(hungry,hungry).
%adj(hurt,hurt).


adj(icy,icy).
%adj(ill,ill).
%adj(important,important).
adj(impossible,impossible).
adj(included,included).
adj(incorrect,incorrect).
adj(incredible,incredible).
adj(independent,independent).
adj(individual,individual).
%adj(indoor,indoor).
adj(initial,initial).
adj(intelligent,intelligent).
%adj(interested,interested).
%adj(interesting,interesting).
adj(intermediate,intermediate).
adj(internal,internal).
adj(international,international).
adj(irregular,irregular).


adj(jealous,jealous).


adj(keen,keen).
adj(kind,kind).
%%adj(known,known). PP


%adj(large,large).
%adj(last,last).
%adj(late,late).
%adj(lazy,lazy).
adj(least,least).
%adj(leather,leather).
%adj(left,left).
%adj(lefthand,left-hand).
%adj(light,light).
adj(likely,likely).
adj(limited,limited).
adj(liquid,liquid).
%adj(little,little).
adj(live,live).
%adj(local,local).
adj(lonely,lonely).
%adj(long,long).
adj(loose,loose).
%adj(lost,lost).
%adj(loud,loud).
adj(lousy,lousy).
%adj(lovely,lovely).
%adj(low,low).
%adj(lucky,lucky).


%adj(mad,mad).
adj(magic,magic).
adj(magnificient,magnificient).
adj(male,male).
adj(main,main).
adj(male,male).
adj(mandatory,mandatory).
adj(manual,manual).
%%adj(married,married). %PP
adj(marvellous,marvellous).
adj(material,material).
adj(maximum,maximum).
adj(medical,medical).
adj(medium,medium).
adj(messy,messy).
%adj(metal,metal).
adj(middle,middle).
adj(middleAged,[middle,aged]).
adj(mild,mild).
adj(military,military).
adj(minimum,minimum).
adj(miserable,miserable).
%adj(missing,missing).
adj(mixed,mixed).
%adj(modern,modern).
%adj(monthly,monthly).
%adj(musical,musical).


adj(narrow,narrow).
adj(nasty,nasty).
%adj(national,national).
adj(natural,natural).
adj(navyBlue,[navy,blue]).
adj(near,near).
adj(neat,neat).
adj(nearby,nearby).
adj(necessary,necessary).
adj(negative,negative).
adj(nervous,nervous).
adj(net,net).
%adj(new,new).
%adj(next,next).
%adj(nice,nice).
%adj(noisy,noisy).
%adj(normal,normal).
%adj(north,north).
adj(northeast,northeast).
adj(northern,northern).
adj(northwest,northwest).


adj(obvious,obvious).
adj(odd,odd).
adj(offline,offline).
%adj(old,old).
adj(oldFashioned,[old,fashioned]).
%adj(online,online).
%adj(only,only).
%adj(open,open).
adj(opposite,opposite).
adj(optional,optional).
%adj(orange,orange).
adj(ordinary,ordinary).
adj(original,original).
adj(other,other).
%adj(outdoor,outdoor).
adj(outer,outer).
adj(outside,outside).
adj(overall,overall).
adj(overnight,overnight).
%adj(own,own).


adj(painful,painful).
%adj(pale,pale).
%adj(paper,paper).
adj(parallel,parallel).
adj(particular,particular).
adj(partTime,part-time).
adj(past,past).
adj(patient,patient).
adj(peaceful,peaceful).
%adj(perfect,perfect).
adj(permanent,permanent).
adj(permitted,permitted).
adj(personal,personal).
adj(physical,physical).
%adj(pink,pink).
adj(plain,plain).
%adj(plastic,plastic).
%adj(pleasant,pleasant).
%adj(pleased,pleased).
%adj(polite,polite).
adj(political,political).
%adj(poor,poor).
%adj(popular,popular).
adj(positive,positive).
%adj(possible,possible).
adj(positive,positive).
adj(powerful,powerful).
adj(pregnant,pregnant).
adj(prepared,prepared). %PP
adj(present,present).
adj(preserved,preserved).
%adj(pretty,pretty).
adj(previous,previous).
adj(primary,primary).
adj(principial,principial).
adj(private,private).
adj(probable,probable).
adj(professional,professional).
adj(proper,proper).
adj(proud,proud).
adj(poisonous,poisonous).
adj(positive,positive).
adj(possible,possible).
adj(public,public).
adj(pure,pure).
%adj(purple,purple).


adj(qualified,qualified).
%adj(quick,quick).
%adj(quiet,quiet).


adj(rare,rare).
adj(raw,raw).
%adj(ready,ready).
%adj(real,real).
adj(realistic,realistic).
adj(reasonable,reasonable).
adj(rear,rear).
adj(recent,recent).
adj(recycled,recycled).
%adj(red,red).
adj(regular,regular).
adj(related,related).
adj(relaxed,relaxed).
adj(reliable,reliable).
adj(remaining,remaining).
adj(rescued,rescued).
adj(resistant,resistant).
adj(responsible,responsible).
%adj(rich,rich).
%adj(right,right).
%adj(rightHand,right-hand).
%adj(roast,roast).
adj(romantic,romantic).
adj(rough,rough). %HU:durva
%adj(round,round). %HU:kerek
adj(rude,rude).
adj(rusty,rusty).


%adj(sad,sad).
%adj(safe,safe).
%adj(same,same).
adj(satisfactory,satisfactory).
adj(satisfied,satisfied).
%%adj(saved,saved). PP
%adj(scared,scared).
%adj(scary,scary).
adj(scientific,scientific).
adj(secondary,secondary).
adj(secondHand,[second,hand]).
adj(secret,secret).
adj(selfish,selfish).
adj(selfService,self-service).
adj(senior,senior).
adj(sensible,sensible).
adj(sensitive,sensitive).
adj(separate,separate).
adj(serious,serious).
adj(sharp,sharp).
adj(shiny,shiny).
adj(shocked,shocked).
adj(shocking,shocking).
%adj(short,short).
adj(shut,shut).
adj(shy,shy).
%adj(sick,sick).
adj(silent,silent).
adj(silk,silk).
adj(silly,silly).
%adj(silver,silver).
adj(similar,similar).
%adj(simple,simple).
%adj(single,single).
adj(situated,situated).
%adj(slim,slim).
%adj(slow,slow).
%adj(small,small).
adj(smart,smart).
adj(smooth,smooth).
adj(sociable,sociable).
adj(social,social).
%adj(soft,soft).
adj(solid,solid).
adj(sore,sore).
%adj(sorry,sorry).
adj(soSo,so-so).
adj(sour,sour).
%adj(south,south).
adj(southeast,southeast).
adj(southwest,southwest).
%adj(spare,spare).
adj(sparkling,sparkling).
%adj(special,special).
adj(spectacular,spectacular).
adj(specific,specific).
adj(specified,specified).
adj(spherical,spherical).
adj(spicy,spicy).
%adj(square,square).
adj(stable,stable).
adj(steep,steep). %HU:ragad�s
adj(sticky,sticky). %HU:ragad�s
adj(still,still).
%adj(straight,straight). %HU: egyenes
%adj(strange,strange). %HU furcsa, k�l�n�s
adj(stressed,stressed).
adj(stressful,stressful).
%adj(strict,strict).
%adj(striped,striped).
%adj(strong,strong).
adj(structural,structural).
adj(stupid,stupid).
adj(stylish,stylish).
%adj(successful,successful).
adj(sudden,sudden).
adj(suitable,suitable).
%adj(sunny,sunny).
%adj(sure,sure).
%adj(surprised,surprised).
adj(surprising,surprising).
adj(symmetrical,symmetrical).
adj(synchronzed,synchronized).
%adj(sweet,sweet).


adj(talented,talented).
%adj(tall,tall).
adj(tasty,tasty).
adj(temporary,temporary).
%adj(terrible,terrible).
adj(terrific,terrific).
adj(terrified,terrified).
adj(tertiary,tertiary).
adj(thick,thick). %HU: vastag
%adj(thin,thin).
%adj(thirsty,thirsty).
%adj(tidy,tidy).
adj(tight,tight).
adj(tiny,tiny).
%adj(tired,tired).
adj(tiring,tiring).
adj(top,top).
%adj(total,total).
adj(traditional,traditional).
adj(transparent,transparent).
%adj(true,true).
adj(typical,typical).


adj(ugly,ugly).
adj(unable,unable).
adj(unbelievable,unbelievable).
adj(uncomfortable,unconfortable).
%adj(underground,underground).
adj(unemployed,unemployed).
adj(unexpected,unexpected).
adj(unfair,unfair).
adj(unfit,unfit).
adj(unforgettable,unforgettable).
adj(unfriendly,unfriendly).
adj(unhappy,unhappy).
adj(unhealthy,unhealthy).
adj(unkind,unkind).
adj(unimportant,unimportant).
adj(uninterested,uninterested).
adj(uninteresting,uninteresting).
adj(unknown,unknown).
adj(unlikely,unlikely).
adj(unlucky,unlucky).
adj(unnecessary,unnecessary).
adj(unpleasant,unpleasant).
adj(unsatisfactory,unsatisfactory).
adj(unservicable,unservicable).
%adj(unusual,unusual).
adj(unwanted,unwanted).
adj(unwell,unwell).
adj(upper,upper).
%adj(upset,upset).
adj(upstairs,upstairs).
adj(urgent,urgent).
%adj(useful,useful).
%adj(usual,usual).


adj(valuable,valuable).
%adj(various,various).
adj(vegetarian,vegetarian).
adj(vertical,vertical).
adj(visual,visual).
adj(violent,violent).


%adj(warm,warm).
adj(waste,waste).
adj(weak,weak).
adj(weird,weird).
%adj(weekly,weekly).
%adj(welcome,welcome).
adj(wellDressed,well-dressed).
adj(wellKnown,well-known).
adj(wellMade,[well,made]).
adj(wellMade,well-made).
%adj(west,west).
adj(western,western).
%adj(wet,wet).
%adj(white,white).
%adj(whole,whole).
%adj(wide,wide). %HU:sz�les
%adj(wild,wild).
adj(willing,willing).
%adj(wise,wise). %HU:b�lcs
%adj(windy,windy).
%adj(wonderful,wonderful).
%adj(wooden,wooden).
adj(working,working).
%adj(worried,worried).
adj(worth,worth).
%%adj(written,written). %PP
%adj(wrong,wrong).


%adj(yellow,yellow).
%adj(young,young).

/+
adjGr(bad,worse,worst).
adjGr(good,better,best).

adjKind(white,color).
adjKind(black,color).
adjKind(green,color).
adjKind(blue,color).
adjKind(red,color).
adjKind(brown,color).
adjKind(yellow,color).
adjKind(grey,color).

%adv(east,place,east).
%adv(north,place,north).
%adv(south,place,south).
%adv(west,place,west).

advGr(badly,worse,worst).
advGr(far,farther,farthest).
advGr(far,further,furthest).
advGr(little,less,least).
advGr(well,better,best).
+/

%adv(about,degree,about).
%adv(above,place,above).
adv(abroad,place,abroad).
adv(absolutely,degree,absolutely).
adv(accidentally,manner,accidentally).
adv(accurately,manner,accurately).
%adv(across,place,across).
%adv(actually,freq,actually).
%adv(after,time,after).
%adv(afterwards,time,afterwards).
%adv(again,time,again).
%adv(ago,time,ago).
adv(ahead,place,ahead).
adv(alike,manner,alike).
%adv(all,degree,all).
%adv(allDay,time,[all,day]).
%adv(allRight,manner,[all,right]).
%adv(almost,freq,almost).
%adv(almost,degree,almost).
%adv(alone,manner,alone).
%adv(along,place,along).
%adv(aloud,manner,aloud).
%adv(already,time,already).
%adv(allRight,manner,alright).
%adv(also,link,also).
adv(altogether,manner,altogether).
adv(alternatively,manner,alternatively).
%adv(always,freq,always).
adv(am,time,'a.m.').
adv(amazingly,manner,amazingly).
adv(angrily,manner,angrily).
%adv(anymore,time,anymore).
%adv(anyway,focus,anyway).
%adv(anywhere,place,anywhere).
adv(apart,place,apart).
adv(apartFrom,place,[apart,from]).
adv(apparently,manner,apparently).
adv(approximately,degree,approximately).
%adv(around,degree,around).
%adv(aswell,link,[as,well]).
adv(automatically,manner,automatically).
adv(averagely,manner,averagely).
%adv(away,place,away).
adv(awefully,degree,awefully).
adv(awesomely,manner,awesomely).


%adv(back,place,back).
adv(backwards,place,backwards).
%adv(badly,manner,badly).
adv(basically,manner,basically).
%adv(before,time,before).
%adv(behind,place,behind).
%adv(below,place,below).
adv(beside,place,beside).
adv(between,place,between).
adv(beyond,place,beyond).
adv(brightly,manner,brightly).


%adv(carefully,manner,carefully).
%adv(certainly,prob,certainly).
%adv(clearly,manner,clearly).
adv(clockwise,place,clockwise).
adv(close,place,close).
adv(comfortably,manner,comfortably).
adv(completely,degree,completely).
adv(consequently,link,consequently).
adv(constantly,degree,constantly).
adv(continuously,manner,continuously).
adv(correctly,manner,correctly).
adv(counterclockwise,place,counterclockwise).
adv(curiously,manner,curiously).


adv(dangerously,manner,dangerously).
%adv(daily,time,daily).
adv(deep,place,deep).
adv(definitely,prob,definitely).
adv(diagonally,place,diagonally).
adv(differently,degree,differently).
adv(dimly,manner,dimly).
adv(directly,manner,directly).
%adv(down,place,down).
%adv(downstairs,place,downstairs).


%adv(early,time,early).
%adv(easily,manner,easily).
%adv(east,place,east).
adv(either,freq,either).
adv(electrically,manner,electrically).
%adv(else,manner,else).
%adv(enough,degree,enough).
adv(entirely,degree,entirely).
adv(equally,degree,equally).
%adv(especially,focus,especially).
%adv(even,focus,even).
%adv(ever,freq,ever).
%adv(everywhere,place,everywhere).
%adv(exactly,manner,exactly).
adv(extra,degree,extra).
adv(extremely,degree,extremely).
adv(externally,manner,externally).


adv(faceToFace,place,[face,to,face]).
adv(fairly,degree,fairly).
%adv(far,place,far).
%adv(fast,manner,fast).
%adv(finally,time,finally).
adv(finely,manner,finely).
%adv(first,time,first).
%adv(firstofall,time,[first,of,all]).
adv(forever,time,forever).
adv(fortunately,manner,fortunately).
adv(forward,place,forward).
adv(forward,place,forwards).
adv(free,manner,free).
adv(fully,manner,fully).
adv(funnily,manner,funnily).
adv(further,place,further).


adv(generally,freq,generally).


adv(half,degree,half).
adv(happily,manner,happily).
%adv(hard,manner,hard).
adv(hardly,freq,hardly).
adv(heavily,manner,heavily).
%adv(here,place,here).
adv(high,place,high).
adv(highly,degree,highly).
%adv(home,place,home).
adv(honestly,manner,honestly).
adv(hopefully,manner,hopefully).
%adv(how,manner,how).
%adv(however,link,however).


%adv(immediately,manner,immediately).
adv(importantly,manner,importantly).
%adv(in,place,in).
adv(independently,manner,independently).
adv(indeed,manner,indeed).
%adv(indoors,place,indoors).
%adv(inside,place,inside).
%adv(insteadOf,manner,[instead,of]). %of
adv(initially,manner,initially).


%adv(just,focus,just).


adv(largely,focus,largely).
adv(lastpast,time,last).
adv(lastYear,time,[last,year]).
%adv(late,time,late).
adv(lately,time,lately).
%adv(later,time,later).
%adv(least,super,least).
%adv(left,place,left).
%adv(less,comp,less).
%adv(like,manner,like).
adv(lively,manner,lively).
adv(loosely,manner,loosely).
adv(lots,degree,lots).


adv(mainly,focus,mainly).
adv(manually,manner,manually).
%adv(maybe,prob,maybe).
adv(meanwhile,tiome,meanwhile).
adv(moderately,manner,moderately).
adv(momentarily,time,momentarily).
%adv(monthly,time,monthly).
%adv(more,degree,more).
%adv(more,comp,more).
%adv(most,super,most).
%adv(much,degree,much).


%adv(near,place,near).
adv(nearby,place,nearby).
%adv(nearly,degree,nearly).
%adv(nearly,freq,nearly).
%adv(never,freq,never).
%adv(next,place,next).
adv(newly,manner,newly).
adv(nicely,manner,nicely).
%adv(no,neg,no).
adv(normally,manner,normally).
%adv(north,place,north).
%adv(not,neg,not).
%adv(now,time,now).
adv(nowhere,place,nowhere).


adv(obviously,manner,obviously).
adv(occasionally,freq,occasionally).
adv(oClock,time,[o,clock]).
%adv(off,manner,off).
%adv(offline,manner,offline).
%adv(ofcourse,focus,[of,course]).
%adv(often,freq,often).
adv(oldly,manner,oldly).
%adv(on,manner,on).
%adv(once,freq,once).
%adv(online,manner,online).
%adv(only,focus,only).
%adv(open,manner,open).
adv(opposite,place,opposite).
adv(otherwise,manner,otherwise).
%adv(out,place,out).
%adv(outdoors,place,outdoors).
%adv(outside,place,outside).
%adv(over,time,over).
adv(overnight,time,overnight).


adv(partly,time,partly).
adv(particularly,focus,particularly).
adv(partTime,time,[part,time]).
adv(percent,degree,percent).
adv(perfectly,degree,perfectly).
adv(permanently,manner,permanently).
%adv(perhaps,prob,perhaps).
adv(personally,manner,personally).
adv(pm,time,'p.m.').
adv(positively,manner,positively).
%adv(possibly,prob,possibly).
adv(pretty,degree,pretty).
adv(previously,time,previously).
%adv(probably,prob,probably).


%adv(quickly,manner,quickly).
%adv(quite,degree,quite).


adv(rarely,freq,rarely).
adv(rather,degree,rather).
%adv(really,degree,really).
adv(rearwards,place,rearwards).
%adv(reasonably,degree,reasonably).
adv(recently,time,recently).
adv(regularly,manner,regularly).
adv(remarkably,degree,remarkably).
%adv(right,manner,right).
adv(round,degree,round).


adv(sadly,manner,sadly).
adv(safely,manner,safely).
adv(satisfactorily,manner,satisfactorily).
adv(second,link,second).
adv(secret,manner,secretly).
adv(seldom,freq,seldom).
adv(seriously,manner,seriously).
adv(shortly,time,shortly).
adv(sickly,manner,sickly).
adv(simply,focus,simply).
adv(since,time,since).
adv(sincerely,time,sincerely).
adv(slightly,degree,slightly).
%adv(slowly,manner,slowly).
%adv(so,degree,so).
adv(somehow,manner,somehow).
%adv(sometimes,freq,sometimes).
adv(somewhat,degree,somewhat).
%adv(somewhere,place,somewhere).
%adv(soon,time,soon).
%adv(sooner,time,sooner).
%adv(south,place,south).
adv(specially,manner,specially).
%adv(still,focus,still).
adv(still,manner,still).
%adv(straigth,place,straigh).
adv(structurally,manner,structurally).
adv(subsequently,manner,subsequently).
%adv(suddenly,manner,suddenly).
adv(sure,manner,sure).
adv(symmetrically,manner,symmetrically).
adv(systematically,manner,systematically).


adv(temporarily,time,temporarily).
adv(terribly,degree,terribly).
%adv(then,time,then).
%adv(then,link,then).
%adv(there,place,there).
adv(therefore,link,there).
adv(tightly,manner,tightly).
adv(tiredly,manner,tiredly).
%adv(today,time,today).
%adv(together,manner,together).
%adv(tomorrow,time,tomorrow).
%adv(tonight,time,tonight).
%adv(too,degree,too).
adv(totally,degree,totally).
%adv(twice,freq,twice).
adv(typically,manner,typically).


adv(unfortunately,manner,unfortunately).
adv(unsatisfactorily,manner,unsatisfactorily).
adv(unusually,freq,unusually).
%adv(up,place,up).
%adv(upstairs,place,upstairs).
adv(urgently,time,urgently).
%adv(usually,freq,usually).


adv(vertically,place,vertically).
%adv(very,degree,very).
%adv(veekly,time,veekly).
adv(visually,manner,visually).


%adv(well,manner,well).
%adv(west,place,west).
%adv(when,time,when).
%adv(where,place,where).
%adv(why,reason,why).
adv(wrongly,manner,badly).


%adv(yesterday,time,yesterday).
%adv(yet,degree,yet).
adv(youngly,manner,youngly).

%adv(up,part,up).
%adv(for,part,for).
*/
/*
%Per comma connecting adverbs may come in the front of sentences
%following by a comma. Eg: "However, we were sleeping."
adv(accordingly,connecting(comma),accordingly).
adv(besides,connecting(comma),besides).
adv(consequently,connecting(comma),consequently).
adv(furthermore,connecting(comma),furthermore).
adv(hence,connecting(comma),hence).
adv(however,connecting(comma),however).
adv(likewise,connecting(comma),likewise).
adv(moreover,connecting(comma),moreover).
adv(nevertheless,connecting(comma),nevertheless).
adv(nonetheless,connecting(comma),nonetheless).
adv(otherwise,connecting(comma),otherwise).
adv(quickly,connecting(comma),quickly).
adv(still,connecting(comma),still).
adv(therefore,connecting(comma),therefore).
adv(thus,connecting(comma),thus).
adv(so,connecting(comma),so).

infPart(to).
*/





