:- encoding(iso_latin_1).

%@TBD: hat **sich** am�siert

% German simple verbs with zu+infinite subclause
%
% anfangen, aufh�ren, beabsichtigen, *beginnen, behaupten, beschlie�en,
% dazu beitragen dazu neigen, dazu tendieren, drohen, geloben, glauben,
% hoffen, jemandem anbieten, lernen, planen,schw�ren, stoppen
% vereinbaren, vorhaben, *verbieten, *vergessen, vers�umen, versprechen,
% *versuchen, vort�uschen, vorziehen, wagen, warnen vor, sich weigern,
% z�gern
%

% sich bereit erkl�ren, sich daranmachen, sich entscheiden,
% sich erinnern, sich leisten (dative)sich sehnen, sich trauen,
% sich verpflichten, sich wagen, (dative)sich weigern

aux(sei).
aux(hab).
aux(werd).


modal(k�nn,k�nnt,ability,verb).    %DE
modal(d�rf,durft,permission,verb). %DE
modal(m�g,mocht,possibility,verb). %DE
modal(m�ss,m�sst,obligation,verb). %DE
modal(soll,sollt,necessity,verb).  %DE
modal(woll,wollt,intention,verb).  %DE


verbPrefix_(ab,separable).
verbPrefix_(an,separable).
verbPrefix_(aner,inseparable).
verbPrefix_(auf,separable).
verbPrefix_(aus,separable).
verbPrefix_(be,inseparable).
verbPrefix_(bei,separable).
verbPrefix_(dar,separable).
verbPrefix_(ein,separable).
verbPrefix_(emp,inseparable).
verbPrefix_(ent,inseparable).
verbPrefix_(fern,separable).
verbPrefix_(ge,inseparable).
verbPrefix_(heim,separable).
verbPrefix_(her,separable).
verbPrefix_(herunter,separable).
verbPrefix_(hin,separable).
verbPrefix_(hinaus,separable).
verbPrefix_(hinein,separable).
verbPrefix_(in,separable).
verbPrefix_(kennen,separable).
verbPrefix_(leid,separable).
verbPrefix_(mit,separable).
verbPrefix_(nach,separable).
verbPrefix_(nieder,separable).
verbPrefix_(out,separable).
verbPrefix_(rein,separable).
verbPrefix_(raus,separable).
verbPrefix_(statt,separable).
verbPrefix_(teil,separable).
verbPrefix_(um,separable).
verbPrefix_(unter,separable).
verbPrefix_(�ber,separable).
verbPrefix_(ver,inseparable).
verbPrefix_(vor,separable).
verbPrefix_(weg,separable).
verbPrefix_(weh,separable).
verbPrefix_(weiter,separable).
verbPrefix_(wieder,separable).
verbPrefix_(zer,inseparable).
verbPrefix_(zu,separable).
verbPrefix_(zur�ck,separable).
verbPrefix_(zusammen,separable).
verbPrefix_(�ber,_).



verb(accept,akzeptier,en,akzeptier,akzeptierte,
     h,akzeptiert,[nom,acc]).                                 %DE
%verb(access,access,[nom,acc]).
%verb(accompany,accompany,[nom,acc]).
%verb(achieve,achieve,[nom,verbal(to)]).
%verb(act,act,[nom]).
%verb(adapt,adapt,[nom,acc]).
%verb(add,add,[nom,acc]).
%verb(adjust,adjust,[nom,acc]).
%verb(admire,admire,[nom,acc]).
%verb(admit,admit,[nom,clause(that)]).
%verb(advance,advance,[nom,acc]).
%verb(advertise,advertise,[nom,acc]).
verb(advise,berat,en,ber�,h,beraten,[nom,verbal(to)]).        %DE
%verb(afford,afford,([nom,acc];[nom,verbal(to)])).
verb(agree,vereinbar,en,vereinbar,h,vereinbart,
    ([nom,clause(that)];[nom,verbal(to)];
                  [nom,?(with(acc))])).                       %DE
%verb(aim,aim,([nom,acc];[nom,verbal(to)])).
%verb(alarm,alarm,[nom,acc]).
%verb(align,align,[nom,acc]).
verb(allow,erlaub,en,erlaub,h,erlaubt,
     [nom,dat,verbal(to)]).                                   %DE
verb(allowed,d�rf,en,'','',h(ge),durft,[nom,?(acc)]).         %DE
%verb(amuse,am�sier,en,am�siert,am�sierte,h,am�siert,
%     [nom,acc,verbal(to)]).                                  %DE
verb(analyse,analysier,en,analysier,analysierte,
     h,analysiert, [nom,acc]).                                %DE
verb(annoy,�rger,n,�rger,h(ge),�rgert,[nom,acc]).             %DE
verb(answer,antwort,en,antworte,h(ge),antwortet,[nom]).       %DE
verbPrefix_(answerACC,antwort,be,[nom,acc]).                  %DE
%verb(apologise,apologise,[nom,acc]).
%verb(appear,appear,[nom,verbal(to)]).
verb(apply,bewerb,en,bewirb,bewarb,h,beworben,[nom,acc]).     %DE
%verb(approach,approach,[nom,acc]).
%verb(appreciate,appreciate,[nom,acc]).
%verb(approve,approve,[nom,acc]).
%verb(argue,argue,[nom,(acc)]).
verb(arrange,vereinbar,en,vereinbar,h,vereinbart,
     [nom,acc]).                                              %DE
%verb(arrest,arrest,[nom,acc]).
%verb(arrive,arrive,[nom,?(to(acc))]).
verb(ask,bitt,en,bitte,bat,h(ge),beten,[nom,acc]).            %DE
%verb(assemble,assemble,[nom,acc]).
%verb(attach,attach,[nom,acc]).
%verb(attack,attack,[nom,acc]).
verb(attempt,versuch,en,versuch,h,versucht,
     [nom,?(verbal(to))]).                                    %DE
%verb(attend,attend,[nom,acc]).
%verb(attract,attract,[nom,acc]).
%verb(average,average,[nom,acc]).
%verb(avoid,avoid,[nom,acc]).
%verb(awake,awake,awakes,awoke,awaken,[nom,acc]).
%
% BBBBB
%
%verb(babysit,babysit,[nom,?(acc)]).
verb(bake,back,en,b�ck,h(ge),backen,[nom,acc]).               %DE
%verb(barbecue,barbecue,[nom]).
%verb(base,base,[nom,acc]). %HU:alapoz, alap�t
verb(bath,bad,en,bade,h(ge),badet,[nom]).                     %DE
verb(be,sei,n,'','',i(ge),wesen,
     [nom,?(nom)];[nom,clause]).                              %DE
verb(beCalled,hei�,en,hei�,hie�,h(ge),hei�en,[nom,acc]).      %DE
verb(beHappy,freu,en,freu,h(ge),freut,
     [nom,refl,�ber(acc)]).                                   %DE
verb(beat,schlag,en,schl�g,schlug,h(ge),schlagen,
     [nom,acc]).                                              %DE
verbPrefix_(propose,schlag,vor,[nom,acc]).                    %DE
verb(become,werd,en,wird,wurde,i(ge),worden,[nom,nom]).       %DE
verb(begin,beginn,en,beginn,begann,h,begonnen,
     [nom,acc];[nom,verbal(to)]).                             %DE
%verb(behave,behave,[nom,?(acc)]).
verb(believe,glaub,en,glaub,h(ge),glaubt,[nom,?(dat)];
     [nom,clause(that)]).                                     %DE
verb(belong,geh�r,en,geh�r,h,geh�rt,[nom,zu(dat)]).           %DE
verb(bend,bieg,en,bieg,bog,i(ge),bogen,[nom]).                %DE
verbPrefix_(turn,bieg,ab,[nom]).                              %DE
%verb(bite,bite,bites,bit,bitten,[nom,acc]). %HU: harap
%verb(blame,blame,[nom,acc]).
%verb(bleed,bleed,[nom]).
%verb(blow,blow,blows,blew,blown,[nom,?(acc)]). %HU: f�j
verb(blog,blogg,en,blogg,bloggte,h(ge),bloggt,[nom,?(acc)]).  %DE
%verb(board,board,[nom,?(acc)]).
%verb(boil,boil,[nom,?(acc)]). %HU: f�z/f�
%verb(bomb,bomb,[nom,acc]).
%verb(bond,bond,[nom,acc]). %HU:k�t, ragaszt
verb(book,buch,en,buch,h(ge),bucht,[nom,acc]).                %DE
%verb(bear,bear,bears,bore,born,[nom,acc]).
%verb(borrow,borrow,[nom,acc]).
%verb(bother,bother,[nom,acc]).
%verb(brake,brake,[nom,?(acc)]).
%verb(break,break,breaks,broke,broken,[nom,acc]).
%verbPrefix_(breakDown,break,down,[nom]).
%verbPrefix_(breakIn,break,in,[nom,?(acc)]).
%verbPrefix_(breakUp,break,up,[nom]).
verb(breakfastEssen,fr�hst�ck,en,fr�hst�ck,
      h(ge),fr�hst�ckt,[nom,?(acc)]).                         %DE
%verb(breathe,breathe,[nom]).
verb(bring,bring,en,bring,h(ge),bracht,[nom,acc]).            %DE
verbPrefix_(bringWith,bring,mit,[nom,acc]).                   %DE
verbPrefix_(bringAway,bring,weg,[nom,acc]).                   %DE
verbPrefix_(bringOut,bring,raus,[nom,acc]).                   %DE
%verbPrefix_(bringUp,bring,up,[nom,acc]).
%verb(brush,brush,[nom,acc]).
verb(build,bau,en,bau,h(ge),baut,[nom,acc]).                  %DE
%verb(burn,burn,[nom,acc]).
%verb(burn,burn,burnt,[nom,acc]).
%verb(burst,burst,[nom,acc]).
%verb(bury,bury,[nom,acc]).
verb(buy,kauf,en,kauf,h(ge),kauft,[nom,acc]).                 %DE
verbPrefix_(shop,kauf,ein,[nom,acc]).                         %DE
%
% CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
%
verb(call,ruf,en,ruf,rief,h(ge),rufen,[nom,?(acc)]).          %DE
verbPrefix_(callPhone,ruf,an,[nom,acc]).                      %DE
%verbPrefix_(callIn,call,in,[nom,?(acc)]).
verb(callName,nenn,nenn,nannte,h(ge),nannt,
     [nom,?(acc)]).                                           %DE
verb(camp,camp,en,camp,campte,h(ge),campt,[nom]).             %DE
verb(calculate,rechn,en,rechne,h(ge),rechnet,[nom,acc]).      %DE
verb(can,k�nn,en,'','',h(ge),konnt,[nom,?(acc)]).             %DE
verb(cancel,k�ndig,en,k�ndig,k�ndigte,h(ge),k�ndigt,
     [nom,acc]).                                              %DE
verbPrefix_(announce,k�ndig,an,[nom,verbal(ing)]).            %DE
%verb(care,care,[nom,?(for(acc))]).
verb(carry,trag,en,tr�g,trug,h(ge),tragen,[nom,acc]).         %DE
verbPrefix_(transfer,trag,�ber,[nom,acc]).                    %DE
verbPrefix_(deliver,trag,aus,[nom,acc]).                      %DE
verbPrefix_(enter,trag,ein,[nom,acc]).                        %DE
%verbPrefix_(carryOut,carry,out,[nom,acc]).
%verb(cash,cash,[nom,acc]).
verb(catch,fang,en,f�ng,fing,h(ge),fangen,[nom,acc]).         %DE
verbPrefix_(begin,fang,an,[nom,acc];[nom,verbal(to)]).        %DE
verb(caughtCold,erk�lt,en,erk�lte,h,erk�ltet,
     [nom,acc]).                                              %DE
%verb(cause,cause,[nom,acc]). %HU:okoz
verb(celebrate,feier,n,feier,h(ge),feiert,[nom,acc]).         %DE
%verb(challenge,challenge,[nom,acc]).
verb(change,�nder,n,�nder,h(ge),�ndert,[nom,?(acc)]).         %DE
%verb(charge,charge,[nom,?(acc)]).
%verb(chase,chase,[nom,acc]).
verb(chat,chatt,en,chatt,h(ge),chattet,[nom]).                %DE
%verb(cheat,cheat,[nom,?(acc)]).
verb(check,check,en,check,h(ge),checkt,[nom,acc]).            %DE
verb(check,pr�f,en,pr�f,h(ge),pr�ft,[nom,acc]).               %DE
verbPrefix_(checkIn,check,in,[nom,?(to(acc))]).
verbPrefix_(checkOut,check,out,[nom,?(of(acc))]).
verb(choose,w�hl,en,w�hl,h(ge),w�hlt,[nom,acc]).              %DE
verbImpStem(w�hl,sg/2,w�hle).
verbImpStem(w�hl,pl/2,w�hl).
%verb(clap,clap,[nom,?(acc)]).
verb(clean,putz,en,putz,h(ge),putzt,[nom,acc]).               %DE
%verb(clear,clear,[nom,acc]).
verb(click,klick,en,klick,h(ge),klickt,[nom]).                %DE
verbPrefix_(click,klick,an,[nom,acc]).                        %DE
verb(climb,steig,en,steig,stieg,i(ge),stiegen,[nom,acc]).     %DE
verbPrefix_(changeVehicle,steig,um,[nom,?(acc)]).             %DE
verbPrefix_(getOut,steig,aus,[nom,acc]).                      %DE
verbPrefix_(getIn,steig,ein,[nom,acc]).                       %DE
verb(close,schlie�,en,schlie�,schloss,h(ge),schlossen,
     [nom,acc]).                                              %DE
verbPrefix_(closeDown,schlie�,ab,[nom,acc]).                  %DE
verb(collect,sammel,n,sammel,h(ge),sammelt,[nom,acc]).        %DE
%verb(colour,colour,[nom,acc]).
%verb(comb,comb,[nom,acc]).
verb(come,komm,en,komm,kam,i(ge),kommen,[nom]).               %DE
verbPrefix_(arrive,komm,an,[nom]).                            %DE
verbPrefix_(comeBack,komm,zur�ck,[nom]).                      %DE
verbPrefix_(comeHome,komm,heim,[nom]).                        %DE
verbPrefix_(comeIn,komm,rein,[nom]).                          %DE
verbPrefix_(comeOut,komm,raus,[nom]).                         %DE
%verb(command,command,[nom]).
%verb(comment,comment,[nom,?(acc)]).
%verb(communicate,communicate,[nom,with(acc)]).
verb(compare,vergleich,en,vergleich,verglich,h,verglichen,
     ([nom,acc,with(acc)];[nom,acc,to(acc)])).                %DE
%verb(compete,compete,[nom,?(with(acc))]).
verb(complain,beschwer,en,beschwer,h,beschwert,
     [nom,?(of(acc))]).                                       %DE
%verb(complete,complete,[nom,acc]).
%verb(compress,compress,[nom,acc]).
%verb(concentrate,concentrate,[nom,?(acc),?(on(acc))]).
verb(confirm,best�tig,en,best�tig,h,best�tigt,
     [nom,acc];[nom,clause(that)]).                           %DE
verb(congratulate,gratulier,en,gratulier,h,gratuliert,
     [nom,dat]).                                              %DE
%verb(connect,connect,[nom,acc]).
%verb(consider,consider,[nom,acc];[nom,verbal(to)];[nom,clause(that)]).
%verb(consist,consist,[nom,of(acc)]).
%verb(contact,contact,[nom,acc]).
%verb(contain,contain,[nom,acc]).
verb(control,kontrollier,en,kontrollier,h,kontrolliert,
     [nom,acc]).                                              %DE
%verb(convince,convince,[nom,acc,?(verbal(to))];[nom,acc,clause(that)]).
verb(cook,koch,en,koch,h(ge),kocht,[nom,?(acc)]).             %DE
%verb(copy,copy,[nom,acc]).
%verb(correct,correct,[nom,acc]).
verb(cost,kost,en,koste,h(ge),kostet,[nom,acc]).              %DE
verb(cough,hust,en,huste,h(ge),hustet,cough,[nom]).           %DE
verb(count,zahl,en,zahl,h(ge),zahlen,[nom,acc]).              %DE
%verb(cover,cover,[nom,acc]).
%verb(crash,crash,[nom]).
%%verb(crack,crack,[nom,acc]).
verb(create,schaff,en,schaff,schuf,h(ge),schaffen,
     [nom,acc]).                                              %DE
verb(cross,kreuz,en,kreuz,kreuzte,h(ge),kreuzt,
      [nom,?(acc)]).                                          %DE
verbPrefix_(crossmark,kreuz,an,[nom,acc]).                    %DE
%verbPrefix_(crossOut,cross,out,[nom,acc]).
%verb(crush,crush,[nom,acc]). %HU: �sszenyom, -pr�sel
verb(cry,wein,en,wein,h(ge),weint,[nom]).                     %DE
%verb(cure,cure,[nom,acc]).
verb(cut,schneid,en,schneide,h(ge),schnitten,[nom,acc]).      %DE
%verbPrefix_(cutUp,cut,up,[nom,acc]).
%verb(cycle,cycle,[nom]).
%
% DDDDDDDDDDDDDDDDDDDDDDDDD
%
%verb(damage,damage,[nom,acc]).
verb(dance,tanz,en,tanz,h(ge),tanzt,[nom,?(acc)]).            %DE
%verb(date,date,[nom,acc]).
%verb(deal,deal,[nom,with(acc)]).
verb(decide,entscheid,en,entscheid,entschied,h,entschieden,
     [nom,refl,?(verbal(to))]).                               %DE
%verb(declare,declare,[nom,acc,?(acc)];
%     [nom,?(verbal(to))];[nom,?(clause(that))]).
%verb(decorate,decorate,[nom,?(acc)]).
%verb(decrease,decrease,[nom,acc]).
%verb(defeat,defeat,[nom,?(acc)]).
%verb(defend,defend,[nom,acc]).
%verb(deflate,deflate,[nom,acc]).
%verb(delay,delay,[nom,acc]).
%verb(delete,delete,[nom,acc]).
verb(deliver,liefer,n,liefer,h(ge),liefert,[nom,acc]).        %DE
%verb(demand,demand,[nom,acc];[nom,?(verbal(to))]).
%verb(depart,depart,[nom,?(from(acc))]).
%verb(depend,depend,[nom,on(acc)]).
%verbPrefix_(dependOn,depend,on,[nom,clause(wh)]).
%verb(desire,desire,[nom,?(verbal(to))]).
%verb(deserve,deserve,[nom,acc]).
%verb(design,design,[nom,acc]).
%verb(destroy,destroy,[nom,acc]).
%verb(develop,develop,[nom,acc]).
%verb(dial,dial,[nom,acc]).
verb(die,sterb,en,stirb,starb,i(ge),storben,
     [nom,?(an(dat))]).                                       %DE
%verb(dig,dig,[nom,?(acc)]).
%verb(digest,digest,[nom,acc]).
%verb(direct,direct,[nom,acc]).
%verb(disagree,disagree,([nom,clause(that)];
%                  [nom,verbal(to)];
%                  [nom,?(with(acc))])).
%verb(disappear,disappear,[nom]).
%verb(disappoint,disappoint,[nom,acc]).
%verb(discard,discard,[nom,acc]).
%verb(discover,discover,[nom,acc]).
verb(discuss,diskutier,en,diskutiert,h,diskutiert,
     [nom,acc]). %DE
%verb(dislike,dislike,[nom,acc]).
%verb(dispatch,dispatch,[nom,acc]).
%verb(display,display,[nom,acc]).
verb(disturb,st�r,en,st�r,h(ge),st�rt,[nom,acc]).             %DE
%verb(dive,dive,[nom]).
%verb(divide,divide,[nom,acc]).
%verb(divorce,divorce,[nom,from(acc)]).
verb(do,tu,n,tu,ta,h(ge),tan,[nom,?(acc),?(to(acc))]).        %DE
verbPrefix_(doPain,tu,weh,[nom]).                             %DE
verbPrefix_(doSorry,tu,leid,[nom]).                           %DE
verb(donate,schenk,en,schenkt,h(ge),schenkt,[nom,acc]).       %DE
%verb(doubt,doubt,([nom,clause];
%                  [nom,clause(that)];
%                  [nom,clause(if)])).
%verb(download,download,[nom,acc]).
%verb(drag,drag,[nom,acc]).
%verb(drain,drain,[nom,acc]).
verb(draw,zeichn,en,zeichne,h(ge),zeichnet,[nom,acc]).        %DE
verb(dream,tr�um,en,tr�um,h(ge),tr�umt,[nom,acc]).            %DE
%verb(dress,dress,[nom,acc]).
verb(drink,trink,en,trink,trank,h(ge),trunken,[nom,acc]).     %DE
verb(drive,fahr,en,f�hr,fuhr,i(ge),fahren,[nom,?(acc)]).      %DE
verbPrefix_(leave,fahr,ab,[nom]).                             %DE
verbPrefix_(driveAway,fahr,weg,[nom]).                        %DE
verbPrefix_(driveBack,fahr,zur�ck,[nom]).                     %DE
%verb(drop,drop,[nom,acc]).
%verb(dry,dry,[nom,acc]).
verb(dwell,wohn,en,wohn,h(ge),wohnt,[nom,?(in(dat))]).        %DE
%
% EEEEEEEEEEEEEEEEEEEEEEEEEEE
%
verb(earn,verdien,en,verdien,h,verdient,[nom,acc]).           %DE
verb(eat,ess,en,iss,a�,h(ge),gessen,[nom,?(acc)]).            %DE
%verb(educate,educate,[nom,acc]).
%verb(email,email,[nom,acc]).
%verb(employ,employ,[nom,acc]).
%verb(effect,effect,[nom,acc]).
%verb(elaborate,elaborate,[nom,acc]).
%verb(encourage,encourage,[nom,acc]).
verb(end,end,en,ende,h(ge),endet,[nom,?(acc)]).               %DE
%verbPrefix_(endUp,end,up,[nom,verbal(ing)]).
%verb(enjoy,enjoy,([nom,acc];[nom,verbal(ing)])).
verb(entertain,unterhalt,en,unterh�l,unterhielt,
     h,unterhalten,[nom,acc]).                                %DE
%verb(erase,erase,[nom,acc]).
%verb(escape,escape,[nom,?(acc),?(from(acc))]).
%verb(estimate,estimate,[nom,acc]).
%verb(exaggerate,exaggerate,[nom,acc]).
verb(examine,untersuch,en,untersuch,h,untersucht,
     [nom,acc]).                                              %DE
verb(exchange,tausch,en,tausch,h(ge),tauscht,[nom,acc]).      %DE
verbPrefix_(exchangeMutually,tausch,aus,[nom,acc]).           %DE
%verb(excite,excite,[nom,acc]).
verb(excuse,entschuldig,en,entschuldig,h,entschuldigt,
     [nom,acc]).                                              %DE
%verb(exercise,exercise,[nom,acc]).
%verb(exist,exist,[nom]).
%verb(expect,expect,[nom,?(acc),?(verbal(to))]).
verb(explain,erkl�r,en,erkl�r,h,erkl�rt,[nom,acc]).
%verb(explode,explode,[nom]).
%verb(explore,explore,[nom,acc]).
verb(evacuate,r�um,en,r�um,h(ge),r�umt,[nom,acc]).            %DE
verbPrefix_(cleanUp,r�um,auf,[nom,acc]).                      %DE
%
% FFFFFFFFFFFFFF
%
%verb(fail,fail,[nom]).
%verb(faint,faint,[nom]).
verb(fall,fall,en,f�ll,fiel,i(ge),fallen,[nom]).              %DE
%verb(fancy,fancy,[nom]).
%verb(farm,farm,[nom,acc]).
%verb(fasten,fasten,[nom]).
verb(fax,fax,en,fax,faxte,h(ge),faxt,[nom,acc,?(acc)]).       %DE
%verb(feed,feed,[nom,acc]).
verb(feel,f�hl,en,f�hl,h(ge),f�hlt,[nom,?(acc)]).             %DE
verb(fetch,hol,en,hol,holte,h(ge),holt,[nom,acc]).            %DE
verbPrefix_(pickUp,hol,ab,[nom,acc]).                         %DE
verbPrefix_(repeat,hol,wieder,[nom,acc]).                     %DE
%verb(fight,fight,fought,[nom,acc]).
verb(fill,f�ll,en,f�ll,h(ge),f�llt,[nom,acc]).                %DE
verbPrefix_(fillIn,f�ll,aus,[nom,acc]).                       %DE
%verbPrefix_(fillUp,fill,up,[nom,acc,?(with(acc))]).
%verb(film,film,[nom,acc]).
verb(find,find,en,finde,fand,h(ge),funden,[nom,acc]) .        %DE
verbPrefix_(takePlace,find,statt,[nom,clause(wh)]).           %DE
verb(finish,beend,en,beendet,h,beendet,([nom,?(acc)];
                    [nom,verbal(ing)])).                      %DE
%verb(fire,fire,[nom,acc]).
%verb(fish,fish,[nom]).
verb(fit,pass,en,pass,h(ge),passt,[nom,?(acc)]).              %DE
verbPrefix_(listen,pass,auf,[nom,with(acc)]).                 %DE
verbPrefix_(lookAfter,pass,auf,[nom,with(acc)]).              %DE
%verb(fix,fix,[nom,acc]).
verb(flap,klapp,en,klapp,h(ge),klappt,[nom]).                 %DE
%verb(float,float,[nom]).
%verb(flood,flood,[nom,acc]).
%verb(flow,flow,[nom,acc]).
verb(fly,flieg,en,flieg,flog,i(ge),flogen,[nom]).             %DE
verbPrefix_(flyAway,flieg,ab,[nom]).                          %DE
%verb(fold,fold,[nom,acc]).
%verb(follow,follow,[nom,acc]).
verb(forbid,verbiet,en,verbiete,verbot,h,verboten,
     [nom,acc];[nom,verbal(to)]).                             %DE
verb(forget,vergess,en,vergiss,verga�,h,vergessen,
     [nom,acc];[nom,verbal(to)]).                             %DE
verbImpStem(vergess,sg/2,vergiss).
verbImpStem(vergess,pl/2,vergesst).
%verb(forgive,forgive,[nom,acc]).
%verb(forward,forward,[nom,acc]). %HU:tov�bb�t
%verb(freeze,freeze,freezes,froze,frozen,[nom]).
%verb(frighten,frighten,[nom,acc]).
verb(fry,brat,en,br�,briet,h(ge),braten,[nom,acc]).           %DE
verb(function,funktionier,en,funktionier,funktioniert,
       h,funktioniert,[nom]).                                 %DE
%
%
%
%
%
%
% GGGGGGGGGGGGGGGGGGGGGGGGGGGGG
%
%verb(gain,gain,[nom,acc]).
%verb(gather,gather,[nom,acc]).
verb(get,bekomm,en, bekomm, h,bekommen,[nom,?(acc)]).         %DE
%verbPrefix_(getAlong,get,along,[nom,with(acc)]).
%verbPrefix_(getBack,get,back,[nom,?(to(acc))]).
%verbPrefix_(getDown,get,down,[nom,?(acc)]).
%verbPrefix_(getIn,get,in,[nom]).
%verbPrefix_(getOff,get,off,[nom,?(acc)]).
%verbPrefix_(getOn,get,on,[nom,?(with(acc))]).
%verbPrefix_(getRidOf,get,[rid,of],[nom,?(acc)]).
%verbPrefix_(getUp,get,up,[nom]).
verb(give,geb,en,gib,gab,g�b,h(ge),geben,[nom,dat,acc];
                                 [nom,acc,?(f�r(acc))]).      %DE
verbImpStem(geb,sg/2,gib).
verbImpStem(geb,pl/2,geb).
verbPrefix_(deliver,geb,ab,[nom,acc,bei(dat)]).               %DE
verbPrefix_(specify,geb,an,[nom,acc]).                        %DE
verbPrefix_(output,geb,aus,[nom,acc,?(to(acc))]).             %DE
verbPrefix_(giveBack,geb,zur�ck,[nom,acc,?(to(acc))]).        %DE
%verbPrefix_(giveOut,give,out,[nom,acc]).
%verbPrefix_(giveUp,give,up,[nom,verbal(ing)]).
%verbPrefix_(giveWay,give,way,[nom,to(acc)]).
%verb(glance,glance,[nom,acc]).
verb(go,geh,en,geh,ging,i(ge),gangen,                         %DE
      [nom,?(acc)];[nom,dat];[nom,to(acc)];[nom,verbal(to)]).
verbPrefix_(goHome,geh,heim,[nom]).                           %DE
verbPrefix_(goOut,geh,aus,[nom,acc]).                         %DE
verbPrefix_(goAway,geh,weg,[nom,acc]).                        %DE
verbPrefix_(goBack,geh,zur�ck,[nom,acc]).                     %DE
%verbPrefix_(goFor,go,for,[nom,acc]).
%verbPrefix_(goOff,go,off,[nom]).
verb(google,googl,en,googel,googelte,h(ge),googelt,
      [nom]).                                                 %DE
%verbPrefix_(goOn,go,on,[nom]).
%verbPrefix_(goOut,go,out,[nom,?(acc)]).
%verbPrefix_(goWith,go,with,[nom,acc]).
%verbPrefix_(goTogether,go,together,[nom]).
%verb(grab,grab,[nom,acc]).
%verb(grip,grip,[nom,acc]). %HU:megragad
%verb(greet,greet,[nom,acc]).
verb(grill,grill,en,grill,h(ge),grillt,[nom,acc]).            %DE
verb(grow,wachs,en,w�chst,wuchs,i(ge),wachsen,[nom,?(acc)]).  %DE
verbPrefix_(growUp,wachs,auf,[nom]).                          %DE
verb(guess,rat,en,r�,riet,h(ge),raten,[nom,acc]).             %DE
%verb(guide,guide,[nom,acc]).
%
% HHHHHHHHHHHHHHHHHHHHHHH
%
%verb(hand,hand,[nom,acc]).
%verbPrefix_(handIn,hand,in,[nom,acc]).
%verbPrefix_(handOut,hand,out,[nom,acc]).
verb(makeHang,h�ng,en,hang,h(ge),hangen,[nom,?(acc)]).        %DE
verb(hang,h�ng,en,h�ng,hing,h(ge),h�ngt,[nom,?(acc)]).        %DE
verbPrefix_(depend,h�ng,ab,[nom,von(dat)]).                   %DE
%verbPrefix_(hangOut,hang,out,[nom]).
%verbPrefix_(hangUp,hang,up,[nom,?(acc)]).
verb(happen,passier,en,passier,h,passiert,[nom,?(acc)]).      %DE
%verb(hate,hate,[nom,acc];[nom,verbal(to)]).
verb(have,hab,en,ha,hatte,h(ge),habt,
      [nom,acc];[nom,verbal(to)]).                            %DE
verbPrefix_(wear,hab,an,[nom,?(acc)]).                        %DE
verb(hear,h�r,en,h�r,h(ge),h�rt,[nom,acc]).                   %DE
verbPrefix_(cease,h�r,auf,[nom,?(verbal(to))]).               %DE
verbPrefix_(listenTo,h�r,zu,[nom,?(acc)]).                    %DE
verb(help,helf,en,hilf,half,h(ge),holfen,
     [nom,?(dat),?(verbal(to))]).                             %DE
verbPrefix_(helpOn,helf,weiter,[nom,?(acc)]).                 %DE
%verb(hide,hide,[nom,acc]).
verb(hike,wander,n,wander,h(ge),wandert,[nom]).               %DE
%verb(hire,hire,[nom,?(acc)]).
%verb(hit,hit,hit,[nom,acc]).
%verb(hitchhike,hitchhike,[nom]).
verb(hold,halt,en,h�l,hielt,h(ge),halten,[nom,?(acc)]).       %DE
%verbPrefix_(holdUp,hold,up,[nom,?(acc)]).
verb(honor,ehr,en,ehr,h(ge),ehrt,[nom,acc]).                  %DE
verb(hope,hoff,en,hoff,h(ge),hofft,([nom,?(verbal(to))];
                [nom,clause];[nom,clause(that)])).            %DE
%verb(hug,hug,[nom,acc]).
%verb(hunt,hunt,[nom,acc]).
verb(hurry,beeil,en,beeil,h,beeilt,[nom]).                    %DE
%verb(hurt,hurt,[nom,?(acc)]).
%
% IIIIIIIIIIIIIIIIIIIII
%
%verb(identify,identify,[nom,acc]).
%verb(ignore,ignore,[nom,acc]).
%verb(imagine,imagine,[nom,?(acc),?(clause(that))]).
%verb(impact,impact,[nom]).
%verb(imply,imply,[nom,acc]).
%verb(imprison,imprison,[nom,acc]).
verb(improve,verbesser,n,verbesser,h,verbessert,[nom,acc]).   %DE
%verb(include,include,[nom,acc]).
%verb(increase,increase,[nom,?(acc),?(by(acc))]). %HU:n�vekszik, n�vel
verb(inform,informier,en,informier,h,informiert,
     [nom,acc]).                                              %DE
verb(injure,verletz,en,verletz,h,verletzt,[nom,?(acc)]).      %DE
%verb(inquire,inquire,[nom,about(acc)]).
%verb(insist,insist,[nom,on(acc)];[nom,clause];[nom,clause(that)]).
%verb(install,install,[nom,acc]).
%verb(intend,intend,[nom,verbal(to)]).
verb(interest,interessier,en,interessier,h,interessiert,
     [nom,refl,f�r(acc)]).                                    %DE
%verb(interpret,interpret,[nom,acc]).
%verb(interrupt,interrupt,[nom,acc]).
%verb(interview,interview,[nom,acc]).
%verb(invent,invent,[nom,acc]).
%verb(involve,involve,[nom,acc]).
%verb(iron,iron,[nom,acc]).
%
% JJJJJJJJJJJJJJJJJJJJJJJJ
%
verb(job,jobb,en,jobb,jobbte,h(ge),jobbt,[nom]).              %DE
verb(jog,jogg,en,jogg,joggte,i(ge),joggt,[nom]).              %DE
%verb(join,join,[nom,?(acc)]).
%verb(joke,joke,[nom]).
%verb(judge,judge,[nom,acc]). %HU:�t�l
%verb(jump,jump,[nom]).
verb(justify,begr�nd,en,begr�nde,h,begr�ndet,[nom,acc]).      %DE
%
% KKKKKKKKKKKKKKKKKKKKKKKKKKKKK
%
%verb(keep,keep,kept,[nom,?(acc)]).
%verbPrefix_(keepIn,keep,in,[nom,?(acc)]).
%verbPrefix_(keepOn,keep,on,[nom,verbal(ing)]).
%verbPrefix_(keepUp,keep,up,[nom,acc]).
%verb(kick,kick,[nom,acc]).
%verb(kill,kill,[nom,acc]).
%verb(kiss,kiss,[nom,acc]).
%verb(knit,knit,[nom,acc]).
%verb(knock,knock,[nom,acc]).
%verbPrefix_(knockDown,knock,down,[nom,acc]).
verb(know,wiss,en,wei�,wusste,w�sste,h(ge),wusst,
     [nom,acc];[nom,clause(that)];[nom,clause(if)]).          %DE
verb(know,kenn,en,kenn,kannte,h(ge),kannt,[nom,?(acc)]).      %DE
verbPrefix_(acknowledge,kenn,aner,[nom,acc]).                 %DE

%
% LLLLLLLLLLLLLLLLLLL
%
%verb(land,land,[nom]).
verb(last,dauer,n,dauer,h(ge),dauert,[nom]).                  %DE
verb(laugh,lach,en,lach,h(ge),lacht,[nom,?(at(acc))]).        %DE
verb(lay,leg,en,leg,h(ge),legt,[nom,?(acc)]).                 %DE
%verb(lead,lead,[nom,acc]).
verb(lean,lehn,en,lehn,lehnte,h(ge),lehnt,[nom,acc]).         %DE
verbPrefix_(reject,lehn,ab,[nom,acc]).                        %DE
verb(learn,lern,en,lernt,h(ge),lernt,
     ([nom,acc];[nom,verbal(to)];
     [nom,clause(that)];[nom,verbal(wh)])).                   %DE
verbPrefix_(getKnow,lern,kennen,[nom,acc]).                   %DE

%verb(leave,leave,left,[nom,?(acc)]).
verb(lend,leih,en,leih,lieh,h(ge),liehen,
     [nom,acc,?(acc)];[nom,acc,to(acc)]).                     %DE
verb(let,lass,en,l�ss,lie�,h(ge),lassen,
     [nom,acc,?(verbal)]).                                    %DE
verbPrefix_(leave,lass,ver,[nom,acc]).                        %DE
verb(lie,lieg,en,lieg,lag,i(ge),legen,[nom,?(acc)]).          %DE
verb(tellLie,l�g,en,l�g,lag,h(ge),logen,[nom,acc]).           %DE
%verb(lift,lift,[nom,acc]).
%verb(light,light,[nom,acc]).
verb(like,gefall,en,gef�ll,gefiel,h(ge),fallen,([nom,dat];    %DE
                [nom,verbal(ing)];
                [nom,verbal(to)];
                [nom,clause(wh)])).
%verb(light,light,[nom,?(acc)]).
%verb(listen,listen,[nom,?(to(acc))]).
verb(live,leb,en,leb,h(ge),lebt,[nom]).                       %DE
verb(load,lad,en,l�d,h(ge),laden,[nom,acc]).                  %DE
verbPrefix_(invite,lad,ein,[nom,acc]).                        %DE
verbPrefix_(download,lad,unter,[nom,acc]).                    %DE
verbPrefix_(download,lad,herunter,[nom,acc]).                 %DE
%verb(locate,locate,[nom,acc]).
%verb(lock,lock,[nom,acc]).
%verb(look,look,[nom,?(acc)]).
%verbPrefix_(lookAfter,look,after,[nom,acc]). %HU:gondoskodik
%verbPrefix_(lookFor,look,for,[nom,acc]). %HU:keres
%verbPrefix_(lookForward,look,forward,[nom,to(acc)]). %HU:keres
%verbPrefix_(lookLike,look,like,[nom,acc]). %HU:keres
%verbPrefix_(lookOut,look,out,[nom,of(acc)]). %HU: keres, kin�z, kir�
%verbPrefix_(lookUp,look,up,[nom,acc]). %HU: keres, kin�z, kir�
verb(lose,verlier,en,verlier,verlor,h,verloren,
     [nom,acc]).                                              %DE
verb(love,lieb,en,lieb,h(ge),liebt,
     [nom,acc];[nom,verbal(ing)]).                            %DE
verbPrefix_(loveFall,lieb,ver,[nom,acc]).                     %DE
%
% MMMMMMMMMMMMMMMMMMMMMM
%
verb(mail,mail,en,mail,mailte,h(ge),mailt,[nom,acc]).         %DE
verb(make,mach,en,mach,h(ge),macht,[nom,acc,?(clause)]).      %DE
verbPrefix_(agree,mach,ab,[nom,clause(that)]).                %DE
verbPrefix_(open,mach,auf,[nom,clause(that)]).                %DE
verbPrefix_(turnOff,mach,aus,[nom,acc]).                      %DE
verbPrefix_(putAway,mach,weg,[nom,acc]).                      %DE
verbPrefix_(continue,mach,weiter,[nom,acc]).                  %DE
verbPrefix_(shut,mach,zu,[nom,acc]).                          %DE
verbPrefix_(participate,mach,mit,[nom,acc]).                  %DE

%verb(manage,manage,[nom,acc];[nom,verbal(to)]).
%verb(map,map,[nom,?(acc)]).
verb(marry,heirat,en,heirate,h(ge),heiratet,[nom,?(acc)]).    %DE
%verb(match,match,[nom,acc]).
%verb(matter,matter,[nom,?(acc)]). %it doesn't matter
verb(may,m�g,en,'','',h(ge),mocht,[nom,acc]).                 %DE
verb(mean,bedeut,en,bedeute,h,bedeutet,[nom,acc]).            %DE
verb(mean,mein,en,mein,h(ge),meint,[nom,acc]).                %DE
%verb(measure,measure,[nom,acc]).
verb(meet,treff,en,triff,traf,h(ge),troffen,[nom,?(acc)]).    %DE
%verb(mend,mend,[nom,?(acc)]). %HU:jav�t
%verb(mention,mention,[nom,acc]).
%verb(melt,melt,[nom]).
%verb(mind,mind,([nom,?(acc)];[nom,verbal(ing)];
%                [nom,clause(if)];
%                [nom,clause(wh)])).
verb(miss,verpass,en,verpass,h,verpasst,[nom,?(acc)]).        %DE
verb(missing,fehl,en,fehl,i(ge),fehlt,[nom]).                 %DE
%verb(mix,mix,[nom,acc]).
%verb(mount,mount,[nom]).
%verb(move,move,[nom,?(acc),?(to(acc))]). %HU: mozog, mozgat
%verb(multiply,multiply,[nom,?(acc),?(by(acc))]).
verb(must,m�ss,en,'',musste,h(ge),musst,
     [nom,?(acc),?(to(acc))]).                                %DE
%
% NNNNNNNNNNNNNNNNNNNNNN
%
%verb(name,name,[nom,acc]).
verb(need,brauch,en,brauch,h(ge),braucht,([nom,acc];
               [nom,verbal(to)])).                            %DE
%verb(nod,nod,[nom]).
verb(note,notier,en,notier,h,notiert,[nom,?(acc)]).           %DE
verb(notice,merk,en,merk,h(ge),merkt,[nom,acc]).              %DE
%
% OOOOOOOOOOOOOOOOOOOOOOOOO
%
%verb(obey,obey,[nom,acc]).
verb(obtain,krieg,en,krieg,h(ge),kriegt,[nom,acc]).           %DE
%verb(occur,occur,[nom,acc]).
verb(offer,biet,en,biete,bot,h(ge),boten,
     [nom,?(acc),?(verbal(to))]).                             %DE
verbPrefix_(offer,biet,an,[nom,acc]).
verb(open,�ffn,en,�ffne,h(ge),�ffnet,[nom,acc]).              %DE
%verb(operate,operate,[nom,acc]).
verb(order,bestell,en,bestell,h,bestellen,[nom,acc]).         %DE
%%%verbPrefix_(assign,ordnen,zu,[nom,acc]).                   %DE
verb(organise,ordn,en,ordne,ordnete,h(ge),ordnet,[nom,acc]).  %DE
verb(organise,organisier,en,organisiert,h,organisiert,
     [nom,acc]).                                              %DE
%verb(oversee,oversee,[nom,acc]).
%verb(owe,owe,[nom,acc]).
%verb(own,own,[nom,acc]).
%
% PPPPPPPPPPPPPPPPPP
%
verb(pack,pack,en,pack,h(ge),packt,[nom,acc]).                %DE
verbPrefix_(unpack,pack,aus,[nom,acc]).                       %DE
verbPrefix_(packIn,pack,ein,[nom,acc]).                       %DE
verb(paint,mal,en,mal,h(ge),malt,[nom,?(acc)]).               %DE
verb(park,park,en,park,h(ge),parkt,[nom,?(acc)]).             %DE
%verb(pause,pause,[nom]).
verb(pay,bezahl,en,bezahl,h,bezahlen,[nom,acc]).              %DE
verb(payAttention,acht,en,achte,achtete,h(ge),achtet,
     [nom,auf(acc)]).                                         %DE
%verb(peel,peel,[nom,acc]).
%verb(perform,perform,[nom]).
%verb(permit,permit,[nom,acc]).
%verb(persuade,persude,[nom,acc,?(verbal(to))];
%     [nom,acc,?(clause(that))]).
%verb(phone,phone,[nom,acc]).
%verb(pick,pick,[nom,acc]). %pick.up
%verbPrefix_(pickUp,pick,up,[nom,acc]).
%verb(pin,pin,[nom,acc]). %HU: t�z, szegez
verb(plan,plan,en,plan,h(ge),plant,[nom,verbal(to)]).         %DE
%verb(plant,plant,[nom,acc]).
verb(play,spiel,en,spiel,h(ge),spielt,[nom,?(acc)]).          %DE
%verb(please,please,[nom,acc]).
%verb(point,point,[nom,at(acc)]).
%verb(post,post,[nom,acc]).
verb(postpone,verschieb,en,verschieb,verschob,
     h,verschoben,[nom,acc]).                                 %DE
%verb(pour,pour,[nom,acc]).
%verb(polish,polish,[nom,acc]).
verb(practise,�b,en,�b,h(ge),�bt,[nom,?(verbal(to))]).        %DE
%verb(pray,pray,[nom]).
%verb(precede,precede,[nom,acc]).
%verb(predict,predict,[nom,acc];[nom,verbal(to)]
%     ;[nom,clause(that)]).
%verb(prefer,prefer,[nom,acc,?(to(acc))];[nom,verbal(to)]).
verb(prepare,vorbereit,en,vorbereite,h,vorbereitet,
     [nom,acc]).                                              %DE
%verb(present,present,[nom,acc,acc]).
%verb(preserve,preserve,[nom,acc]).
verb(press,dr�ck,en,dr�ck,h(ge),dr�ckt,[nom,acc]).            %DE
verb(press,druck,en,druck,h(ge),druckt,[nom,acc]).            %DE
%verb(pretend,pretend,[nom,acc]).
%verb(prevent,prevent,[nom,acc]).
%verb(print,print,[nom,acc]).
%verb(produce,produce,[nom,acc]).
%verb(promise,promise,[nom,acc];[nom,verbal(to)]
%     ;[nom,clause(that)]).
%verb(promote,promote,[nom,acc]).
%verb(protect,protect,[nom,acc]).
%verb(prove,prove,[nom,acc]).
%verb(provide,provide,[nom,acc]).
%verb(protest,protest,[nom,against(acc)]).
%verb(prove,prove,[nom,acc]).
%verb(provide,provide,[nom,acc]).
%verb(publish,publish,[nom,acc]).
verb(pull,zieh,en,zieh,zog,h(ge),zogen,[nom,acc]).            %DE
verbPrefix_(moveIn,zieh,ein,[nom,acc]).                       %DE
verbPrefix_(moveHouse,zieh,um,[nom,?(acc),?(to(acc))]).       %DE
verbPrefix_(putOn,zieh,an,[nom,acc]).                         %DE
verbPrefix_(takeOff,zieh,aus,[nom,acc]).                      %DE
%verb(pump,pump,[nom,acc]).
%verb(punish,punish,[nom,acc]).
%verb(push,push,[nom,acc]).
verb(put,stell,en,stell,h(ge),stellt,
     [nom,acc,?(to(acc))]).                                   %DE
verbPrefix_(introduce,stell,vor,[nom,acc]).                   %DE
verbPrefix_(produce,stell,her,[nom,acc]).                     %DE
%verbPrefix_(putAway,put,away,[nom,acc]).
%verbPrefix_(putDown,put,down,[nom,acc]).
%verbPrefix_(putOff,put,off,[nom,acc]).
%verbPrefix_(putOn,put,on,[nom,acc]).
%verbPrefix_(putOut,put,out,[nom,acc]).
%verbPrefix_(putThrough,put,out,[nom,acc]).
%verbPrefix_(putUp,put,up,[nom,acc]).
%
% QQQQQQQQQQQQQQQ
%
verb(quarrel,streit,en,streite,stritt,h(ge),stritten,
     [nom,acc]).                                              %DE
verb(question,frag,en,frag,h(ge),fragt,[nom,acc]).            %DE
%verb(queue,queue,[nom]).
%verb(quit,quit,[nom]).
%
% RRRRRRRRRRRRRRRRRR
%
%verb(race,race,[nom,?(acc)]).
verb(rain,regn,en,regne,h(ge),regnet,[nom]).                  %DE
%verb(raise,raise,[nom,acc]).
verb(reach,erreich,en,erreich,h,erreicht,[nom,acc]).          %DE
verb(read,les,en,lies,las,h(ge),lesen,[nom,?(acc)]).          %DE
%verb(realise,realise,[nom,?(acc)];[nom,verbal]).
%verb(rebuild,rebuild,[nom,acc]).
%verb(receive,receive,[nom,acc]).
%verb(recognise,recognise,[nom,acc]).
verb(recommend,empfehl,en,empfiehl,empfahl,
     h,empfohlen, [nom,acc]).                                 %DE
%verb(record,record,[nom,acc]).
%verb(recover,recover,[nom]).
%verb(recycle,recycle,[nom,acc]).
%verb(reduce,reduce,[nom,acc]).
%verb(refer,refer,[nom,to(acc)]).
%verb(refuel,refuel,[nom,acc]).
%verb(refuse,refuse,[nom,acc]).
%verb(register,register,[nom,acc]).
%verb(regret,regret,[nom,acc];[nom,clause(that)]).
%verb(regret,regret,[nom,acc]).
%verb(relate,relate,[nom,to(nom)]).
%verb(release,release,[nom,acc]).
%verb(relax,relax,[nom]).
%verb(remain,remain,[nom]).
verb(remember,erinner,n,erinner,h,erinnert,
     [nom,acc];[nom,clause(that)]).                           %DE
%verb(remind,remind,[nom,acc];[nom,verbal(to)];
%     [nom,clause(that)]).
%verb(remove,remove,[nom,acc]).
verb(rent,miet,en,miete,h(ge),mietet,[nom,acc]).              %DE
verbPrefix_(rent,miet,ver,[nom,acc]).                         %DE
verb(renovate,renovier,en,renovier,h,renoviert,
     [nom,acc]).                                              %DE
verb(repair,reparier,en,reparier,h,repariert,
     [nom,acc]).                                              %DE
%verb(replace,replace,[nom,acc]).
%verb(reply,reply,[nom,acc]).
verb(report,meld,en,melde,meldete,h(ge),meldet,[nom,acc]).    %DE
verbPrefix_(register,meld,an,[nom,acc]).                      %DE
verb(report,bericht,en,berichte,h,berichtet,
     [nom,acc]).                                              %DE
%verb(request,request,[nom,acc]).
%verb(require,require,[nom,acc]).
%verb(rescue,rescue,[nom,acc]).
%verb(research,research,[nom,acc]).
verb(reserve,reservier,en,reservier,h,reserviert,
     [nom,acc]).                                              %DE
%verb(respect,respect,[nom,acc];[nom,clause(that)]).
verb(rest,ruh,en,ruh,h(be),ruht,[nom]).                       %DE
verbPrefix_(takeRest,ruh,aus,[nom]).                          %DE
%verb(retire,retire,[nom]).
%verb(retract,retract,[nom,acc]).
%verb(return,return,[nom,?(from(acc))];[nom,acc]).
%verb(revise,revise,[nom,acc]).
%verb(reward,reward,[nom,acc]).
verb(ride,reit,en,reite,ritt,i(ge),ritten,[nom,acc]).         %DE
%verb(ring,ring,rings,rang,rung,[nom]).
%verbPrefix_(ringBack,ring,back,[nom,acc]).
%verbPrefix_(ringUp,ring,up,[nom,acc]).
%verb(rise,rise,[nom]).
%verb(risk,risk,[nom,acc]).
%verb(roast,roast,[nom,acc]).
%verb(rob,rob,[nom,acc]).
%verb(roll,roll,[nom,acc]). %�sszeg�ngy�l�t
%verb(rub,rub,[nom,acc]).
%verb(rule,rule,[nom,acc]).
verb(run,lauf,en,l�uf,lief,i(ge),laufen,[nom,?(acc)]).        %DE
verbPrefix_(runOut,lauf,aus,[nom,of(acc)]).                   %DE
verbPrefix_(runAway,lauf,weg,[nom,of(acc)]).                  %DE
verbPrefix_(runBack,lauf,zur�ck,[nom,of(acc)]).               %DE
%
% SSSSSSSSSSSSSSSSSSSSS
%
%verb(sail,sail,[nom,?(acc)]).
verb(save,spar,en,spar,sparte,h(ge),spart,[nom,acc]).         %DE
verb(say,sag,en,sag,sagte,h(ge),sagt,
     [nom,?(acc),?(to(acc))]).                                %DE
verbPrefix_(cancel,sag,ab,[nom,acc]).                         %DE
%verb(scare,scare,[nom,acc]). %HU:f�leml�t
%verb(schedule,schedule,[nom,acc]).
verb(should,soll,en,'','','',[nom]).                          %DE
verb(scold,schimpf,en,schimpf,h(ge),schimpft,[nom,acc]).      %DE
%verb(score,score,[nom,acc]).
%verb(scream,scream,[nom]).
%verb(seal,seal,[nom,?(acc)]).
verb(search,such,en,such,h(ge),sucht,[nom,acc]).              %DE
verb(see,seh,en,sieh,sah,s�h,h(ge),sehen,[nom,acc];
                [nom,clause(if)];[nom,clause(that)];
                [nom,clause(wh)]).                            %DE
verbImpStem(seh,sg/2,sieh).
verbImpStem(seh,pl/2,seh).
verbPrefix_(lookAt,seh,an,[nom,dat]).                         %DE
verbPrefix_(look,seh,aus,[nom]).                              %DE
verbPrefix_(watchTV,seh,fern,[nom]).                          %DE
%verb(seem,seem,[nom,?(verbal(to))]).
%verb(select,select,[nom,acc]).
verb(sell,verkauf,en,verkauf,h,verkauft,
     [nom,acc];[nom,?(dat),?(f�r(acc))]).                     %DE
verb(send,schick,en,schick,h(ge),schickt,
     [nom,?(dat),?(f�r(acc))]).                               %DE
%verb(serve,serve,[nom,?(acc)]).
verb(set,setz,en,setz,h(ge),setzt,[nom,?(acc),?(to(acc))]).   %DE
%verbPrefix_(setOff,set,off,[nom]).
%verbPrefix_(setOut,set,out,[nom]).
%verbPrefix_(setUp,set,up,[nom]).
%verb(separate,separate,[nom,acc]).
%verb(sew,sew,sews,sewed,sewn,[nom,?(acc),?(to(acc))]).
%verb(shake,shake,shakes,shook,shaken,[nom,acc]).
verb(share,teil,en,teil,h(ge),teilt,[nom,acc]).               %DE
%verb(shave,shave,[nom,acc]).
verb(shine,schein,en,schein,schien,h(ge),schienen,[nom]).     %DE
%verb(ship,ship,[nom,acc]).
%verb(shoot,shoot,[nom,?(at(acc))]).
%verb(shout,shout,[nom,?(at(acc))];[nom,to(acc)]).
verb(show,zeig,en,zeig,h(ge),zeigt,
     [nom,acc,?(acc)];[nom,acc,to(acc)];
     [nom,clause(that)]).                                     %DE
verb(shower,dusch,en,dusch,h(ge),duscht,[nom,?(acc)]).
verb(sign,unterschreib,en,unterschreib,unterschrieb,
    h,unterschrieben,[nom,acc]).                              %DE
%verb(signal,signal,[nom,acc]).
verb(sing,sing,en,sing,sang,h(ge),sungen,[nom,?(acc)]).       %DE
%verb(sink,sink,sinks,sank,sunk,[nom,acc]).
verb(sit,sitz,en,sitz,sa�,i(ge),sessen,[nom]).                %DE
%verbPrefix_(sitDown,sit,down,[nom]).
%verb(skate,skate,[nom]).
%verb(ski,ski,[nom]).
verb(sleep,schlaf,en,schl�f,schlief,h(ge),schlafen,
     [nom,?(acc)]).                                           %DE
%verb(slip,slip,[nom]).
%verb(smash,smash,[nom]).
verb(smell,riech,en,riech,roch,h(ge),rochen,[nom,acc]).       %DE
%verb(smile,smile,[nom,?(at(acc))]).
verb(smoke,rauch,en,rauch,h(ge),raucht,[nom,?(acc)]).         %DE
%verb(sneeze,sneeze,snoze,[nom]). %HU:t�sszent
%verb(sneeze,sneeze,[nom]). %HU:t�sszent
verb(snow,schnei,en,schnei,h(ge),schneit,[nom]).              %DE
%verb(snowboard,snowboard,[nom]).
verb(solve,l�s,en,l�s,l�ste,h(ge),l�st,[nom,acc]).            %DE
%verb(sound,sound,[nom,?(acc)]).
%verb(spare,spare,[nom,?(acc)]).
verb(speak,sprech,en,sprich,sprach,h(ge),sprochen,
     [nom,?(acc)];[nom,�ber(dat)];[nom,von(dat)]).            %DE
verbPrefix_(pronounce,sprech,aus,[nom,acc]).                  %DE
verb(spell,buchstabier,en,buchstabiert,h,buchstabiert,
     [nom,acc]).                                              %DE
%verb(spend,spend,spent,[nom,acc]).
verb(spendNight,�bernacht,en,�bernachte,h,�bernachtet,
     [nom,acc]).                                              %DE
%verb(spill,spills,spill,spillt,spilled,[nom,?(acc)]). %HU:ki�nt/�mlik
%verb(split,split,[nom,acc]).
%verbPrefix_(splitUp,split,up,[nom,?(with(acc))]). %HU:szak�t p�rj�val
%verb(spoil,spoil,[nom,acc]). %HU:elk�nyeztet
%verb(spot,spot,[nom,acc]).
%verb(sprain,sprain,[nom,acc]).
verb(stand,steh,en,steh,stand,h(ge),standen,[nom]).           %DE
verbPrefix_(pass,steh,be,[nom,acc]).                          %DE
verbPrefix_(consist,steh,be,[nom,aus(dat)]).                  %DE
verbPrefix_(getUp,steh,auf,[nom]).                            %DE
%verb(star,star,[nom,acc]).
%verb(stare,stare,[nom,acc]).
verb(start,start,en,starte,startet,h(ge),startet,[nom,acc]).  %DE
verb(start,start,en,starte,startet,i(ge),startet,[nom]).      %DE
%verb(station,station,[nom,?(acc)]).
verb(stay,bleib,en,bleib,blieb,i(ge),blieben,[nom,?(verbal(ing))]). %DE
%verbPrefix_(stayBehind,stay,behind,[nom]). %HU:h�tra/ottmarad
%verb(steal,steal,steals,stole,stolen,[nom,acc]).
%verb(step,step,[nom,to(acc)]).
%verb(stick,stick,[nom,acc]).
%verb(stir,stir,[nom,acc]).
%verb(stitch,stitch,[nom,acc,to(acc)]). %r�varr, r�t�z
verb(stop,stop,pen,stopped,h(ge),stopped,
     [nom,?(acc)];[nom,verbal(to)]).                          %DE
verb(store,speicher,n,speicher,h(ge),speichert,[nom,acc]).    %DE
%verb(stretch,stretch,[nom,acc]). %HU:fesz�t, ny�jt
%verb(strike,strike,struck,[nom,acc]). %HU:csap
%verb(struggle,struggle,[nom,?(verbal(to))]).
verb(study,studier,en,studier,h,studiert,[nom,acc]).          %DE
%verb(subordinate,subordinate,[nom,acc]).
verb(subscribe,abonnier,en,abonnier,abonnierte,
     h,abonniert,[nom,acc]).                                  %DE
%verb(subtract,subtract,[nom,acc,?(from(acc))]).
%verb(succeed,succeed,[nom,acc,?(to(acc))]). %+ing
%verb(suffer,suffer,[nom,?(from(acc))]).
%verb(suggest,suggest,[nom,acc,?(to(acc))];[nom,verbal(to)];
%     [nom,verbal(ing)]).
%verb(sunbathe,sunbathe,[nom]).
%verb(supply,supply,[nom,acc,?(with(acc))]).
%verb(support,support,[nom,acc]).
verb(surf,surf,en,surf,surfte,i(ge),surft,[nom]).             %DE
verb(surf,surf,en,surf,surfte,h(ge),surft,[nom,acc]).         %DE
%verb(surprise,surprise,[nom,acc]).
%verb(surround,surround,[nom,acc]).
verb(swim,schwimm,en,schwimm,swamm,h(ge),schwommen,
     [nom,?(acc)]).                                           %DE
verb(swap,wechsel,n,wechsel,h(ge),wechselt,[nom,acc]).        %DE
%verb(switch,switch,[nom,?(acc)]).
%
% TTTTTTTTTTTTTTTTTTTTTTTT
%
%verb(tag,tag,[nom,acc]).
verb(take,nehm,en,nimm,nahm,h(ge),nommen,[nom,acc]).          %DE
verbImpStem(nehm,sg/2,nimm).
verbImpStem(nehm,pl/2,nehm).

verbPrefix_(remove,nehm,ab,[nom,acc]).                        %DE
verbPrefix_(suppose,nehm,an,[nom,acc]).                       %DE
verbPrefix_(loseWeight,nehm,ab,[nom,acc]).                    %DE
verbPrefix_(undertake,nehm,unter,[nom,acc]).                  %DE
verbPrefix_(takeAway,nehm,weg,[nom,?(acc)]).                  %DE
%verbPrefix_(takeOff,take,off,[nom,?(acc)]).
verbPrefix_(takePart,nehm,teil,[nom,?(acc)]).                 %DE
verbPrefix_(takeWith,nehm,mit,[nom]).                         %DE
%verbPrefix_(takePlace,take,place,[nom]).
%verbPrefix_(takeUp,take,up,[nom,acc]).
verb(takePhotos,fotografier,en,fotografier,
      h,fotografiert,[nom,acc]).                              %DE
verb(takeCare,k�mmer,n,k�mmer,h(ge),k�mmert,
     [nom,of(acc)];[nom,refl,um(acc)]).                       %DE
verb(talk,red,en,rede,h(ge),redet,([nom,?(acc)];
               [nom,about(acc)])).                            %DE
verbPrefix_(arrangeMeeting,red,verab,[nom,acc]).              %DE
verb(taste,schmeck,en,schmeck,h(ge),schmeckt,[nom,acc]).      %DE
%verb(teach,teach,taught,[nom,acc,?(verbal(to))]).
%verb(tear,tear,[nom,acc]).
verb(telephone,telefonier,en,telefonier,h,telefoniert,
     [nom,acc]).                                              %DE
verb(tell,erz�hl,en,erz�hl,h,erz�hlt,[nom,acc,?(dat)]).       %DE
%verb(tend,tend,[nom,verbal(to)]).
%verb(test,test,[nom,acc]).
%verb(text,text,[nom,acc,?(acc)]).
verb(thank,dank,en,dank,h(ge),dankt,[nom]).                   %DE
verbPrefix_(thankACC,dank,be,[nom,refl,?(f�r(acc))]).         %DE
verb(think,denk,en,denk,dachte,h(ge),dacht,[nom,?(acc)];
    [nom,an(acc)];[nom,clause(that)]).                        %DE
%denken �ber(...), von(...) is also acceptable!
%verb(threaten,threaten,[nom,acc]).
verb(throw,werf,en,wirf,warf,h(ge),worfen,[nom,acc]).         %DE
verbImpStem(werf,sg/2,wirf).
verbImpStem(werf,pl/2,werft).
verbPrefix_(throwAway,werf,weg,[nom,acc]).                    %DE
%verb(tick,tick,[nom,?(acc)]).
%verb(tidy,tidy,[nom,acc]).
%verbPrefix_(tidyUp,tidy,up,[nom,?(acc)]).
%verb(tie,tie,[nom,acc]).
%verb(tighten,tighten,[nom,acc]).
%verb(time,time,[nom,acc]).
verb(tinker,bastel,n,bastel,h(ge),bastelt,[nom,acc]).         %DE
%verb(touch,touch,[nom,acc]).
%verb(tour,tour,[nom]).
%verb(tow,tow,[nom,acc]).
verb(train,trainier,en,trainiert,h,trainiert,[nom,acc]).      %DE
verb(translate,�bersetz,en,�bersetzt,h,�bersetzt,
     [nom,acc]).                                              %DE
verb(transfer,�berweis,en,�berweis,�berwies,
     h,�berwiesen,[nom,acc]).                                 %DE
%verb(transmit,transmit,[nom,acc]).
verb(travel,reis,en,reis,i(ge),reist,[nom,?(acc)]).           %DE
verbPrefix_(travelAway,reis,ver,[nom,acc]).                   %DE
%verb(trust,trust,[nom,in(acc)]).
verb(try,probier,en,probier,h,probiert,
     [nom,?(acc)];[nom,verbal(to)]).                          %DE
%verbPrefix_(tryOn,try,on,[nom,acc]). %HU: felpr�b�l
verb(tune,stimm,en,stimm,stimmte,h(ge),stimmt,[nom,acc]).     %DE
verbPrefix_(agree,stimm,ab,[nom,acc]).                        %DE
%verb(turn,turn,[nom,?(to(acc))]). %turn.off, turn.on
%verbPrefix_(turnDown,turn,down,[nom,acc]). %HU: lecsavar/kapcsol
%verbPrefix_(turnInto,turn,into,[nom,acc]). %HU: �talakul
%verbPrefix_(turnOff,turn,off,[nom,acc]). %HU: �talakul
%verbPrefix_(turnOn,turn,on,[nom,acc]). %HU: bekapcsol
%verbPrefix_(turnUp,turn,up,[nom,acc]). %HU: felkapcsol
%verb(twist,twist,[nom]).
verb(twitter,twitter,n,twitter,twitterte,
     h(ge),twittert,[nom,acc]).                               %DE
%verb(type,type,[nom,acc]).
%
% UUUUUUUUUUUUUUUUUUUU
%
%verb(underline,underline,[nom,acc]).
verb(understand,versteh,en,versteh,verstand,
     h,verstanden, [nom,acc]).                                %DE
verbPrefix_(agree,versteh,ein,[nom,?(acc)]).
%verb(undress,undress,[nom]).
%verb(update,update,[nom,acc]).
%verb(upload,upload,[nom,acc,to(acc)]).
verb(use,benutz,en,benutz,h,benutzt,[nom,?(acc)]).            %DE
%
% VVVVVVVVVVVVVVVVVVVVVV
%
%verb(video,video,[nom,acc]).
verb(visit,besuch,en,besuch,h,besucht,[nom,acc]).             %DE
verb(visit,besichtig,en,besichtig,h,besichtigt,
     [nom,acc]).                                              %DE
%verb(vote,vote,[nom,?(for(acc))]).
%
% WWWWWWWWWWWWWWWWWWWWWW
%
verb(wait,wart,en,wartet,h(ge),wartet,[nom,?(auf(acc))]).     %DE
verb(wake,wach,en,wacht,i(ge),wacht,[nom,?(acc)]).            %DE
verbPrefix_(wakeUp,wach,auf,[nom,?(acc)]).                    %DE
%verbPrefix_(wakeUp,wake,up,[nom,?(acc)]).
verb(walk,spazier,en,spazier,i,spaziert,[nom]).               %DE
verb(want,woll,en,will,wollte,h(ge),wollt,
     ([nom,?(acc)];[nom,verbal(to)])).                        %DE
%verb(warn,warn,[nom,?(acc)]).
verb(wash,wasch,en,w�sch,wusch,h(ge),waschen,[nom,acc]).      %DE
verbPrefix_(washUp,wasch,ab,[nom,?(acc)]).                    %DE
%verb(waste,waste,[nom,acc]).
verb(watch,schau,en,schau,schaut,h(ge),schauen,[nom,?(acc)];[nom,clause]). %DE
%verb(water,water,[nom,acc]).
%verb(wave,wave,[nom]).
%verb(wear,wear,wears,wore,worn,[nom,acc]).
%verbPrefix_(wearOut,wear,out,[nom,?(acc)]).
verb(weigh,wieg,en,wieg,h(ge),wiegt,[nom,acc]).               %DE
%verb(welcome,welcome,[nom,acc]).
%verb(weld,weld,[nom,acc]).
%verb(whip,whip,[nom,acc]).
verb(win,gewinn,en,gewinn,gewann,h,gewonnen,[nom,acc]).       %DE
verb(wish,w�nsch,en,w�nsch,h(ge),w�nscht,
     ([nom,acc,?(acc)];[nom,clause])).                        %DE
%verb(whistle,whistle,[nom,?(acc)]).
%verb(wonder,wonder,[nom,?(clause(if))]).
verb(work,arbeit,en,arbeite,h(ge),arbeitet,[nom]).            %DE
verb(workOut,tranier,en,tranier,h,traniert,[nom]).            %DE
%verb(worry,worry,[nom,?(about(acc))]).
%verb(wrap,wrap,[nom,?(acc)]). %HU:becsomagol/teker
%verbPrefix_(wrapUp,wrap,up,[nom,acc]).
verb(write,schreib,en,schreib,schrieb,h(ge),schrieben,
      [nom,acc]).                                             %DE
verbPrefix_(writeUp,schreib,auf,[nom,acc]).                   %DE
verbPrefix_(writeDown,schreib,ab,[nom,acc]).                  %DE
verbPrefix_(describe,schreib,be,[nom,acc]).                   %DE


verbConj(sei,indicative, present,bin,  bist, ist, sind,  seid,  sind  ).
verbConj(sei,conjunctive,present,sei,  seist,sei, seien, seiet, seien ).
verbConj(sei,indicative, past,   war,  warst,war, waren, wart,  waren ).
verbConj(sei,conjunctive,past,   w�re, w�rst,w�re,w�ren, w�rt,  w�ren ).
verbConj(sei,indicative, future, werde,wirst,wird,werden,werdet,werden).
verbConj(sei,imperative, present,'', sei, '', '', seid, '' ).

verbConj(m�ss,indicative, present,muss,   musst,   muss,  m�ssen, m�sst,  m�ssen ).
verbConj(m�ss,conjunctive,present,m�sse,  m�ssest, m�sse, m�ssen, m�sset, m�ssen ).
verbConj(m�ss,indicative, past,   musste, musstest,musste,mussten,musstet,mussten).
verbConj(m�ss,conjunctive,past,   m�sste, m�sstest,m�sste,m�sste, m�sstet,m�ssten).

verbConj(k�nn,indicative, present,kann,   kannst,  kann,  k�nnen, k�nnt,  k�nnen ).
verbConj(k�nn,conjunctive,present,k�nne,  k�nnest, k�nne, k�nnen, k�nnet, k�nnen ).
verbConj(k�nn,indicative, past,   konnte, konntest,konnte,konnten,konntet,konnten).
verbConj(k�nn,conjunctive,past,   k�nnte, k�nntest,k�nnte,k�nnten, k�nntet,k�nnten).

verbConj(m�g, indicative, present,mag,    magst,   mag,   m�gen,  m�gt,   m�gen  ).
verbConj(m�g, conjunctive,present,m�ge,   m�gest,  m�ge,  m�gen,  m�get,  m�gen  ).
verbConj(m�g, indicative, past,   mochte, mochtest,mochte,mochten,mochtet,mochten).
verbConj(m�g, conjunctive,past,   m�chte, m�chtest,m�chte,m�chten,m�chtet,m�chten).

verbConj(d�rf,indicative, present,darf,   darfst,  darf,  d�rfen, d�rft,  d�rfen ).
verbConj(d�rf,conjunctive,present,d�rfe,  d�rfest, d�rfe, d�rfen, d�rfet, d�rfen ).
verbConj(d�rf,indicative, past,   durfte, durftest,durfte,durften,durftet,durften).
verbConj(d�rf,conjunctive,past,   d�rfte, d�rftest,d�rfte,d�rften,d�rftet,d�rften).

verbConj(werd,indicative, present,werde,  wirst,   wird,  werden, werdet, werden ).
verbConj(werd,conjunctive,present,werde,  werdest, werde, werden, werdet, werden ).
verbConj(werd,indicative, past,   wurde,  wurdest, wurde, wurden, wurdet, wurden ).
verbConj(werd,conjunctive,past,   w�rde,  w�rdest, w�rde, w�rden, w�rdet, w�rden ).


verbConj(hab, conjunctive,past,   h�tte,  h�ttest, h�tte, h�tten, h�ttet, h�tten).

verbConj(woll,indicative, present,will,   willst,  will,  wollen, wollt,  wollen ).


verbConj(soll,indicative, present,soll,   sollst,  soll,  sollen, sollt,  sollen ).
verbConj(soll,conjunctive,present,solle,  sollest, solle, sollen, sollet, sollen ).
verbConj(soll,indicative, past,   sollte, solltest,sollte,sollten,solltet,sollten).
verbConj(soll,conjunctive,past,   sollte, solltest,sollte,sollte, solltet,sollten).



%verbPresentConjBut(STEM,MODE,NUM/PERS,FORM).
verbPresentConjBut(hab,indicative,sg/2,hast).
verbPresentConjBut(hab,indicative,sg/3,hat).

verbPresentConjBut(wiss,indicative,sg/1,wei�).
verbPresentConjBut(wiss,indicative,sg/3,wei�).






