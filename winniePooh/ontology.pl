:- include('../commonsense/ontology').


/*HU:R�ges-r�gen valamikor az �sid�kben, de legal�bbis m�lt p�ntek el�tt,
 Micimack� a Sz�zholdas Pagony nev� erd�ben �lt, saj�t kunyh�j�ban.
 S ugyanebben az erd�ben lakott a fontoskod� Nyuszi, a tud�l�kos Bagoly,
 a f�l�nk Malacka, a s�rt�d�s F�les, Kanga, a kengurumama kicsiny�vel,
 Zsebibab�val, no meg a vid�m, ugr�l�s Tigris �s persze mindny�juk
 szeretett gazd�ja, Micimack� legjobb bar�tja: R�bert Gida.
*/

/*EN:once upon a time Winnie-the-pooh had been living in his corner
  in Hundred Acre Wood. In the same wood there have been also living
  some friends of his, namely: Rabbit the pedant, Owl the scholastic,
  Eyeore the touchy, Kanga-Roo with her Baby-Roo, Tiger the bouncing,
  and their keeper, Winnie-the-pooh's best friend, Christopher Robin.
*/
:- dynamic mother/3, emotional/3,
    honey/2, maize/2, thistle/2, maltExtract/2.

:- multifile mother/3, emotional/3,
    honey/2, maize/2, thistle/2, maltExtract/2.

%to denote an instance is belonging to a class
:- op(400,yfx,#).


%wordlet structure
worldlet(winniepooh).
worldlet(W):-var(W), worldlet(W#worldlet).

subWorldletOf(winniepooh,root).

%
% root worldlet - better to say general worldlet
% general information - extension of the ontology
%

%
% worldlet winniepooh
%



model:class(winniepooh,friend,class).
model:subClassOf(winniepooh,friend,humanoid).


model:class(winniepooh,humanoid,class).
model:subClassOf(winniepooh,humanoid,human).
model:subClassOf(winniepooh,humanoid,animal).



%in this worldlet everybody is humanoid	- should it be human or animal
humanoid(winniepooh,X):-human(winniepooh,X); animal(winniepooh,X).

friend(winniepooh,chrisRobin#boy).
friend(winniepooh,X):- keeper(winniepooh,X,chrisRobin#boy).

animal(W,A):- omnivore(W,A).
animal(W,A):- carnivore(W,A).
animal(W,A):- herbivore(W,A).
animal(W,A):- bird(W,A).
animal(W,A):- marsupial(W,A).

bird(W,B):- owl(W,B).

omnivore(W,A):- pig(W,A).
omnivore(W,A):- bear(W,A).
omnivore(W,A):- rabbit(W,A).

carnivore(W,A):- tiger(W,A).

herbivore(W,A):- donkey(W,A).

marsupial(W,A):- kangaroo(W,A).

%inheritance from (root world) modeless knowledge
fodder(winniepooh,X,FOOD):- fodder(X,FOOD).

%woodland(winnepooh,hundredAcreWood#woodland).

geopart(winniepooh,hundredAcreWood#woodland,hundredAcreWood(_)).
geopart(winniepooh,hundredAcreWood(west),poohsTree).
geopart(winniepooh,hundredAcreWood(east),chrisRobinsTree).
geopart(winniepooh,hundredAcreWood(southWest),pigletsTree).
geopart(winniepooh,hundredAcreWood(southEast),eeyoresGloomyPlace).
geopart(winniepooh,hundredAcreWood(northWest),kangasHouse).
geopart(winniepooh,hundredAcreWood(north),rabbitsHole).

geopart(winniepooh,poohsTree,poohsHouse).
geopart(winniepooh,chrisRobinsTree,chrisRobinsHouse).
geopart(winniepooh,pigletsTree,pigletsHouse).
geopart(winniepooh,eeyoresGloomyPlace,poohsCorner).

bear(winniepooh,winnieThePooh#bear).
sex(winniepooh,winnieThePooh#bear,male).
emotional(winniepooh,winnieThePooh#bear,littlebrain).
residence(winniepooh,winnieThePooh#bear,poohsHouse).
nameplate(winniepooh,poohsHouse,"Sanders").
keeper(winniepooh,winnieThePooh#bear,chrisRobin#boy).
fodder(winniepooh,winnieThePooh#bear,condensedMilk).
fodder(winniepooh,winnieThePooh#bear,bread).
fodder(winniepooh,winnieThePooh#bear,honey).


rabbit(winniepooh,rabbit#rabbit).
emotional(winniepooh,rabbit#rabbit,pedant).
residence(winniepooh,rabbit#rabbit,rabbitsHole).
keeper(winniepooh,rabbit#rabbit,chrisRobin#boy).


owl(winniepooh,owl#owl).
emotional(winniepooh,owl#owl,scholastic).
residence(winniepooh,owl#owl,hundredAcreWood#woodland).
keeper(winniepooh,owl#owl,chrisRobin#boy).

pig(winniepooh,piglet#pig).
sex(winniepooh,piglet#pig,male).
emotional(winniepooh,piglet#pig,shy).
residence(winniepooh,piglet#pig,pigletsHouse).
nameplate(winniepooh,pigletsHouse,"Trespassers W").
keeper(winniepooh,piglet#pig,chrisRobin#boy).

donkey(winniepooh,eeyore#donkey).
emotional(winniepooh,eeyore#donkey,touchy).
residence(winniepooh,eeyore#donkey,poohsCorner).
keeper(winniepooh,eeyore#donkey,chrisRobin#boy).
fodder(winniepooh,eeyore#donkey,thistle).

kangaroo(winniepooh,kanga#kangaroo).
adult(winniepooh,kanga#kangaroo).
sex(winniepooh,kanga#kangaroo,female).
residence(winniepooh,kanga#kangaroo,kangasHouse).
keeper(winniepooh,kanga#kangaroo,chrisRobin#boy).

kangaroo(winniepooh,roo#kangaroo).
child(winniepooh,roo#kangaroo).
mother(winniepooh,roo#kangaroo,kanga#kangaroo).
keeper(winniepooh,roo#kangaroo,chrisRobin#boy).

tiger(winniepooh,tigger#tiger).
sex(winniepooh,tigger#tiger,male).
residence(winniepooh,tigger#tiger,hundredAcreWood#woodland).
keeper(winniepooh,tigger#tiger,chrisRobin#boy).
fodder(winniepooh,tigger#tiger,maltExtract).

human(winniepooh,chrisRobin#boy).
child(winniepooh,chrisRobin#boy).
sex(winniepooh,chrisRobin#boy,male).
residence(winniepooh,chrisRobin#boy,chrisRobinsHouse).

friend(winniepooh,chrisRobin#boy,X):- keeper(winniepooh,X,chrisRobin#boy).
friend(winniepooh,X,chrisRobin#boy):- keeper(winniepooh,X,chrisRobin#boy).
friend(winniepooh,X,Y):- keeper(winniepooh,X,chrisRobin#boy),
	keeper(winniepooh,Y,chrisRobin#boy), X \= Y .

% non instantiatable/abstract classes (at least in winniepooh)
honey(winniepooh,_).
maize(winniepooh,_).
thistle(winniepooh,_).
maltExtract(winniepooh,_).

mother(winniepooh,MOTHER):- mother(winniepooh,_,MOTHER).

name(winniepooh,NAME):- string(NAME).

live(winniepooh,SUBJX,under(name),NAME):- !,
    once(residence(winniepooh,SUBJX,RES)),
    nameplate(winniepooh,RES,NAME).
live(winniepooh,SUBJX,in(_),HERE):- !,
    live(winniepooh,SUBJX,HERE).



love(winniepooh,X,_,WHOM):- friend(winniepooh,X,WHOM).

be(winniepooh,from(_),FROM,TO,DISTANCE):- be(from(_),FROM,TO,DISTANCE).



















