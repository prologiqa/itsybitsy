
user:file_search_path(prolog,'..').

%to denote an instance is belonging to a class
:- op(400,yfx,#).


:- use_module(library(http/http_open)).
:- use_module(library(http/json)).
:- use_module(library(uri)).

:- set_prolog_flag(encoding,iso_latin_1).

param_to_atom(PAR=VALUE,PARVALUE):-
	atomic_list_concat([PAR,=,VALUE],PARVALUE).


restCall(ENDPOINT,PARAMS,DICT):-
	maplist(param_to_atom,PARAMS,ATOMPARMS),
	atomic_list_concat(ATOMPARMS,'&',PARMLIST),
	atomic_list_concat(
	    [ENDPOINT,'?',PARMLIST],FULLURL),
	http_open(FULLURL,IN,[]), json_read_dict(IN,DICT), close(IN).


location(LOC,WGSATOM):-
	uri_encoded(query_value,LOC,LOCE),
        restCall('https://api.openrouteservice.org/geocode/search',
	[api_key='5b3ce3597851110001cf6248d1cae51ce91748c3afc71e71335e90c9',
		     text=LOCE],DICT),
	[FEATURES|_]=DICT.features,LATLON=FEATURES.geometry,
		     [LAT,LON]=LATLON.coordinates,
		     atomic_list_concat([LAT,LON],',',WGSATOM).

route(FROM,TO,DICT):-
	location(FROM,WGSFROM), location(TO,WGSEND),
	atom_concat('https://api.openrouteservice.org',
	'/v2/directions/driving-car',ENDPOINT),
        restCall(ENDPOINT,[
	    api_key='5b3ce3597851110001cf6248d1cae51ce91748c3afc71e71335e90c9',
%    api_key="5b3ce3597851110001cf6248d1cae51ce991748c3afc71e71335e90c9",
		     start=WGSFROM,
		     end=WGSEND],DICT).

r(DICT):-URL='https://api.openrouteservice.org/v2/directions/driving-car?api_key=5b3ce3597851110001cf6248d1cae51ce91748c3afc71e71335e90c9&start=8.681495,49.41461&end=8.687872,49.420318',
	writeln(URL),
		  http_open(URL,IN,[]), json_read_dict(IN,DICT), close(IN).

%DIST: distance in meters
%TIME: duration in seconds
distance(FROM,TO,DIST,TIME):- route(FROM,TO,DICT),
	[FEATURES]=DICT.features,
	PROPERTIES=FEATURES.properties,	SUMMARY=PROPERTIES.summary,
	DIST=SUMMARY.distance, TIME=SUMMARY.duration.


l(SETTL,DICT):-URL='https://api.openrouteservice.org/geocode/search?api_key=5b3ce3597851110001cf6248d1cae51ce91748c3afc71e71335e90c9&text=',
	atom_concat(URL,SETTL,FULLURL),
	writeln(FULLURL),
	http_open(FULLURL,IN,
	[proxy_authorization(basic("","")),
	 proxy('http://ibinproxy01.itsh.itsh-internal.hu',3128)]),
	json_read_dict(IN,DICT), close(IN).














