﻿:- module(deParser,
	  [parseSentence/2
	   ,parseSentence/3
	   ,collectDictErrors/2
           ,dict/2
           ]).

/** <module> German Parser module
 *
 * The module parses German sentences,
 * and creates parse trees, as Prolog structures
 * The module is based on Contralog DCG
 *
 * @author Kilián Imre 12-Apr-2021
 *
 */

:- use_module(pack(pclog/prolog/clog)).
:- use_module(library(apply)).
%%%:-use_module(pclog/prolog/ctrace).

:- use_module(library(lists)).

:- use_module(deMorph).

%:- use_module(deDict).
%dynamic import - module should be explicitely loaded by boot
%and here only the module name is used (instead of file name)
:- add_import_module(parser,deDict,end).


%:- use_module(semantics).

switchOut:- fail.

%Parse tree format
%
%Sentence formats:
%compound(CONN,CLAUSE1,CLAUSE2) CONN: connective, CLAUSE: clause
%CLAUSE a pure clause
%?(CLAUSE) interrogative clause - without interrogative word
%?(WH,CLAUSE) - interrogative clause with interrogative wordSDEM
%
%Clause formats:
% clause(nominal/KIND,FREE,NPS,VP) - nominal clause wo verbal
% clause(verbal/KIND,FREE,NPS,VP) - verbal clause w free args
% with/out free arguments
%
%
%ARGUMENTS - a list of NounPhrase arguments or adverbs
%FREEARGS - a list of free arguments (NP-s or adverbs)
%
%NounPhrases(NPS)
%... is a Prolog list of possibly cumulated noun phrases
%
%NounPhrase(NP) formats
%poss(NP,NUMPERS,OWNER,GENDER)	- possessive noun phrase
%poss(NP,NUMPERS,OWNER)	- possessive noun phrase
% - NP and OWNER are (Numeral)NounPhrases
%
%
%numeral(NR,NUMR,KIND,DECL,NUM,GENDER,CASE
% - NR:   numeral value, or NUMR itself (for indef numerals)
% - NUMR: numeral token stem
% - KIND: ord;card;indef
%         - further possibilities: frac, decfrac, real, etc.
% - DECL:  weak;mixed;strong;unflected
% - NUM: sg;pl
% - GENDER: masculin;feminin;neutral
% - CASE: nom;acc;gen;dat
%
%adj(ADJ,NP)
% - ADJ: a single or a list of ADJectives,
% - NP:  an pure noun
%
%noun(N,GENDER,NUM,CASE) - a pure noun... NUM: sg;pl;VARIABLE
%pronoun(NUMPERS,GENDER,CASE) - an personal pronoun NUMPERS=NUM/PERS
%
%VerbalPhrase formats
%vp(VERB,ADVERBS,MODE,TENSE,ARGUMENTS,FREES)
% simple verbal phrase with verb structure, adverbs mode, tense,
% arguments and eventuell free arguments too
%
%adverb(ADV,KIND,GRADE)
% KIND (time;loc(ation);manner)
% GRADE (base;comp;super)



%
% feature geometry for German Parser
% (if a language independent common formalism is found,
% then it should go to parser.pl)
%
dict(pronoun,[numpers,gender,case]).
dict(common,[noun,gender,numpers,case]).
dict(art,[art,numpers,kind,np]).
dict(adj,[adj,grade]).
dict(adj,[adjp,grade,np]).
dict(adjp,[adv,grade,np]).
dict(proper,[noun,class,numpers,case]).
dict(numeral,[kind,nr,numpers,np]).
dict(poss,[np,numpers,owner]).
dict(poss,[np,numpers,owner,gender]).
dict(post,[np,post]).
dict(coordinating,[conn,np1,np2]).
dict(coordinating,[conn,nps]).
dict(verb,[verb,tense,numpers]).
dict(verbal,[prep,vp]).
dict(vp,[verb,adv,mode,tense,args,free]).
dict(clause,[kind,dir,free,subj,vp,numpers]).
dict( ? ,[interr,clause]).
dict( ? ,[clause]).
dict(interr,[wh,kind]).
dict(interr,[wh,kind,adv]).
dict(interr,[wh,kind,np,numpers]).
dict(interj,[interj]).
dict(prep,[prep,np]).
dict(actpart,[np,vp]).
dict(compound,[interj,clause1,clause2]).



value2Num(1,sg):- !.
value2Num(N,pl):- N>1.


%Fails, if any element isnt present in
%either the German dictionary
%or in the semantic thesaurus
%checkDictionary(SENTENCE):-
%  forall(member(W,SENTENCE),
%    (functor(W,WF,_), toSemantics(WF,S), isClass(S))).

%! collectDictErrors(+SENTENCE:list,-ERRORS:list) is det.
%The predicate collects dictionary errors.
%Collects unknown German semantic items.
%
% @arg SENTENCE list of German tokens
% @arg ERRORS list of error messages
%
collectDictErrors(SENTENCE, ERRORS):-
%    checkDictionary(SENTENCE)-> true;
    findall('*** Unknown German':W,
	     (member(W,SENTENCE),
	     \+ word(_SEM,W)), ERRORS).
%    findall('*** Unknown concept':SEM,
%            (member(W,SENTENCE),
%	     functor(W,WF,_), toSemantics(WF,SEM),
%	     atom(SEM), \+ isClass(SEM)), ERR2),
%    append(ERR1,ERR2,ERRORS).


cleanAll:- clean, fail; true.

goalAll:- goal, fail; true.


fireWord(measure(MEAS,QUANT,KIND))-->
    fire_measure(MEAS,QUANT,KIND).


%verb: NUM _ ; sg/3... if sg/3==>FORM=inf
%verb:FORM inf;past;past(participle);present(participle)
fireWord(common(STEM,GENDER,NUM,CASE))-->
    fire_noun(STEM,GENDER,NUM,CASE).
fireWord(common(STEMLIST,GENDER,NUM,CASE,N))-->
    fire_noun(STEMLIST,GENDER,NUM,CASE,N).

fireWord(proper(STEM,CLASS,GENDER,NUM,CASE))-->
    fire_proper(STEM,CLASS,GENDER,NUM,CASE).
fireWord(proper(STEMLIST,CLASS,GENDER,NUM,CASE,N))-->
    fire_proper(STEMLIST,CLASS,GENDER,NUM,CASE,N).

fireWord(pronoun(NUMPERS,GENDER,CASE))-->
    fire_pronoun(NUMPERS,GENDER,CASE).

fireWord(refl(NUMPERS))-->
    fire_reflexive(NUMPERS).

fireWord(possPronoun(NUMPERS,OWNGENDER,GENDER,CASE))-->
    fire_possPronoun(NUMPERS,OWNGENDER,GENDER,CASE).

fireWord(indefPronoun(PRONOUN,GEND,NUM,CASE))-->
    fire_indefPronoun(PRONOUN,GEND,NUM,CASE).
fireWord(indefPronoun(PRONOUN,GEND,NUM,CASE,NR))-->
    fire_indefPronoun(PRONOUN,GEND,NUM,CASE,NR).

fireWord(demPronoun(PRONOUN,KIND,GENDNUM,CASE))-->
    fire_demPronoun(PRONOUN,KIND,GENDNUM,CASE).


%verbs, auxiliaries, modals
fireWord(verb(STEM,MODE,TENSE,NUMPERS))-->
    {aux(STEM)}, fire_aux(STEM,MODE,TENSE,NUMPERS).
fireWord(verb(STEM,MODE,TENSE,NUMPERS))-->
    fire_verb(STEM,MODE,TENSE,NUMPERS).
fireWord(verb(MODAL,MODE,TENSE,FRAME))-->
    {modal(MODAL,_,_,_)}, fire_modal(MODAL,MODE,TENSE,FRAME).

%adjectives
fireWord(adj(ADJ,GRAD,DECL,GENDNUM,CASE))-->
    fire_adject(ADJ,GRAD,DECL,GENDNUM,CASE).
fireWord(adj(ADJLIST,GRAD,DECL,GENDNUM,CASE,N))-->
    fire_adject(ADJLIST,GRAD,DECL,GENDNUM,CASE,N).

%adverbs
fireWord(adv(ADV,KIND,GRADE))-->
    fire_adverb(ADV,KIND,GRADE).
fireWord(adv(ADV,KIND,GRADE,N))-->
    fire_adverb(ADV,KIND,GRADE,N).

%articles
fireWord(art(DECL,KIND,GENDNUM,CASE))-->
    fire_art(DECL,KIND,GENDNUM,CASE).

%determiners
fireWord(det(DET,NUM,NUM0,KIND))-->
    fire_determ(DET,NUM,NUM0,KIND).
fireWord(det(DET,NUM,NUM0,KIND,N))-->
    fire_determ(DET,NUM,NUM0,KIND,N).

%interrogatives
fireWord(interr(INTERR,GENDNUM,CASE,KIND))-->
    fire_interr(INTERR,GENDNUM,CASE,KIND).
fireWord(interr(INTERR,KIND,N))-->
    fire_interr(INTERR,KIND,N).

%interjections
fireWord(interj(IJ,INTERJ))-->
    fire_interj(IJ,INTERJ).

fireWord(prep(PREP,CASE))-->
    fire_prep(PREP,CASE).
fireWord(postp(POSTP,CASE))-->
    fire_postp(POSTP,CASE).

fireWord(conj(CONJ,KIND))-->
    fire_connect(CONJ,KIND).

fireWord(card(INT,CARD,NUM,GENDER,CASE))--> {value2Num(INT,NUM)},
    fire_numeral(INT,CARD,card,_DECL,NUM,GENDER,CASE).
fireWord(card(INT,CARD))--> {value2Num(INT,NUM)},
    fire_numeral(INT,CARD,card,_DECL,NUM,_GENDER,_CASE).
fireWord(ord(NR,ORD,DECL,GENDER,CASE))-->
    fire_numeral(NR,ORD,ord,DECL,sg,GENDER,CASE).
fireWord(ord(NR))-->
    fire_numeral(NR,NR,ord,weak,sg,GENDER,CASE).
fireWord(indefNumeral(INDEF,DECL,GENDNUM,CASE))-->
    {gendnum2GenderNumber(GENDNUM,GENDER,NUM)},
    fire_numeral(INDEF,INDEF,indef,DECL,NUM,GENDER,CASE).
fireWord(tag(TAG))-->
    fire_tag(TAG).
fireWord(infPart(INFPART))-->
    fire_infPart(INFPART).

fireWord(verbPrefix(VERBPREF))-->
    fire_verbPrefix(VERBPREF).

fireWord(token(T))-->
    fire_token(T).

fireQuoted(STRING)--> {atom_string(WORD,STRING),
  morphAnal(WORD,proper(PROPER,CLASS,GENDER,NUM,CASE))}, !,
  fire_proper(PROPER,CLASS,GENDER,NUM,CASE).
fireQuoted(STRING)--> fire_proper(STRING,_CLASS,_,sg,_CASE).


%! parseSentence(+EN:list,-FOREST:exprOrList) is semidet.
%
% Converts a German sentence to parse tree (perhaps a forest)
% It is a shortcut to en2ParseTree/3 with strict semantics
% builds sentence environment
%
%@arg EN a list of German words (delimiter: a lexical issue)
%@arg FOREST List of German parse trees

parseSentence(SENTENCE,FOREST):-
  parseSentence(SENTENCE,true,FOREST).


% ! parseSentence(+EN:list,?OPTIONS,-FOREST:exprOrList) is semidet.
% Converts a German sentence to parse tree (perhaps a forest)
% and thereby builds sentence environment...
%
%@arg EN list of German notation words
%@arg OPTIONS boolOrOptlist - list of parsing options
%@arg FOREST exprOrList of German parse trees

parseSentence(SENTENCE,STRICT,FOREST):-
  cleanAll, %clean blackboard
  goalAll, %it is necessary only in case of Contralog facts
%%   Contralog should generate an empty 'goal' rule!!!!!!!!
  nb_setval(options,STRICT),
  length(SENTENCE,Y), nb_setval(nrofwords,Y), nb_setval(parsetree,[]),
  (nth0(NTH,SENTENCE,X), NEXT is NTH+1,
     (NTH=0->morph1Anal(X,LEXP),
        (string(LEXP)->fireQuoted(LEXP,NTH,NEXT), fail;
        fireWord(LEXP,NTH,NEXT), fail);
     string(X)->fireQuoted(X,NTH,NEXT),fail;
     punct(X), \+ punctWordTag(X)->fire_punct(X,NTH,NEXT), fail;
     punctWordTag(X),fire_punct(X,NTH,NEXT), fail;
     NTH=1, morphInterr2Anal(X,LEXP)->fireWord(LEXP,NTH,NEXT),fail;
     morphAnal(X,XEXP),fireWord(XEXP,NTH,NEXT),fail)
  ;nb_getval(parsetree,FOREST)).

%
%Hook procedures for Contralog exports
%They are used for tracing/logging purposes
%
exp_clause(CL,KIND,DIR,TYPE,SUBJ,X,Y):-
	debug(deParser,'Clause:~w/~w~w~w~w@~d-~d',[CL,KIND,DIR,TYPE,SUBJ,X,Y]).

exp_sentence(TREE,0,Y):- nb_getval(nrofwords,Y), !,
	nb_getval(parsetree,FOREST), nb_setval(parsetree,[TREE|FOREST]),
	debug(deParser,'Sentence:~w@~d-~d',[TREE,0,Y]).
exp_sentence(TREE,X,Y):- X>0,
	debug(deParser,'IncompleteSentence:~w@~d-~d',[TREE,X,Y]).
exp_sentence(TREE,X,Y ):- nb_getval(nrofwords,Z), Y<Z,
	debug(deParser,'IncompleteSentence:~w@~d-~d',[TREE,X,Y]).

exp_nounPhrase(NP,GENDER,NUM/PERS,CASE,X,Y):- NP\=ref(_,_), !,
	debug(deParser,'NounPhrase:~w/~w(~w)-~w-~w@~d-~d',
              [NP,NUM,PERS,GENDER,CASE,X,Y]).

exp_verbalPhrase(VP,MODE,TENSE,NUM/PERS,PDEM,XDEM,X,Y):-
	debug(deParser,'VerbalPhrase:~w/~w-~w-~w(~w)-~w-~w@~d-~d',
              [VP,MODE,TENSE,NUM,PERS,PDEM,XDEM,X,Y]).

exp_interrPhrase(IP,KIND,G,NUMPERS,X,Y):-
	debug(deParser,'InterrogativePhrase/4:~w/~w-~w-~w@~d-~d',
	      [IP,G,NUMPERS,KIND,X,Y]).

%artNPDeclination(ARTK,NPD,DECL).
%ARTK: article kind (def;indef;nodef)
%NPD:  declination of noun phrase (strong;weak;mixed)
%DECL: declination of determined (resulting) noun phrase
artNPDeclination(ARTKIND,+NPDECL,DECL):-
    atom(NPDECL), !, artNPDeclination(ARTKIND,NPDECL,DECL).
artNPDeclination(indef,mixed,indef).
artNPDeclination(def,weak,weak).
artNPDeclination(nodef,mixed,mixed).

%
% GERMAN ACTIVE TENSES
%
%
tensePresent(present).
tensePresent(present(perfect)).
tensePresent(mod(present(perfect))).

%verbal tenses that are the same in conj(conjugable) forms
%fahr(en)
tenseConj(present).
%fuhr
tenseConj(past).

%
% verb perfect tenses where auxiliary verbs
% must be cheched (if sein or haben is required)
%
%active tenses
tensePerfect(present(perfect)).
tensePerfect(mod(present(perfect))).
tensePerfect(past(perfect)).
tensePerfect(mod(past(perfect))).
%passive tenses
tensePerfect(passive(mod,perfect)).
tensePerfect(passive(mod,past(perfect))).


% - inf                         fahren
% - past(participle)            gefahren
% - inf(perfect)                gefahren sein/haben
% - mod(inf)                    fahren können
% - mod(inf(perfect))           sein/haben fahren können
% - worden                      past(part)
% - inf(passive)                gefahren werden
% - inf(passive,part)           gefahren worden
% - inf(inf(passive,part))      gefahren worden sein
% - mod(inf(passive,perfect))   gefahren werden können
% - mod(inf(passive,perfect,inf)) haben gefahren werden können

:- discontiguous tense2inf/3, tense2inf/4.
%
% active infinites
%
%fahren
tense2inf(infinite,inf).
%gefahren
tense2inf(past(participle),past(participle)).

%gefahren sein
tense2inf(past(participle),infinite,inf(perfect)).
%fahren können
tense2inf(infinite,infinite,mod(inf)).
%haben fahren können
tense2inf(infinite,infinite,infinite,mod(inf(perfect))).

%
% passive infinites
%
%gefahren werden
tense2inf(past(participle),infinite,inf(passive)).
%gefahren worden
tense2inf(past(participle),past(part),inf(passive,part)).
%gefahren worden sein
tense2inf(past(participle),past(part),infinite,inf(inf(passive,part))).
%gefahren werden können
tense2inf(inf(passive),infinite,mod(inf(passive,perfect))).
%haben gefahren werden können
tense2inf(infinite,mod(inf(passive,perfect)),mod(inf(passive,perfect))).
%
% active tenses
%
%habe gelesen
conjinf2tense(present,past(participle),present(perfect)).
%hatte gelesen
conjinf2tense(past,past(participle),past(perfect)).
%kann lesen
conjinf2tense(present,inf,present).
%könnte lesen
conjinf2tense(past,inf,past).
%habe lesen können
conjinf2tense(present,mod(inf),mod(present(perfect))).
%hatte lesen können
conjinf2tense(past,mod(inf),mod(past(perfect))).
%wird lesen
conjinf2tense(future,inf,future).
%wird gelesen haben
conjinf2tense(future,inf(perfect),future(perfect)).
%wird lesen können
conjinf2tense(future,mod(inf),mod(future)).
%wird haben lesen können
conjinf2tense(future,mod(inf(perfect)),mod(future(perfect))).


%
% passive tenses
%
%wird ... gelesen
conjinf2tense(present,past(participle),passive(present)).
%ist ... gelesen worden
conjinf2tense(present,inf(passive,part),passive(present(perfect))).
%kann ... gelesen werden
conjinf2tense(present,inf(passive),passive(mod,present)).
%hat ... gelesen werden können
conjinf2tense(present,mod(inf(passive,perfect)),passive(mod,perfect)).
%wurde ... gelesen
conjinf2tense(past,past(participle),passive(past)).
%war ... gelesen worden
conjinf2tense(past,inf(passive,part),passive(past(perfect))).
%könnte ... gelesen werden
conjinf2tense(past,inf(passive),passive(mod,past)).
%hatte ... gelesen werden können
conjinf2tense(past,mod(inf(passive,perfect)),passive(mod,past(perfect))).





:- discontiguous auxTense2Inf/4.

dictTense(inf).
dictTense(present(participle)).
dictTense(past(participle)).
dictTense(present).
dictTense(past).

inf2Tense(inf,present,NUM/PERS):-
    freeze(PERS, NUM/PERS\=sg/3).
inf2Tense(INF,INF,_):- dictTense(INF).

%XXX have driven
auxTense2Inf(have,inf,past(participle),inf(perfect)).
auxTense2Inf(have,present,past(participle),inf(perfect)).
%XXX be driving
auxTense2Inf(be,inf,present(participle),inf(continuous)).
%XXX been driving
auxTense2Inf(be,past(participle),present(participle),
             inf(perfect,continuous)).
%XXX have been driving
auxTense2Inf(have,inf,inf(perfect,continuous),
             inf(inf(perfect,continuous))).

:- discontiguous tense2Conj/2, auxTense2Conj/3, auxTense2Conj/4.

%tense2Conj(VERBTENSE,VERBCONSTRUCT)
%tense2Conj(AUXTENSE,VERBTENSE,VERBCONSTRUCT)
%tense2Conj(AUX,AUXTENSE,VERBTENSE,VERBCONSTRUCT)
%
tense2Conj(present(participle),present(participle)).
tense2Conj(past(participle),past(participle)).

%
% ACTIVE TENSES
%


%fahr(en)
tense2Conj(present,present).
%tense2Conj(inf,present). %%%inf should already be given
tense2Conj(inf,inf).
%do drive/don't drive
auxTense2Conj(do,inf,present,present).
%auxTense2Conj(do,present,present,present).
auxTense2Conj(do,present,inf,present).
auxTense2Conj(do,inf,inf,inf).
%have driven
auxTense2Conj(have,present,past(participle),present(perfect)).
auxTense2Conj(inf,past(participle),present(perfect)).
auxTense2Conj(have,inf,past(participle),present(perfect)).

%can drive
auxTense2Conj(present,inf,present).
%can have driven
auxTense2Conj(present,inf(perfect),present(perfect)).

%fuhr
tense2Conj(past,past).
%did drive
auxTense2Conj(do,past,inf,past).
%had driven
auxTense2Conj(have,past,past(participle),past(perfect)).
auxTense2Conj(past,past(participle),past(perfect)).

%could drive
auxTense2Conj(past,inf,past).
%could have driven
auxTense2Conj(past,inf(perfect),past(perfect)).

%will drive
auxTense2Conj(be,future,inf,future).
%will have driven
auxTense2Conj(be,future,inf(perfect),future(perfect)).


%
% PASSIVE TENSES
%
%be driven
auxTense2Inf(be,inf,past(participle),inf(passive)).
auxTense2Inf(be,present,past(participle),inf(passive)).
%been driven
auxTense2Inf(be,past(participle),past(participle),inf(passive(perfect))).
%have been driven
auxTense2Inf(have,inf,inf(passive(perfect)),inf(inf(passive(perfect)))).

%am driven
auxTense2Conj(be,present,past(participle),passive(present)).
%have been driven
auxTense2Conj(have,inf,inf(passive(perfect)),
              passive(present(perfect))).
auxTense2Conj(have,present,inf(passive(perfect)),
              passive(present(perfect))).

%can be driven
auxTense2Conj(present,inf(passive),passive(present)).
%can have been driven
auxTense2Conj(present,inf(inf(passive(perfect))),
              passive(perfect)).

%was driven
auxTense2Conj(be,past,past(participle),passive(past)).
%had been driven
auxTense2Conj(have,past,inf(passive(perfect)),
              passive(past(perfect))).

%could be driven
auxTense2Conj(past,inf(passive),passive(past)).
%could have been driven
auxTense2Conj(past,inf(inf(passive(perfect))),
              passive(perfect)).

%will be driven
auxTense2Conj(be,future,inf(passive),passive(future)).
%will have been driven
auxTense2Conj(be,future,inf(inf(passive(perfect))),
              passive(future(perfect))).


auxModTense2Conj(AUX,AT,VT,TENSE):-
  modal(AUX)->once(auxTense2Conj(AT,VT,TENSE));
  once(auxTense2Conj(AUX,AT,VT,TENSE)).


advNumeralKind(neg,_).
advNumeralKind(focus,_).
advNumeralKind(degree,NUM):- NUM\=indef.

backPrepInterr(nom).
backPrepInterr(acc).

insertInterrPrep(interr(WH,KIND,GN,CASE),PREP,interr(WH,PKIND,GN,CASE)):-
  PKIND=..[PREP,KIND].

whBeforeNounPhrase(adj).
whBeforeNounPhrase(adv(manner)).

verbclauseDem(VERBCL):- VERBCL=..[verbal|_].
verbclauseDem(VERBCL):- VERBCL=..[clause|_].

freeMatchFrame(?(FRAME),prep(PRE,_)):- functor(FRAME,PRE,1).
freeMatchFrame(?(VERBCL),_):- verbclauseDem(VERBCL).
freeMatchFrame(VERBCL,_):- verbclauseDem(VERBCL).

freeMatchesFrame([FRAME|_],[FREE|_]):- freeMatchFrame(FRAME,FREE), !.
freeMatchesFrame([_,FRAME|_],[FREE|_]):- freeMatchFrame(FRAME,FREE), !.

matchingLongShortVP(vp(verb([_VERB],_KIND,TENSE,_),_,_,_,_,_),
                    vp(verb([do,''],_KIND,TENSE,_),_,_,_,_,_)):- !.
matchingLongShortVP(vp(verb([AUX|_],_,_,_),_,_,_,_,_),
                    vp(verb([AUX,''],_,_,_),_,_,_,_,_)):- !.

mainVerb(vp(verb(VERB,_,_,_),_MADV,_MODE,_TENSE,_ARGS,_FREE),VERB):-
    atom(VERB), !.
mainVerb(vp(verb(VERBS,_,_,_),_MADV,_MODE,_TENSE,_ARGS,_FREE),VERB):-
    is_list(VERBS), last(VERBS,VERB).

sentenceKind(declarative).
sentenceKind(interrogative(yn)).
sentenceKind(interrogative(wh)).
sentenceKind(interrogative(subj)).
sentenceKind(interrogative(pred)).
sentenceKind(interrogative(about)).
sentenceKind(exclamatory).
sentenceKind(imperative(strong)).
sentenceKind(imperative(weak)).


matchingPunct(_SUBJ,declarative,'.').
matchingPunct(_SUBJ,interrogative(_),'?').
matchingPunct(SUBJ,exclamatory,'!'):- SUBJ\=''.
matchingPunct(SUBJ,imperative(weak),'.'):- SUBJ==''.
matchingPunct(SUBJ,imperative(strong),'!'):- SUBJ==''.

matchingPunct(declarative,'.').
matchingPunct(interrogative(_),'?').
matchingPunct(exclamatory,'!').
matchingPunct(imperative(weak),'.').
matchingPunct(imperative(strong),'!').


%replaceLast(LIST,RESULT,LAST,REPLACEMENT)
%+LIST:   original list
%-RESULT: the last item is replaced
%-LAST:   last item from LIST
%?REPLACEMENT: the item to replace LAST
%
%utility predicate for verbal argument list
%that manages syntactic discontinuity
%The last element is taken out, and replaced by a var
%German: VAUX-VARG-VINF-RELSUBCL,
%Mein Vater hat etwas gebracht, womit er arbeiten kann.
replaceLast([ITEM],[VAR],ITEM,VAR):- !.
replaceLast([HEAD|TAIL],[HEAD|VARTAIL],ITEM,VAR):-
    replaceLast(TAIL,VARTAIL,ITEM,VAR).



:- discontiguous(clean/0). %%% curing of sick Contralog pretranslator

:- contra.


%In export and import declarations NOT ContraDCG,
%but pure Contralog predicates must be specified!
:- export([clause/5,sentence/3,nounPhrase/6
          ,interrPhrase/6 ,verbalPhrase/8]).

:- import([art/6]).
:- import([aux/6]).
:- import([noun/6]).
:- import([noun/7]).
:- import([proper/7]).
:- import([proper/8]).
:- import([modal/6]).
:- import([verb/6]).
:- import([connect/4]).
:- import([prep/4]).
:- import([postp/4]).
:- import([interr/5]).
:- import([interr/6]).
:- import([interj/4]).
:- import([infPart/3]).
:- import([measure/5]).
:- import([numeral/9]).
:- import([adject/7]).
:- import([adject/8]).
:- import([adverb/5]).
:- import([adverb/6]).
:- import([determ/6]).
:- import([determ/7]).
:- import([demPronoun/6]).
:- import([pronoun/5]).
:- import([possPronoun/6]).
:- import([indefPronoun/6]).
:- import([indefPronoun/7]).
:- import([punct/3]).
:- import([reflexive/3]).
:- import([tag/3]).
:- import([token/3]).
:- import([verbPrefix/3]).


%@tbd... to complete it!
%constructing ordinals from cardinal and '.'
numeral(NUM,NR,ord,weak,sg,G,CASE)-->
  numeral(NUM,NR,card,_,_,G,CASE), punct('.').


adverb([],_KIND,_GRADE,_N,_LIST,X,X):- true. %Contralog(!) fact
adverb(LIST,KIND,GRADE)-->
  adverb(LIST,KIND,GRADE,1,LIST), {LIST \= [], LIST\=[_]}.
adverb([AM|AN],KIND,GRADE,M,LIST)-->
  adverb(AM,KIND,GRADE,M,LIST), {atom(AM)},
  adverb(AN,KIND,GRADE,N,LIST), {is_list(AN)}, {N is M+1, M>0}.


adject([],_GRADE,_DECL,_GENDNUM,_CASE,_N,X,X):- true. %Contralog(!) fact
adject(LIST,GRADE,DECL,GENDNUM,CASE)-->
  adject(LIST,GRADE,DECL,GENDNUM,CASE,1), {LIST \= [], LIST\=[_]}.
adject([AM|AN],GRADE,DECL,GENDNUM,CASE,M)-->
  adject(AM,GRADE,DECL,GENDNUM,CASE,M), {atom(AM)},
  adject(AN,GRADE,DECL,GENDNUM,CASE,N), {is_list(AN)}, {N is M+1, M>0}.
adject(W1-W2,GRADE,DECL,GENDNUM,CASE)-->
  adject(W1-W2,GRADE,DECL,GENDNUM,CASE,1), {atom(W1)}, punct('-'),
  adject(W1-W2,GRADE,DECL,GENDNUM,CASE,2), {atom(W2)}.

%constructing proper nouns from several (2) tags (eg. New York)
proper(LIST,CLASS,GENDER,NUM,CASE)-->
  proper(LIST,CLASS,_,_,_,1),
  proper(LIST,CLASS,GENDER,NUM,CASE,2).


% constructing common nouns from several tags
% (eg. ice cream, bus station)
noun([],_GENDER,_NUM,_CASE,_N,X,X):- true. %Contralog(!) fact
%noun(LIST,GENDER,NUM,CASE)-->
%  noun(LIST,GENDER,NUM,CASE,1), {LIST \= [], LIST\=[_]}.
noun([W1,W2],GENDER,NUM,CASE)-->
  noun([W1,W2],_G1,NUM,CASE,1), {atom(W1)},
  noun([W1,W2],GENDER,NUM,CASE,2), {atom(W2)}.
noun([PM|PN],GENDER,NUM,CASE,M)-->
  noun(PM,_G1,NUM,CASE,M), {atom(PM)},
  noun(PN,GENDER,NUM,CASE,N), {is_list(PN), N is M+1, M>0}.
noun(W1-W2,GENDER,NUM,CASE)-->
  noun(W1-W2,_G1,NUM,CASE,1), {atom(W1)}, punct('-'),
  noun(W1-W2,GENDER,NUM,CASE,2), {atom(W2)}.


%constructing determiners from 2 tags (eg. 'ein paar ...')
determ([P1,P2],NUM,NUM0,KIND)-->
  determ([P1,P2],NUM,NUM0,KIND,1), determ([P1,P2],NUM,NUM0,KIND,2).
determ([P1,P2,P3],NUM,NUM0,KIND)-->
  determ([P1,P2,P3],NUM,NUM0,KIND,1), determ([P1,P2,P3],NUM,NUM0,KIND,2),
    determ([P1,P2,P3],NUM,NUM0,KIND,3).


%constructing interrogatives from 2 tags (eg. 'wie viel')
interr([W1,W2],KIND,_GN,_CASE)-->
  interr([W1,W2],KIND,1), {atom(W1)},
  interr([W1,W2],KIND,2), {atom(W2)}.

%constructing indefinite pronouns from several tags (eg. 'a few')
indefPronoun([],_GEND,_NUM,_CASE,_N,X,X):- true. %Contralog(!) fact
indefPronoun(LIST,GEND,NUM,CASE)-->
  indefPronoun(LIST,GEND,NUM,CASE,1), {LIST \= [], LIST\=[_]}.
indefPronoun([IM|IN],GEND,NUM,CASE,M)-->
  indefPronoun(IM,GEND,NUM,CASE,M), {atom(IM)},
  indefPronoun(IN,GEND,NUM,CASE,N), {is_list(IN)}, {N is M+1, M>0}.


auxMod(V,MODE,TENSE,NUMPERS,MODAL,AUX,NOT)-->
    verb(V,MODE,TENSE,NUMPERS,NOT),
    {modal(V)->MODAL=V;aux(V)->AUX=V;
    V=werd,MODE=conjunctive,TENSE=past->AUX=V}.

auxModVerb(V,MODE,TENSE,NUMPERS,MODAL,AUX,NOT)-->
  verb(V,MODE,TENSE,NUMPERS,NOT),
  {modal(V)->MODAL=V;aux(V)->AUX=V;
  V=werd,MODE=conjunctive,TENSE=past->AUX=V;true}.

verb(V,MODE,TENSE,NUMPERS,_NOT)-->
  verb(V,MODE,TENSE,NUMPERS).
verb(V,MODE,TENSE,NUMPERS,NOT)-->
  verb(V,MODE,TENSE,NUMPERS),
  adverb(NOT,neg,base).


%(Ich sehe eine) schöne (Frau.)
%(Ich sehe eine) tanzende (Frau)
adjPhrase(adj(A,GRADE,DECL,GENDNUM,CASE),GRADE,DECL,GENDNUM,CASE)-->
    adject(A,GRADE,DECL,GENDNUM,CASE), {once(atom(A);A=_-end)}.

% adjPhrase(V,base)--> verb(V,_,present(participle),_),
% {optDems(FRAMES)}. most cold|least cold|more cold|less cold
%Echt süße (Äpfeln sind rot).
adjPhrase(adjP(adverb(ADV,KIND,base),GRADE,
               adj(ADJ,GRADE,DECL,GENDNUM,CASE)),
          APGRADE,DECL,GENDNUM,CASE)-->
   adverb(ADV,KIND,ADVGR), {preAdjAdvKind(KIND)},
   {once(ADVGR=base;ADVGR=comp)},
   adject(ADJ,GRADE,DECL,GENDNUM,CASE),
   {once(atom(ADJ); ADJ= _-_ ; ADJ= _-_-_ )},
   {preAdjAdvGrade(GRADE,KIND,APGRADE)},
   {KIND\=comp->true; GRADE\=comp}. %adj
%(Auto ist) nicht sehr schnell.
adjPhrase(adjP([ADV1,ADV2],GRADE,adj(ADJ,GRADE,DECL,GENDNUM,CASE)),
          APGRADE,DECL,GENDNUM,CASE)-->
  adverb(ADV1,KIND1,base), {preAdjAdvKind(KIND1)},
  adverb(ADV2,KIND2,base),
  {preAdjAdvKind(KIND2)}, adject(ADJ,GRADE,DECL,GENDNUM,CASE),
   {preAdjAdvGrade(GRADE,KIND2,APGRADE)}. %adj
%(Auto ist) 3 Meter lang
%(Auto ist) halb Meter lang
adjPhrase(adjP(KIND,NR,UNIT,adj(ADJ,base,DECL,GENDNUM,CASE)),
          base,DECL,GENDNUM,CASE)-->
  numeral(_NUMEXP,NR,KIND,DECL,NUM,GENDER,CASE),  noun(UNIT,GENDER,NUM,nom),
  adject(ADJ,base,DECL,GENDNUM,CASE),
  {once((measure(UNIT,_,_,ADJS),member(ADJ,ADJS)))}.
% (Auto ist) halb meter lang, sehr schön --- only 2 list members!!
adjPhrase([ADJ,ADJP],GRADE,DECL,GENDNUM,CASE)-->
  adjPhrase(ADJ,GRADE,DECL,GENDNUM,CASE), {\+ is_list(ADJ)}, punct(','),
  adjPhrase(ADJP,GRADE,DECL,GENDNUM,CASE), {\+ is_list(ADJP)}.
%(Auto ist) klein, schön.
%@tbd --- több elem? minek a sok alternatíva?
adjPhrase(adj([ADJ,ADJP]),GRADE,DECL,GENDNUM,CASE)-->
  adjPhrase(adj(ADJ),GRADE,DECL,GENDNUM,CASE), {atom(ADJ)}, punct(','),
  adject(ADJP,GRADE,DECL,GENDNUM,CASE), {atom(ADJP)}. %adj
adjPhrase(adj(ADJ|ADJP),GRADE,DECL,GENDNUM,CASE)-->
  adjPhrase(adj(ADJP),GRADE,DECL,GENDNUM,CASE), {is_list(ADJP)},
  adject(ADJ,GRADE,DECL,GENDNUM,CASE), {atom(ADJ)}. %adj
%'(Die) von dir gesehene (Frau gerade kommt.)
adjPhrase(passpart(vp(verb(VERB,MODE,past(participle),NUM/3),
                      ADVERBS,MODE,TENSE,ARGS,FREE),
                  GRADE,DECL,GENDNUM,CASE),
          GRADE,DECL,GENDNUM,CASE)-->
  verbalArgs(ARGS0,CASES), optFreeArgs(FREE),
  optFreeAdverbs(ADVERBS),
  adject(VERB-et,GRADE,DECL,GENDNUM,CASE),
  {VERB\= hab, TENSE=past(participle), GRADE=base},
  {verbAPrefixFrame(VERB,[nom,acc|FRAMES])},
  {\+ freeMatchesFrame(FRAMES,FREE)},
  {butLastNominalDemands(FRAMES)},
  {once(allDemsSat([?(von(dat))|FRAMES],CASES,ARGS0,ARGS))}.

% $ 5 ...
quantity(quantity(NR,UNIT,abbr,NUM/3,CASE),abbr,NUM/3,CASE)-->
    measure(UNIT,money,abbr), numeral(_NUMEXP,NR,card,_DECL,NUM,GENDER,CASE).
% 1000 km
quantity(quantity(NR,UNIT,abbr,NUM/3,CASE),abbr,NUM/3,CASE)-->
    numeral(_NUMEXP,NR,card,_DECL,NUM,GENDER,CASE), measure(UNIT,_,abbr).
%the number of the quantity is not agreed with that of measure
quantity(quantity(NR,UNIT,full,NUM/3,CASE),abbr,NUM/3,CASE)-->
    numeral(_NUMEXP,NR,card,_DECL,NUM,GENDER,CASE), noun(UNIT,GENDER,NUM,CASE),
    {once(measure(UNIT,_,_,_))}.

%	NounPhrase
%
%NP:       syntax structure of NP
%DECL:      adjective-declination kind (weak;mixed;strong)
%GENDER:   gender of noun (to be attached)
%NUM/PERS: number and person of NP (if any)... 1<=PERS<=3
%CASE:     nom;acc;gen;poss
%Noun Phrase with or without some common adjective
%SEM: class of double quoted noun

%'Österreich':a known proper noun
adjectiveNounPhrase(proper(PROPER,CLASS,GENDER,NUM/3,CASE),
                    _DECL,GENDER,NUM/3,CASE)-->
  proper(PROPER,CLASS,GENDER,NUM,CASE).
%'Tochter'
adjectiveNounPhrase(common(NOUN,GENDER,NUM/3,CASE),
                    _DECL,GENDER,NUM/3,CASE)-->
  noun(NOUN,GENDER,NUM,CASE).
% sea journey - composed of two nouns ???
% there are no good German examples
adjectiveNounPhrase(common([ADJN,NOUN],GENDER,NUM/3,CASE),
                    _DECL,GENDER,NUM/3,CASE)-->
  noun(ADJN,_GENDER,sg,nom), {atom(ADJN)},
  noun(NOUN,GENDER,NUM,CASE), {atom(NOUN)}.
%Gleis 8
adjectiveNounPhrase(#(common(N,GENDER,sg/3,CASE),NR),
                    _DECL,GENDER,sg/3,CASE)-->
  noun(N,GENDER,sg,CASE), numeral(NR,NR,card,_,_,_,nom),
  {integer(NR)}.

%Stadt "Pécs" - noun as class, and an unknown proper between quotes
%either the class is the prefix noun, or no alternative parses
adjectiveNounPhrase([common(NOUN,GENDER,NUM/3,CASE),
    proper(NAME,CLASS,PGENDER,NUM/3,PCASE)],_DECL,PGENDER,NUM/3,PCASE)-->
  noun(NOUN,GENDER,NUM,nom), proper(NAME,CLASS,PGENDER,NUM,PCASE),
  {once(CLASS=NOUN;
        ( \+ proper_(_,NAME,_,_,_,NOUN,_),
          \+ (proper(NAME,CL,PGENDER,NUM,PCASE,_,_),CL \= CLASS) )
        )}.
%Nicht wirklich schön grün Auto ist hier.
adjectiveNounPhrase(adj(AP,GRADE,NP),DECL,GENDER,NUM/3,CASE)-->
  adjPhrase(AP,GRADE,DECL,GENDNUM,CASE),
  {gendnum2GenderNumber(GENDNUM,GENDER,NUM)},
  adjectiveNounPhrase(NP,DECL,GENDER,NUM/3,CASE), {NP\=adj(_,_)}.


%NounPhrase with or without numeral
%numeralNounPhrase(NP,DECL,GENDER,NUMPERS,CASE)
%DECL: weak;mixed;strong
%NP: resulting noun phrase
%NUM/NR: plurality indicator (pl;sg) - 1<=NR<=3
%CASE:nom;acc;dat;gen.
numeralNounPhrase(NP,DECL,GENDER,NUM/3,CASE)-->
  adjectiveNounPhrase(NP,DECL,GENDER,NUM/3,CASE).
% quantity expressions
% 3 Mädchen (sind hier)
numeralNounPhrase(NP,weak,GENDER,NUM/3,CASE)-->
  quantity(NP,abbr,NUM/3,CASE).
%3 schöne Mädchen (sind hier)
%'Ich aß viel Eis'
%'Jedes rote Auto'
%'Alle roten Autos'  strong+weak(pl)
%'Manche roten Mäntel' strong+weak(pl)
%'Etwas Wasser'
%(eine) halbe Stunde'
%'mehr Information' -> unflected
numeralNounPhrase(numeral(KIND,NR,NUM/3,NP),
                  +DECL,GENDER,NUM/3,CASE)-->
  numeral(_,NR,KIND,NUMDECL,NUM,GENDER,CASE),
  adjectiveNounPhrase(NP,NPDECL,GENDER,NUM/3,CASE),
  {\+ (NP=common(N,_,_,_), measure(N,_,_,_))},
  {KIND==card->NPDECL=strong,DECL= NPDECL;
   NUMDECL==unflected->ignore(NPDECL=unflected),DECL=NPDECL;
   NPDECL=NUMDECL, DECL= NUMDECL->true;
   NPDECL==weak, NUMDECL==strong->DECL=NPDECL}.

%wirklich 3 schöne Mädchen / nicht nur 3 schöne Mädchen
numeralNounPhrase(numeral(KIND,NR,adverb(ADV,AKIND,base),NUM/3,NP),
                  +WEAK,GENDER,NUM/3,CASE)-->
  {WEAK=weak},
  adverb(ADV,AKIND,base), numeral(_,NR,KIND,WEAK,NUM,GENDER,CASE),
  {advNumeralKind(AKIND,KIND)},
  adjectiveNounPhrase(NP,WEAK,GENDER,NUM/3,CASE).


%Noun phrase with or wo determiner
%DECL: weak;mixed;strong;unflected
%NUM: plurality indicator (pl;sg)
%DECL=weak: when a numeral "defines" the phrase
determinedNounPhrase(NP,DECL,GENDER,NUM/3,CASE)-->
  numeralNounPhrase(NP,DECL,GENDER,NUM/3,CASE).
%Die 3 schönen Mädchen / Die zwei roten Äpfel (sind hier)
%Ein roter Apfel (ist hier)
determinedNounPhrase(art(ART,NUM/3,KIND,NP),+DECL,GENDER,NUM/3,CASE)-->
  art(ART,KIND,GENDNUM,CASE), {KIND\==rel},
  numeralNounPhrase(NP,NUMD,GENDER,NUM/3,CASE),
  {artNPDeclination(KIND,NUMD,DECL)},
  {gendnum2GenderNumber(GENDNUM,GENDER,NUM)}.
%Diese zwei roten Äpfel (sind hier.)
%derselbe, dieser, jener, mancher, solcher, welcher, -alle => weak DECL
%that is weak declension (like after definite article)
determinedNounPhrase(det(DET,GENDER,NUM/3,CASE,NP),+weak,GENDER,NUM/3,CASE)-->
  demPronoun(DET,pro,GENDNUM,CASE),
  numeralNounPhrase(NP,DECL,GENDER,NUM/3,CASE),
  {once(DECL=strong;DECL== +strong)},
  {gendnum2GenderNumber(GENDNUM,GENDER,NUM)}.
%ein paar rote Autos - using determiner expressions
determinedNounPhrase(det(DET,GENDER,NUM/PERS,CASE,NP),
                        +NPDECL,GENDER,NUM/PERS,CASE)-->
    determ(DET,NUM,NUM0,possNP),
    numeralNounPhrase(NP,DECL,GENDER,NUM0/PERS,CASE),
    {once(NPDECL=DECL;NPDECL== +DECL), NP\=refl(_,_)}.

%Jemand kommt
determinedNounPhrase(indef(PRONOUN,GENDER,NUM/3,CASE),
                    +_DECL,GENDER,NUM/3,CASE)-->
  indefPronoun(PRONOUN,GENDER,NUM,CASE).

%poss(NP,NUMPERS,OWNER,OGENDER)
%NP: the owned object,
%NUMPERS: the number/3 of owned object
%OWNER: the owner,
%OGENDER: gender of the owner
%Mein Freund (ist hier)
%(die Farbe) meiner Augen (ist blau)
determinedNounPhrase(poss(NP,NUM/3,
                          possPronoun(OWNER,OGENDER,GENDNUM,CASE),
                          OGENDER),
                     +mixed,GENDER,NUM/3,CASE)-->
  possPronoun(OWNER,OGENDER,GENDNUM,CASE),
  numeralNounPhrase(NP,DECL,GENDER,NUM/3,CASE),
  {once(DECL=mixed;DECL== +mixed)},
  {gendnum2GenderNumber(GENDNUM,GENDER,NUM)}.

%nicht viel Geld (habe ich)
determinedNounPhrase(numeral(indef,INDEF,adverb(ADV,AKIND,base),NUM/3,NP),
                     +DECL,GENDER,NUM/3,CASE)-->
  adverb(ADV,AKIND,base), numeral(INDEF,_,indef,DECL,NUM,GENDER,CASE),
  {advNumeralKind(AKIND,indef)},
  adjectiveNounPhrase(NP,DECL,GENDER,NUM/3,CASE).


%Noun phrase with or wo possessive relations
%No more than one possPronoun plus two NPs
%personal pronouns can have neither possessive
%nor plain adjectives, thus they are also possessive NPs
%
%possessiveNounPhrase(POSSNP,DECL,GENDER,NUMPERS,CASE)
%POSSNP: resulting noun phrase
%DECL: weak;mixed;strong
%NUM: number indicator (sg;pl)
%PERS: 1..3
%GENDER: feminin;masculin;neutral ...gender of owned object!!
%CASE:nom;acc;dat;gen.
%
%Du kennst mich.
possessiveNounPhrase(pronoun(NUM/PERS,GENDER,CASE),+_DECL,GENDER,NUM/PERS,
                     CASE)-->
  pronoun(NUM/PERS,GENDER,CASE), {CASE\==gen}.
possessiveNounPhrase(NP,DECL,GENDER,NUM/3,CASE)-->
  determinedNounPhrase(NP,DECL0,GENDER,NUM/3,CASE),
  {DECL0==strong->DECL= +strong;DECL=DECL0}.


%Die Farbe meiner Augen (ist blau)
%Alle 3 braune Hunde unserer 4 Freunden (sind froh)
possessiveNounPhrase(postposs(NP,NUM/3,OWNER),
                     +DECL,GENDER,NUM/3,CASE)-->
  determinedNounPhrase(NP,DECL,GENDER,NUM/3,CASE),
  determinedNounPhrase(OWNER,ODECL,_OGEND,_,GEN),
  {GEN==gen, nonvar(ODECL)}.

%Numeral Determiner/Pronoun + Noun phrase+gen
%Wenige unserer Freunden (sind froh)
possessiveNounPhrase(numeral(KIND,NUMERAL,NUM/PERS,NP),
                     +DECL,GENDER,NUM/PERS,CASE)-->
  numeral(_NR,NUMERAL,KIND,_NUMDECL,NUM,GENDER,CASE),
  possessiveNounPhrase(NP,+DECL,GENDER,pl/PERS,gen),
  {once(DECL==weak;DECL==mixed)}.


%sich
prepositionalNounPhrase(refl(NUMPERS,GENDER),GENDER,NUMPERS,acc)-->
    reflexive(NUMPERS).
%Das (ist gut)
%Alle (sind gut)
%Alles (klar)
prepositionalNounPhrase(dem(DEM,GENDER,NUM/3,CASE),GENDER,NUM/3,CASE)-->
    demPronoun(DEM,_KIND,GENDNUM,CASE),
    {CASE \= gen, gendnum2GenderNumber(GENDNUM,GENDER,NUM)}.
%für sich
prepositionalNounPhrase(prep(PREP,refl(NUMPERS,GENDER)),
                        GENDER,NUMPERS,PRECASE)-->
    prep(PREP,CASE), reflexive(NUMPERS), {PRECASE=..[PREP,CASE]} .
prepositionalNounPhrase(NP,GENDER,NUMPERS,CASE)-->
    possessiveNounPhrase(NP,+DECL,GENDER,NUMPERS,CASE).
% in dem roten Auto meiner alten Freunde (ist es bequem)
% in meinem Haus
prepositionalNounPhrase(prep(PREP,NP),GENDER,NUMPERS,PREPCASE)-->
    prep(PREP,CASE), possessiveNounPhrase(NP,+DECL,GENDER,NUMPERS,CASE),
    {PREPCASE=..[PREP,CASE]} .
%Zweier schwierigen Jahre zufolge
prepositionalNounPhrase(postp(POSTP,NP),GENDER,_,POSTCASE)-->
    possessiveNounPhrase(NP,+DECL,GENDER,_NUMPERS,CASE),  postp(POSTP,CASE),
    {POSTCASE=..[POSTP,CASE]} .

%relative(RELATIVE,NUM,CASE)
%RELATIVE: syntax construction for relative role
%NUM: sg;pl
%CASE: plain cases plus evtl PREP(CASE) constructions
%(Ich mag die Frau,) deren Mann (du auch kennst)
relative(poss(NUMNP,NUM/3,art(ART,rel,GENDNUM,gen),OGENDER),
         OGENDER,ONUM,CASE)-->
    art(ART,rel,GENDNUM,gen), {gendnum2GenderNumber(GENDNUM,OGENDER,ONUM)},
    numeralNounPhrase(NUMNP,DECL,GENDER,NUM/3,CASE).
%(Ich mag das Buch,) das (du gelesen hattest.)
relative(art(ART,rel,GENDNUM,CASE),GENDER,NUM,CASE)-->
    art(ART,rel,GENDNUM,CASE), {CASE \=gen},
    {gendnum2GenderNumber(GENDNUM,GENDER,NUM)}.
%Ich mag Bücher, welche interessant sind.
relative(interr(WH,KIND,GENDNUM,CASE),GENDER,NUM,CASE)-->
    interr(WH,KIND,GENDNUM,CASE),
    {ignore((nonvar(GENDNUM),gendnum2GenderNumber(GENDNUM,GENDER,NUM)))}.
%(Ich mag Bücher,) in denen (du Fehler gefunden hast.)
relative(RELATIVE,GENDER,NUM,CASE)-->
    prep(PREP,ACASE),
    relative(ARELATIVE,GENDER,NUM,ACASE),
    {ACASE \=gen, atomic(ACASE)}, {CASE=..[PREP,ACASE]},
    {ARELATIVE =.. [NAME,ARG1,ARG2,ARG3,ACASE],
    RELATIVE =.. [NAME,ARG1,ARG2,ARG3,CASE]}.


modifiedNounPhrase(NP,GENDER,NUMPERS,CASE)-->
    prepositionalNounPhrase(NP,GENDER,NUMPERS,CASE).
%(Ich sehe) die Frau mit dem blauen Rucksack
%(Ich sehe) das Mädchen mit Brillen
%eine Reihe von schönen Frauen
modifiedNounPhrase(post(NP,NP1),GENDER,NUMPERS,CASE)-->
    prepositionalNounPhrase(NP,GENDER,NUMPERS,CASE),
    prepositionalNounPhrase(NP1,_GENDER,_NUMPERS,PREPCASE),
    {compound(PREPCASE)}, { \+ functor(NP,proper,_)}.
%(Ich mag) das Mädchen, das schön singt.
%????? das Mädchen, die schön singen ?????
modifiedNounPhrase(that(subject,NP,RELATIVE,CL),GENDER,NUM/3,CASE)-->
    prepositionalNounPhrase(NP,GENDER,NUM/3,CASE), punct(','),
    relative(RELATIVE,GENDER,NUM,nom),
    subclause(CL,declarative,TENSE,REF,DEM),
    {DEM==nom},
    {REF=ref(RELATIVE)},
    {TENSE \= inf, TENSE \= past(participle),
     TENSE \= present(participle)}.
%(Ich sehe den Lehrer), den du magst.
%(Ich mag deinen Lehrer), dem du einen Apfel gegeben hast.
%(Ich mag deinen Lehrer), mit dem du geredet hast
%(Ich sehe den Künstler), wem ich einen Brief gegeben habe
modifiedNounPhrase(that(pendent,NP,RELATIVE,CL),
                   GENDER,NUM/3,CASE)-->
    prepositionalNounPhrase(NP,GENDER,NUM/3,CASE),
    punct(','),
    relative(RELATIVE,GENDER,NUM,REFCASE), {REFCASE\==nom},
    subclause(CL,declarative,TENSE,REF,PDEM),
    {(emptyOptDem(PDEM)->true;
     \+ \+ demSat(PDEM,REFCASE), REF=ref(RELATIVE))},
    {TENSE \= inf, TENSE \= past(participle),
     TENSE \= present(participle)}.
%The book to read (is on the desk).
modifiedNounPhrase(infinite(NP,VP),GENDER,NUMPERS,CASE)-->
    prepositionalNounPhrase(NP,GENDER,NUMPERS,CASE), infPart(zu),
    {\+ functor(NP,proper,5)},
    verbalPhrase(VP,_MODE,inf,_NUMPERS,_X,PDEM),
    {emptyOptDem(PDEM)}.
%for my parents both
modifiedNounPhrase(both(NP),GENDER,pl/3,CASE)-->
    prepositionalNounPhrase(NP,GENDER,pl/3,CASE),
    numeral(both,_,indef,DECL,pl,GENDER,CASE).
%my 3 small daughters themselves - agreement!
modifiedNounPhrase(refl(NUMPERS,GENDER,CASE,NP),GENDER,NUMPERS,CASE)-->
    prepositionalNounPhrase(NP,GENDER,NUMPERS,CASE),
    reflexive(NUMPERS).

%Mein (ist rot) - missing noun for possessive noun phrase
nounPhrase(possPronoun(PRONOUN,OWNGEND,OGENDNUM,OCASE),GENDER,NUM/3,OCASE)-->
    possPronoun(PRONOUN,OWNGEND,OGENDNUM,OCASE),
    {gendnum2GenderNumber(OGENDNUM,GENDER,NUM)}.
% Manche (fahren schnell) - missing noun for undef numeral
%nounPhrase(NUMERAL,GENDER,NUM/3,CASE)-->
%    numeral(NUMERAL,_,indef,_DECL,NUM,GENDER,CASE).
nounPhrase(NP,GENDER,NUMPERS,CASE)-->
    modifiedNounPhrase(NP,GENDER,NUMPERS,CASE).
%in meinem Haus und in deinem Auto
nounPhrase(coordinating(CONN,NP1,NP2),GENDER,NUMP,CASE)-->
    modifiedNounPhrase(NP1,_GENDER1,_NUM1P1,CASE),
    connect(CONN,coordinating), {once(listConnective(CONN))},
    modifiedNounPhrase(NP2,GENDER,NUM2P2,CASE2),
    {\+ functor(NP1,prep,2), \+ functor(NP2,prep,2), CONN=und -> NUMP=pl/3;
    CASE=..[_PREP,CASE0], atom(CASE2)-> CASE2=CASE0, NUMP=NUM2P2;
    CASE=CASE2, NUMP=NUM2P2}.
%Double conjunctives (entweder oder, etc.)
%(Wir müssen) entweder mit Bus oder mit Zug (fahren.)
%(Du kannst) weder Bier noch Wein (trinken.)
nounPhrase(correlative(CONN,NP1,NP2),GENDER,NUMPERS,CASE)-->
    connect(C1,corr),
    {correlative(CONN,C1,C2,nounPhrase)},
    modifiedNounPhrase(NP1,_GENDER,_NUMP1,CASE),
    connect(C2,corr), modifiedNounPhrase(NP2,GENDER,NUMPERS,CASE).
%Triple conjunctives (sowohl als auch, etc.)
%Sowohl Klaus als auch Julia (kann Auto fahren)
%Sowohl du und ich (können tanzen)
nounPhrase(correlative(CONN,NP1,NP2),GENDER,pl/PERS,CASE)-->
    connect(C1,corr),
    {correlative(CONN,C1,C2,C3,nounPhrase)},
    modifiedNounPhrase(NP1,_GENDER,_NUMP1,CASE),
    connect(C2,corr), connect(C3,corr),
    modifiedNounPhrase(NP2,GENDER,_NUM1/PERS,CASE).
%Quadruple conjunctives (nicht nur sondern auch, etc.)
%(Ich war) nicht nur in Berlin sondern auch in Hannover
%Nicht nur Klaus sondern auch Julia sind meine Freunde.
nounPhrase(correlative(CONN,NP1,NP2),GENDER,pl/PERS,CASE)-->
    connect(C1,corr), connect(C2,corr),
    {correlative(CONN,C1,C2,C3,C4,nounPhrase)},
    modifiedNounPhrase(NP1,_GENDER,_NUMP1,CASE),
    connect(C3,corr), connect(C4,corr),
    modifiedNounPhrase(NP2,GENDER,_NUM1/PERS,CASE).

%wie... warum ... wann ... wo ... woher ... wohin
interrPhrase(interr(WH,KIND,GN,CASE),GN,_NP,KIND)-->
    interr(WH,KIND,GN,CASE), {atom(WH)}.

% welches nette Mädchen, welches rote Auto?
% Was für eine schöne Frau?
interrPhrase(interr(WH,GENDER,NUM/3,CASE,NP),GENDER,NUM/3,CASE)-->
    interr(WH,adj,GN,CASE), {gendnum2GenderNumber(GN,GENDER,NUM)},
    possessiveNounPhrase(NP,DECL,GENDER,NUM/3,CASE),
    { WH=welch->DECL=strong;
      NUM=pl->DECL=strong, NP\=numeral(_,_,_,_);
      NUM=sg->DECL=mixed, NP\=numeral(_,_,_,_)
    }.

% wessen Buch (liegt auf dem Tisch)
interrPhrase(interr(wessen,GENDER,NUM/3,CASE,NP),GENDER,NUM/3,CASE)-->
    interr(wessen,gen,_,gen), possessiveNounPhrase(NP,strong,GENDER,NUM/3,CASE).

%wie schöne Mädchen
%wie schnelles Auto
interrPhrase(interr(wie,GENDER,NUM/3,CASE,NP),GENDER,NUM/3,CASE)-->
    interr(wie,adv(manner),_GN,CASE),
    adjectiveNounPhrase(NP,strong,GENDER,NUM/3,CASE).

%wie schnellAFF (as an adjective)
%wie unglaublich schnellAFF
interrPhrase(interr(wie,adj,ADJP),_GENDER,_NUMP,adj)-->
    interr(wie,adv(manner),GN,CASE), adjPhrase(ADJP,base,_DECL,GN,CASE).

%wie lang (an adverb)
interrPhrase(interr(wie,adv(KIND),adverb(ADV,KIND,base)),
             _G,_NUMP,adv(KIND))-->
    interr(wie,adv(manner),_GN,_CASE),   adverb(ADV,KIND,base),
    {KIND \== part, KIND \== neg, KIND \== super, KIND \== link,
     KIND \== comp, KIND \== degree}.

%wie viel rote Äpfel
interrPhrase(interr(WH,num,GENDER,NUM/3,NP),GENDER,NUM/3,CASE)-->
    interr(WH,num,_GN,CASE),
    adjectiveNounPhrase(NP,strong,GENDER,NUM/3,CASE).

%an wen (hast du einen Brief geschrieben)
%unter was (lebt)
interrPhrase(interr(WH,G,NUM/3,CASE),G,NUM/3,CASE)-->
    prep(PREP,ICASE), {ICASE \= gen},
    interr(WH,_KIND,_GN,ICASE),
    {CASE=..[PREP,ICASE]}.
% an welches Mädchen (hast du einen Brief geschrieben)
% unter welchem Namen (lebt er)
% unter welchem Baum (sitzt er)
% in was für einer schönen Stadt (wohnen wir)
interrPhrase(interr(WH,GN,NUM/3,CASE,NP),G,NUM/3,CASE)-->
    prep(PREP,WCASE),
    interrPhrase(interr(WH,GN,NUM/3,WCASE,NP),G,NUM/3,WCASE),
    {WCASE \== gen, CASE=..[PREP,WCASE]}.


%Recognition of free adverbs...
%List of free adverbs is limited to 2!!!!!
optFreeAdverbs([],X,X):- true.  %Contralog!! fact!!
%(I run) quickly
optFreeAdverbs([adverb(ADV,KIND,base)|FREES])-->
    adverb(ADV,KIND,base),
    {KIND\=super, KIND\=comp, KIND\= part, KIND\= connecting(_),
     KIND \= neg},
    optFreeAdverbs(FREES), {FREES\=[_,_|_]}.


%Recognition of free arguments/adverbs...
%List of free arguments is limited to 3!!!!!
optFreeArgs([],X,X):- true.  %Contralog!! fact!!
%noun phrase
optFreeArgs([NP|FREES])-->nounPhrase(NP,_GENDER,_NUMPERS,CASE),
    {once((NP=prep(_,_); NP=postp(_,_);
    NP=correlative(_,_,_);NP=det(_,_,_,_,_);
    NP=coordinating(_,_,_);
    NP=refl(_,_))), CASE \== gen, CASE \==nom},
      optFreeArgs(FREES), {FREES\=[_,_,_|_]}.
%(I had enough money) to buy a car - (a subclause of purpose)
%no other arguments after a subclause
optFreeArgs([verbal(zu,VP)])-->
    infPart(zu), verbalPhrase(VP,_MODE,inf,_NUMPERS,_X,DEM),
    {emptyOptDem(DEM)}.
% (Ich warte) dort, wo wir uns immer trafen
% - (local adverbial subclause) should be the last free arg!
optFreeArgs([adverb(ADV,adv(place),base,RELATIVE,CL)])-->
    adverb(ADV,place,base), punct(','),
    relative(RELATIVE,_GENDER,_NUM,adv(place)),
    subclause(CL,declarative,_TENSE,_REF,PDEM),
    {emptyOptDem(PDEM)}.

%(I run) quickly
optFreeArgs([adverb(ADV,KIND,GRAD)|FREES])-->
    adverb(ADV,KIND,GRAD), {ADV \= wie},
    {KIND \= part, KIND \= connecting(_)},
    optFreeArgs(FREES), {FREES\=[_,_,_|_]}.
%(I run) from here
optFreeArgs([prep(PREP,adverb(ADV,KIND,base))|FREES])-->
    prep(PREP,CASE), adverb(ADV,KIND,base),
    {KIND\=super, KIND\=comp, KIND \= part, KIND \= connecting(_)},
    optFreeArgs(FREES), {FREES\=[_,_,_|_]}.

% comparisons
optFreeArgs([COMP|FREES])-->
    comparison(COMP,_CASE),
    optFreeArgs(FREES), {FREES\=[_,_,_|_]}.

reference(NP,nom)-->
    nounPhrase(NP,_GENDER,_NUMPERS,nom).
reference(VCL,_)-->
    verbalClause(VCL, _NUM,_MODE,declarative,_TENSE,_SUBJ,_X,DEM),
    {emptyOptDem(DEM)}.
reference(SVCL,_)-->
    shortVerbalClause(SVCL, _NUMP,declarative,_TENSE).
reference(adj(ADJ,base),CASE)--> adject(ADJ,base,DECL,GENDNUM,CASE).

%(Ich laufe) schneller als mein Bruder
comparison(comp(ADJP,>,REF),CASE)-->
    adjPhrase(ADJP,comp,DECL,GENDNUM,CASE),
    connect(als,comp),reference(REF,CASE).

%AP-lessthan-REF
comparison(comp(ADJ,<,REF),CASE)-->
    adjPhrase(ADJ,comp,DECL,GENDNUM,CASE),
    {ADJ = adjP(adverb(wenig,comp,comp),_,_)},
    connect(als,comp), reference(REF,CASE).
%
%(I run) as quick as my brother
%Mein Auto ist doppelt so schnell wie dein Fahrrad
comparison(comp(ADJ,=,REF),CASE)-->
    connect(so,comp), adjPhrase(ADJ,base,uninflected,GENDNUM,CASE),
    adverb(wie,manner,base), reference(REF,CASE).
%***'Mein Bruder ist so stark ein Mann wie der Arzt.'
%***'Mein Bruder ist so starker Mann wie der Arzt.'
comparison(comp(NP,=,REF),CASE)-->
    connect(so,comp), nounPhrase(NP,_GENDER,_NUMPERS,nom),
    adverb(wie,manner,base), reference(REF,CASE).
%
%As quickly as I could I ate my apple.
%NP V as-AP-as-SVCL (short verbal clause, as far as I could)
comparison(comp(ADV,=,REF),CASE)-->
    connect(so,comp), adverb(ADV,KIND,base),
    {KIND\=super, KIND\=comp, KIND \= part},
    adverb(wie,manner,base), reference(REF,CASE), {\+ functor(REF,np,_)}.

%recognizes the German infinite structure...
%that comes in the end of declarative sentences
%
%verbInfinite(VERBS,AUXMOD,VERB,KIND,TENSE).
%VERBS: a list of verbs that constitute the structure
%AUXMOD: auxiliary/modal verbs
%VERB: pure verb
%KIND: declarative; interrogative; imperative; (infinite?)
%TENSE: as defined below
% - inf                         fahren
% - past(participle)            gefahren
% - inf(perfect)                gefahren sein/haben
% - aux(inf)                    fahren können
% - aux(inf(perfect))           sein/haben fahren können
%
%@tbd for infinite structures - tables instead of rules????
%
%infinite structure consisting of a single element
verbInfinite([V],_AUX,V,KIND,INFTENSE)-->
  verb(V,KIND,TENSE,_NUMPERS), {tense2inf(TENSE,INFTENSE)}.
%infinite structure consisting of a verb+aux
verbInfinite([V,AUX],AUX,V,KIND,INFTENSE)-->
  verb(V,KIND,TENSE,_), {verbParticipleAux(V,AUX)}, aux(AUX,KIND,AMTENSE,_),
  {tense2inf(TENSE,AMTENSE,INFTENSE)}.
%passive infinite structure consisting of a verb+werden
verbInfinite([V,werd],werd,V,KIND,INFTENSE)-->
  verb(V,KIND,TENSE,_), verb(werd,KIND,AMTENSE,_),
  {tense2inf(TENSE,AMTENSE,INFTENSE)}.
%infinite structure consisting of a verb+modal
verbInfinite([V,MODAL],MODAL,V,KIND,INFTENSE)-->
  verb(V,KIND,TENSE,_), modal(MODAL,KIND,AMTENSE,_),
  {tense2inf(TENSE,AMTENSE,INFTENSE)}.
%AUX-VERB-MODAL (haben lesen können)
verbInfinite([AUX,V,MODAL],MODAL,V,KIND,INFTENSE)-->
  aux(AUX,KIND,AUXTENSE,_), verb(V,KIND,TENSE,_), {verbParticipleAux(V,AUX)},
  modal(MODAL,KIND,MODTENSE,_),
  {tense2inf(AUXTENSE,TENSE,MODTENSE,INFTENSE)}.
%VERB-AUX-MODAL passive (gefahren werden können)
verbInfinite([V,werd,MODAL],werd,V,KIND,INFTENSE)-->
  verbInfinite([V,werd],MODAL,V,KIND,INF0TENSE),
  modal(MODAL,KIND,MODTENSE,_),
  {tense2inf(INF0TENSE,MODTENSE,INFTENSE)}.
%AUX+VERB-werden-MODAL passive (haben gefahren werden können)
verbInfinite([V|INFINITE],MODAL,V,KIND,INFTENSE)-->
  aux(AUX,KIND,AUXTENSE,_), verbInfinite(INFINITE,MODAL,V,KIND,INF0TENSE),
  {verbParticipleAux(V,AUX)},
  {tense2inf(AUXTENSE,INF0TENSE,INFTENSE)}.

%denied infinite structure
%NOT: not adverb, if any
verbInfinite(VERB,_AUX,V,KIND,TENSE,_NOT)-->
  verbInfinite(VERB,_AUX,V,KIND,TENSE).
verbInfinite(VERB,_AUX,V,KIND,TENSE,NOT)-->
  adverb(NOT,neg,base),
  verbInfinite(VERB,_AUX,V,KIND,TENSE).



%drink - drinking - drunk
verbInfinite([V],KIND,TENSE,NUMPERS)-->
  verb(V,KIND,INF,NUMPERS), {inf2Tense(INF,TENSE,NUMPERS)}.
%...look forward - verb with verb-prefix
verbInfinite([V-ADV],KIND,TENSE,NUMPERS)-->
  verb(V,KIND,INF,NUMPERS), {inf2Tense(INF,TENSE,NUMPERS)},
  adverb(ADV,part,base), {verbPart_(_,V,ADV,_)}.
%...(I may)...have drunk
%verbInfinite([AUX1|VERB],KIND,TENSE,NUMPERS)-->
%  aux(AUX1,A1K,A1T,NUMPERS),
%  verbInfinite(VERB,KIND,VT,_),
%  {once(auxTense2Inf(AUX1,A1T,VT,TENSE))}.
%...(I may) have never drunk ...
%Special case: adverb after the aux-verb (negation is forbidden)
verbInfinite([AUX1|VERB],KIND,TENSE,NUMPERS)-->
  aux(AUX1,A1K,A1T,NUMPERS), verbInfinite(VERB,KIND,VT,_),
  {once(auxTense2Inf(AUX1,A1T,VT,TENSE))}.

%you 'hardly drink' much wine
verbConj(verb(V,KIND,TENSE,NUMPERS),ADV,'',TENSE,NUMPERS)-->
   verb(V,KIND,TENS,NUMPERS),
   {tense2Conj(TENS,TENSE)}.
% verbConj(verb([MODAL|VERB],KIND,TENSE,NUMPERS),'',MODE,TENSE,NUMPERS)-->
%
%  modal(MODAL,MODE,MT,verb),
%  verbInfinite(VERB,KIND,VT,_), {once(auxTense2Conj(MT,VT,TENSE))}.
%adverb after aux-verb (she may have always been nice).
verbConj(verb([MODAL|VERB],KIND,TENSE,NUMPERS),ADV,MODE,TENSE,NUMPERS)-->
  modal(MODAL,MODE,MT,verb),
  verbInfinite(VERB,KIND,VT,_), {once(auxTense2Conj(MT,VT,TENSE))}.
verbConj(verb([AUX|VERB],KIND,TENSE,NUMPERS),'','',TENSE,NUMPERS)-->
  aux(AUX,AK,AT,NUMPERS),
  verbInfinite(VERB,KIND,VT,_), {once(auxTense2Conj(AUX,AT,VT,TENSE))}.


%verbalArgs(NPS)
%NPS:   recognized verbal arguments
%CASES: recognized cases (for noun phrases!)
%verbal (infinitive and participle) arguments and
%subclausal arguments must be the last!
verbalArgs([COMP],[CASE])-->
    comparison(COMP,CASE).



verbalArgs([],[],X,X):- true.  %Contralog!! fact!!
verbalArgs([CLAUSE],[clause])-->
  clause(CLAUSE,declarative,straight,full,_).
verbalArgs([connect(that,CLAUSE)],[clause(that)])-->
  punct(','), connect(dass,subclause(that)),
  subclause(CLAUSE,declarative,_TENSE,_REF,DEM),
  {emptyOptDem(DEM)}.
%verbalArgs([connect(CONN,CLAUSE)],[clause(if)])-->
%  punct(','), connect(CONN,subclause(if)),
%  subclause(CLAUSE,declarative,_TENSE,_REF,DEM),
%  {emptyOptDem(DEM)}.
verbalArgs([connect(IP,CLAUSE)],[clause(wh)])-->
  subClause(?(IP,CLAUSE),interrogative(SUB),straight,_COMPLETION),
  {once(SUB=wh;SUB=subj)}.
%let us 'go home'
verbalArgs([verbal(VP)],[verbal])-->
  verbalPhrase(VP,_MODE,inf,_NUMPERS,_X,DEM),
  {emptyOptDem(DEM)}.
%where to find a restaurant
verbalArgs([verbal(IP,VP)],[verbal(wh)])-->
  interrPhrase(IP,_,_,_KIND), infPart(zu),
  verbalPhrase(VP,_MODE,inf,_NUMPERS,_X,DEM), {emptyOptDem(DEM)}.
%'(Helga versucht) noch ein Bier zu trinken'
verbalArgs([verbal(to,VP)],[verbal(to)])-->
  zuInfiniteSubphrase(VP,_NUMPERS,_TENSE,'').
%i want 'to be a doctor'
verbalArgs([verbal(to,NPP)],[verbal(to)])-->
  infPart(zu), nominalPhrase(NPP,inf,_NUMPERS).
%we stopped 'talking'
verbalArgs([verbal(ing,VP)],[verbal(ing)])-->
  verbalPhrase(VP,_MODE,present(participle),_NUMPERS,_X,DEM),
  {emptyOptDem(DEM)}.
verbalArgs([NP|ARGS],[CASE|CASES])-->
  nounPhrase(NP,_GENDER,_NUMPERS,NPCASE),
  {accNomDatCase(NPCASE), (NP=refl(_,_)->CASE=refl;CASE=NPCASE)},
  verbalArgs(ARGS,CASES).


%verbalPhrase(VP,MODE,TENSE,NUMPERS,PEND,PDEM).
%VP: Verbal phrase structure
%MODE: indicative;conjunctive;imperative
%TENSE: tense of the verb
%NUMPERS: number/person of verb (pl/1..3;sg/1..3)
%PEND: Prolog variable to hold a pendant argument
%      (that is missing from arguments)
%PDEM: syntactic demand of evt pendant verbal argument or 'none'
%
%eg. Which apple do you like? - Welchen Apfel magst du?
%eg. In which town do you want to live?
%
%VCONJ-ARGS
verbalPhrase(vp(verb(VERB,MODE,TENSE,NUMPERS),
                ADVERBS,MODE,TENSE,ARGS,FREE),MODE,TENSE,NUMPERS,X,PDEM)-->
%an active verb in simple tense with an argument structure
%'(Ich) siehe (nicht)? meinen Freund'
%neither nominal phrases, nor passive should be recognized
    auxModVerb(VERB,MODE,TENSE,NUMPERS,_MODAL,_AUX,NOT),
    optFreeAdverbs(ADVS),
    {VERB\==sei, tenseConj(TENSE)},
    {verbAPrefixFrame(VERB,[nom|FRAMES])},
    {butLastNominalDemands(FRAMES)},
    {once((var(NOT),ADVERBS=ADVS;ADVERBS=[NOT|ADVS]))},
    verbalArgs(ARGS0,CASES),
    {but1DemSat(FRAMES,CASES,ARGS0,ARGS,PDEM,X)},
    optFreeArgs(FREE),
    {\+ freeMatchesFrame(FRAMES,FREE)}.

%VCONJ-ARGS-PREFIX ... verb prefix at the end
%Das 'hängt vom Wetter ab'
verbalPhrase(vp(verb(VERB^PREF,MODE,TENSE,NUMPERS),
                ADVERBS,MODE,TENSE,ARGS,FREE),MODE,TENSE,NUMPERS,X,PDEM)-->
    auxModVerb(VERB,MODE,TENSE,NUMPERS,_MODAL,_AUX,NOT),
    optFreeAdverbs(ADVS),
    {VERB\==sei, tenseConj(TENSE)},
    {verbAPrefixFrame(VERB,[nom|FRAMES])},
    {butLastNominalDemands(FRAMES)},
    {once((var(NOT),ADVERBS=ADVS;ADVERBS=[NOT|ADVS]))},
    verbalArgs(ARGS0,CASES),
    {but1DemSat(FRAMES,CASES,ARGS0,ARGS,PDEM,X)},
    optFreeArgs(FREE),
    {\+ freeMatchesFrame(FRAMES,FREE)},
    verbPrefix(PREF), {verbPrefix_(_,VERB,PREF,_)}.

% VCONJ-PREFIX-ARGZUINF ... verb prefix before zu infinitive arg
% Es 'fing an zu regnen'
verbalPhrase(vp(verb(VERB^PREF,MODE,TENSE,NUMPERS),
                ADVERBS,MODE,TENSE,ARGS,FREE),MODE,TENSE,NUMPERS,_X,_PDEM)-->
    auxModVerb(VERB,MODE,TENSE,NUMPERS,_MODAL,_AUX,NOT),
    optFreeAdverbs(ADVS),
    {VERB\==sei, tenseConj(TENSE)},
    verbPrefix(PREF), {verbPrefix_(_,VERB,PREF,_)},
    {verbAPrefixFrame(PREF-VERB,[nom,verbal(to)])},
    {once((var(NOT),ADVERBS=ADVS;ADVERBS=[NOT|ADVS]))},
    verbalArgs(ARGS,[verbal(to)]),
    optFreeArgs(FREE).

%AUX-ARGS-VINF
verbalPhrase(vp(verb([AUXMOD|VERB],MODE,TENSE,NUMPERS),
               ADVERBS,MODE,TENSE,ARGS,FREE),MODE,TENSE,NUMPERS,X,PDEM)-->
%an active verb in any tense with an argument structure, eg.
% '(Ich) habe meinen Freund (nicht)? gesehen'
% '(Ich) konnte meinen Freund (nicht)? sehen'
% '(Du) wirst Kuchen essen'
    auxMod(AUXMOD,MODE,CTENSE,NUMPERS,MODAL,AUX,NO), {var(NO)},
    optFreeAdverbs(ADVS),
    verbalArgs(ARGS0,CASES),
    optFreeArgs(FREE),
    verbInfinite(VERB,MODAL,VMAIN,KIND,INFTENSE,NOT),
    {VMAIN\==sei}, {AUX\==werd}, {KIND\==imperative},
    {conjinf2tense(CTENSE,INFTENSE,TENSE), \+ functor(TENSE,passive,_)},
    {(var(NOT)->ADVERBS=ADVS;ADVERBS=[NOT|ADVS])},
    {tensePerfect(TENSE)->verbParticipleAux(VMAIN,AUX);true},
    {verbAPrefixFrame(VMAIN,[nom|FRAMES])},
    {\+ freeMatchesFrame(FRAMES,FREE)},
    {butLastNominalDemands(FRAMES)},
    {but1DemSat(FRAMES,CASES,ARGS0,ARGS,PDEM,X)}.


%AUX-ARGS-VINF-RELSUBCL
verbalPhrase(vp(verb([AUXMOD|VERB],MODE,TENSE,NUMPERS),
               ADVERBS,MODE,TENSE,ARGS,FREE),MODE,TENSE,NUMPERS,X,PDEM)-->
%verbal sentence frame with argument+relative clause, eg.
% '(Mein Vater) hat etwas gebracht, womit er arbeiten kann
% '(Ich) konnte meinen Freund (nicht)? sehen'
    auxMod(AUXMOD,MODE,CTENSE,NUMPERS,MODAL,AUX,NO), {var(NO)},
    optFreeAdverbs(ADVS),
    optFreeArgs(FREE),
    verbalArgs(ARGS0,CASES),
    verbInfinite(VERB,MODAL,VMAIN,KIND,INFTENSE,NOT),
    {conjinf2tense(CTENSE,INFTENSE,TENSE), \+ functor(TENSE,passive,_)},
    {VMAIN\==sei}, {AUX\==werd}, {KIND\==imperative},
    {(var(NOT)->ADVERBS=ADVS;ADVERBS=[NOT|ADVS])},
    {verbParticipleAux(VMAIN,AUX)}, {verbAPrefixFrame(VMAIN,[nom|FRAMES])},
    {\+ freeMatchesFrame(FRAMES,FREE)}, {allDemsSat(FRAMES,CASES)},
    {replaceLast(ARGS0,ARGS,LAST,that(back,LAST,RELATIVE,CL))},
    punct(','),
    relative(RELATIVE,_GENDER,_NUM,REFCASE), {REFCASE\==nom},
    subclause(CL,declarative,SUBTENSE,_X,PDEM),
    {emptyOptDem(PDEM),
    SUBTENSE \= inf, SUBTENSE \= past(participle),
    SUBTENSE \= present(participle)}.


%verbalClause(CLAUSE,NUMPERS,MODE,KIND,TENSE,SUBJ,REF,DEM)
%The predicate handles pendent subject
%and pendent/missing piece of a nounphrase in argument list
%This is used for topicalization
%Eg: complete pendent arg:
%    - which apple do you want
%
% @arg CLAUSE clause structure
% @arg NUMPERS sg;pl / 1..3 - number and person of clause
% @arg MODE conjunctive;indicative;imperative
% @arg KIND declarative;interrogative(yn);interrogative(wh)
% @arg SUBJ    subject of the clause
% @arg REF Referred NP structure
%      (eg. a topic, usually a var) embedded in/referred by CLAUSE
% @arg DEM: syntactic demand for missing argument/piece of arg


%
% subject+verbal phrase(+free args after)
% NPSUBJ-VP-FREE2
% 'Sie darf das Buch nicht lesen'.
verbalClause(clause(verbal/KIND,straight,[],SUBJ,VP,NUMPERS),
             NUMPERS,MODE,KIND,TENSE,SUBJ,REF,PDEM)-->
   nounPhrase(SUBJ,_GENDER,NUMPERS,nom),
   {\+functor(SUBJ,that,4)},
   verbalPhrase(VP,MODE,TENSE,NUMPERS,REF,PDEM), {KIND=declarative},
   {TENSE \= inf, TENSE \= past(participle), TENSE \= present(participle)} .
%John, den ich schon gut kenne, war sehr nett.
verbalClause(clause(verbal/KIND,straight,[],SUBJ,VP,NUMPERS),
             NUMPERS,MODE,KIND,TENSE,SUBJ,REF,PDEM)-->
   nounPhrase(SUBJ,_GENDER,NUMPERS,nom),
   {functor(SUBJ,that,4)}, punct(','),
   verbalPhrase(VP,MODE,TENSE,NUMPERS,REF,PDEM), {KIND=declarative},
   {TENSE \= inf, TENSE \= past(participle), TENSE \= present(participle)} .

%AUX-ARGS-VINF
passiveVerbalPhrase(vp(verb([AUXMOD|VERB],MODE,TENSE,NUMPERS),
               ADVERBS,MODE,TENSE,ARGS,FREES),TENSE,NUMPERS,X,PDEM)-->
%an active verb in any tense with an argument structure
%eg.
% '(Mein Freund) wurde von dir (nicht)? gesehen'
% '(Der Apfel) ist von dir (nicht)? gegessen worden'
% '(Der Vertrag) kann heute noch unterschrieben werden'.
% '(Wie viel) Bücher werden hier gesehen'?
    auxMod(AUXMOD,MODE,CTENSE,NUMPERS,MODAL,AUX,NO), {var(NO)},
    {once(AUXMOD=werd;modal(AUXMOD);aux(AUXMOD))},
    optFreeAdverbs(ADVS),
    verbalArgs(ARGS0,CASES),
    optFreeArgs(FREES),
%    verbInfinite(VERB,INFAUXMOD,VMAIN,KIND,INFTENSE,NOT),
    verbInfinite(VERB,AUXMODINF,VMAIN,KIND,INFTENSE,NOT),
    {KIND \== imperative}, {AUXMODINF==werd},
    {conjinf2tense(CTENSE,INFTENSE,TENSE), functor(TENSE,passive,_)},
    {once(\+tensePerfect(TENSE);verbParticipleAux(VMAIN,AUX))},
    {var(NOT)->ADVERBS=ADVS;ADVERBS=[NOT|ADVS]},
    {verbAPrefixFrame(VMAIN,[nom,acc|FRAMES])},
    {\+ freeMatchesFrame(FRAMES,FREES)},
    {butLastNominalDemands(FRAMES)},
    {once((but1DemSat([von(dat)|FRAMES],CASES,ARGS0,ARGS,PDEM,X),
          PDEM\==von(dat));
          but1DemSat(FRAMES,CASES,ARGS0,ARGS,PDEM,X))}.

% a passive verbal clause
% objectAsSubject+passive verbal phrase(+free args after)
% NPOBJ-VP-FREE2
verbalClause(clause(verbal/KIND,straight,[],OBJ,VP,NUMPERS),
             NUMPERS,indicative,KIND,TENSE,OBJ,REF,PDEM)-->
   nounPhrase(OBJ,_GENDER,NUMPERS,nom),
   passiveVerbalPhrase(VP,TENSE,NUMPERS,REF,PDEM),
   {TENSE \= inf, TENSE \= past(participle), TENSE \= present(participle)} .


%invertedVerbalPhrase(VP,NUMPERS,TENSE,SUBJ,REF,DEM)
%The predicate recognizes inverted verbal phrases
%together with the included subject
%It also handles pendent verbal arguments
%
% @arg VP verbal phrase structure
% @arg NUMPERS sg/1...pl/3 - number and person of VP
% @arg TENSE ... tense expression ...
% @arg SUBJ  subject of the inverted clause
% @arg REF Referred NP structure
%      (eg. a topic, usually a var) embedded in/referred by CLAUSE
% @arg DEM: syntactic demand for missing argument/piece of arg
%

%'(gestern) aß ich 3 Äpfel'
%'(was für ein Auto) sehen wir'
invertedVerbalPhrase(vp(verb(VERB,MODE,TENSE,NUMPERS),
           ADVERBS,MODE,TENSE,ARGS,FREES),
                     NUMPERS,TENSE,SUBJ,REF,DEM)-->
   verb(VERB,MODE,TENSE,NUMPERS,NOT),
   {var(NOT), VERB \== sei, VERB \== werd, TENSE \= infinite},
   nounPhrase(SUBJ,_GENDER,NUMPERS,nom),
   optFreeAdverbs(ADVERBS),
   verbalArgs(ARGS0,CASES),
   optFreeArgs(FREES),
   {verbAPrefixFrame(VERB,[nom|FRAMES])},
   {butLastNominalDemands(FRAMES)},
   {but1DemSat(FRAMES,CASES,ARGS0,ARGS,DEM,REF)}.

%verbal particle at the end of the sentence (over the arguments)
%machst du die Tür zu?
invertedVerbalPhrase(vp(verb(?(PREF)-VERB,MODE,TENSE,NUMPERS),
           ADVERBS,MODE,TENSE,ARGS,FREES),
                     NUMPERS,TENSE,SUBJ,REF,DEM)-->
   verb(VERB,MODE,TENSE,NUMPERS,NOT), {atom(VERB)},
   {once(TENSE=present;TENSE=past)},
   {var(NOT), VERB \== sei, VERB \== werd},
   nounPhrase(SUBJ,_GENDER,NUMPERS,nom),
   optFreeAdverbs(ADVERBS),
   verbalArgs(ARGS0,CASES),
   optFreeArgs(FREES),
   verbPrefix(PREF), {verbPrefix_(PREF,separable), verbPrefix_(_,VERB,PREF,_)},
   {verbAPrefixFrame(PREF-VERB,[nom|FRAMES])},
   {butLastNominalDemands(FRAMES)},
   {but1DemSat(FRAMES,CASES,ARGS0,ARGS,DEM,REF)}.

%verbal particle at the end of the sentence
% (inside of verb arguments, as subclauses)li
%machst du die Tür zu?
invertedVerbalPhrase(vp(verb(?(PREF)-VERB,MODE,TENSE,NUMPERS),
           ADVERBS,MODE,TENSE,ARGS,FREES),
                     NUMPERS,TENSE,SUBJ,REF,DEM)-->
   verb(VERB,MODE,TENSE,NUMPERS,NOT), {atom(VERB)},
   {once(TENSE=present;TENSE=past)},
   {var(NOT), VERB \== sei, VERB \== werd},
   nounPhrase(SUBJ,_GENDER,NUMPERS,nom),
   optFreeAdverbs(ADVERBS),
   verbPrefix(PREF), {verbPrefix_(PREF,separable), verbPrefix_(_,VERB,PREF,_)},
   {verbAPrefixFrame(PREF-VERB,[nom|FRAMES]), FRAMES=[verbal(_)]},
   {butLastNominalDemands(FRAMES)},
   verbalArgs(ARGS0,CASES),
   {but1DemSat(FRAMES,CASES,ARGS0,ARGS,DEM,REF)},
   optFreeArgs(FREES).


%'(gestern) habe ich 3 Äpfel (nicht)? gegessen'
%'ist der Apfel gegessen worden'
%passive tenses are excluded here
invertedVerbalPhrase(vp(verb([AUXMOD|VERB],MODE,TENSE,NUMPERS),
           ADVERBS,MODE,TENSE,ARGS,[]),
                     NUMPERS,TENSE,SUBJ,REF,DEM)-->
   auxMod(AUXMOD,MODE,CTENSE,NUMPERS,AUX,MODAL,NO), {var(NO)},
   nounPhrase(SUBJ,_GENDER,NUMPERS,nom),
   optFreeAdverbs(ADVS),
   verbalArgs(ARGS0,CASES),
   verbInfinite(VERB,MODAL,VMAIN,KIND,INFTENSE,NOT),
   {VMAIN\==sei, AUXMOD \== werd }, {KIND\==imperative},
   {conjinf2tense(CTENSE,INFTENSE,TENSE), \+ functor(TENSE,passive,_)},
   {once(\+tensePerfect(TENSE);verbParticipleAux(VMAIN,AUX))},
   {var(NOT)->ADVERBS=ADVS;ADVERBS=[NOT|ADVS]},
   {verbAPrefixFrame(VERB,[nom|FRAMES])},
   {butLastNominalDemands(FRAMES)},
   {but1DemSat(FRAMES,CASES,ARGS0,ARGS,DEM,REF)}.


%invertedVerbalClause(CLAUSE,NUMPERS,KIND,TENSE,FREES,SUBJ,REF,DEM)
%The predicate handles pendent subject
%and pendent/missing piece of a noun phrase in argument list
%This is used for topicalization
%Eg: complete pendent arg:
%    - welcheN?? Apfel willst du?
% @arg CLAUSE clause structure
% @arg NUMPERS sg/1...pl/3 - number and person of missing argument
% @arg KIND ...
% @arg TENSE ... tense expression ...
% @arg FREES ... list of optional free arguments...
% @arg SUBJ  subject of the inverted clause
% @arg REF Referred NP structure
%      (eg. a topic, usually a var) embedded in/referred by CLAUSE
% @arg DEM: syntactic demand for missing argument/piece of arg

%interrogated/inverted negated passive verbal clause
% a y/n question with aux and infinitive verb
%AUXMOD-NPSUBJ-ARGS-FREE-VERBINF
%'Muss das Buch gelesen werden'? ez itt biztosan nem fog működni!!!
%(Warum) 'hat er mir ein Buch gegeben'?
invertedVerbalClause(clause(verbal/KIND,inverted,FREES,SUBJ,VP,NUMPERS),
                     NUMPERS,KIND,TENSE,FREES,SUBJ,REF,DEM)-->
  optFreeArgs(FREES), invertedVerbalPhrase(VP,NUMPERS,TENSE,SUBJ,REF,DEM).

%inverted verbal clause
%a y/n question with aux and infinitive verb
%AUX-NPSUBJ-ADVS-VERB-ARGS-FREE
%(Bist du einmal schon dort gewesen)?
%(Gestern habe ich meine Arbeit beendet)?
invertedVerbalClause(clause(verbal/KIND,inverted,FREES,SUBJ,
        vp(verb([AUXMOD|INFVERB],MODE,TENSE,NUMPERS),
           ADVERBS,MODE,TENSE,ARGS,VPFREES),NUMPERS),
                     NUMPERS,KIND,TENSE,FREES,SUBJ,REF,DEM)-->
   optFreeArgs(FREES),
   auxMod(AUXMOD,MODE,CTENSE,NUMPERS,_AUX,MODAL,NO), {var(NO)},
   nounPhrase(SUBJ,_GENDER,NUMPERS,nom),
   optFreeAdverbs(ADVS),
   verbalArgs(ARGS0,CASES),
   optFreeArgs(VPFREES),
   verbInfinite(INFVERB,MODAL,VMAIN,IKIND,INFTENSE,NOT),
   {VMAIN \== sei, AUXMOD \== werd},
   {conjinf2tense(CTENSE,INFTENSE,TENSE)}, {\+ functor(TENSE,passive,_)},
   {var(NOT)->ADVERBS=ADVS;ADVERBS=[NOT|ADVS]},
   {verbAPrefixFrame(VMAIN,[nom|FRAMES])},
   {\+ freeMatchesFrame(FRAMES,VPFREES)}, {FREES \= [prep(by,_)|_] },
   {but1DemSat(FRAMES,CASES,ARGS0,ARGS,DEM,REF)}.

%inverted verbal clause with aux and zu infinite argument
% a y/n question with aux and infinitive verb
%AUX-NPSUBJ-VINF-ZUCLAUSE-FREE
%'Könntest du aufhören schlimme Geschichte zu erzählen'?
invertedVerbalClause(clause(verbal/KIND,inverted,FREES,SUBJ,
        vp(verb([AUXMOD|INFVERB],MODE,TENSE,NUMPERS),
           ADVERBS,MODE,TENSE,ARGS,VPFREES),NUMPERS),
                     NUMPERS,KIND,TENSE,FREES,SUBJ,REF,DEM)-->
   optFreeArgs(FREES),
   auxMod(AUXMOD,MODE,CTENSE,NUMPERS,_AUX,MODAL,NO), {var(NO)},
   nounPhrase(SUBJ,_GENDER,NUMPERS,nom),
   optFreeAdverbs(ADVS),
   verbInfinite(INFVERB,MODAL,VMAIN,IKIND,INFTENSE,NOT),
   verbalArgs(ARGS0,CASES), {CASES=[verbal(to)]},
   optFreeArgs(VPFREES),
   {VMAIN \== sei, AUXMOD \== werd},
   {conjinf2tense(CTENSE,INFTENSE,TENSE)}, {\+ functor(TENSE,passive,_)},
   {var(NOT)->ADVERBS=ADVS;ADVERBS=[NOT|ADVS]},
   {verbAPrefixFrame(VMAIN,[nom|FRAMES])},
   {\+ freeMatchesFrame(FRAMES,VPFREES)}, {FREES \= [prep(by,_)|_] },
   {but1DemSat(FRAMES,CASES,ARGS0,ARGS,DEM,REF)}.


%AUX-SUBJ-FREEADVS-ARGS-FREEARGS-VPASSINF
invertedPassiveVerbalClause(clause(verbal/KIND,inverted,FREES,SUBJ,
        vp(verb([AUXMOD|INFVERB],MODE,TENSE,NUMPERS),
           ADVERBS,MODE,TENSE,ARGS,VPFREES),NUMPERS),
                     NUMPERS,KIND,TENSE,[],SUBJ,REF,PDEM)-->
%a passive verb in any tense with an argument structure
%eg.
%??? 'Muss das Buch gelesen werden'?
%??? 'Wurde mein Freund von dir nicht gesehen'?
%??? 'Ist der Apfel von dir nicht gegessen worden'?
    auxMod(AUXMOD,MODE,CTENSE,NUMPERS,MODAL,AUX,NO), {var(NO)},
    {once(AUXMOD=werd;modal(AUXMOD);aux(AUXMOD))},
    nounPhrase(SUBJ,_GENDER,NUMPERS,nom),
    optFreeAdverbs(ADVS),
    verbalArgs(ARGS0,CASES),
    optFreeArgs(VPFREES),
    verbInfinite(INFVERB,MODAL,VMAIN,KIND,INFTENSE,NOT),
    {KIND \== imperative},
    {conjinf2tense(CTENSE,INFTENSE,TENSE), functor(TENSE,passive,_)},
    {once(\+tensePerfect(TENSE);verbParticipleAux(VMAIN,AUX))},
    {var(NOT)->ADVERBS=ADVS;ADVERBS=[NOT|ADVS]},
    {verbAPrefixFrame(VMAIN,[nom,acc|FRAMES])},
    {\+ freeMatchesFrame(FRAMES,FREES)},
    {butLastNominalDemands(FRAMES)},
    {once((but1DemSat([von(dat)|FRAMES],CASES,ARGS0,ARGS,PDEM,REF),
          PDEM\==von(dat));
          but1DemSat(FRAMES,CASES,ARGS0,ARGS,PDEM,REF))}.


%subphrase(VP,NUMPERS,TENSE,SUBJ,REF,DEM)
%The predicate recognizes subordinated phrases
% without the referring pronoun in front
% that must match pendent verbal arguments
%
% @arg VP verbal phrase structure
% @arg NUMPERS sg/1...pl/3 - number and person of VP
% @arg TENSE ... tense expression ...
% @arg SUBJ  subject of the inverted clause
% @arg REF Referred NP structure
%      (eg. a topic, usually a var) embedded in/referred by CLAUSE
% @arg DEM: syntactic demand for missing argument/piece of arg
%
%an active verb in simple tense with an argument structure
%(das Mädchen, das) schön singt
%(mein Freund, den) ich nicht siehe
%(FREE)-ARGS-VADVS-VCONJ
subphrase(vp(verb(VERB,MODE,TENSE,NUMPERS),
                ADVERBS,MODE,TENSE,ARGS,FREE),TENSE,NUMPERS,X,PDEM)-->
    optFreeArgs(FREE),
    verbalArgs(ARGS0,CASES),
    optFreeAdverbs(ADVS),
    auxModVerb(VERB,MODE,TENSE,NUMPERS,_MODAL,_AUX,NOT),
    {VERB\==sei, tenseConj(TENSE)},
    {verbAPrefixFrame(VERB,[nom|FRAMES])},
    {butLastNominalDemands(FRAMES)},
    {once((var(NOT),ADVERBS=ADVS;ADVERBS=[NOT|ADVS]))},
    {but1DemSat(FRAMES,CASES,ARGS0,ARGS,PDEM,X)},
    {\+ freeMatchesFrame(FRAMES,FREE)}.
%nominal subphrase (verb==sei, argument:nominalPredicate)
%FREE-NOMPRED-VADVS-VCONJ
subphrase(vp(verb(VERB,MODE,TENSE,NUM/PERS),
                ADVERBS,MODE,TENSE,[NOMPRED],FREE),TENSE,NUM/PERS,X,PDEM)-->
    optFreeArgs(FREE),
    nominalPredicate(NOMPRED,NUM),
    optFreeAdverbs(ADVS),
    auxModVerb(VERB,MODE,TENSE,NUM/PERS,_MODAL,_AUX,NOT),
    {VERB==sei, tenseConj(TENSE)},
%    {verbAPrefixFrame(VERB,[nom|FRAMES])},
%    {butLastNominalDemands(FRAMES)},
    {once((var(NOT),ADVERBS=ADVS;ADVERBS=[NOT|ADVS]))}.
%    {but1DemSat(FRAMES,CASES,ARGS0,ARGS,PDEM,X)},
%    {\+ freeMatchesFrame(FRAMES,FREE)}.

%VCONJ-ARGS-PREFIX
%'...hängt vom Wetter ab'
%subphrase(vp(verb(PREF-VERB,MODE,TENSE,NUMPERS),
%                ADVERBS,MODE,TENSE,ARGS,FREE),TENSE,NUMPERS,X,PDEM)-->
%  verbalPhrase(vp(verb(VERB,MODE,TENSE,NUMPERS),
%                ADVERBS,MODE,TENSE,ARGS,FREE),MODE,TENSE,NUMPERS,X,PDEM),
%                {atom(VERB)}, verbPrefix(PREF),
%                {verbPrefix_(_,VERB,PREF,_)}.

%an active verb in any tense with an argument structure eg.
%(das Mädchen, das) so schön gesungen hat
%????? '(Ich) habe meinen Freund (nicht)? gesehen'
%????? '(Ich) konnte meinen Freund (nicht)? sehen'
%AUX-ARGS-VINF
subphrase(vp(verb([AUXMOD|VERB],MODE,TENSE,NUMPERS),
               ADVERBS,MODE,TENSE,ARGS,FREE),TENSE,NUMPERS,X,PDEM)-->
    optFreeArgs(FREE),
    verbalArgs(ARGS0,CASES),
    verbInfinite(VERB,MODAL,VMAIN,KIND,INFTENSE,NOT),
    optFreeAdverbs(ADVS),
    auxModVerb(AUXMOD,MODE,CTENSE,NUMPERS,MODAL,AUX,NO), {var(NO)},
    {VMAIN\==sei},
    {conjinf2tense(CTENSE,INFTENSE,TENSE), \+ functor(TENSE,passive,_)},
    {(var(NOT)->ADVERBS=ADVS;ADVERBS=[NOT|ADVS])},
    {verbParticipleAux(VMAIN,AUX)},
    {verbAPrefixFrame(VMAIN,[nom|FRAMES])},
    {\+ freeMatchesFrame(FRAMES,FREE)},
    {butLastNominalDemands(FRAMES)},
    {but1DemSat(FRAMES,CASES,ARGS0,ARGS,PDEM,X)}.

% nominal subphrase: 'sei' in any tense with nominal predicate
% '(Ich) habe meinen Freund (nicht)? gesehen' ????? '(Ich) konnte meinen
% Freund (nicht)? sehen'
%NPPRED-VINF-VAUX
subphrase(vp(verb([AUXMOD|VERB],MODE,TENSE,NUM/PERS),
               ADVERBS,MODE,TENSE,[NOMPRED],FREE),TENSE,NUM/PERS,X,PDEM)-->
    optFreeArgs(FREE),
    nominalPredicate(NOMPRED,NUM),
    verbInfinite(VERB,MODAL,SEIN,KIND,INFTENSE,NOT),
    {SEIN==sei},
    optFreeAdverbs(ADVS),
    auxModVerb(AUXMOD,MODE,CTENSE,NUM/PERS,MODAL,AUX,NO), {var(NO)},
    {conjinf2tense(CTENSE,INFTENSE,TENSE), \+ functor(TENSE,passive,_)},
    {(var(NOT)->ADVERBS=ADVS;ADVERBS=[NOT|ADVS])},
    {verbParticipleAux(SEIN,AUX)}.


%subclause(CLAUSE,KIND,TENSE,REF,PDEM)
% @arg CLAUSE  clause structure
% @arg KIND    declarative;interrogative(_);imperative kind of sentence
% @arg TENSE   tense expression ...
% @arg REF     reference variable of pendent argument
% @arg PDEM    case constraint of pendent argument
%
% (CONJ)-ARGS-VERB-AUX
% nominal/subjectal subclause (conjective is the subject)
% (Ich sehe die Frau, die) das Buch nicht lesen kann.
% (ich mag das Mädchen , das) schön singt
subclause(clause(verbal/DECL,opposite,[],SUBJ,
                       VP,CLNUMP),DECL,TENSE,SUBJ,nom)-->
   subphrase(VP,TENSE,CLNUMP,_REF,PDEM), {DECL=declarative},
   {emptyOptDem(PDEM)},
   {TENSE \= inf, TENSE \= past(participle), TENSE \= present(participle)} .
% argumental subclause (conjective is ein verbal argument)
% ???(Du hast das Auto, mit dem) ich gestern gefahren bin (, gesehen.)
subclause(clause(verbal/DECL,opposite,[],SUBJ,VP,CLNUMP),
             DECL,TENSE,REF,PDEM)-->
   nounPhrase(SUBJ,_GENDER,CLNUMP,nom),
   subphrase(VP,TENSE,CLNUMP,REF,PDEM), {DECL=declarative},
   {TENSE \= inf, TENSE \= past(participle), TENSE \= present(participle)}.
%   ({emptyOptDem(PDEM)}->{true};
%   {demSat(PDEM,CASE)}, {REF=art(ART,weak,GENDNUM,CASE)}).


%where to build a house
%WH-to-VInf-VArgs-FREE
% interrogativeVerbalClause(clause(interrogative,inverted,FREE,wh(WH),VP))-->
% verbalPhrase(VP,MODE,TENSE,NUM/PERS,PEND,PDEM),
% optFreeArgs(FREEARGS), interrogative(WH,KIND), {KIND \= nom}.


shortVerbalClause(clause(verbal/declarative,straight,FREES,NP,
                         vp(verb(AUX,MODE,TENSE,NUMPERS),
                            NOT,MODE,TENSE,[],[]),NUMPERS),
             NUMPERS,declarative,TENSE)-->
   optFreeArgs(FREES),
   nounPhrase(NP,_GENDER,NUMPERS,nom),
   {\+ functor(NP,actpart,_), \+ functor(NP,passpart,_)},
   auxMod(AUX,MODE,TENSE,NUMPERS,AUX,_MODAL,NOT),
   {ignore(NOT='')},
   {TENSE \= inf, TENSE \= past(participle), TENSE \= present(participle)}.
:- if(false).
shortVerbalClause(clause(verbal/declarative,straight,FREE,NP,
                         vp(verb([AUX,''],MODE,TENSE,NUMPERS),
                            NOT,MODE,TENSE,[],[]),NUMPERS),
             NUMPERS,declarative,TENSE)-->
   optFreeArgs(FREE),
   nounPhrase(NP,_GENDER,NUMPERS,nom),
   {\+ functor(NP,actpart,_), \+ functor(NP,passpart,_)},
   auxMod(AUX,MODE,TENSE,NUMPERS,AUX,_MODAL,NOT),
   {ignore(NOT='')},
   {TENSE \= inf, TENSE \= past(participle), TENSE \= present(participle)}.
:- endif.
shortInvertedVerbalClause(clause(verbal/declarative,inverted,[],NP,
                         vp(verb([AUX,''],MODE,TENSE,NUMPERS),
                            NOT,MODE,TENSE,[],[]),NUMPERS),
             NUMPERS,declarative,TENSE)-->
   auxMod(AUX,MODE,TENSE,NUMPERS,AUX,_MODAL,NOT),
   {ignore(NOT='')},
   {TENSE \= inf, TENSE \= past(participle), TENSE \= present(participle)},
   nounPhrase(NP,_GENDER,NUMPERS,nom),
   {\+ functor(NP,actpart,_), \+ functor(NP,passpart,_)}.


%predicate is nominal, etc.'(my car is) RED/a vehicle' ????
%nominal predicate may refer to the subject
%nominalPredicate(NPP,NUM).
%?NUM: sg/pl
%-NPP: noun phrase structure
%
%NP: you are 'a doctor' -- NUM agreement!
%NP: you are 'in a car' -- no NUM agreement!
nominalPredicate(NP,NUM)-->
    nounPhrase(NP,_GENDER,NUM/_PERS,CASE), {NP \= refl(_,_)},
    {once(CASE=nom;compound(CASE))}.
nominalPredicate(adv(ADV,KIND,base),_NUM)-->
    adverb(ADV,KIND,base), {nomPredAdvKind(KIND)}.
%possPronouns are recognized as nounphrases
%nominalPredicate(possPronoun(OWNER,OGENDER,GENDNUM,CASE),NUM)-->
%    possPronoun(OWNER,OGENDER,GENDNUM,CASE),
%    {gendnum2GenderNumber(GENDNUM,_GENDER,NUM)}.
%ADV-NP: you are 'really a doctor'
nominalPredicate(adverb(ADV,NUM,NP),NUM)-->
    adverb(ADV,KIND,base), {preAdjAdvKind(KIND), ADV \= wie},
    nounPhrase(NP,_GENDER,NUM/_PERS,nom), {NP \= refl(_,_)}.
%AP "really sweeter"
nominalPredicate(ADJP,_NUM)--> adjPhrase(ADJP,_GRADE,uninflected,_,_),
    {ADJP \= adjP(adverb(_,comp,base),_,_)}.
%ORD - 3-rd
nominalPredicate(ord(NR),NUM)-->numeral(_,NR,ord,weak,NUM,GENDER,CASE).
%less strong than you brother
nominalPredicate(COMP,_NUM)-->comparison(COMP,nom).
%(The temperature is) 30 degrees
nominalPredicate(QUANT,_NUMPERS)-->quantity(QUANT,_KIND,_,nom).
%Mein Auto ist schön, aber klein.
nominalPredicate(connected(CONN,NPP1,punct(,),NPP2),NUMP)-->
    nominalPredicate(NPP1,NUMP), punct(,), connect(CONN,coordinating),
    nominalPredicate(NPP2,NUMP).
%Mein Auto ist schön und klein.
nominalPredicate(connected(CONN,NPP1,NPP2),NUMP)-->
    nominalPredicate(NPP1,NUMP), connect(CONN,coordinating),
    {listConnective(CONN)}, nominalPredicate(NPP2,NUMP).
%(That is) where I keep all my stuff
nominalPredicate(SUBCL,_NUMPERS)-->
    clause(SUBCL,declarative,straight,full,_),
    {SUBCL=clause(_,_,[FREE],_,_,_,_),
    FREE=adverb(where,place,base)}.

%Nominal phrase in simple (present/past) tense with predicate
%(Apfel) ist rot
nominalPhrase(vp(verb(SEIN,MODE,TENSE,NUM/PERS),ADV,MODE,TENSE,[NPP],FREE),
                  TENSE,NUM/PERS)-->
    {SEIN=sei},
    verbConj(verb(SEIN,MODE,TENSE,NUM/PERS),ADV,_,TENSE,NUM/PERS),
    {TENSE \= futurePast},
    optFreeAdverbs(FREE),
    nominalPredicate(NPP,NUM), {NPP \= adj(verb(_,_),_)}.

%Nominal phrase in simple (present/past) tense w/o predicate
%(Apfel) ist rot
% nominalPhrase(vp(verb(SEIN,MODE,TENSE,NUM/PERS),ADV,MODE,TENSE,[],FREE),
%
%                  TENSE,NUM/PERS)-->
%    {SEIN=sei},
%    verbConj(verb(SEIN,MODE,TENSE,NUM/PERS),ADV,_,TENSE,NUM/PERS),
%    {TENSE \= futurePast},
%    optFreeAdverbs(FREE).

%(Apfel) ist rot gewesen
nominalPhrase(vp(verb([SEIN|VERBINF],MODE,TENSE,NUM/PERS),
                 ADV,MODE,TENSE,[NPP],FREE),
                  TENSE,NUM/PERS)-->
    {SEIN=sei},
    verbConj(verb(SEIN,MODE,CTENSE,NUM/PERS),ADV,_,CTENSE,NUM/PERS),
    {CTENSE \= futurePast},
    optFreeAdverbs(FREE),
    nominalPredicate(NPP,NUM), {NPP \= adj(verb(_,_),_)},
    verbInfinite(VERBINF,_MODAL,SEIN,MODE,INFTENSE,NOT),
    {(var(NOT)->ADVERBS=ADVS;ADVERBS=[NOT|ADVS])},
    {verbAPrefixFrame(SEIN,[nom,nom])},
    {conjinf2tense(CTENSE,INFTENSE,TENSE), \+ functor(TENSE,passive,_)}.


%who 'are you'/'am I' - this is predicate interrogative
%'you' is subject
predInterrogativeNominalClause(clause(nominal/KIND,inverted,[],NPS,
       vp(verb(V,VKIND,TENSE,NUM/PERS),ADV,MODE,TENSE,[''],FREE),NUM/PERS),
                                    NPS,KIND)-->
    verbConj(verb(V,VKIND,TENSE,NUM/PERS),ADV,MODE,TENSE,NUM/PERS),
    {last(V,sei)}, {TENSE \= futurePast},
    nounPhrase(NPS,_GENDER,NUM/PERS,nom),
    optFreeArgs(FREE),
    {NPS \= common(_,_,_,_), NPS \= art(_,_,_,_) },
    {NPS \= poss(_,_,_), NPS \=actpart(_,_)},
    {KIND=interrogative(pred)}.


%NPSUBJ-NOMPRED-FREE
%"Ich bin hungrig bereits vor zwei Stunden"
%   {functor(SUBJ,that,4)}, punct(','),
nominalClause(clause(nominal/KIND,straight,CLFREES,NPS,NPP,NUMPERS),
              TENSE,KIND,NPS)-->
   nounPhrase(NPS,_GENDER,NUMPERS,nom), %{KIND=declarative},
   nominalPhrase(NPP,TENSE,NUMPERS),
   optFreeArgs(CLFREES).

%NPSUBJ-RELSUB-NOMPRED-FREE
%"John, den du kennst, ist lustig".
nominalClause(clause(nominal/KIND,straight,CLFREES,NPS,NPP,NUMPERS),
              TENSE,KIND,NPS)-->
   nounPhrase(NPS,_GENDER,NUMPERS,nom), %{KIND=declarative},
   {functor(NPS,that,4)}, punct(','),
   nominalPhrase(NPP,TENSE,NUMPERS),
   optFreeArgs(CLFREES).

%(FREE)-VERB.BE-NPSUBJ-NOMPRED
%"Ist er hungrig" - "Heute bin ich müde."
invertedNominalClause(clause(nominal/KIND,inverted,FREES,NPS,
            vp(verb([sei],MODE,TENSE,NUM/PERS),NOT,MODE,TENSE,[NPP],VPFREES),
                             NUM/PERS),KIND,NPS,FREES)-->
   optFreeArgs(FREES),
   auxMod(sei,MODE,TENSE,NUM/PERS,_AUX,_MODAL,NOT), {ignore(NOT=[])},
   {TENSE \= futurePast, TENSE \= future},
   nounPhrase(NPS,_GENDER,NUM/PERS,nom),
   optFreeArgs(VPFREES),
   nominalPredicate(NPP,NUM), {NPP \= adj(verb(_,_),_)}.

%(FREE)-VERB.BE-NPSUBJ
%(Who) am I?" - empty predicate - interrogative word
invertedNominalClause(clause(nominal/KIND,inverted,[],NPS,
            vp(verb([sei],MODE,TENSE,NUM/PERS),NOT,MODE,TENSE,[''],FREES),
                             NUM/PERS),KIND,NPS,FREES)-->
   optFreeArgs(FREES),
   auxMod(sei,MODE,TENSE,NUM/PERS,_AUX,_MODAL,NOT), {ignore(NOT=[])},
   {TENSE \= futurePast, TENSE \= future},
   nounPhrase(NPS,_GENDER,NUM/PERS,nom).

%(FREE)-AUXMOD-NPSUBJ-VERBINFINITE-NOMPRED
%"Gestern bist du hungrig gewesen."
%"Bist du hungrig gewesen?"  "Kannst du hungrig sein?"
invertedNominalClause(clause(nominal/KIND,inverted,[],NPS,
            vp(verb([AUX|VERB],MODE,TENSE,NUM/PERS),
               NOT,MODE,TENSE,[NPP],FREES),NUM/PERS),KIND,NPS,FREES)-->
   optFreeArgs(FREES),
   auxMod(AUX,MODE,CTENSE,NUM/PERS,AUX,MODAL,NOT),{ignore(NOT=[])},
   nounPhrase(NPS,_GENDER,NUM/PERS,nom),
   nominalPredicate(NPP,NUM), {NPP \= adj(verb(_,_),_)},
   verbInfinite(VERB,MODAL,sei,KIND,INFTENSE,NOT),
   {conjinf2tense(CTENSE,INFTENSE,TENSE)}.


addressing(proper(PROP,human,GENDER,NUM,nom),NUM)-->
    proper(PROP,human,GENDER,NUM,nom).

%with separate zu prefix
zuInfiniteVerb(verb(VERB,MODE,infinite,NUMPERS),
               MODE,infinite,NUMPERS,VERB)-->
    infPart(zu), verb(VERB,MODE,infinite,NUMPERS),
    {once((atom(VERB);VERB=PREF-_V,verbPrefix_(PREF,inseparable)))}.
%zu inzwischen dem verb und trennbares prefix
%(morphological analysis detaches them)
zuInfiniteVerb(verb(PREF-VERB,MODE,infinite,NUMPERS),
               MODE,infinite,NUMPERS,VERB)-->
    verb(PREF-(zu-VERB),MODE,infinite,NUMPERS).
%present with modalverb attachment
zuInfiniteVerb(verb([VERB,MODAL],MODE,infinite,_),
               MODE,infinite,_,VERB)-->
    verb(VERB,MODE,infinite,_), infPart(zu),
    modal(MODAL,MODE,infinite,_).
%infinitive perfect
zuInfiniteVerb(verb([VERB,AUX],MODE,infinite(perfect),_),
               MODE,infinite(perfect),_,VERB)-->
    verb(VERB,MODE,past(participle),_), infPart(zu),
    aux(AUX,MODE,infinite,_), {verbParticipleAux(VERB,AUX)}.


%Nebensätze - Infinitivsatz
zuInfiniteSubphrase(vp(VERB,ADVERBS,MODE,TENSE,ARGS,[]),NUMPERS,TENSE,_)-->
   verbalArgs(ARGS0,CASES),
   optFreeAdverbs(ADVERBS),
   zuInfiniteVerb(VERB,MODE,TENSE,NUMPERS,MAINVERB),
   {verbAPrefixFrame(MAINVERB,[nom|FRAMES])},
   {nominalDemands(FRAMES)},
   {allDemsSat(FRAMES,CASES,ARGS0,ARGS)}.


%I do not know 'where she has gone'. INTERR-SUBJ-PRED-ARGS
%I do not know 'why I like my teacher'.
subClause(?(IP,CL),interrogative(wh),straight,full)-->
    interrPhrase(IP,_G,NUMP,KIND),
    verbalClause(CL,NUMP,_MODE,_KIND,_TENSE,_SUBJ,_NP,DEM),
    {debug(deParser,'interrogative+straight subclause',[])},
    {emptyOptDem(DEM)->
       (nonvar(KIND), KIND=adv(_)->true;
        compound(KIND), arg(1,KIND,acc)->true);
    nonvar(DEM), interrSatDem(DEM,KIND)->
       (nonvar(KIND),KIND=adv(_)->true;
        compound(KIND), arg(1,KIND,acc)->true)
    }.
%I do not know 'who wrote this book'. INTERR(subj)-PRED-ARGS
%I do not know 'why I like my teacher'.
subClause(?(IP,CL),interrogative(wh),straight,full)-->
    interrPhrase(IP,_G,NUMP,nom),
    verbalPhrase(VP,_MODE,_TENSE,NUMP,_,DEM),
    {emptyOptDem(DEM)},
    {CL=clause(verbal/declarative,straight,[],'',VP,NUMP)},
    {debug(deParser,'interrogative+straight subclause',[])}.
%I do not know 'who it was'. INTERR-SUBJ-SHORTPRED
subClause(?(IP,CL),interrogative(wh),straight,short)-->
   interrPhrase(IP,_G,NUMP,KIND), {ignore(KIND=nom)},
   shortVerbalClause(CL,NUMP,_,_TENSE),
   {debug(deParser,'interrogative+straight short subclause',[])}.

%
%clause(CLAUSE,MODE,TYPE,KIND,SUBJ).
% @arg MODE=declarative;interrogative(yn);interrogative(pred)
%           interrogative(wh),interrogative(subj);interj
% @arg TYPE=straight;inverted;infinite;interj
%%%%% @arg KIND=nominal;verbal NOT YET IMPLEMENTED


% simple straight verbal clause that has a clausal pendent argument
%clause(CLAUSE,MODE,TYPE,KIND,SUBJ,SUBKIND).
% 'Ich habe nicht gesehen, (ob du gestern hier warst.)
% DEM=clause(SUBKIND)... you find SUBKIND
% in missing demand of the clause
clause(CL,KIND,straight,full,SUBJ,SUBKIND)-->
    verbalClause(CL,_NUM,MODE,KIND,TENSE,SUBJ,_NP,DEM),
    {nonvar(DEM), DEM=clause(SUBKIND)},
    { \+ (MODE==conjunctive, tensePresent(TENSE))},
    {debug(deParser,'simple straight verbal clause',[])}.


%'Ich liebe meinen Lehrer'
%'Sie hat den Apfel gegessen'
clause(CL,KIND,straight,full,SUBJ)-->
    verbalClause(CL,_NUM,MODE,KIND,TENSE,SUBJ,_NP,DEM), {emptyOptDem(DEM)},
    { \+ (MODE==conjunctive, tensePresent(TENSE))},
    {debug(deParser,'simple straight verbal clause',[])}.

%simple inverted verbal clauses
%'Gestern habe ich drei Birnen gegessen'.
clause(CL,KIND,inverted,full,SUBJ)-->
    {KIND=declarative},
    invertedVerbalClause(CL,_NUM,KIND,_TENSE,_FREES,SUBJ,_REF,DEM),
    {emptyOptDem(DEM)},
    {debug(deParser,'simple inverted verbal clause',[])}.

%y/n interrogative verbal clauses
%'Aß ich gestern drei Birnen?'
%'Muss das Buch gelesen werden?'
%'Machst du die Tür zu?'
clause(?(CL),INTERROG,inverted,full,SUBJ)-->
    {INTERROG=interrogative(yn)},
    invertedVerbalClause(CL,_NUMPERSPV,INTERROG,
                         _TENSE,[],SUBJ,X,DEM),
    {emptyOptDem(DEM)-> nonvar(INTERROG),
      once(INTERROG=adv(_); functor(INTERROG,_,1));
    atom(DEM)->X=IP,demSat(DEM,INTERROG);
    compound(INTERROG)->X=IP,demSat(DEM,INTERROG)},
    {debug(deParser,'interrogative(yn)+verbal question',[])}.

%'Ich esse Apfel und trinke Bier.' without comma
clause(clause(verbal/KIND,straight,
           FREE,SUBJ,coordinating(CONN,[VP1,VP2]),
           NUMPERS),KIND,straight,full,SUBJ)-->
    verbalClause(clause(verbal/KIND,straight,FREE,SUBJ,VP1,NUMPERS),
                 NUMPERS,MODE,KIND,TENSE1,SUBJ,_X1,DEM),
    {emptyOptDem(DEM)},
    connect(CONN,coordinating), {listConnective(CONN)},
    verbalPhrase(VP2,MODE,TENSE,NUMPERS,_X2,PDEM),
    {debug(deParser,'simple verbal clause',[])},
    {emptyOptDem(PDEM)},
    {TENSE1 \=inf, TENSE \= inf}.

%'Ich esse Apfel, oder trinke Bier.' with comma
clause(clause(verbal/KIND,straight,
           FREE,SUBJ,coordinating(CONN,[VP1,VP2]),
           NUMPERS),KIND,straight,full,SUBJ)-->
    verbalClause(clause(verbal/KIND,straight,FREE,SUBJ,VP1,NUMPERS),
                 NUMPERS,MODE,KIND,TENSE1,SUBJ,_X1,DEM),
    {emptyOptDem(DEM)},
    punct(','), connect(CONN,coordinating),
    verbalPhrase(VP2,MODE,TENSE,NUMPERS,_X2,PDEM),
    {debug(deParser,'simple verbal clause',[])},
    {emptyOptDem(PDEM)},
    {TENSE1 \=inf, TENSE \= inf}.

%'Thank you' - subjectless
%other fake parses are also generated!! ??????
clause(clause(verbal/KIND,straight,[],'',VP,
           NUM/1),KIND,straight,subjless,'')-->
    verbalPhrase(VP,MODE,present,NUM/1,_X,PDEM),
    {emptyOptDem(PDEM), KIND=declarative},
    {debug(deParser,'subjectless verbal clause',[])}.

%'Eat apple' - imperative
clause(clause(verbal/KIND,straight,[],'',VP,
           NUMPERS),KIND,straight,full,'')-->
    verbalPhrase(VP,MODE,inf,NUMPERS,_X,PDEM),
    {emptyOptDem(PDEM), KIND=imperative(_)},
    {debug(deParser,'verbal imperative clause',[])}.

%'Be quiet' - imperative
%clause(clause(nominal/KIND,straight,[],'',NPP,
%           NUMPERS),KIND,straight,full,'')-->
clause(clause(nominal/imperative(IKIND),straight,[],'',NPP,
           NUMPERS),imperative(IKIND),straight,full,'')-->
    nominalPhrase(NPP,inf,NUMPERS),
    {debug(deParser,'nominal imperative clause',[])}.

%'Please be quiet' - imperative
clause(clause(CLKIND,straight,[interj(IJ)],'',PRED,
           NUMPERS),KIND,straight,full,SUBJ)-->
    interj(please,IJ),
    clause(clause(CLKIND,straight,[],'',PRED,
           NUMPERS),KIND,straight,full,SUBJ),
    {debug(deParser,'imperative clause with please',[])}.


% Subject interrogative active questions with linear word order
% and free args at the end... verb must be sg/pl but always pers3
%
%'Wer mag unseren Lehrer' ?
%'Was parkt vor dem Haus auf der Straße'?
%'Wer hat dieses Buch geschrieben' ?
%'Wie viel Bücher sind here' ?
clause(?(IP,CL),
       interrogative(subj),straight,full,IP)-->
    interrPhrase(IP,_G,NUM/3,nom),
    verbalPhrase(VP,MODE,TENSE,NUM/3,_,DEM),
    {TENSE\=infinite}, {emptyOptDem(DEM)},
    {CL=clause(verbal/interrogative(subj),straight,[],ref(IP),VP,NUM/3)},
    {debug(deParser,'Subject interrogative active verbal clause',[])}.

% Subject interrogative passive questions with linear word order
% and free args at the end... verb must be sg/pl but always pers3
%
%'How many apples can be seen here' ?
clause(?(IP,CL),
       interrogative(subj),straight,full,IP)-->
    interrPhrase(IP,_G,NUM/3,nom),
    passiveVerbalPhrase(VP,TENSE,NUM/3,_,DEM),
    {emptyOptDem(DEM)},
    {CL=clause(verbal/interrogative(subj),straight,[],ref(IP),VP,NUM/3)},
    {debug(deParser,'Subject interrogative passive verbal clause',[])}.


% Yes/no interrogative passive questions with inverted word order
% and free args inside the verbal frame...
% verb must be sg/pl but always pers3
%
% 'Muss das Buch heute noch gelesen werden'?
% 'Wurden Aepfel gestern noch hier gesehen'?
%
clause(?(CL),
       interrogative(yn),inverted,full,SUBJ)-->
    invertedPassiveVerbalClause(clause(verbal/KIND,inverted,[],SUBJ,
        VP,NUM/3),NUM/3,KIND,TENSE,[],SUBJ,_REF,DEM),
    {emptyOptDem(DEM)},
    {CL=clause(verbal/interrogative(yn),inverted,[],SUBJ,VP,NUM/3)},
    {debug(deParser,'Yes/no interrogative passive verbal clause',[])}.


%'An wem hast du einen Brief geschrieben'?
%'Welchen Apfel magst du'?
%'Warum mag ich unseren Lehrer'?
%'Warum hat er mir ein Buch gegeben'?
%'Wer bin ich'?
%'Wie lang hast du auf sie gewartet'?
%'Was für ein Auto sehen wir?'
clause(?(IP,clause(verbal/INTERROGATIVE,inverted,[],SUBJ,VP,NUMPERSVP)),
       INTERROGATIVE,inverted,full,SUBJ)-->
    interrPhrase(IP,_G,_NUMPERSNP,KINDCASE),
    invertedVerbalPhrase(VP,NUMPERSVP,_TENSE,SUBJ,X,DEM),
    {INTERROGATIVE=interrogative(wh)},
    {emptyOptDem(DEM)-> nonvar(KINDCASE);
    atom(DEM)->X=IP,demSat(DEM,KINDCASE);
    compound(KINDCASE)->X=IP,demSat(DEM,KINDCASE)},
    {debug(deParser,'interrogative+verbal wh question',[])}.

%'Whom did you send a letter to?' - verb arg
%'Whom did you travel in Europe with?' - free arg ???????????
clause(?(IPP,CL),interrogative(wh),inverted,full,SUBJ)-->
    interrPhrase(IP,_G,_NUMPERSNP,KIND), {once(backPrepInterr(KIND))},
    invertedVerbalClause(CL,_NUMPERSVP,interrogative(wh),_TENSE,[],SUBJ,X,DEM),
    prep(PREP,CASE),
    {debug(deParser,'interrogative+verbal question',[])},
    {(\+ emptyOptDem(DEM), IKIND=..[PREP,KIND],
       interrSatDem(DEM,IKIND),
       insertInterrPrep(IP,PREP,IPP), X=IPP)}.

%'Mein Vater ist ein Arzt'
clause(CL,KIND,straight,full,SUBJ)-->
    nominalClause(CL,TENSE,KIND,SUBJ), {TENSE \= infinite},
    {debug(deParser,'simple nominal clause',[])}.
%'Heute bin ich müde'
clause(CL,KIND,inverted,full,SUBJ)-->
    invertedNominalClause(CL,KIND,SUBJ,_FREES),
    {debug(deParser,'inverted nominal clause',[])}.

%'is my mother a teacher' ?
clause(?(CL),interrogative(yn),inverted,full,SUBJ)-->
    invertedNominalClause(CL,interrogative(yn),SUBJ,[]),
    {debug(deParser,'Y/N nominal question',[])}.

%I can / Could I
clause(clause(verbal/declarative,DIR,FREE,SUBJ,VP,NUMPERS),
             declarative,DIR,short,SUBJ)-->
    shortVerbalClause(clause(verbal/declarative,DIR,FREE,SUBJ,VP,NUMPERS),
             NUMPERS,declarative,_TENSE),
    {debug(deParser,'short declaration/question',[])}.

%'could I'?
clause(clause(verbal/declarative,DIR,[],
              SUBJ,VP,NUMPERS),declarative,inverted,short,SUBJ)-->
    shortInvertedVerbalClause(clause(verbal/declarative,
             DIR,[],SUBJ,VP,NUMPERS),NUMPERS,declarative,_TENSE),
    {debug(deParser,'short inverted question tag',[])}.


%'why is your mother a teacher'? - 'wo ist das Bad'?
%'Who am I'? 'Who are you'? - this is a predicative question
clause(?(IP,CL),interrogative(wh),inverted,full,SUBJ)-->
    interrPhrase(IP,_G,_NUMPERS,KIND),
    {nonvar(KIND), once(KIND=adv(_);KIND\=nom)},
    invertedNominalClause(CL,interrogative(wh),SUBJ,[]),
    {CL= clause(_,_,[],SUBJ,vp(_V,_,_MODE,_TENSE,_ARGS,_FREE),_)},
    {debug(deParser,'wh interrogative',[])}.

%'who is doctor'? - subject interrogative with straight word order
clause(?(IP,clause(nominal/interrogative(wh),straight,[],IP,VP,NUM/3)),
         interrogative(wh),straight,full,IP)-->
    interrPhrase(IP,_G,NUM/3,nom),
    nominalPhrase(VP,_TENSE,NUM/3),
    {VP=vp(_V,_,_MODE,_TENSE,[NPP],_FREE), NPP\=proper(_,_,_,_,_),
    NPP\=coordinating(_,proper(_,_,_,_,_),proper(_,_,_,_,_))},
    {debug(deParser,'wh subject interrogative',[])}.

%'who was your father'? (eg. president)
%predicate interrogative, inverted (subject:your father)
clause(?(IP,CL),KIND,inverted,full,IP)-->
    interrPhrase(IP,_G,_NUMP,IKIND),
    {nonvar(IKIND), once(IKIND=adv(_);IKIND=nom)},
    predInterrogativeNominalClause(CL,_,KIND),
    {debug(deParser,'wh pred-interrogative',[])}.

%'wow' | 'yes' | 'no'
clause(interj(IJ),interj,interj,interj,'')-->
    interj(_,IJ),
    {debug(deParser,'interjection as a clause',[])}.

:-if(switchOut).
%clause(CL,declarative,_,_)--> %'Hungry'-->I am hungry
%	{debug(deParser,'subjectless nominal clause',[])},
%	subjlessNominalClause(CL).
%clause(?(CL),interrogative(yn),_,_)--> %'hungry?'-->Are you hungry?
%	{debug(deParser,'subjectless nominal clause',[])},
%	subjlessNominalClause(CL).
%clause(CL,declarative,_,_)--> %'My father'
%	{debug(deParser,'predicateless nominal clause',[])},
%	predlessNominalClause(CL).
%clause(?(IP,CL),interrogative(wh),_,_)-->%'What is your name?'
%	{debug(deParser,'predicateless nominal,?',[])},
%	interrPhrase(IP,_G,_NUMP,KIND), predlessNominalClause(CL).
%	%, {KIND\=nom}.
:- endif.


:- op(1050,yfx,<-).

% subordinate clause, that is connected with the
% main clause by a comma, and it stands beforehead
%'consequently, I took a bus'
commaPreclause(connected(adverb(ADV,connecting(comma),base),CL),CL)-->
  adverb(ADV,connecting(comma),_).

%Sam, why did you come here?
commaPreclause(preclause(ADDR,CL),CL)-->
  addressing(ADDR,_NUM).

%Wow, she is also my best friend.
commaPreclause(compound(',',CL1,CL2),CL2)-->
  clause(CL1,interj,interj,interj,_).


% subordinate clause, that is connected with the
% main clause by a comma, and it stands afterwards
%Why did you come here, Sam?
commaPostclause(postclause(CL,ADDR),CL,KIND)-->
  punct(','), addressing(ADDR,_NUM),
  punct(P), {matchingPunct(KIND,P)}.

% Wir fahren mit Bahn oder wir fahren mit Bus.
commaPostclause(compound(connect(CONN,coordinating),CL1,CL2),
                CL1,declarative)-->
  connect(CONN,coordinating), {listConnective(CONN)},
  clause(CL2,declarative,_,_,_), punct('.').

% Er hat nicht gearbeitet, denn er war krank.
% Ich esse Apfel, oder du trinkst Bier.
commaPostclause(compound(connect(CONN,coordinating),CL1,CL2),
                CL1,declarative)-->
  punct(','), connect(CONN,coordinating),
  clause(CL2,declarative,_,_,_), punct('.').

%Could you be quiet, please?
commaPostclause(postclause(CL,interj(IJ)),CL,KIND)-->
  punct(','), interj(please,IJ),
  punct(P), {matchingPunct(KIND,P)}.

%I am fine, thank you.
commaPostclause(postclause(CL,SLESS),CL,_)-->
  punct(','), clause(SLESS,declarative,_,_,_), punct('.').


sentence(incomplete(IP,KIND))-->interrPhrase(IP,_G,_NUM,_), punct(PUNCT),
    {matchingPunct(KIND,PUNCT)}.
sentence(CL)--> clause(CL,KIND,_,_,SUBJ), punct(PUNCT),
    {matchingPunct(SUBJ,KIND,PUNCT)}.
sentence(EXPR)-->
    commaPreclause(EXPR,CL), punct(','), clause(CL,KIND,_,_,SUBJ),
    punct(P), {matchingPunct(SUBJ,KIND,P)}.
sentence(EXPR)-->
     clause(CL,KIND,_,_,_SUBJ), {KIND \= interj},
     commaPostclause(EXPR,CL,KIND),
     {EXPR=questag(',',CL,QT)->
          CL=clause(_,_,_,S1,VP1,_,P1),
          QT = clause(_,_,_,S2,VP2,_,P2),
          S1=S2, P1\=P2, matchingLongShortVP(VP1,VP2);
     true}.
sentence(EX2PR)-->
    commaPreclause(EX2PR,EXPR), punct(','),
    commaPreclause(EXPR,CL), punct(','),
    clause(CL,KIND,_,_,SUBJ), punct(P), {matchingPunct(SUBJ,KIND,P)}.
%She is my wife; she is also my best friend.
sentence(compound(';',CL1,CL2))-->
  clause(CL1,declarative,_,_,_), punct(';'),
  clause(CL2,declarative,_,_,_), punct('.').
%Look out - she is going to fall.
sentence(compound('-',CL1,CL2))-->
  clause(CL1,imperative(_),_,_,''), punct('-'),
  clause(CL2,KIND,_,_,_),
      {once(KIND=exclamatory;KIND=imperative(strong))}, punct('!').
%no sooner...than...
sentence(correlated(CONN,CL1,CL2))-->
  adverb(C1,_,base), adverb(C2,_,base),
  {correlative(CONN,C1,C2,inverse,C3,straight)},
  invertedVerbalClause(CL1,_NUMPERS,declarative,_TENSE,_FREE,_SUBJ,_REF,DEM),
  {emptyOptDem(DEM)},
  connect(C3,comp), clause(CL2,declarative,_,_,_), punct('.').

%Entweder wir gehen heute in das Kino oder wir gehen morgen
sentence(correlated(CONN,CL1,CL2))-->
  connect(C1,corr),
  {correlative(CONN,C1,DIR1,C2,DIR2)}, clause(CL1,declarative,DIR1,_,_),
  {listConnective(C2)},
  connect(C2,corr), clause(CL2,declarative,DIR2,_,_), punct('.').
%Einerseits würde ich mit ihr gehen, andererseits bin ich müde.
%Mal kann der Hund ruhig sein, mal ist er nervös.
sentence(correlated(CONN,CL1,',',CL2))-->
  connect(C1,corr),
  {correlative(CONN,C1,DIR1,C2,DIR2)}, clause(CL1,declarative,DIR1,_,_),
  {\+ listConnective(C2)}, punct(','),
  connect(C2,corr), clause(CL2,declarative,DIR2,_,_), punct('.').

%first...the...
sentence(correlated(CONN,CL1,CL2))-->
  adverb(C1,_,base),
  {correlative(CONN,C1,straight,C2,straight)}, clause(CL1,declarative,_,_,_),
  connect(C2,corr), clause(CL2,declarative,_,_,_), punct('.').
%
% subordinated sentences (untergeordnete Sätze)
%
% both of rules below are the same,
% only the logical structure is different!!!!! WHY
% subclause/subordinating
%
%Ich wusste, dass du schön singst
sentence(complex(connect(CONN,subordinating),MAIN->SUB))-->
  clause(MAIN,KIND,_,_,SUBJ), punct(','),
  connect(CONN,subordinating),
  {MAIN=clause(_,_,[],_,_,_)}, %empty FREE in MAIN
  subclause(SUB,declarative,_,_,DEM),{emptyOptDem(DEM)},
  punct(P), {matchingPunct(SUBJ,KIND,P)}.
%Ich habe nicht gewusst, ob du verheiratet bist
sentence(complex(connect(CONN,subclause(SUBKIND)),MAIN->SUB))-->
  clause(MAIN,KIND,_,_,SUBJ,SUBKIND), punct(','),
  connect(CONN,subclause(SUBKIND)),
  {MAIN=clause(_,_,[],_,_,_)},
  subclause(SUB,declarative,_,_,DEM),{emptyOptDem(DEM)},
  punct(P), {matchingPunct(SUBJ,KIND,P)}.
%Als ich Kind war, aß ich viel Äpfel
sentence(complex(connect(CONN,subordinating),(SUB<-MAIN)))-->
  connect(CONN,subordinating),
  subclause(SUB,declarative,_TENSE,_REF,PDEM),
  {emptyOptDem(PDEM)},
  punct(','), clause(MAIN,KIND,_,_,SUBJ),
  {MAIN=clause(_,_,[],_,_,_)},
  punct(P), {matchingPunct(SUBJ,KIND,P)}.

%Conjunctional adverbs..
%Ich bin krank, deswegen gehe ich heute nicht zur Arbeit.
%Ich wusste, dass du schön singst
sentence(complex(connect(CONN,conjunctional/SUBC),MAIN->SUB))-->
  clause(MAIN,KIND,straight,full,SUBJ), punct(','),
  connect(CONN,conjunctional/SUBC),
  {once(CONN\=wenn;(MAIN=clause(_,_,[],_,_,_,_),
   SUB=clause(_,_,_,_,VP,_,_), VP=vp(_,_,_,_,_,[])))},
  clause(SUB,declarative,inverted,full,_), punct(P), {matchingPunct(SUBJ,KIND,P)}.
:- pro.




















