﻿:- use_module(deDict).
:- begin_tests(deDict).
%
%@tbd recognition/generation of adjectives
%     especially jung
%@tbd recognition/generation of only plural nouns
%     eltern
%@tbd herren, studenten
%@tbd geben-verb
%@wissen - weißt

test(genAdj):-
  adj_(_,nett,base,def,masculin,nom,nette).
test(genAdj):-
  adj_(_,nett,base,def,masculin,gen,netten).
test(genAdj):-
  adj_(_,nett,base,def,masculin,dat,netten).
test(genAdj):-
  adj_(_,nett,base,def,masculin,acc,netten).

test(genAdj):-
  adj_(_,nett,base,indef,masculin,nom,netter).
test(genAdj):-
  adj_(_,nett,base,indef,masculin,gen,netten).
test(genAdj):-
  adj_(_,nett,base,indef,masculin,dat,netten).
test(genAdj):-
  adj_(_,nett,base,indef,masculin,acc,netten).

test(genAdj):-
  adj_(_,nett,base,no,masculin,nom,netter).
test(genAdj):-
  adj_(_,nett,base,no,masculin,gen,netten).
test(genAdj):-
  adj_(_,nett,base,no,masculin,dat,nettem).
test(genAdj):-
  adj_(_,nett,base,no,masculin,acc,netten).

test(genAdj):-
  adj_(_,nett,base,def,feminin,nom,nette).
test(genAdj):-
  adj_(_,nett,base,def,feminin,gen,netten).
test(genAdj):-
  adj_(_,nett,base,def,feminin,dat,netten).
test(genAdj):-
  adj_(_,nett,base,def,feminin,acc,nette).

test(genAdj):-
  adj_(_,nett,base,indef,feminin,nom,nette).
test(genAdj):-
  adj_(_,nett,base,indef,feminin,gen,netten).
test(genAdj):-
  adj_(_,nett,base,indef,feminin,dat,netten).
test(genAdj):-
  adj_(_,nett,base,indef,feminin,acc,nette).

test(genAdj):-
  adj_(_,nett,base,no,feminin,nom,nette).
test(genAdj):-
  adj_(_,nett,base,no,feminin,gen,netter).
test(genAdj):-
  adj_(_,nett,base,no,feminin,dat,netter).
test(genAdj):-
  adj_(_,nett,base,no,feminin,acc,nette).

test(genAdj):-
  adj_(_,nett,base,def,neutral,nom,nette).
test(genAdj):-
  adj_(_,nett,base,def,neutral,gen,netten).
test(genAdj):-
  adj_(_,nett,base,def,neutral,dat,netten).
test(genAdj):-
  adj_(_,nett,base,def,neutral,acc,nette).

test(genAdj):-
  adj_(_,nett,base,indef,neutral,nom,nettes).
test(genAdj):-
  adj_(_,nett,base,indef,neutral,gen,netten).
test(genAdj):-
  adj_(_,nett,base,indef,neutral,dat,netten).
test(genAdj):-
  adj_(_,nett,base,indef,neutral,acc,nettes).

test(genAdj):-
  adj_(_,nett,base,no,neutral,nom,nettes).
test(genAdj):-
  adj_(_,nett,base,no,neutral,gen,netten).
test(genAdj):-
  adj_(_,nett,base,no,neutral,dat,nettem).
test(genAdj):-
  adj_(_,nett,base,no,neutral,acc,nettes).

test(genAdj):-
  adj_(_,nett,base,def,pl,nom,netten).
test(genAdj):-
  adj_(_,nett,base,def,pl,gen,netten).
test(genAdj):-
  adj_(_,nett,base,def,pl,dat,netten).
test(genAdj):-
  adj_(_,nett,base,def,pl,acc,netten).

test(genAdj):-
  adj_(_,nett,base,no,pl,nom,nette).
test(genAdj):-
  adj_(_,nett,base,no,pl,gen,netter).
test(genAdj):-
  adj_(_,nett,base,no,pl,dat,netten).
test(genAdj):-
  adj_(_,nett,base,no,pl,acc,nette).
%
%recognition
%
test(recAdj):-
  adj_(_,STEM,base,def,masculin,nom,nette), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,def,masculin,gen,netten), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,def,masculin,dat,netten), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,def,masculin,acc,netten), STEM==nett.

test(recAdj):-
  adj_(_,STEM,base,indef,masculin,nom,netter), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,indef,masculin,gen,netten), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,indef,masculin,dat,netten), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,indef,masculin,acc,netten), STEM==nett.

test(recAdj):-
  adj_(_,STEM,base,no,masculin,nom,netter), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,no,masculin,gen,netten), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,no,masculin,dat,nettem), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,no,masculin,acc,netten), STEM==nett.

test(recAdj):-
  adj_(_,STEM,base,def,feminin,nom,nette), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,def,feminin,gen,netten), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,def,feminin,dat,netten), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,def,feminin,acc,nette), STEM==nett.

test(recAdj):-
  adj_(_,STEM,base,indef,feminin,nom,nette), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,indef,feminin,gen,netten), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,indef,feminin,dat,netten), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,indef,feminin,acc,nette), STEM==nett.

test(recAdj):-
  adj_(_,STEM,base,no,feminin,nom,nette), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,no,feminin,gen,netter), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,no,feminin,dat,netter), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,no,feminin,acc,nette), STEM==nett.

test(recAdj):-
  adj_(_,STEM,base,def,neutral,nom,nette), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,def,neutral,gen,netten), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,def,neutral,dat,netten), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,def,neutral,acc,nette), STEM==nett.

test(recAdj):-
  adj_(_,STEM,base,indef,neutral,nom,nettes), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,indef,neutral,gen,netten), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,indef,neutral,dat,netten), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,indef,neutral,acc,nettes), STEM==nett.

test(recAdj):-
  adj_(_,STEM,base,no,neutral,nom,nettes), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,no,neutral,gen,netten), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,no,neutral,dat,nettem), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,no,neutral,acc,nettes), STEM==nett.

test(recAdj):-
  adj_(_,STEM,base,def,pl,nom,netten), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,def,pl,gen,netten), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,def,pl,dat,netten), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,def,pl,acc,netten), STEM==nett.

test(recAdj):-
  adj_(_,STEM,base,no,pl,nom,nette), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,no,pl,gen,netter), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,no,pl,dat,netten), STEM==nett.
test(recAdj):-
  adj_(_,STEM,base,no,pl,acc,nette), STEM==nett.


%exceptions
%
%ending with -e
test(genAdj):-
  adj_(_,leise,base,def,masculin,gen,leisen).
%ending with -el
test(genAdj):-
  adj_(_,dunkel,base,indef,masculin,nom,dunkler).
%ending with -er
test(genAdj):-
  adj_(_,teuer,base,indef,neutral,acc,teures).
%hoch - c falls out
test(genAdj):-
  adj_(_,hoch,base,indef,neutral,acc,hohes).

%ending with -e
test(recAdj):-
  adj_(_,STEM,base,def,masculin,gen,leisen), STEM=leise.
%ending with -el
test(recAdj):-
  adj_(_,STEM,base,indef,masculin,nom,dunkler), STEM=dunkel.
%ending with -er
test(recAdj):-
  adj_(_,STEM,base,indef,neutral,acc,teures), STEM=teuer.
%hoch - c falls out
test(recAdj):-
  adj_(_,STEM,base,indef,neutral,acc,hohes), STEM=hoch.

%
% ARTICLES
%

test(genArt):-
  art_(der,def,masculin,nom,WORD), WORD==der.
test(genArt):-
  art_(der,def,masculin,acc,WORD), WORD==den.
test(genArt):-
  art_(der,def,masculin,gen,WORD), WORD==des.
test(genArt):-
  art_(der,def,masculin,dat,WORD), WORD==dem.

test(genArt):-
  art_(die,def,feminin,nom,WORD), WORD==die.
test(genArt):-
  art_(die,def,feminin,acc,WORD), WORD==die.
test(genArt):-
  art_(die,def,feminin,gen,WORD), WORD==der.
test(genArt):-
  art_(die,def,feminin,dat,WORD), WORD==der.

test(genArt):-
  art_(das,def,neutral,nom,WORD), WORD==das.
test(genArt):-
  art_(das,def,neutral,acc,WORD), WORD==das.
test(genArt):-
  art_(das,def,neutral,gen,WORD), WORD==des.
test(genArt):-
  art_(das,def,neutral,dat,WORD), WORD==dem.

test(genArt):-
  art_(die,def,pl,nom,WORD), WORD==die.
test(genArt):-
  art_(die,def,pl,acc,WORD), WORD==die.
test(genArt):-
  art_(die,def,pl,gen,WORD), WORD==der.
test(genArt):-
  art_(die,def,pl,dat,WORD), WORD==den.


test(genArt):-
  art_(ein,indef,masculin,nom,WORD), WORD==ein.
test(genArt):-
  art_(ein,indef,masculin,acc,WORD), WORD==einen.
test(genArt):-
  art_(ein,indef,masculin,gen,WORD), WORD==eines.
test(genArt):-
  art_(ein,indef,masculin,dat,WORD), WORD==einem.

test(genArt):-
  art_(ein,indef,feminin,nom,WORD), WORD==eine.
test(genArt):-
  art_(ein,indef,feminin,acc,WORD), WORD==eine.
test(genArt):-
  art_(ein,indef,feminin,gen,WORD), WORD==einer.
test(genArt):-
  art_(ein,indef,feminin,dat,WORD), WORD==einer.

test(genArt):-
  art_(ein,indef,neutral,nom,WORD), WORD==ein.
test(genArt):-
  art_(ein,indef,neutral,acc,WORD), WORD==ein.
test(genArt):-
  art_(ein,indef,neutral,gen,WORD), WORD==eines.
test(genArt):-
  art_(ein,indef,neutral,dat,WORD), WORD==einem.

%
%recognition
%
test(recArt,all(S-G-C=[der-masculin-nom,
                       die-feminin-gen,
                       die-feminin-dat,
                       die-pl-gen])):-
  art_(S,def,G,C,der).
test(recArt,all(S-G-C=[der-masculin-acc,
                       die-pl-dat])):-
  art_(S,def,G,C,den).
test(recArt,all(S-G-C=[der-masculin-dat,
                       das-neutral-dat])):-
  art_(S,def,G,C,dem).
test(recArt,all(S-G-C=[der-masculin-gen,
                       das-neutral-gen])):-
  art_(S,def,G,C,des).
test(recArt,all(S-G-C=[die-feminin-nom,
                       die-feminin-acc,
                       die-pl-nom,
                       die-pl-acc])):-
  art_(S,def,G,C,die).
test(recArt,all(S-G-C=[das-neutral-nom,
                       das-neutral-acc])):-
  art_(S,def,G,C,das).


test(recArt,all(S-G-C=[ein-masculin-nom,
                       ein-neutral-nom,
                       ein-neutral-acc])):-
  art_(S,indef,G,C,ein).
test(recArt):-
  art_(S,indef,G,C,einen), S==ein, G==masculin, C==acc.
test(recArt,all(S-G-C=[ein-masculin-gen,ein-neutral-gen])):-
  art_(S,indef,G,C,eines).
test(recArt,all(S-G-C=[ein-masculin-dat,ein-neutral-dat])):-
  art_(S,indef,G,C,einem).
test(recArt,all(S-G-C=[ein-feminin-nom,ein-feminin-acc])):-
  art_(S,indef,G,C,eine).
test(recArt,all(S-G-C=[ein-feminin-gen,ein-feminin-dat])):-
  art_(S,indef,G,C,einer).

%
%NOUNS
%
% generation-recognition
%
test(genNoun,set(N-C-W=
    [sg-nom-baum,sg-acc-baum,sg-dat-baum,sg-gen-baumes,
    pl-nom-bäume,pl-acc-bäume,pl-dat-bäumen,pl-gen-bäume])):-
  noun_(tree,S,G,N,C,W), S==baum, G==masculin.
test(recNoun,set(N-C=[sg-nom,sg-acc,sg-dat])):-
  noun_(SE,ST,G,N,C,baum),
    SE-ST-G==tree-baum-masculin.
test(recNoun):-
  noun_(SE,ST,G,N,C,baumes),N-C=sg-gen,
    SE-ST-G==tree-baum-masculin.
test(recNoun,set(N-C=[pl-nom,pl-acc,pl-gen])):-
  noun_(SE,ST,G,N,C,bäume),
    SE-ST-G==tree-baum-masculin.
test(recNoun):-
  noun_(SE,ST,G,N,C,bäumen),N-C=pl-dat,
    SE-ST-G==tree-baum-masculin.
%
% sBier
%
test(genNoun,set(N-C-W=
    [sg-nom-bier,sg-acc-bier,sg-dat-bier,sg-gen-biers,
    pl-nom-biere,pl-acc-biere,pl-dat-bieren,pl-gen-biere])):-
  noun_(beer,S,G,N,C,W), S==bier, G=neutral.
test(recNoun,set(N-C=[sg-nom,sg-acc,sg-dat])):-
  noun_(SE,ST,G,N,C,bier),
  SE-ST-G==beer-bier-neutral.
test(recNoun):-
  noun_(SE,ST,G,N,C,biers),
  SE-ST-G-N-C==beer-bier-neutral-sg-gen.
test(recNoun,set(N-C=[pl-nom, pl-acc, pl-gen])):-
  noun_(SE,ST,G,N,C,biere), SE==beer, ST==bier, G=neutral.
test(recNoun):-
  noun_(SE,ST,G,N,C,bieren),
  SE-ST-G-N-C==beer-bier-neutral-pl-dat.
%
% rStuhl
%
test(genNoun,set(N-C-W=
    [sg-nom-stuhl,sg-acc-stuhl,sg-dat-stuhl,sg-gen-stuhles
    ,pl-nom-stühle,pl-acc-stühle,pl-gen-stühle,pl-dat-stühlen])):-
  noun_(chair,S,G,N,C,W), S==stuhl, G=masculin.
test(recNoun,set(N-C=[sg-nom,sg-acc,sg-dat])):-
  noun_(SE,ST,G,N,C,stuhl), SE-ST-G==chair-stuhl-masculin.
test(recNoun):-
  noun_(SE,ST,G,N,C,stuhles),
  SE-ST-G-N-C==chair-stuhl-masculin-sg-gen.
test(recNoun,set(N-C=[pl-nom,pl-acc,pl-gen])):-
  noun_(SE,ST,G,N,C,stühle),
  SE-ST-G==chair-stuhl-masculin.
test(recNoun):-
  noun_(SE,ST,G,N,C,stühlen),
  SE-ST-G-N-C==chair-stuhl-masculin-pl-dat.
%
% rApfel
%
test(genNoun,set(N-C-W=
    [sg-nom-apfel,sg-acc-apfel,sg-gen-apfels,sg-dat-apfel
    ,pl-nom-äpfel,pl-acc-äpfel,pl-gen-äpfel,pl-dat-äpfeln])):-
  noun_(apple,S,G,N,C,W),S==apfel, G=masculin.
test(recNoun,set(N-G-C=[sg-masculin-nom, sg-masculin-acc, sg-masculin-dat])):-
  noun_(SE,ST,G,N,C,apfel), SE-ST==apple-apfel.
test(recNoun):-
  noun_(SE,ST,G,N,C,apfels), SE-ST-G-N-C==apple-apfel-masculin-sg-gen.
test(recNoun,set(N-C=[pl-nom,pl-acc,pl-gen])):-
  noun_(SE,ST,G,N,C,äpfel), SE-ST-G==apple-apfel-masculin.
test(recNoun):-
  noun_(SE,ST,G,N,C,äpfeln), SE-ST-G-N-C==apple-apfel-masculin-pl-dat.
%
% sHaus
%
test(genNoun,set(N-C-W=
    [sg-nom-haus,sg-acc-haus,sg-gen-hauses,sg-dat-haus
    ,pl-nom-häuser,pl-acc-häuser,pl-gen-häuser,pl-dat-häusern])):-
  noun_(house,S,G,N,C,W), S==haus, G=neutral.
test(recNoun,set(N-C=[sg-nom,sg-acc,sg-dat])):-
  noun_(SE,ST,G,N,C,haus), SE-ST-G==house-haus-neutral.
test(recNoun):-
  noun_(SE,ST,G,N,C,hauses), SE-ST-G-N-C==house-haus-neutral-sg-gen.
test(recNoun,set(N-C=[pl-nom,pl-acc,pl-gen])):-
  noun_(SE,ST,G,N,C,häuser), SE-ST-G==house-haus-neutral.
test(recNoun):-
  noun_(SE,ST,G,N,C,häusern), SE-ST-G-N-C==house-haus-neutral-pl-dat.
%
% rStudent
%
test(genNoun,set(N-C-W=
    [sg-nom-student,sg-acc-studenten,sg-gen-studenten,sg-dat-studenten
    ,pl-nom-studenten,pl-acc-studenten,pl-gen-studenten,pl-dat-studenten])):-
  noun_(student,S,G,N,C,W), S==student, G=masculin.
test(recNoun):-
  noun_(SE,ST,G,N,C,student), SE-ST-G-N-C==student-student-masculin-sg-nom.
test(recNoun,set(N-C=[sg-acc,sg-gen,sg-dat,pl-nom,pl-acc,pl-gen,pl-dat])):-
  noun_(SE,ST,G,N,C,studenten), SE-ST-G==student-student-masculin.

test(genNoun,set(N-C-W=
[sg-nom-erwachsene,sg-acc-erwachsenen,sg-dat-erwachsenen,sg-gen-erwachsenen
,pl-nom-erwachsenen,pl-acc-erwachsenen,pl-dat-erwachsenen,pl-gen-erwachsenen])):-
  noun_(adult,S,G,N,C,W), S==erwachsene, G=masculin.

test(genNoun,set(N-C-W=
    [sg-nom-nachbar,sg-acc-nachbarn,sg-dat-nachbarn,sg-gen-nachbarn
    ,pl-nom-nachbarn,pl-acc-nachbarn,pl-gen-nachbarn,pl-dat-nachbarn])):-
  noun_(neighbour,S,G,N,C,W), S==nachbar, G==masculin.
test(recNoun):-
  noun_(SE,ST,G,N,C,nachbar),
  SE-ST-G-N-C==neighbour-nachbar-masculin-sg-nom.
test(recNoun,set(N-C=[sg-acc,sg-dat,sg-gen,
                      pl-nom,pl-acc,pl-gen,pl-dat])):-
  noun_(SE,ST,G,N,C,nachbarn), SE-ST-G==neighbour-nachbar-masculin.

test(genNoun,set(N-C-W=
    [sg-nom-kuh,sg-acc-kuh,sg-dat-kuh,sg-gen-kuh
    ,pl-nom-kühe,pl-acc-kühe,pl-gen-kühe,pl-dat-kühen])):-
  noun_(cow,S,G,N,C,W), S==kuh, G==feminin.
test(recNoun,set(N-C=[sg-nom,sg-acc,sg-dat,sg-gen])):-
  noun_(SE,ST,G,N,C,kuh), SE-ST-G==cow-kuh-feminin.
test(recNoun,set(N-C=[pl-nom,pl-acc,pl-gen])):-
  noun_(SE,ST,G,N,C,kühe), SE-ST-G==cow-kuh-feminin.
test(recNoun):-
  noun_(SE,ST,G,N,C,kühen), SE-ST-G-N-C==cow-kuh-feminin-pl-dat.

%
%
%
%sehen, lesen, kommen, werden, sprechen
%nehmen, arbeiten, wollen, wissen, sagen, schlafen, geben, können
%vergessen, helfen, trinken, finden, laufen, lernen, treffen, leben
%müssen, finden, tun, sollen, lassen
%
%
% VERBS
%
test(genVerb,all(PN-W=[sg/1-gehe,sg/2-gehst,sg/3-geht,
                      pl/1-gehen,pl/2-geht,pl/3-gehen])):-
  verb_(go,ST,indicative,present,PN,W), ST==geh.
test(genVerb,all(PN-W=[sg/1-gehe,sg/2-gehest,sg/3-gehe,
                      pl/1-gehen,pl/2-gehet,pl/3-gehen])):-
  verb_(go,ST,conjunctive,present,PN,W),ST==geh.
test(genVerb,all(PN-W=[sg/1-ging,sg/2-gingst,sg/3-ging,
                      pl/1-gingen,pl/2-gingt,pl/3-gingen])):-
  verb_(go,ST,indicative,past,PN,W), ST==geh.
test(genVerb,all(PN-W=[sg/1-ginge,sg/2-gingest,sg/3-ginge,
                      pl/1-gingen,pl/2-ginget,pl/3-gingen])):-
  verb_(go,ST,conjunctive,past,PN,W), ST==geh.
test(genVerb):-
  verb_(go,_,imperative,present,sg/2,FORM), FORM=geh.
test(genVerb):-
  verb_(go,_,imperative,present,pl/2,FORM), FORM=geht.
test(genVerb):-
  verb_(go,_,_,present(participle),_,FORM), FORM=gehende.
test(genVerb):-
  verb_(go,_,_,past(participle),_,FORM), FORM=gegangen.
/*
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                 [write-schreib-indicative-present-sg/1
                  ,write-schreib-conjunctive-present-sg/1
                  ,write-schreib-conjunctive-present-sg/3])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,schreibe).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,schreibe),
  SEM-INF-MODE-TENSE-NUMPERS==write-schreib-indicative-present-sg/1.
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                 [write-schreib-indicative-present-sg/3
                  ,write-schreib-indicative-present-pl/2
                  ,write-schreib-imperative-present-pl/2
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,schreibt).
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                 [write-schreib-indicative-present-pl/1
                  ,write-schreib-indicative-present-pl/3
                  ,write-schreib-conjunctive-present-pl/1
                  ,write-schreib-conjunctive-present-pl/3
                  ,write-schreib-infinite-infinite-_
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,schreiben).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,schreibest),
  SEM-INF-MODE-TENSE-NUMPERS==write-schreib-conjunctive-present-sg/2.
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,schreibet),
  SEM-INF-MODE-TENSE-NUMPERS==write-schreib-conjunctive-present-pl/2.
*/
test(genVerb,all(PN-W=[sg/1-schreibe,sg/2-schreibst,sg/3-schreibt,
                      pl/1-schreiben,pl/2-schreibt,pl/3-schreiben])):-
  verb_(write,ST,indicative,present,PN,W), ST==schreib.
test(genVerb,all(PN-W=[sg/1-schreibe,sg/2-schreibest,sg/3-schreibe,
                      pl/1-schreiben,pl/2-schreibet,pl/3-schreiben])):-
  verb_(write,ST,conjunctive,present,PN,W),ST==schreib.
test(genVerb,all(PN-W=[sg/1-schrieb,sg/2-schriebst,sg/3-schrieb,
                      pl/1-schrieben,pl/2-schriebt,pl/3-schrieben])):-
  verb_(write,ST,indicative,past,PN,W), ST==schreib.
test(genVerb,all(PN-W=[sg/1-schriebe,sg/2-schriebest,sg/3-schriebe,
                      pl/1-schrieben,pl/2-schriebet,pl/3-schrieben])):-
  verb_(write,ST,conjunctive,past,PN,W), ST==schreib.
test(genVerb):-
  verb_(write,_,imperative,present,sg/2,FORM), FORM=schreib.
test(genVerb):-
  verb_(write,_,imperative,present,pl/2,FORM), FORM=schreibt.
test(genVerb):-
  verb_(write,_,_,present(participle),_,FORM), FORM=schreibende.
test(genVerb):-
  verb_(write,_,_,past(participle),_,FORM), FORM=geschrieben.

test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                 [write-schreib-indicative-present-sg/1
                  ,write-schreib-conjunctive-present-sg/1
                  ,write-schreib-conjunctive-present-sg/3])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,schreibe).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,schreibe),
  SEM-INF-MODE-TENSE-NUMPERS==write-schreib-indicative-present-sg/1.
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                 [write-schreib-indicative-present-sg/3
                  ,write-schreib-indicative-present-pl/2
                  ,write-schreib-imperative-present-pl/2
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,schreibt).
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=[
                  write-schreib-indicative-present-pl/1
                  ,write-schreib-indicative-present-pl/3
                  ,write-schreib-conjunctive-present-pl/1
                  ,write-schreib-conjunctive-present-pl/3
                  ,write-schreib-infinite-infinite-_
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,schreiben).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,schreibest),
  SEM-INF-MODE-TENSE-NUMPERS==write-schreib-conjunctive-present-sg/2.
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,schreibet),
  SEM-INF-MODE-TENSE-NUMPERS==write-schreib-conjunctive-present-pl/2.

test(genVerb,all(PN-W=[sg/1-sehe,sg/2-siehst,sg/3-sieht,
                      pl/1-sehen,pl/2-seht,pl/3-sehen])):-
  verb_(see,ST,indicative,present,PN,W), ST==seh.
test(genVerb,all(PN-W=[sg/1-sehe,sg/2-sehest,sg/3-sehe,
                      pl/1-sehen,pl/2-sehet,pl/3-sehen])):-
  verb_(see,ST,conjunctive,present,PN,W),ST==seh.
test(genVerb,all(PN-W=[sg/1-sah,sg/2-sahst,sg/3-sah,
                      pl/1-sahen,pl/2-saht,pl/3-sahen])):-
  verb_(see,ST,indicative,past,PN,W), ST==seh.
test(genVerb,all(PN-W=[sg/1-sähe,sg/2-sähest,sg/3-sähe,
                      pl/1-sähen,pl/2-sähet,pl/3-sähen])):-
  verb_(see,ST,conjunctive,past,PN,W), ST==seh.
test(genVerb):-
  verb_(see,_,imperative,present,sg/2,FORM), FORM=sieh.
test(genVerb):-
  verb_(see,_,imperative,present,pl/2,FORM), FORM=seht.
test(genVerb):-
  verb_(see,_,_,present(participle),_,FORM), FORM=sehende.
test(genVerb):-
  verb_(see,_,_,past(participle),_,FORM), FORM=gesehen.
/*
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                 [write-schreib-indicative-present-sg/1
                  ,write-schreib-conjunctive-present-sg/1
                  ,write-schreib-conjunctive-present-sg/3])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,schreibe).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,schreibe),
  SEM-INF-MODE-TENSE-NUMPERS==write-schreib-indicative-present-sg/1.
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                 [write-schreib-indicative-present-sg/3
                  ,write-schreib-indicative-present-pl/2
                  ,write-schreib-imperative-present-pl/2
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,schreibt).
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                 [write-schreib-indicative-present-pl/1
                  ,write-schreib-indicative-present-pl/3
                  ,write-schreib-conjunctive-present-pl/1
                  ,write-schreib-conjunctive-present-pl/3
                  ,write-schreib-infinite-infinite-_
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,schreiben).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,schreibest),
  SEM-INF-MODE-TENSE-NUMPERS==write-schreib-conjunctive-present-sg/2.
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,schreibet),
  SEM-INF-MODE-TENSE-NUMPERS==write-schreib-conjunctive-present-pl/2.
*/



test(genVerb,all(PN-W=[sg/1-tanze,sg/2-tanzt,sg/3-tanzt,
                      pl/1-tanzen,pl/2-tanzt,pl/3-tanzen])):-
  verb_(dance,ST,indicative,present,PN,W), ST=tanz.
test(genVerb,all(PN-W=[sg/1-tanze,sg/2-tanzest,sg/3-tanze,
                      pl/1-tanzen,pl/2-tanzet,pl/3-tanzen])):-
  verb_(dance,ST,conjunctive,present,PN,W), ST=tanz.
test(genVerb,all(PN-W=[sg/1-tanzte,sg/2-tanztest,sg/3-tanzte,
                      pl/1-tanzten,pl/2-tanztet,pl/3-tanzten])):-
  verb_(dance,ST,indicative,past,PN,W), ST=tanz.
test(genVerb,all(PN-W=[sg/1-tanzte,sg/2-tanztest,sg/3-tanzte,
                      pl/1-tanzten,pl/2-tanztet,pl/3-tanzten])):-
  verb_(dance,ST,conjunctive,past,PN,W), ST=tanz.

test(recVerb,set(SEM-INF-MODE-TENSE-NUMPERS=
                  [dance-tanz-indicative-present-sg/1
                  ,dance-tanz-conjunctive-present-sg/1
                  ,dance-tanz-conjunctive-present-sg/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,tanze).
test(recVerb,set(SEM-INF-MODE-TENSE-NUMPERS=
                  [dance-tanz-indicative-present-sg/3
                  ,dance-tanz-indicative-present-sg/2
                  ,dance-tanz-indicative-present-pl/2
                  ,dance-tanz-imperative-present-pl/2
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,tanzt).
test(recVerb,set(SEM-INF-MODE-TENSE-NUMPERS=
                  [dance-tanz-infinite-infinite-_
                  ,dance-tanz-indicative-present-pl/1
                  ,dance-tanz-indicative-present-pl/3
                  ,dance-tanz-conjunctive-present-pl/1
                  ,dance-tanz-conjunctive-present-pl/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,tanzen).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,tanzest),
  SEM-INF-MODE-TENSE-NUMPERS=dance-tanz-conjunctive-present-sg/2.
test(recVerb,set(SEM-INF-MODE-TENSE-NUMPERS=
                  [dance-tanz-indicative-past-sg/1
                  ,dance-tanz-indicative-past-sg/3
                  ,dance-tanz-conjunctive-past-sg/1
                  ,dance-tanz-conjunctive-past-sg/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,tanzte).
test(recVerb,set(SEM-INF-MODE-TENSE-NUMPERS=
                  [dance-tanz-indicative-past-pl/1
                  ,dance-tanz-indicative-past-pl/3
                  ,dance-tanz-conjunctive-past-pl/1
                  ,dance-tanz-conjunctive-past-pl/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,tanzten).
test(recVerb,set(SEM-INF-MODE-TENSE-NUMPERS=[
                  dance-tanz-indicative-past-sg/2,
                  dance-tanz-conjunctive-past-sg/2])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,tanztest).
test(recVerb,set(SEM-INF-MODE-TENSE-NUMPERS=[
                  dance-tanz-indicative-past-pl/2,
                  dance-tanz-conjunctive-past-pl/2])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,tanztet).


test(genVerb,set(PN-W=[sg/1-habe,sg/2-hast,sg/3-hat,
                       pl/1-haben,pl/2-habt,pl/3-haben])):-
  verb_(have,_,indicative,present,PN,W).
test(genVerb,all(PN-W=[sg/1-habe,sg/2-habest,sg/3-habe,
                       pl/1-haben,pl/2-habet,pl/3-haben])):-
  verb_(have,_,conjunctive,present,PN,W).
test(genVerb,all(PN-W=[sg/1-hatte,sg/2-hattest,sg/3-hatte,
                       pl/1-hatten,pl/2-hattet,pl/3-hatten])):-
  verb_(have,_,indicative,past,PN,W).
test(genVerb,all(PN-W=[sg/1-hätte,sg/2-hättest,sg/3-hätte,
                       pl/1-hätten,pl/2-hättet,pl/3-hätten])):-
  verb_(have,_,conjunctive,past,PN,W).

test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                  [have-hab-indicative-present-sg/1
                  ,have-hab-conjunctive-present-sg/1
                  ,have-hab-conjunctive-present-sg/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,habe).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,hast),
  SEM-INF-MODE-TENSE-NUMPERS==have-hab-indicative-present-sg/2.
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,hat),
  SEM-INF-MODE-TENSE-NUMPERS==have-hab-indicative-present-sg/3.
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=[
                  have-hab-indicative-present-pl/1
                  ,have-hab-indicative-present-pl/3
                  ,have-hab-conjunctive-present-pl/1
                  ,have-hab-conjunctive-present-pl/3
                  ,have-hab-infinite-infinite-_
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,haben).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,habt),
  SEM-INF-MODE-TENSE-NUMPERS==have-hab-indicative-present-pl/2.
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,habest),
  SEM-INF-MODE-TENSE-NUMPERS==have-hab-conjunctive-present-sg/2.
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,habet),
  SEM-INF-MODE-TENSE-NUMPERS==have-hab-conjunctive-present-pl/2.
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                  [have-hab-indicative-past-sg/1
                  ,have-hab-indicative-past-sg/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,hatte).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,hattest),
  SEM-INF-MODE-TENSE-NUMPERS==have-hab-indicative-past-sg/2.
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                  [have-hab-indicative-past-pl/1
                  ,have-hab-indicative-past-pl/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,hatten).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,hattet),
  SEM-INF-MODE-TENSE-NUMPERS==have-hab-indicative-past-pl/2.

test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                  [have-hab-conjunctive-past-sg/1
                  ,have-hab-conjunctive-past-sg/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,hätte).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,hättest),
  SEM-INF-MODE-TENSE-NUMPERS==have-hab-conjunctive-past-sg/2.
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                  [have-hab-conjunctive-past-pl/1
                  ,have-hab-conjunctive-past-pl/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,hätten).

test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,bin),
  SEM-INF-MODE-TENSE-NUMPERS==be-sei-indicative-present-sg/1.
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,bist),
  SEM-INF-MODE-TENSE-NUMPERS==be-sei-indicative-present-sg/2.
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,ist),
  SEM-INF-MODE-TENSE-NUMPERS==be-sei-indicative-present-sg/3.
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                  [be-sei-indicative-present-pl/1
                  ,be-sei-indicative-present-pl/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,sind).
test(recVerb,all(MODE-TENSE-NUMPERS=[
                 indicative-present-pl/2,imperative-present-pl/2])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,seid), SEM-INF==be-sei.
test(recVerb,all(MODE-TENSE-NUMPERS=
                  [conjunctive-present-sg/1
                  ,imperative-present-sg/2
                  ,conjunctive-present-sg/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,sei), SEM-INF==be-sei.
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,seist),
  SEM-INF-MODE-TENSE-NUMPERS==be-sei-conjunctive-present-sg/2.
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                  [be-sei-conjunctive-present-pl/1
                  ,be-sei-conjunctive-present-pl/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,seien).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,seiet),
  SEM-INF-MODE-TENSE-NUMPERS==be-sei-conjunctive-present-pl/2.
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                  [be-sei-indicative-past-sg/1
                  ,be-sei-indicative-past-sg/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,war).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,warst),
  SEM-INF-MODE-TENSE-NUMPERS==be-sei-indicative-past-sg/2.
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                  [be-sei-indicative-past-pl/1
                  ,be-sei-indicative-past-pl/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,waren).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,wart),
  SEM-INF-MODE-TENSE-NUMPERS==be-sei-indicative-past-pl/2.
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                  [be-sei-conjunctive-past-sg/1
                  ,be-sei-conjunctive-past-sg/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,wäre).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,wärst),
  SEM-INF-MODE-TENSE-NUMPERS==be-sei-conjunctive-past-sg/2.
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                  [be-sei-conjunctive-past-pl/1
                  ,be-sei-conjunctive-past-pl/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,wären).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,wärt),
  SEM-INF-MODE-TENSE-NUMPERS==be-sei-conjunctive-past-pl/2.

test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,_NUMPERS,gewesen),
  SEM-INF-MODE-TENSE==be-sei-indicative-past(participle).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,_NUMPERS,seiende),
  SEM-INF-MODE-TENSE==be-sei-indicative-present(participle).


test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                  [allowed-dürf-indicative-present-sg/1
                  ,allowed-dürf-indicative-present-sg/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,darf).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,darfst),
  SEM-INF-MODE-TENSE-NUMPERS==
    allowed-dürf-indicative-present-sg/2.
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=[
                  allowed-dürf-indicative-present-pl/1
                  ,allowed-dürf-conjunctive-present-pl/1
                  ,allowed-dürf-indicative-present-pl/3
                  ,allowed-dürf-conjunctive-present-pl/3
                  ,allowed-dürf-infinite-infinite-_
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,dürfen).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,dürft),
  SEM-INF-MODE-TENSE-NUMPERS==
    allowed-dürf-indicative-present-pl/2.
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                  [allowed-dürf-conjunctive-present-sg/1
                  ,allowed-dürf-conjunctive-present-sg/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,dürfe).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,dürfest),
  SEM-INF-MODE-TENSE-NUMPERS==
    allowed-dürf-conjunctive-present-sg/2.
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,dürfet),
  SEM-INF-MODE-TENSE-NUMPERS==
    allowed-dürf-conjunctive-present-pl/2.

test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                  [allowed-dürf-indicative-past-sg/1
                  ,allowed-dürf-indicative-past-sg/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,durfte).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,durftest),
  SEM-INF-MODE-TENSE-NUMPERS==
    allowed-dürf-indicative-past-sg/2.
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                  [allowed-dürf-indicative-past-pl/1
                  ,allowed-dürf-indicative-past-pl/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,durften).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,durftet),
  SEM-INF-MODE-TENSE-NUMPERS==
    allowed-dürf-indicative-past-pl/2.

test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                  [allowed-dürf-conjunctive-past-sg/1
                  ,allowed-dürf-conjunctive-past-sg/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,dürfte).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,dürftest),
  SEM-INF-MODE-TENSE-NUMPERS==
    allowed-dürf-conjunctive-past-sg/2.
test(recVerb,all(SEM-INF-MODE-TENSE-NUMPERS=
                  [allowed-dürf-conjunctive-past-pl/1
                  ,allowed-dürf-conjunctive-past-pl/3
                 ])):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,dürften).
test(recVerb):-
  verb_(SEM,INF,MODE,TENSE,NUMPERS,dürftet),
  SEM-INF-MODE-TENSE-NUMPERS==
    allowed-dürf-conjunctive-past-pl/2.



test(genCard,all(GENDER-CASE-W=
                [masculin-nom-ein,masculin-acc-einen,masculin-gen-eines
                ,masculin-dat-einem,feminin-nom-eine,feminin-acc-eine
                ,feminin-gen-einer,feminin-dat-einer,neutral-nom-ein
                ,neutral-acc-ein,neutral-gen-eines,neutral-dat-einem
                ])):-
  card(1,CARD,NUM,GENDER,CASE,W), CARD-NUM==ein-sg.
test(genCard,all(NUM-CASE-W=
                [pl-nom-zwei,pl-acc-zwei,pl-gen-zweier,pl-dat-zweien
                ])):-
  card(2,CARD,NUM,GENDER,CASE,W), var(GENDER), CARD==zwei.
test(genCard,all(NUM-CASE-W=
                [pl-nom-drei,pl-acc-drei,pl-gen-dreier,pl-dat-dreien
                ])):-
  card(3,CARD,NUM,GENDER,CASE,W), var(GENDER), CARD==drei.
test(genCard):-
  card(4,CARD,NUM,GENDER,CASE,W),
  CARD-NUM-W=vier-pl-vier, var(GENDER), var(CASE).
test(genCard):-
  card(12,CARD,NUM,GENDER,CASE,W),
  CARD-NUM-W=zwölf-pl-zwölf, var(GENDER), var(CASE).
test(genCard):-
  card(17,CARD,NUM,GENDER,CASE,W),
  CARD-NUM-W=siebzehn-pl-siebzehn, var(GENDER), var(CASE).
test(genCard):-
  card(20,CARD,NUM,GENDER,CASE,W),
  CARD-NUM-W=zwanzig-pl-zwanzig, var(GENDER), var(CASE).
test(genCard):-
  card(76,CARD,NUM,GENDER,CASE,W),
  CARD-NUM-W=sechsundsiebzig-pl-sechsundsiebzig, var(GENDER), var(CASE).
test(genCard):-
  card(200,CARD,NUM,GENDER,CASE,W),
  CARD-NUM-W=zweihundert-pl-zweihundert, var(GENDER), var(CASE).
test(genCard):-
  card(773,CARD,NUM,GENDER,CASE,W),
  CARD-NUM-W=siebenhundertdreiundsiebzig-pl-siebenhundertdreiundsiebzig,
  var(GENDER), var(CASE).


test(recCard,all(NR-CARD-NUM-GENDER-CASE=
                [1-ein-sg-masculin-nom
                ,1-ein-sg-neutral-nom
                ,1-ein-sg-neutral-acc
                ])):-
  card(NR,CARD,NUM,GENDER,CASE,ein).
test(recCard,all(NR-CARD-NUM-GENDER-CASE=
                [1-ein-sg-feminin-nom
                ,1-ein-sg-feminin-acc
                ])):-
  card(NR,CARD,NUM,GENDER,CASE,eine).
test(recCard):-
  card(NR,CARD,NUM,GENDER,CASE,einen),
  NR-CARD-NUM-GENDER-CASE=1-ein-sg-masculin-acc.
test(recCard,all(NR-CARD-NUM-GENDER-CASE=
                [1-ein-sg-masculin-gen
                ,1-ein-sg-neutral-gen
                ])):-
  card(NR,CARD,NUM,GENDER,CASE,eines).
test(recCard,all(NR-CARD-NUM-GENDER-CASE=
                [1-ein-sg-feminin-gen
                ,1-ein-sg-feminin-dat
                ])):-
  card(NR,CARD,NUM,GENDER,CASE,einer).
test(recCard):-
  card(NR,CARD,NUM,GENDER,CASE,einem),
  NR-CARD-NUM-GENDER-CASE=1-ein-sg-masculin-dat.
test(recCard,all(NR-CARD-NUM-CASE=
                [2-zwei-pl-nom
                ,2-zwei-pl-acc
                ])):-
  card(NR,CARD,NUM,GENDER,CASE,zwei), var(GENDER).
test(recCard):-
  card(NR,CARD,NUM,GENDER,CASE,zweier),
  NR-CARD-NUM-CASE=2-zwei-pl-gen, var(GENDER).
test(recCard):-
  card(NR,CARD,NUM,GENDER,CASE,zweien),
  NR-CARD-NUM-CASE=2-zwei-pl-dat, var(GENDER).
test(recCard):-
  card(NR,CARD,NUM,GENDER,CASE,sechs),
  NR-CARD-NUM=6-sechs-pl, var(GENDER), var(CASE).
test(recCard):-
  card(NR,CARD,NUM,GENDER,CASE,zwölf),
  NR-CARD-NUM=12-zwölf-pl, var(GENDER), var(CASE).
test(recCard):-
  card(NR,CARD,NUM,GENDER,CASE,siebzehn),
  NR-CARD-NUM=17-siebzehn-pl, var(GENDER), var(CASE).
test(recCard):-
  card(NR,CARD,NUM,GENDER,CASE,fünfundsiebzig),
  NR-CARD-NUM=75-fünfundsiebzig-pl, var(GENDER), var(CASE).
test(recCard):-
  card(NR,CARD,NUM,GENDER,CASE,dreihundert),
  NR-CARD-NUM=300-dreihundert-pl, var(GENDER), var(CASE).
test(recCard):-
  card(NR,CARD,NUM,GENDER,CASE,fünfhundertzweiundsiebzig),
  NR-CARD-NUM=572-fünfhundertzweiundsiebzig-pl, var(GENDER), var(CASE).

test(genOrd,all(ORD-GENDNUM-CASE-W=
                [erst-masculin-nom-erste
                ,erst-masculin-gen-ersten
                ,erst-masculin-dat-ersten
                ,erst-masculin-acc-ersten
                ,erst-feminin-nom-erste
                ,erst-feminin-gen-ersten
                ,erst-feminin-dat-ersten
                ,erst-feminin-acc-erste
                ,erst-neutral-nom-erste
                ,erst-neutral-gen-ersten
                ,erst-neutral-dat-ersten
                ,erst-neutral-acc-erste
                ,erst-pl-nom-ersten
                ,erst-pl-gen-ersten
                ,erst-pl-dat-ersten
                ,erst-pl-acc-ersten
                ])):-
  ord(1,ORD,def,GENDNUM,CASE,W).
test(genOrd,all(ORD-GENDNUM-CASE-W=
                [erst-masculin-nom-erster
                ,erst-masculin-gen-ersten
                ,erst-masculin-dat-ersten
                ,erst-masculin-acc-ersten
                ,erst-feminin-nom-erste
                ,erst-feminin-gen-ersten
                ,erst-feminin-dat-ersten
                ,erst-feminin-acc-erste
                ,erst-neutral-nom-erstes
                ,erst-neutral-gen-ersten
                ,erst-neutral-dat-ersten
                ,erst-neutral-acc-erstes
                ,erst-pl-nom-ersten
                ,erst-pl-gen-ersten
                ,erst-pl-dat-ersten
                ,erst-pl-acc-ersten
                ])):-
  ord(1,ORD,indef,GENDNUM,CASE,W).
test(genOrd,all(ORD-GENDNUM-CASE-W=
                [erst-masculin-nom-erster
                ,erst-masculin-gen-ersten
                ,erst-masculin-dat-erstem
                ,erst-masculin-acc-ersten
                ,erst-feminin-nom-erste
                ,erst-feminin-gen-erster
                ,erst-feminin-dat-erster
                ,erst-feminin-acc-erste
                ,erst-neutral-nom-erstes
                ,erst-neutral-gen-ersten
                ,erst-neutral-dat-erstem
                ,erst-neutral-acc-erstes
                ,erst-pl-nom-erste
                ,erst-pl-gen-erster
                ,erst-pl-dat-ersten
                ,erst-pl-acc-erste
                ])):-
  ord(1,ORD,no,GENDNUM,CASE,W).


test(genIndef,all(INDEF-GENDNUM-CASE-W=
                [viel-masculin-nom-viele
                ,viel-masculin-gen-vielen
                ,viel-masculin-dat-vielen
                ,viel-masculin-acc-vielen
                ,viel-feminin-nom-viele
                ,viel-feminin-gen-vielen
                ,viel-feminin-dat-vielen
                ,viel-feminin-acc-viele
                ,viel-neutral-nom-viele
                ,viel-neutral-gen-vielen
                ,viel-neutral-dat-vielen
                ,viel-neutral-acc-viele
                ,viel-pl-nom-vielen
                ,viel-pl-gen-vielen
                ,viel-pl-dat-vielen
                ,viel-pl-acc-vielen
                ])):-
  indef(muchmany,INDEF,def,GENDNUM,CASE,W).
test(genIndef,all(INDEF-GENDNUM-CASE-W=
                [viel-masculin-nom-vieler
                ,viel-masculin-gen-vielen
                ,viel-masculin-dat-vielen
                ,viel-masculin-acc-vielen
                ,viel-feminin-nom-viele
                ,viel-feminin-gen-vielen
                ,viel-feminin-dat-vielen
                ,viel-feminin-acc-viele
                ,viel-neutral-nom-vieles
                ,viel-neutral-gen-vielen
                ,viel-neutral-dat-vielen
                ,viel-neutral-acc-vieles
                ,viel-pl-nom-vielen
                ,viel-pl-gen-vielen
                ,viel-pl-dat-vielen
                ,viel-pl-acc-vielen
                ])):-
  indef(muchmany,INDEF,indef,GENDNUM,CASE,W).
test(genIndef,all(INDEF-GENDNUM-CASE-W=
                [viel-masculin-nom-vieler
                ,viel-masculin-gen-vielen
                ,viel-masculin-dat-vielem
                ,viel-masculin-acc-vielen
                ,viel-feminin-nom-viele
                ,viel-feminin-gen-vieler
                ,viel-feminin-dat-vieler
                ,viel-feminin-acc-viele
                ,viel-neutral-nom-vieles
                ,viel-neutral-gen-vielen
                ,viel-neutral-dat-vielem
                ,viel-neutral-acc-vieles
                ,viel-pl-nom-viele
                ,viel-pl-gen-vieler
                ,viel-pl-dat-vielen
                ,viel-pl-acc-viele
                ])):-
  indef(muchmany,INDEF,no,GENDNUM,CASE,W).

test(genPersPronoun,all(C-W=[nom-ich,acc-mich
                             ,dat-mir,gen-mein])):-
  persPronoun(sg/1,_G,C,W).
test(genPersPronoun,all(C-W=[nom-du,acc-dich
                             ,dat-dir,gen-dein])):-
  persPronoun(sg/2,_G,C,W).
test(genPersPronoun,all(G-C-W=[
         feminin-nom-sie,masculin-nom-er,neutral-nom-es,
         feminin-acc-sie,masculin-acc-ihn,neutral-acc-es,
         feminin-dat-ihr,masculin-dat-ihm,neutral-dat-ihm,
         feminin-gen-ihr,masculin-gen-sein,neutral-gen-sein
                             ])):-
  persPronoun(sg/3,G,C,W).
test(genPersPronoun,all(C-W=[nom-wir,acc-uns
                             ,dat-uns,gen-unser])):-
  persPronoun(pl/1,_G,C,W).
test(genPersPronoun,all(C-W=[nom-ihr,acc-euch
                             ,dat-euch,gen-euer,gen-eur])):-
  persPronoun(pl/2,_G,C,W).
test(genPersPronoun,all(C-W=[nom-sie,acc-sie
                             ,dat-ihnen,gen-ihr])):-
  persPronoun(pl/3,_G,C,W).

test(recPersPronoun):-
  persPronoun(PLNUM,_G,C,ich), PLNUM-C=sg/1-nom.
test(recPersPronoun):-
  persPronoun(PLNUM,_G,C,mich), PLNUM-C=sg/1-acc.
test(recPersPronoun):-
  persPronoun(PLNUM,_G,C,mein), PLNUM-C=sg/1-gen.
test(recPersPronoun):-
  persPronoun(PLNUM,_G,C,mir), PLNUM-C=sg/1-dat.

test(recPersPronoun):-
  persPronoun(PLNUM,_G,C,du), PLNUM-C=sg/2-nom.
test(recPersPronoun):-
  persPronoun(PLNUM,_G,C,dich), PLNUM-C=sg/2-acc.
test(recPersPronoun):-
  persPronoun(PLNUM,_G,C,dein), PLNUM-C=sg/2-gen.
test(recPersPronoun):-
  persPronoun(PLNUM,_G,C,dir), PLNUM-C=sg/2-dat.

test(recPersPronoun):-
  persPronoun(PLNUM,G,C,er), PLNUM-G-C=sg/3-masculin-nom.
test(recPersPronoun):-
  persPronoun(PLNUM,G,C,ihn), PLNUM-G-C=sg/3-masculin-acc.
test(recPersPronoun,all(PLNUM-G-C=[sg/3-masculin-gen,
                                   sg/3-neutral-gen])):-
  persPronoun(PLNUM,G,C,sein).
test(recPersPronoun,all(PLNUM-G-C=[sg/3-masculin-dat,
                                   sg/3-neutral-dat])):-
  persPronoun(PLNUM,G,C,ihm).
test(recPersPronoun,set(PLNUM-G-C=['Sie'-_-acc,
                                  'Sie'-_-nom,
                                   sg/3-feminin-acc,
                                   sg/3-feminin-nom,
                                   pl/3-_-acc,
                                   pl/3-_-nom])):-
  persPronoun(PLNUM,G,C,sie).
test(recPersPronoun,all(PLNUM-G-C=['Sie'-_-gen,
                                   pl/2-_-nom,
                                   sg/3-feminin-dat,
                                   sg/3-feminin-gen,
                                   pl/3-_-gen])):-
  persPronoun(PLNUM,G,C,ihr).
test(recPersPronoun,all(PLNUM-C=['Sie'-dat,
                                   pl/3-dat])):-
  persPronoun(PLNUM,_G,C,ihnen).
test(recPersPronoun,all(PLNUM-G-C=[sg/3-neutral-nom,
                                   sg/3-neutral-acc])):-
  persPronoun(PLNUM,G,C,es).

test(recPersPronoun):-
  persPronoun(PLNUM,_G,C,wir), PLNUM-C=pl/1-nom.
test(recPersPronoun,set(PLNUM-C=[pl/1-acc,pl/1-dat])):-
  persPronoun(PLNUM,_G,C,uns).
test(recPersPronoun):-
  persPronoun(PLNUM,_G,C,unser), PLNUM-C=pl/1-gen.

test(recPersPronoun):-
  persPronoun(PLNUM,_G,C,euer), PLNUM-C=pl/2-gen.
test(recPersPronoun,set(PLNUM-C=[pl/2-acc,pl/2-dat])):-
  persPronoun(PLNUM,_G,C,euch).

:- end_tests(deDict).

















