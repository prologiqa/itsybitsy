%
% Winnie-the-Pooh verbs
% Kili�n Imre, 2020-June

/*verb(accept,accept,[nom,acc]).
verb(access,access,[nom,acc]).
verb(accompany,accompany,[nom,acc]).
%verb(act,act,[nom]).
verb(adapt,adapt,[nom,acc]).
%verb(add,add,[nom,acc]).
verb(adjust,adjust,[nom,acc]).
verb(advance,advance,[nom,acc]).
%verb(agree,agree,([nom,clause(that)];
%                  [nom,verbal(to)];
%                  [nom,?(with(acc))])).
verb(alarm,alarm,[nom,acc]).
verb(align,align,[nom,acc]).
%verb(allow,allow,[nom,acc,verbal(to)]).
verb(announce,announce,[nom,acc]).
%verb(answer,answer,([nom,acc,?(acc)];
%                    [nom,for(_)];
%                    [nom,to(acc)])).
verb(apply,apply,[nom,acc]).
verb(appreciate,appreciate,[nom,acc]).
verb(approve,approve,[nom,acc]).
verb(arrest,arrest,[nom,acc]).
%verb(arrive,arrive,[nom,to(acc)]).
%verb(ask,ask,[nom,acc]).
verb(assemble,assemble,[nom,acc]).
verb(attach,attach,[nom,acc]).
verb(attack,attack,[nom,acc]).
verb(attempt,attempt,[nom,?(verbal(to))]).
verb(average,average,[nom,acc]).
verb(avoid,avoid,[nom,acc]).
verb6(awake,awake,awakes,awoke,awaken,[nom,acc]).
%verb(bake,bake,[nom,acc]).
verb(base,base,[nom,acc]). %HU:alapoz, alap�t
verb(bath,bath,[nom,?(acc)]).
%verb(be,be,was,were,been,being,will,[nom,?(acc)]).
%verb6(become,become,becomes,became,become,[nom,acc]).
%verb(begin,begin,[nom,acc]).
%verb(believe,believe,[nom,in(acc)];[nom,clause]).
%verb(belong,belong,[nom,to(acc)]).
verb(bend,bend,bent,[nom]). %HU:hajol
verb6(bite,bite,bites,bit,bitten,[nom,acc]). %HU: harap
verb(bleed,bleed,[nom]).
verb6(blow,blow,blows,blew,blown,[nom,?(acc)]). %HU: f�j
%verb(boil,boil,[nom,?(acc)]). %HU: f�z/f�
%verb(bond,bond,[nom,acc]).
%verb(book,book,[nom,acc]).
%verb(born,born,[nom,acc]).
%verb(borrow,borrow,[nom,acc]).
%verb(bother,bother,[nom,acc]).
%verb6(break,break,breaks,broke,broken,[nom,acc]).
%verb(breathe,breathe,[nom]).
%verb(bring,bring,brought,[nom,acc]). %bring+back
%verb(brush,brush,[nom,acc]).
%verb(build,build,built,[nom,acc]).
verb(burn,burn,[nom,acc]).
verb(burn,burn,burnt,[nom,acc]).
verb(burst,burst,[nom,acc]).
%verb(buy,buy,bought,[nom,acc]).
%verb(call,call,[nom,acc]).
%verb(camp,camp,[nom]).
%verb(calculate,calculate,[nom,acc]).
verb(cancel,cancel,[nom,acc]).
verb(care,care,[nom,?(for(acc))]).
%verb(carry,carry,[nom,acc]).
%verb(cash,cash,[nom,acc]).
%verb(catch,catch,caught,[nom,acc]).
verb(cause,cause,[nom,acc]). %HU:okoz
verb(challenge,challenge,[nom,acc]).
%verb(change,change,[nom,?(acc)]).
verb(charge,charge,[nom,?(acc)]).
verb(chase,chase,[nom,acc]).
%verb(check,check,[nom,acc]).
%verb6(choose,choose,chooses,chose,chosen,[nom,acc]).
%verb(clean,clean,[nom,acc]).
%verb(click,click,[nom,acc]).
%verb(climb,climb,[nom,acc]).
%verb(close,close,[nom,acc]).
%verb(collect,collect,[nom,acc]).
%verb(colour,colour,[nom,acc]).
%verb6(come,come,comes,came,come,[nom]). %come.back
verb(command,command,[nom]).
verb(comment,comment,[nom,?(acc)]).
verb(communicate,communicate,[nom,with(acc)]).
verb(compare,compare,([nom,acc,with(acc)];
                      [nom,acc,to(acc)])).
%verb(complete,complete,[nom,acc]).
verb(compress,compress,[nom,acc]).
verb(compete,compete,[nom,in(nom)]).
verb(connect,connect,[nom,acc]).
%verb(contact,contact,[nom,acc]).
verb(contain,contain,[nom,acc]).
%verb(continue,continue,[nom,acc]).
verb(control,control,[nom,acc]).
%verb(cook,cook,[nom,acc]).
%verb(copy,copy,[nom,acc]).
verb(correct,correct,[nom,acc]).
%verb(cost,cost,[nom,acc]).
verb(count,count,[nom,acc]).
%verb(cover,cover,[nom,acc]).
verb(crack,crack,[nom,acc]).
%verb(cross,cross,[nom,acc]).
verb(crush,crush,[nom,acc]).
%verb(cry,cry,[nom]).
%verb(cut,cut,cut,[nom,acc]).
%verb(cycle,cycle,[nom]).
verb(damage,damage,[nom,acc]).
%verb(dance,dance,[nom,?(acc)]).
verb(date,date,[nom,acc]).
%verb(decide,decide,[nom,?(verbal(to))]).
verb(decrease,decrease,[nom,acc]).
verb(deflate,deflate,[nom,acc]).
%verb(delay,delay,[nom,acc]).
verb(desire,desire,[nom,?(verbal(to))]).
%verb(describe,describe,[nom,acc]).
verb(develop,develop,[nom,acc]).
%verb(die,die,[nom]).
verb(digest,digest,[nom,acc]).
verb(disappear,disappear,[nom]).
verb(discard,discard,[nom,acc]).
%verb(discuss,discuss,[nom,acc]).
verb(dispatch,dispatch,[nom,acc]).
verb(display,display,[nom,acc]).
%verb(dive,dive,[nom]).
verb(divide,divide,[nom,acc]).
verb(divorce,divorce,[nom,from(acc)]).
%verb6(do,do,does,did,done,[nom,?(acc),?(to(acc))]).
%verb(doubt,doubt,([nom,clause];
%                  [nom,clause(that)];
%                  [nom,clause(if)])).
%verb(download,download,[nom,acc]).
verb(drain,drain,[nom,acc]).
%verb(draw,draw,[nom,acc]).
%verb(dream,dream,[nom,acc]).
%verb(dress,dress,[nom,acc]).
%verb6(drink,drink,drinks,drank,drunk,[nom,acc]).
%verb6(drive,drive,drives,drove,driven,[nom,?(acc)]).
verb(drop,drop,[nom,acc]).
%verb(dry,dry,[nom,acc]).
%verb(earn,earn,[nom,acc]).
%verb6(eat,eat,eats,ate,eaten,[nom,?(acc)]).
%verb(educate,educate,[nom,acc]).
%verb(email,email,[nom,acc]).
verb(effect,effect,[nom,acc]).
verb(elaborate,elaborate,[nom,acc]).
%verb(end,end,[nom,?(acc)]).
%verb(enjoy,enjoy,([nom,acc];
%                 [nom,verbal(ing)])).
%verb(enter,enter,[nom,?(acc)]).
verb(erase,erase,[nom,acc]).
verb(estimate,estimate,[nom,acc]).
verb(exaggerate,exaggerate,[nom,acc]).
verb(examine,examine,[nom,acc]).
verb(excite,excite,[nom,acc]).
%verb(excuse,excuse,[nom,acc]).
%verb(exercise,exercise,[nom,acc]).
%verb(explain,explain,[nom,acc,?(acc)]).
%verb(explore,explore,[nom,acc]).
%verb(fail,fail,[nom]).
verb(faint,faint,[nom]).
%verb6(fall,fall,falls,fell,fallen,[nom]).
verb(farm,farm,[nom,acc]).
%verb(feel,feel,felt,[nom,acc]).
%verb(fight,fight,fought,[nom,acc]).
%verb(fill,fill,[nom,acc]). %fill.in
%verb(film,film,[nom,acc]).
%verb(find,find,found,[nom,acc]). %find.out
%verb(finish,finish,([nom,acc];
%                    [nom,verbal(ing)])).
%verb(fire,fire,[nom,acc]).
%verb(fish,fish,[nom,acc]).
verb(fix,fix,[nom,acc]).
verb(flow,flow,[nom,acc]).
%verb6(fly,fly,flies,flew,flown,[nom]).
verb(fold,fold,[nom,acc]).
%verb(follow,follow,[nom,acc]).
%verb(forget,forget,[nom,acc]).
verb(forward,forward,[nom,acc]). %HU:tov�bb�t
verb6(freeze,freeze,freezes,froze,frozen,[nom]).
verb(gain,gain,[nom,acc]).
verb(gather,gather,[nom,acc]).
%verb6(get,get,gets,got,got,([nom,acc];[nom,verbal(ing)];
%                           [nom,?(acc),verbal(to)])). %get.back,
%                           get.up, get.off
%verb6(give,give,gives,gave,given,([nom,acc,acc];
%                                 [nom,acc,to(acc)])). %give.back
%verb6(go,go,goes,went,gone,([nom,?(to(acc))]; %go.out
%                            [nom,verbal(to)])).
verb(grip,grip,[nom,acc]). %HU:megragad
%verb6(grow,grow,grows,grew,grown,[nom,?(acc)]). %grow up
%verb(guess,guess,[nom,acc]).
%verb(guide,guide,[nom,acc]).
verb(hang,hang,hung,[nom,?(acc)]). %HU:f�gg, l�g
%verb(happen,happen,[nom,?(acc)]).
%verb(hate,hate,([nom,acc];[nom,verbal(to)])).
%verb6(have,have,has,had,had,[nom,acc]). %have.got.to, have.to
%verb(hear,hear,[nom,acc]).
%verb(help,help,[nom,?(acc),?(verbal(to))]).
%verb(hike,hike,[nom]).
%verb(hit,hit,hit,[nom,acc]).
%verb(hold,hold,held,[nom,acc]).
%verb(hope,hope,([nom,?(verbal(to))];
%                [nom,clause];[nom,clause(that)])).
%verb(hurry,hurry,[nom]).
%verb(hurt,hurt,[nom,acc]).
verb(identify,identify,[nom,acc]).
verb(ignore,ignore,[nom,acc]).
verb(impact,impact,[nom]).
verb(imply,imply,[nom,acc]).
verb(imprison,imprison,[nom,acc]).
%verb(improve,improve,[nom,acc]).
%verb(include,include,[nom,acc]).
verb(increase,increase,[nom,?(acc),?(by(acc))]). %HU:n�vekszik, n�vel
verb(inform,inform,[nom,acc]).
verb(inquire,inquire,[nom,about(acc)]).
verb(install,install,[nom,acc]).
verb(interpret,interpret,[nom,acc]).
verb(introduce,introduce,[nom,acc]).
%verb(invite,invite,[nom,acc]).
%verb(join,join,[nom,acc]).
verb(judge,judge,[nom,acc]). %HU:�t�l
%verb(jump,jump,[nom]).
%verb(keep,keep,kept,[nom,acc]).
%verb(kick,kick,[nom,acc]).
verb(kill,kill,[nom,acc]).
%verb(kiss,kiss,[nom,acc]).
%verb6(know,know,knows,knew,known,([nom,?(acc)];
%                [nom,clause];[nom,clause(wh)]
%                ;[nom,clause(if)];[nom,clause(that)];
%                                 [nom,verbal(wh)])).
%verb(laugh,laugh,[nom,?(at(acc))]).
verb(lead,lead,[nom,acc]).
%verb6(learn,learn,learns,learned,learnt,([nom,acc];
%                                         [nom,verbal(to)];
%                                         [nom,clause(that)];
%                                         [nom,verbal(wh)])).
%verb(leave,leave,left,[nom,?(acc)]).
%verb(lend,lend,lent,[nom,acc]).
%verb(let,let,let,[nom,acc]).
%verb(lie,lie,[nom,acc]). %lie.down
verb(lift,lift,[nom,acc]).
%verb(like,like,([nom,acc];
%                [nom,verbal(ing)];
%                [nom,verbal(to)];
%                [nom,clause(wh)])).
%verb(listen,listen,[nom,to(acc)]).
verb(light,light,[nom,?(acc)]).
%verb(listen,listen,[nom,?(to(acc))]).
%verb(live,live,[nom]).
verb(load,load,[nom,acc]).
verb(locate,locate,[nom,acc]).
verb(lock,lock,[nom,acc]).
%verb(look,look,([nom,acc]; %look.after, look.at, look.for, look.out
%                [nom,at(acc)])).
verb(looklike,look,[nom,like(acc)]).
verb(lookfor,look,[nom,for(acc)]).
%verb(lose,lose,lost,[nom,acc]).
%verb(love,love,([nom,acc];[nom,verbal(ing)])).
verb(mail,mail,[nom,acc]).
%verb(make,make,made,[nom,acc,?(clause)]). %make.sure
verb(map,map,[nom,?(acc)]).
verb(marry,marry,[nom,?(acc)]).
%verb(matter,matter,[nom,?(acc)]). %it doesn't matter
%verb(mean,mean,meant,[nom,acc]).
verb(measure,measure,[nom,acc]).
%verb(meet,meet,met,[nom,acc]).
verb(melt,melt,[nom]).
%verb(mind,mind,([nom,verbal(ing)];
%                [nom,clause(if)];
%                [nom,clause(wh)])).
%verb(miss,miss,[nom,?(acc)]).
%verb(mix,mix,[nom,acc]).
verb(mount,mount,[nom]).
%verb(move,move,[nom,?(acc),?(to(acc))]). %HU: mozog, mozgat
%verb(need,need,([nom,acc];
%               [nom,verbal(to)])).
verb(nod,nod,[nom]).
%verb(note,note,[nom,acc]).
verb(notice,notice,[nom,acc]).
verb(obey,obey,[nom,acc]).
verb(occur,occur,[nom,acc]).
%verb(offer,offer,[nom,?(acc),?(verbal(to))]).
%verb(open,open,[nom,acc]).
verb(operate,operate,[nom,acc]).
%verb(order,order,[nom,acc]).
verb(oversee,oversee,[nom,acc]).
verb(own,own,[nom,acc]).
%verb(pack,pack,[nom]).
%verb(paint,paint,[nom,?(acc)]).
%verb(park,park,[nom]).
%verb(pass,pass,[nom,acc]).
%verb(pay,pay,[nom,acc]).
verb(permit,permit,[nom,acc]).
%verb(phone,phone,[nom,acc]).
%verb(pick,pick,[nom,acc]). %pick.up
verb(pin,pin,[nom,acc]). %HU: t�z, szegez
%verb(plan,plan,[nom,acc]).
verb(plant,plant,[nom,acc]).
%verb(play,play,[nom,?(acc)]).
%verb(please,please,[nom,acc]).
%verb(point,point,[nom,at(acc)]).
%verb(post,post,[nom,acc]).
verb(polish,polish,[nom,acc]).
%verb(practise,practise,[nom,to(acc)]).
%verb(prefer,prefer,[nom,to(acc)]).
%verb(prepare,prepare,[nom,acc]).
verb(present,present,[nom,acc,acc]).
verb(preserve,preserve,[nom,acc]).
verb(pretend,pretend,[nom,acc]).
verb(prevent,prevent,[nom,acc]).
%verb(print,print,[nom,acc]).
verb(produce,produce,[nom,acc]).
verb(protest,protest,[nom,against(acc)]).
verb(prove,prove,[nom,acc]).
verb(provide,provide,[nom,acc]).
%verb(pull,pull,[nom,acc]).
verb(pump,pump,[nom,acc]).
verb(punish,punish,[nom,acc]).
%verb(push,push,[nom,acc]).
%verb(put,put,put,[nom,acc,to(acc)]). %put-on
verb(queue,queue,[nom]).
%verb(race,race,[nom]).
%verb(rain,rain,[nom]).
%verb(read,read,read,[nom,?(acc)]).
%verb(receive,receive,[nom,acc]).
verb(recommend,recommend,[nom,acc]).
%verb(record,record,[nom,acc]).
verb(refer,refer,[nom,to(acc)]).
verb(refuel,refuel,[nom,acc]).
verb(refuse,refuse,[nom,acc]).
verb(reject,reject,[nom,acc]).
verb(regret,regret,[nom,acc]).
verb(relate,relate,[nom,to(nom)]).
verb(release,release,[nom,acc]).
verb(relax,relax,[nom]).
%verb(remember,remember,[nom,acc]).
verb(remove,remove,[nom,acc]).
%verb(rent,rent,[nom,acc]).
%verb(repair,repair,[nom,acc]).
%verb(repeat,repeat,[nom,acc]).
verb(replace,replace,[nom,acc]).
verb(require,require,[nom,acc]).
verb(rescue,rescue,[nom,acc]).
verb(research,research,[nom,acc]).
%verb(rest,rest,[nom]).
%verb(retract,retract,[nom,acc]).
%verb(return,return,[nom,from(acc)]).
verb(reward,reward,[nom,acc]).
%verb6(ride,ride,rides,rode,ridden,[nom,acc]).
%verb(ring,ring,[nom]).
verb(risk,risk,[nom,acc]).
%verb(roast,roast,[nom,acc]).
verb(roll,roll,[nom,acc]). %�sszeg�ngy�l�t
verb(rub,rub,[nom,acc]).
%verb(run,run,ran,[nom]).
%verb(sail,sail,[nom,?(acc)]).
%verb(save,save,[nom,acc]).
%verb(say,say,[nom,acc]).
verb(scare,scare,[nom,acc]). %HU:f�leml�t
verb(schedule,schedule,[nom,acc]). %HU:f�leml�t
verb(search,search,[nom,acc]).
%verb6(see,see,sees,saw,seen,([nom,acc];
%                [nom,clause(if)];
%                [nom,clause(that)];
%                [nom,clause(wh)])).
%verb(seem,seem,[nom]).
verb(seal,seal,[nom,?(acc)]).
%verb(sell,sell,sold,[nom,?(acc),?(to(acc))]).
%verb(send,send,sent,[nom,?(acc),?(to(acc))]).
%verb(serve,serve,[nom,acc]).
verb(set,set,set,[nom,?(acc),?(to(acc))]).
verb(separate,separate,[nom,acc]).
%verb(share,share,[nom,acc]).
verb6(shake,shake,shakes,shook,shaken,[nom,acc]).
verb(ship,ship,[nom,acc]).
%verb(shop,shop,[nom,acc]).
%verb(shout,shout,[nom,?(with(acc))]).
%verb(show,show,shown,[nom,acc]).
verb(shower,shower,[nom,?(acc)]).
%verb(shut,shut,shut,[nom,acc]).
verb(sign,sign,[nom,acc]).
%verb6(sing,sing,sings,sang,sung,[nom,?(acc)]).
%verb6(sit,sit,sits,sat,sat,[nom]). %sit.down
%verb(skate,skate,[nom]).
%verb(ski,ski,[nom]).
%verb(sleep,sleep,[nom,?(through(acc))]).
verb(slip,slip,[nom]).
verb(smash,smash,[nom]).
verb(smell,smell,[nom,acc]).
verb(smile,smile,[nom]).
%verb(smoke,smoke,[nom,?(acc)]).
verb(sneeze,sneeze,snoze,[nom]). %HU:t�sszent
%verb(snow,snow,[nom]).
%verb(sound,sound,[nom,acc]).
%verb(speak,speak,[nom,?(to(acc))]).
%verb(spell,spell,[nom,acc]).
%verb(spend,spend,spent,[nom,acc]).
verb(spot,spot,[nom,acc]).
verb(sprain,sprain,[nom,acc]).
%verb(stand,stand,[nom]).
%verb(star,star,[nom,acc]).
verb(stare,stare,[nom,acc]).
%verb(start,start,[nom,?(acc)]).
verb(station,station,[nom,?(acc)]).
%verb(stay,stay,[nom,loc]).
%verb6(steal,steal,steals,stole,stolen,[nom,acc]).
verb(step,step,[nom,to(acc)]).
verb(stitch,stitch,[nom,acc,to(acc)]). %r�varr, r�t�z
%verb(stop,stop,stopped,([nom,acc];[nom,verbal(to)];[nom,verbal(ing)])).
verb(stretch,stretch,[nom,acc]). %HU:fesz�t, ny�jt
verb(struggle,struggle,[nom,?(verbal(to))]).
%verb(study,study,[nom,acc]).
verb(subordinate,subordinate,[nom,acc]).
verb(subtract,subtract,[nom,acc,?(from(acc))]).
%verb(suggest,suggest,[nom,acc,?(to(acc))]). %+ing
verb(supply,supply,[nom,acc,?(with(acc))]).
verb(support,support,[nom,acc]).
%verb(suppose,suppose,[nom,acc]).
%verb(surf,surf,[nom,acc]).
verb(surprise,surprise,[nom,acc]).
%verb(swim,swim,swam,[nom]).
verb(tag,tag,[nom,acc]).
% verb6(take,take,takes,took,taken,([nom,acc,?(acc)]; %take.off
% %take.part
%                                 [nom,from(acc)];
%                                 [nom,up(acc)])).
%verb(talk,talk,([nom,?(acc)];
%               [nom,about(acc)])).
verb(taste,taste,[nom,acc]).
%verb(teach,teach,taught,[nom,acc,?(verbal(to))]).
%verb(telephone,telephone,[nom,acc]).
%verb(tell,tell,[nom,acc,?(acc)]).
verb(tend,tend,[nom,verbal(to)]).
%verb(test,test,[nom,acc]).
%verb(text,text,[nom,acc,?(acc)]).
%verb(thank,thank,[nom,acc,?(acc)]).
%verb(think,think,([nom,acc];[nom,clause(that)];
%                             [nom,clause])).
verb(threaten,threaten,[nom,acc]).
%verb6(throw,throw,throws,threw,thrown,[nom,acc]).
%verb(tidy,tidy,[nom,acc]). %tidy.up
verb(tighten,tighten,[nom,acc]).
verb(touch,touch,[nom,acc]).
verb(tow,tow,[nom,acc]).
verb(train,train,[nom,acc]).
verb(transmit,transmit,[nom,acc]).
%verb(travel,travel,[nom,in(acc)]).
%verb(try,try,([nom,?(acc)]; %try.on
%              [nom,verbal(to)])).
verb(tune,tune,[nom,acc]).
%verb(turn,turn,[nom,to(acc)]). %turn.off, turn.on
verb(twist,twist,[nom]).
verb(type,type,[nom,acc]).
%verb(understand,understand,understood,([nom,acc];
%                                      [nom,clause(wh)];
%                                      [nom,clause(if)];
%                                      [nom,verbal(to)])).
%verb(upload,upload,[nom,acc,to(acc)]).
%verb(use,use,[nom,?(acc)]).
%verb(visit,visit,[nom,acc]).
%verb(wait,wait,[nom,?(for(acc))]).
%verb6(wake,wake,wakes,waked,woken,[nom]). %wake up
%verb(walk,walk,[nom]).
%verb(want,want,[nom,?(acc),?(verbal(to))]).
%verb(wash,wash,[nom,acc]). %wash.up
%verb(watch,watch,[nom,acc]).
verb(weigh,weigh,[nom,acc]).
%verb6(wear,wear,wears,wore,worn,[nom,acc]).
verb(weld,weld,[nom,acc]).
verb(whip,whip,[nom,acc]).
%verb(win,win,won,[nom,acc]).
%verb(wish,wish,([nom,acc,?(acc)];
%               [nom,clause])).
verb(whistle,whistle,[nom,?(acc)]).
%verb(work,work,[nom]).
%verb(worry,worry,[nom,?(about(acc))]).
%verb6(write,write,writes,wrote,written,[nom,acc]). %write down

%verbPart(growUp,grow,up).
%verbPart(lookFor,look,for).

*/










