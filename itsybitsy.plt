:- use_module(['itsybitsy.pl']).
:- begin_tests(itsybitsy).

:- op(200,xfy,:=).
:- op(400,yfx,#).


%
% Tests for answer generation
%
%nominal predicative query
%should rather be: X^(X instanceOf eeyore#donkey).
test(whoIsEeyore):-
    sentence2Log("Who is Eeyore?",_,donkey,donkey:=donkey^true).
test(whoIsEeyore):-
    tryAnswer("Who is Eeyore?","Eeyore is donkey.").

%nominal predicative query with two subjects
test(whoAreKangaAndBabyRoo):-
    tryAnswer("Who are Kanga and Baby Roo?",
              "Kanga and Baby Roo are kangaroos.").
test(whoAreKangaAndBabyRoo):-
    sentence2Log("Who are Kanga and Baby Roo?",_,
                 kangaroo,kangaroo:=kangaroo^true).
test(whoAreKangaAndPiglet):-
    tryAnswer("Who are Kanga and Piglet?",
              "Kanga and Piglet are vertebrates.").


%nominal subject query
test(whoIsBear):-
    sentence2Log("Who is bear?",_,X,X:=X^bear(X)).
test(whoIsBear):-
    tryAnswer("Who is bear?","Winnie the Pooh is bear.").

%nominal single subject query with definite article
test(whoIsThePig):-
    sentence2Log("Who is the pig?",_,P,P:=only(X^pig(X))).
test(whoIsThePig):-
    tryAnswer("Who is the pig?","Piglet is the pig.").

test(whichAnimalIsBear):-
    tryAnswer("Which animal is bear?","Winnie the Pooh is bear.").

%nominal single subject query with indefinite article
test(whoIsAKangaroo):-
    sentence2Log("Who is a kangaroo?",_,X,X:=X^kangaroo(X)).
test(whoIsAKangaroo,all(A=["Kanga is a kangaroo.",
                           "Baby Roo is a kangaroo."])):-
    tryAnswer("Who is a kangaroo?",A).

%nominal plural subject query
test(whoAreKangaroos):-
    sentence2Log("Who are kangaroos?",_,K,K:=K^kangaroo(K)).
test(whoAreKangaroos,set(A=["Baby Roo is kangaroo.",
                           "Kanga is kangaroo."])):-
    tryAnswer("Who are kangaroos?",A).

%nominal plural definite subject query
test(whoAreTheKangaroos):-
    sentence2Log("Who are the kangaroos?",_,CL,
                 L:=each(X^kangaroo(X))).
test(whoAreTheKangaroos):-
    tryAnswer("Who are the kangaroos?",
              "Baby Roo and Kanga are the kangaroos.").

%nominal subject query with genitive construct
test(whoIsBabyRoosMother):-
    sentence2Log("Who is Baby Roo's mother?",
                 _,X,X:=X^(mother(X),have(roo#kangaroo,mother,X))).
test(whoIsBabyRoosMother):-
    tryAnswer("Who is Baby Roo\'s mother?",
              "Kanga is Baby Roo's mother.").

%nominal subject query with adjective construct
test(whoArePigletsMaleFriends):-
    sentence2Log("Who are Piglet's male friends?",
      _,FL,FL:=each(X^(friend(X),male(X),have(piglet#pig,friend,X)))).
test(whoArePigletsMaleFriends):-
    tryAnswer("Who are Piglet's male friends?",
 "Christopher Robin, Winnie the Pooh and Tigger are Piglet\'s male friends.").

%nominal yn question
test(isPigletAPig):-
    sentence2Log("Is Piglet a pig?",_,_,?((pig(piglet#pig),true))).
test(isPigletAPig):-
    tryAnswer("Is Piglet a pig?","Yes, Piglet is a pig.").

test(isPigletAShyPig):-
    sentence2Log("Is Piglet a shy pig?",_,_,
                 ?((pig(piglet#pig),shy(piglet#pig),true))).
test(isPigletAPig):-
    tryAnswer("Is Piglet a shy pig?","Yes, Piglet is a shy pig.").

test(isPigletADonkey):-
    sentence2Log("Is Piglet a donkey?",_,_,?((donkey(piglet#pig),true))).
test(isPigletADonkey):-
    tryAnswer("Is Piglet a donkey?","No, Piglet isn\'t a donkey.").

test(isPigletShyAnimal):-
    sentence2Log("Is Piglet a shy animal?",
      _,_,?((animal(piglet#pig),shy(piglet#pig),true))).
test(isPigletShyAnimal):-
    tryAnswer("Is Piglet a shy animal?","Yes, Piglet is a shy animal.").

test(areKangaBabyRooKangaroos):-
    sentence2Log("Are Kanga and Baby Roo kangaroos?",_,_,
    ?(foreach((lists:member(X,[kanga#kangaroo,roo#kangaroo]),true,true),
               kangaroo(X)))).
test(areKangaBabyRooKangaroos):-
    tryAnswer("Are Kanga and Baby Roo kangaroos?",
              "Yes, Kanga and Baby Roo are kangaroos.").

%cardinality question on subject
test(howManyMaleAnimalsLiveInHundredAcreWood):-
    sentence2Log("How many male animals live in Hundred Acre Wood?",
      _,NR,NR:=count(X^(animal(X),male(X),
                      live(X,in(woodland),hundredAcreWood#woodland)))).
test(howManyMaleAnimalsLiveInHundredAcreWood):-
    tryAnswer("How many male animals live in Hundred Acre Wood?",
              "Three male animals live in Hundred Acre Wood.").

%cardinality question on object
test(howManyMaleFriendsDoesPigletHave):-
    sentence2Log("How many male friends does Piglet have?",
      _,NR,NR:=count(X^(friend(X),male(X),have(piglet#pig,friend,X)))).
test(howManyMaleFriendsDoesPigletHave):-
    tryAnswer("How many male friends does Piglet have?",
              "Piglet has three male friends.").


%subject question with object
test(whoEatsMaize):-
    tryAnswer("Who eats maize?","Piglet eats maize.").

%subject question with object
test(whichAnimalEatsMaize):-
    sentence2Log("Which animal eats maize?",
      _,A,A:=A^(animal(A),maize(M),eat(A,maize,M))).
test(whichAnimalEatsMaize):-
    tryAnswer("Which animal eats maize?",
              "Piglet eats maize.").

%noun phrase with present participle
test(whoAreAnimalsEatingMaize):-
    sentence2Log("Who are animals eating maize?",
      _,X,X:=X^(animal(X),maize(M),eat(X,maize,M))).
test(whoAreAnimalsEatingMaize):-
    tryAnswer("Who are animals eating maize?",
              "Piglet is animal eating maize.").

%subject question, inference in root worldlet
test(whatDoesPigletEat):-
    sentence2Log("What does Piglet eat?",
      _,WH,WH:=WH^eat(piglet#pig,WH)).

%subject question, inference in root worldlet
test(whatDoPigletEeyoreEat):-
    sentence2Log("What do Piglet and Eeyore eat?",
      _,WH,WH:=each(F^(lists:member(A,[piglet#pig,eeyore#donkey]),eat(A,F)))).
test(whatDoPigletEeyoreEat):-
    tryAnswer("What do Piglet and Eeyore eat?",
      "Piglet and Eeyore eat thistle and maize.").

%locative question
test(whereDoesEeyoreLive):-
    tryAnswer("Where does Eeyore live?",
      "Eeyore lives in Pooh\'s Corner.").
test(whereDoesEeyoreLive):-
    sentence2Log("Where does Eeyore live?",
      _,LOC,LOC:=LOC^live(eeyore#donkey,in(noun),LOC)).

%locative question with two subjects
test(whereDoPigletEeyoreLive):-
    tryAnswer("Where do Piglet and Eeyore live?",
              "Piglet and Eeyore live in Hundred Acre Wood.").
test(whereDoPigletEeyoreLive):-
    sentence2Log("Where do Piglet and Eeyore live?",
      _,LOC,LOC:=list(L^(lists:member(X,[piglet#pig,eeyore#donkey]),
                      live(X,in(noun),L)),geopart)).

%nondeterministic locative question
test(whoLivesInHundredAcreWood,
    set(A=["Owl lives in Hundred Acre Wood.",
           "Tigger lives in Hundred Acre Wood.",
           "Winnie the Pooh lives in Hundred Acre Wood.",
           "Christopher Robin lives in Hundred Acre Wood.",
           "Piglet lives in Hundred Acre Wood.",
           "Eeyore lives in Hundred Acre Wood.",
           "Kanga lives in Hundred Acre Wood.",
           "Rabbit lives in Hundred Acre Wood."])):-
    tryAnswer("Who lives in Hundred Acre Wood?",A).


%free arg question with preposition
%a free argument question
test(underWhatNameDoesPigletLive):-
    sentence2Log("Under what name does Piglet live?",
      _,N,N:=(N^(live(piglet#pig,under(name),N),name(N)))).
test(underWhatNameDoesPigletLive):-
    tryAnswer("Under what name does Piglet live?",
              "Piglet lives under the name of Trespassers W.").


%verbal yn question
test(doesPigletLoveEeyore):-
    sentence2Log("Does Piglet love Eeyore?",
      _,_,?(love(piglet#pig,donkey,eeyore#donkey))).
test(doesPigletLoveEeyore):-
    tryAnswer("Does Piglet love Eeyore?","Yes, Piglet loves Eeyore.").

test(doesPigletEatThistles):-
    sentence2Log("Does Piglet eat thistles?",
      _,_,?((thistle(T),eat(piglet#pig,thistle,T)))).
test(doesPigletEatThistles):-
    tryAnswer("Does Piglet eat thistles?",
              "No, Piglet doesn't eat thistles.").
%
% commonsense questions
%
test(howFarIsBudapestFromVienna):-
    sentence2Log("How far is Budapest from Vienna?",
                 _,_,X:=X^be(from(city),budapest#city,vienna#city,X)).
test(howFarIsBudapestFromVienna):-
    tryAnswer("How far is Budapest from Vienna?",
              "Budapest is 280 kilometers far from Vienna.").

test(howManyDaysAreThereInWeek):-
    sentence2Log("How many days are there in a week?",
                 _,_,NR:=count(X^(day(X),be(X,day,in(week))))).
test(howManyDaysAreThereInWeek):-
    tryAnswer("How many days are there in a week?",
              "There are seven days in a week.").

test(whichDayComesAfterSunday):-
    sentence2Log("Which day comes after Sunday?",
                 _,_,N:=N^(day(N),come(N,after(weekday),sunday#weekday))).
test(whichDayComesAfterSunday):-
    tryAnswer("Which day comes after Sunday?",
              "Monday comes after Sunday.").

test(whichDayFollowsSunday):-
    sentence2Log("Which day follows Sunday?",
                 _,_,N:=N^(day(N),follow(N,weekday,sunday#weekday))).
test(whichDayComesAfterSunday):-
    tryAnswer("Which day comes after Sunday?",
              "Monday comes after Sunday.").

test(whichDayPrecedesSunday):-
    sentence2Log("Which day precedes Sunday?",
                 _,_,N:=N^(day(N),precede(N,weekday,sunday#weekday))).
test(whichDayPrecedesSunday):-
    tryAnswer("Which day precedes Sunday?",
              "Saturday precedes Sunday.").

test(howManyMonthsAreThereInYear):-
    sentence2Log("How many months are there in a year?",
                 _,_,NR:=count(X^(month(X),be(X,month,in(year))))).
test(howManyMonthsAreThereInYear):-
    tryAnswer("How many months are there in a year?",
              "There are twelve months in a year.").

test(whichMonthFollowsDecember):-
    sentence2Log("Which month follows December?",
                 _,_,N:=N^(month(N),follow(N,month,december#month))).
test(whichMonthComesAfterMarch):-
    tryAnswer("Which month comes after March?",
              "April comes after March.").

test(whichMonthPrecedesJanuary):-
    sentence2Log("Which month precedes January?",
                 _,_,N:=N^(month(N),precede(N,month,january#month))).
test(whichMonthPrecedesJanuary):-
    tryAnswer("Which month precedes January?",
              "December precedes January.").



%declaratives - nominal implications
test(elephantsAreHerbivores):-
    sentence2Log("Elephants are herbivores.",_,_,elephant:herbivore).

%declaratives - nominal facts
test(heffalumpIsElephant):-
    sentence2Log("\"Heffalump\" is elephant.",
                 "Heffalump",heffalump#elephant,heffalump#elephant:elephant).
%declaratives - verbal relation-facts
test(winnieThePoohEatsHoney):-
    sentence2Log("Winnie the Pooh eats honey.",
                 _,winnieThePooh#bear,winnieThePooh#bear:eat(honey,_)).
%declaratives - verbal relation-facts with injected free arguments
test(winnieThePoohLivesInHundredAcreWood):-
    sentence2Log("Winnie the Pooh lives in Hundred Acre Wood.",
                 _,winnieThePooh#bear,
                 winnieThePooh#bear:live(in(woodland),hundredAcreWood)).

:- end_tests(itsybitsy).
