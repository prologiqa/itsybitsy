:- module(deMorph, [
              morphAnal/2     %analysing words on any but first position
                              %any words but interrogatives
              ,morph1Anal/2   %analysing words on the first position
              ,morphInterr2Anal/2 %interrogatives on the second position
              ,tokens2Line//1
          ]).

%general utility imports
%
%
% @tbd transcribe... fix tokens (z.B.)
% @tbd indef numerals (meisten) ...
% @tbd ...�s numerals �ltal�noss�gban!!!

%list operations
:- use_module(library(lists)).
%lazy reading tokens from a file to an open list
:- use_module('../lazytokens').

%:- use_module(deDict).
%dynamic import - module should be explicitely loaded by boot
%and here only the module name is used (instead of file name)
:- add_import_module(deMorph,deDict,start).
%:- use_module('../parser').


tranScribe(TOKEN,TOK1,TOK2):-
  transcribe(TOKEN,TOK1,TOK2), !.
tranScribe(TOKEN,TOK1,TOK2):-
  atom(TOKEN)->downcase_atom(TOKEN,DTOKEN), transcribe(DTOKEN,T1,TOK2),
  atom(TOK1), downcase_atom(TOK1,TOK2)->transcribe(TOKEN,T1,TOK2);
  downcase_atom(TOK1,DTOK1), transcribe(DTOKEN,DTOK1,TOK2),
    sub_atom(DTOKEN,0,1,_,DT1), sub_atom(DTOKEN,1,_,0,TAIL1),
    upcase_atom(DT1,UPDT1), atom_concat(UPDT1,TAIL1,TOKEN).
tranScribe(TOKEN1,TOKEN2,TOK1,TOK2):-
    transcribe(TOKEN1,TOKEN2,TOK1,TOK2), !.
tranScribe(TOKEN1,TOKEN2,TOK1,TOK2):-
  atom(TOKEN1), atom(TOKEN2)->
    downcase_atom(TOKEN1,DTOKEN1), transcribe(DTOKEN1,TOKEN2,T1,TOK2),
    sub_atom(T1,0,1,_,LET1), sub_atom(T1,1,_,0,TAIL1),
    upcase_atom(LET1,UPLET1), atom_concat(UPLET1,TAIL1,TOK1);
  atom(TOK1), atom(TOK2),     tranScribe(TOK1,TOK2,TOKEN1,TOKEN2).

%Reads a sentence from a list of tokens.
%The remainding list is returned
%The sentence is trans-tokenized if necessary
%
%It expects lines being closed by \n
%tokens2Line(-SENTENCE,+TOKENS,-REM)
%TOKENS: list of tokens (an open/lazy list)
%REM: remaining of list
%SENTENCE: one sentence (here: a line)

%Testing tokens2Line
% deMorph:tokens2Line(['An',dem, 'Xorx',geht,'\'',s,los, "."
% ],L,[alma]).
% deMorph:tokens2Line(X,['An',dem, 'Xorx',geht,'\'',s,los,
% '.', alma],R).

tokens2Line([])--> ["\n"], !.
tokens2Line(['!'])--> ["!"], !.
tokens2Line(['?'])--> ["?"], !.
tokens2Line(['.'])--> ["."], !.

%reading tokens from accumulator
tokens2Line([T1,T2|SENTENCE])--> [TOKENS1,"\'",TOKENS2],
  {nonvar(TOKENS1), nonvar(TOKENS2)}, !,
  {atom_string(TOKEN1,TOKENS1),atom_string(TOKEN2,TOKENS2),
   tranScribe(TOKEN1,TOKEN2,T1,T2)}, !,
     tokens2Line(SENTENCE).
%reading tokens from parameter
tokens2Line([T1,T2|SENTENCE])--> [TOKENS1,"\'",TOKENS2],
  {nonvar(T1), nonvar(T2), tranScribe(TOKEN1,TOKEN2,T1,T2)}, !,
  {atom_string(TOKEN1,TOKENS1),atom_string(TOKEN2,TOKENS2)},
     tokens2Line(SENTENCE).
tokens2Line([NUMBER|SENTENCE])--> [NUMTOKEN],
  {var(NUMBER), atom_number(NUMTOKEN,NUMBER)}, !,
  tokens2Line(SENTENCE).
tokens2Line([T1,T2|SENTENCE])--> [CONTRS],
  {nonvar(CONTRS), atom_string(CONTR,CONTRS), tranScribe(CONTR,T1,T2)}, !,
     tokens2Line(SENTENCE).
tokens2Line([T1,T2|SENTENCE])--> [CONTRS],
  {nonvar(T1), nonvar(T2), tranScribe(CONTR,T1,T2)}, !,
     {atom_string(CONTR,CONTRS)}, tokens2Line(SENTENCE).
tokens2Line([THISTOKEN|SENTENCE])--> [DQUOTED],
  {nonvar(DQUOTED), sub_string(DQUOTED,0,1,_,"\""),
     string_length(DQUOTED,TL), TL>1,
     TL2 is TL-2, sub_string(DQUOTED,1,TL2,_,THISTOKEN)}, !,
     tokens2Line(SENTENCE).
tokens2Line([STRING|SENTENCE])--> [DQUOTED],
  {string(STRING),
     atomics_to_string(["\"",STRING,"\""],DQUOTED)}, !,
     tokens2Line(SENTENCE).
tokens2Line([THISTOKEN|SENTENCE])--> [TOKEN],
  {atom_string(THISTOKEN,TOKEN)}, !, tokens2Line(SENTENCE).
tokens2Line([])--> [].


kind2FormNumPers(sg/3,inf,sg,3):- !.
kind2FormNumPers(KIND,KIND,_,_).

%a noun starts with capital letter, but is downcased
%if a noun is fully capitalized, than it remains so
normNoun(WORD,WORD):-
  upcase_atom(WORD,WORD), !.
normNoun(WORD,DOWNWORD):-
    sub_atom(WORD,0,1,_,FIRSTCH), upcase_atom(FIRSTCH,FIRSTCH),
    downcase_atom(WORD,DOWNWORD).

%common nouns (single nouns or consisting of at most three tags)
%noun_(SEM,NOUN,GEND,NUM,CASE,WORD):-
morphNounAnal(WORD,common(STEM,GENDER,NUM,CASE)):-
    normNoun(WORD,NORMED), noun_(_SEM,STEM,GENDER,NUM,CASE,NORMED).
morphNounAnal(WORD,common(STEMLIST,GENDER,NUM,CASE,NTH)):-
    normNoun(WORD,NORMED), noun_(_SEM,STEMLIST,GENDER,NUM,CASE,NTH,NORMED).


%proper nouns (single nouns or consisting of at most three tags)
%connected maybe by dashes, eg. T-shirt, James-Bond-Film
morphNounAnal(WORD,proper(STEM,CLASS,GEND,NUM,CASE)):-
    proper_(_,STEM,GEND,NUM,CASE,CLASS,WORD).
morphNounAnal(WORD,proper(COMPLETE,CLASS,GEND,NUM,CASE,NTH)):-
    proper_(_,COMPLETE,GEND,NUM,CASE,CLASS,NTH,WORD).


%verb: NUM _ ; sg/3... if sg/3==>FORM=inf
%verb:FORM inf;past;perfect;past(participle)
%morphAnal(+WORD,?INF).
%+WORD form, being read from input
%?INF abstract form
%
%
%punctuation sign
morphNoNounAnal(PUNCT,punct(PUNCT)):-
    punct(PUNCT), !.
%measurement unit abbreviation
morphNoNounAnal(ABBR,measure(ABBR,QUANT,abbr)):- measure(_,ABBR,QUANT,_).

%infinite particle ('to')
morphNoNounAnal(PART,infPart(PART)):- infPart(PART).

%ordinal with '.'
morphNoNounAnal(ORD,ord(NR)):- sub_atom(ORD,_,1,0,'.'),
    atom_concat(CARD,'.',ORD), atom_number(CARD,NR), !.
%cardinal digits as characters ('123')
morphNoNounAnal(CARD,card(NR,CARD)):-
    atom(CARD), atom_number(CARD,NR), !.
morphNoNounAnal(NR,card(NR,NR)):-
    number(NR), !.
%ordinal literally ('first'...'seventh', etc.)
morphNoNounAnal(WORD,ord(NR,ORD,weak,GENDER,CASE)):-
    ord(NR,ORD,GENDER,CASE,WORD).
%cardinal literally or as a number
morphNoNounAnal(WORD,card(NR,CARD,NUM,GENDER,CASE)):-
    card(NR,CARD,NUM,GENDER,CASE,WORD).
%indefinite numerals
morphNoNounAnal(WORD,indefNumeral(INDEF,DECL,GENDNUM,CASE)):-
    indefNum(_,INDEF,DECL,GENDNUM,CASE,WORD).

%personal pronouns (I, you, hers, etc.)
morphNoNounAnal(WORD,pronoun(NUMPERS,GENDER,CASE)):-
    persPronoun(NUMPERS,GENDER,CASE,WORD).

%reflexive pronouns (mich, sich etc.)
morphNoNounAnal(WORD,refl(NUMPERS)):-
    reflPronoun(NUMPERS,WORD).


%indefinite pronouns (someone, one, this etc.)
%indefPronoun(SEM,NUM,CASE) - SEMantics, NUMber, CASE
morphNoNounAnal(WORD,indefPronoun(INDEF,GENDER,NUM,CASE)):-
    indefPronoun(_,INDEF,_DECL,GENDER,NUM,CASE,WORD).

morphNoNounAnal(PRE,verbPrefix(PRE)):-
    verbPrefix_(PRE,separable).
%verbs with prefixes
morphNoNounAnal(WORD,verb(STEM,MODE,TENSE,NUM/PERS)):-
% %%supposing, there are no verbal homonyms (different forms)
    verb_(_SEM,STEM,MODE,TENSE,NUM/PERS,WORD),
    kind2FormNumPers(TENSE,FORM,NUM,PERS).  %??????
%modal verb particles (can, must, ought to...)
morphNoNounAnal(MODAL,modal(MODAL,MODE,present,FR)):-
    modal(MODAL,_,MODE,FR).
morphNoNounAnal(MODPAST,modal(MODAL,MODE,past,FR)):-
    modal(MODAL,MODPAST,MODE,FR).


%adjectives (consisting of at most of 3 tags)
morphNoNounAnal(WORD,adj(ADJ,GRAD,DECL,GENDNUM,CASE)):-
    adj_(_SEM,ADJ,GRAD,DECL,GENDNUM,CASE,WORD).

%adverbs
morphNoNounAnal(WORD,adv(ADV,KIND,GRADE)):-
    adv_(_SEM,ADV,KIND,GRADE,WORD).

%articles
morphNoNounAnal(WORD,art(ART,KIND,GENDER,CASE)):-
    art_(ART,KIND,GENDER,CASE,WORD).

%demonstrative pronouns
morphNoNounAnal(WORD,demPronoun(DEM,DKIND,GENDNUM,CASE)):-
    demPronoun(_,DEM,DKIND,GENDNUM,CASE,WORD).

%determiners
morphNoNounAnal(DET,det(DET,NUM,NUM0,KIND)):-
    det(_,NUM,DET,NUM0,KIND).

morphNoNounAnal(WORD,det([WORD|LIST],NUM,NUM0,KIND,1)):-
    det(_,NUM,[WORD|LIST],NUM0,KIND).
morphNoNounAnal(WORD,det([W1,WORD|LIST],NUM,NUM0,KIND,2)):-
    det(_,NUM,[W1,WORD|LIST],NUM0,KIND).
morphNoNounAnal(WORD,det([W1,W2,WORD],NUM,NUM0,KIND,3)):-
    det(_,NUM,[W1,W2,WORD],NUM0,KIND).

%possessive pronouns
morphNoNounAnal(WORD,possPronoun(OWNER,OGEND,GENDNUM,CASE)):-
    possPronoun(OWNER,OGEND,GENDNUM,CASE,WORD).

%prepositions, postpositions
morphNoNounAnal(PREP,prep(PREP,CASE)):-
    prep(_,PREP,CASE).
morphNoNounAnal(POSTP,postp(POSTP,CASE)):-
    postp(_,POSTP,CASE).

morphNoNounAnal(WORD,INTERR):-
    morphInterrAnal(WORD,INTERR).
morphNoNounAnal(CONJ,conj(CONJ,KIND)):-
    conj(_,CONJ,KIND).
morphNoNounAnal(CONJ,conj(CONJ,KIND/SUB)):-
    conj(_,CONJ,SUB,KIND).
morphNoNounAnal(INTERJ,interj(IJ,INTERJ)):- %SEM should not get to parser
    interj(IJ,INTERJ).

morphNoNounAnal(s,token(s)).

%common interrogatives
morphInterrAnal(WORD,interr(INTERR,KIND,GENDNUM,CASE)):-
    interr(_SEM,INTERR,KIND,GENDNUM,CASE,WORD).
%first tag of double interrogatives 'wie (viel)'
morphInterrAnal(WORD,interr([LI|ST],KIND,1)):-
    interr(_SEM,[LI|ST],KIND,1,WORD).
%second tag of double interrogatives '(wie) viel'
morphInterrAnal(WORD,interr([L,I|ST],KIND,2)):-
    interr(_SEM,[L,I|ST],KIND,2,WORD).

%interrogatives appearing also in the second position of a sentence
%second tag of double interrogatives '(wie) viel'
morphInterr2Anal(WORD,interr([LI|ST],KIND,2)):-
    interr(_SEM,[LI|ST],KIND,2,WORD).
%interrogatives extendable with prefix '(an) wem' ... '(seit) wann'
morphInterr2Anal(WORD,interr(INTERR,KIND,GENDNUM,CASE)):-
    interr(_SEM,INTERR,KIND,GENDNUM,CASE,WORD), interrPrefixable(INTERR).



morphAnal(WORD,WORD):- string(WORD), !.
%capital cased tokens can be nouns or 'Sie' pronouns
morphAnal(WORD,MORPH):- atom(WORD), \+ downcase_atom(WORD,WORD), !,
    (persPronoun('Sie',G,C,WORD), MORPH=persPronoun('Sie',G,C);
    possPronoun('Sie',OG,G,C,WORD), MORPH=possPronoun('Sie',OG,G,C);
    morphNounAnal(WORD,MORPH)).
%lowcased tokens can be second tags of a compound noun
morphAnal(WORD,common(STEMLIST,GENDER,NUM,CASE,NTH)):-
    noun_(_SEM,STEMLIST,GENDER,NUM,CASE,NTH,WORD).
%or they may be any other kind of words
morphAnal(WORD,MORPH):- morphNoNounAnal(WORD,MORPH).


morph1Anal(WORD,WORD):- string(WORD), !.
morph1Anal(-,punct(-)):- punct(-), !.
morph1Anal(WORD,MORPH):-
     morphNounAnal(WORD,MORPH).
morph1Anal(WORD,MORPH):-
    downcase_atom(WORD,DOWNWORD), morphNoNounAnal(DOWNWORD,MORPH).

:-begin_tests(deMorph).


skipCommentsEmpties--> ["%"], !, tokens2Line(LINE), !,
  {atomic_list_concat(["%"|LINE],' ',LINEATOM),
   debug(morph,'COMMENT:~w',[LINEATOM])},
  skipCommentsEmpties.
skipCommentsEmpties--> ["\n"], !, skipCommentsEmpties.
skipCommentsEmpties--> [].

%morphThisSentence(+SENTENCE,-MORPHS) is semidet.
%+SENTENCE: a string to contain the sentence
%-MORPHS:
%Should parsing fail, either fails the predicate too
%or an empty list will be returned
morphThisSentence(LINE,NR,MORPHS):- string(LINE), !,
  string_to_tokens(LINE,TOKENS), tokens2Line(SENTENCE,TOKENS,[]),
  morphThisSentence(SENTENCE,NR,MORPHS).
morphThisSentence([WORD|SENTENCE],NR,[MORPHLS|MORPHS]):-
  findall(MORPH,morph1Anal(WORD,MORPH),MORPHLS), MORPHLS=[_|_], !,
    maplist(debug(morph,'~w'),MORPHLS),
    (MORPHLS=[punct(-)]->morphThisSentence(SENTENCE,NR,MORPHS);
     morphThisSentenceTail(SENTENCE,NR,MORPHS)).
morphThisSentence([WORD|SENTENCE],NR,MORPHS):-
  debug(morph,'**** ERROR **** ~w',[WORD]),
     morphThisSentenceTail(SENTENCE,NR0,MORPHS), NR is NR0+1.

morphThisSentenceTail([],0,[]):- !.
%after ':' there comes a word starting with capital letter
morphThisSentenceTail([:|SENTENCE],NR,[punct(:)|MORPHLS]):- !,
  debug(morph,'~w',punct(:)), morphThisSentence(SENTENCE,NR,MORPHLS).
morphThisSentenceTail([WORD|SENTENCE],NR,[MORPHLS|MORPHS]):-
  findall(MORPH,morphAnal(WORD,MORPH),MORPHLS),
    MORPHLS=[_|_]-> maplist(debug(morph,'~w'),MORPHLS),
    morphThisSentenceTail(SENTENCE,NR,MORPHS);
  debug(morph,'**** ERROR **** ~w',[WORD]),
    morphThisSentenceTail(SENTENCE,NR0,MORPHS), NR is NR0+1.


morphTestfile(ERR)-->
  skipCommentsEmpties, tokens2Line(SENTENCE), {SENTENCE=[_|_]},
    {debug(morph,'SENTENCE: ~w\n',[SENTENCE])},
    ({morphThisSentence(SENTENCE,NRERR,_MORPHS)}->
      {ERR1 is ERR+NRERR}, morphTestfile(ERR1);
    {ERR1 is ERR+1}, morphTestfile(ERR1)).
morphTestfile(TOTALERROR)--> !,
    {debug(morph,'TOTAL ERRORS: ~w\n',[TOTALERROR])}.

%morphTestfile(+FILE) is det.
%The specified testFILE is parsed. At the end a statistics is displayed
%For diagnostic messages call: debug(morph)
%
morphTestfile(TESTFILE):-
  exists_file(TESTFILE),
  open(TESTFILE, read, In, []), %OpenOptions),
  stream_to_lazy_tokens(In,TOKENS),
  morphTestfile(0,TOKENS,_REM),
  close(In).


%test(a2sentences):-
%    morphTestfile('A2test.txt').

test(sentence):-
  morphThisSentence("Am n�chsten Montag:Jetzt geht's leider nicht.",NR,ML).

:-end_tests(deMorph).










