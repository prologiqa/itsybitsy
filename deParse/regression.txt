﻿%
% Adjective noun phrases
%

%a pure adjective
Ich sehe eine schöne Frau.

%a single noun in singular
Apfel ist rot.

Mutter isst Apfel.

%a single noun in plural
Äpfel sind rot.

%a noun with capitals only
Mutter sieht CD.

%a noun of two tags
Mutter sieht Laptop Computer.

%a noun of two tags
Dein DVD player ist laut.

Dein CD-player ist laut.

%Plural noun
Kinder sind froh.

%an unknown proper name between double quotes
"Bükkösd" ist schön.

%known proper name starting with capital
Europa ist schön.

%A known proper with its class as a noun
Stadt "Wien" ist schön.

%An unknown proper with its class as a noun
Stadt "Pécs" ist schön.

%Proper noun with two tags
New York ist schön.

%Proper noun with two tags and with its class
Stadt New York ist schön.

%Proper noun with two tags but with not its known class
Staat New York ist schön.

%Known proper noun with a known class
Land Schweiz ist schön. 

%Unknown proper noun with a known class
Land "Transylvania" ist schön.

%a noun with a plain/base adjective
Ein rotes Auto ist schön.

Blaue Autos sind schön.

%20Noun with superlative adjective
Das größte Auto ist schön.

%cumulated adjectives
Große, blaue Autos sind schön.

%gerund as an adjective
Tanzende Mädchen sind lustig.

Ich sehe die tanzenden Mädchen.

Das kochende Wasser ist heiß.

%participle as an adjective
Das gelesene Buch war lustig.

Ich finde den mitgebrachten Regenschirm nicht.

%a noun with an adjective, both with two tags
Das ist ein schön aussehendes Auto.

Das ist eine schön aussehende Kamera.

%
% indefinite pronouns/stand-alone determiners
%
Jemand kommt.

Der Hund hat etwas gegessen.

Jemand singt ein Lied.

Jemand hat die Tür zugemacht.

Jeder kann Lehrer werden.

Ich brauche nichts.

Niemand ist gekommen.

Ich habe nichts in meiner Tasche gehabt.

Viele mögen Kaffee.

Manche fahren sehr schnell.

Jeder konnte nach Hause gehen.

%40
Ich habe keine gesehen.

%
% adjective phrases
%
%a noun with a possessive structure
Sein Geschäft ist auf der linken Seite der Straße.

%a noun with an adverb-adjective pair
Ein sehr rotes Auto ist schön.

Ein fast rotes Auto ist schön.

%cumulated adjective phrases
Sehr große, fast rote Autos sind schön.

%measurements
Das zwei Meter lange Auto ist mein.

Der zwei Jahre lange Urlaub hat mir gefallen.

Das 4 Meter lange Auto ist schön.


%
% Numeral noun phrases
%
%a noun with a cardinal numeral
Ich habe drei Hunde gesehen.

Sie ist eine Mutter von drei Kindern.

%a noun with a cardinal numeral and adjective
Ich habe drei große Äpfel gegessen.

%a noun with a numeral as a number
Ich habe 3 große Äpfel gegessen.

%a noun with ordinal numeral
Gestern aß ich den dritten Apfel.

%a noun with ordinal numeral
Gestern aß ich den 3. Apfel.

%a noun with a indefinite numeral, requiring plural
Ich habe viele rote Äpfel gesehen.

%a noun with a indefinite numeral, requiring singular
Ich habe gestern viel Wasser getrunken.

%a noun with a definite article
Der Apfel ist rot.

%a noun with an indefinite article
Ein Apfel ist rot.

Ein Auto ist grün.

%a noun with a single demonstrating pronoun
Ich habe diese schöne Frau nicht gesehen.

Ich liebe diese schöne Frau.

%a noun with a single demonstrating pronoun
Jene rote Autos sind schön.

Diese 3 grüne Äpfel sind zu groß.

%a noun with numerals
Ich habe auch drei Kinder gehabt.

Ich habe nur drei Kinder gehabt.

Das erste Kind war Julia.

%a noun with adjective and quantifier
Jedes rote Auto ist schön.

Alle rote Autos sind schön.

Ich habe nicht alle schöne Frauen gesehen.

Ich möchte etwas Wasser.

Manche Leute trinken keine Milch.

Ich habe viel Äpfel gesammelt.

Er trinkt keine Milch.

Die meisten Leute mögen Milch.

Ich habe keine Hunde gesehen.

Ich habe einige gute Bücher.

Du sollst etwas Erfahrung haben.

Jeder Morgen ist schön.

Meine beiden Eltern sind alt.

Beide Geschäfte sind geschlossen.

%80
Ich habe das Haus zum halben Preis gekauft.

Du hast noch eine halbe Stunde.

Sie hat ein paar schöne Jahre haben müssen.

Sie hatte einige schöne Jahre haben sollen.

%
% Determiners - and more
%
Alle rote Äpfel sind hier.

Alle deiner Kinder sind hier gekommen.

Nur wenige Schlangen sind gefährlich.

Ich habe viel Brot gegessen.

Ich habe zu viel Bier getrunken.

Er hat sein ganzes Geld geliehen.

Keines der Restaurants war geöffnet.

Keiner der Studenten ging nach Hause.

Ein meiner Eltern besucht mich.

Wir könnten unsere Eltern besuchen.

Wir besuchten unsere Eltern.

"Suzy" kannte keine ihrer Großeltern.

Ich brauche ein paar Wochen für diese Aufgabe.

Ich habe viele Kuchen für die Party gebracht.

Wir werden viele Kuchen für die Party brauchen.

%100
Ich brauche ein bisschen mehr Information.

Ich habe eine Reihe von fantastischen Mädchen gesehen.

Ich brauchte eine Brille zum Lesen.

Eine Menge Arbeit ist genug.

Du brauchst genug Geld in New York.

Kinder sollten bestimmte Bücher nicht lesen.

Sie müssen ein anderes Auto kaufen.

Wir finden andere Freunde.

Wir brauchen so einen guten Freund.

%Fragen
%Was für ... an object question - in past tense
Was für einen schönen Urlaub hatten wir?

%Was für ... an object question - in present tense
Was für einen schönen Urlaub haben wir?

%Was für ... an subject question - in present tense
Was für ein schöner Urlaub kommt?

%Was für ... an subject question - in simple future tense
Was für ein schöner Urlaub wird kommen?

%Was für plural
Was für Kenntnisse braucht man?

%Was für Materialnomen
Was für Wasser trinkst du?

%Was für with preposition
In was für einer Stadt wohnen wir?

Von was für Häusern sprichst du?


%Ausrufe
Was für ein schönes Wetter!
Was für nette Leute!
Was für ein guter Arzt!

%121
Wir hatten gestern eine sehr schöne Zeit.

Das Wetter war gestern eher schlecht.

Wir wohnen im selben Haus.

%
%modifiers of noun phrases
%prepositions, postpositions
%
Julia ging die Straße entlang.

Dem Vorschlag gemaß fuhr ich nach Berlin.

%a prepositional noun phrase as a post modifier
Der Junge im Garten ist stark.

Ich sehe das Mädchen mit der Brille.

%a verbal phrase with -participle, as an adjective modifier
Der von meiner Tochter beworbene Job ist interessant.

Das von dem Polizisten gestern gesehene Auto fuhr zu schnell.

Sie mochte das gestern gelesene Buch.

Das von dir gestern gefahrene Auto war schnell.

Ein geprüfter PC ist immer teuer.

Ich finde den gestern von dir gebrachten Regenschirm nicht.

Die von der Firma gebaute Häuser sind modern.

Der von meinem Vater gerufene Mann steht vor der Tür.

%verbal relative nominal/subjective subphrase connected to a noun phrase
Ich mag das Mädchen, das so schön singt.

Magst du das Mädchen, das so schön gesungen hatte?

Das ist die Julia, die so schön gesungen hat.

%nominal relative nominal/subjective subphrase
Du kennst den Klaus, der gerade ein Polizist ist.

%140
Das ist mein Bruder, der immer ein Polizist gewesen ist.

Du kennst den Klaus, der immer kluger war als sein Bruder.

%verbal relative argumental verbal subphrase connected to a noun phrase
Ich sehe den Lehrer, den du magst.

Ich mag den Lehrer, dem du einen Apfel gegeben hast.

Ich mag den Arzt mit dem wir geredet haben.

Ich mag den Künstler, wem du einen Brief gegeben hast.

%Verbal main sentence, infix relative verbal subclauses
John, den ich kenne, hat dich gesehen.

Das Auto, das du auf der Straße gesehen hast, fährt schnell.

Das Auto, das vor der Tür steht, kann schnell fahren.

%Verbal main sentence, infix relative verbal subclauses
Klaus, den du kennst, ist sehr lustig.

Das Auto, das vor der Tür steht, ist rot.

Das Auto, das vor der Tür steht, ist rot gewesen. 

Das Auto, mit dem unsere Freunde gefahren sind, ist rot.

Julia, die schön singt, isst Apfel.

Julia, deren Vater ein Polizist ist, isst Apfel.

Laura, mit der du getanzt hast, trägt die Post aus.

Der Mann, der in Budapest wohnt, heißt Klaus.

Ich mag das Buch, in dem du einen Fehler gefunden hast.

Alles, was ich gesagt habe, ist wahr.

Das ist die Frau, deren Mann du kennst.

%160
Das ist der Lehrer, der unser Freund ist.

Das ist die Frau, die die Post austrägt.

Die sind gemeinsame Autos, mit denen bereits viele gefahren sind.

Ich mag Bücher, in denen du schöne Bilder gefunden hast.

Es gab etwas, womit ich nichts getan habe.

%subclauses relative to adverbs
Ich warte dort, wo wir trafen.

%subclauses relative to the last verbal argument
Mein Vater hat etwas gebracht, womit er arbeiten kann.


%
%Personal pronouns
%
% present tense - personal pronouns
%
Ich bin nett.

Du bist nett.

Er ist nett.

Sie ist nett.

Es ist nett.

Wir sind nett.

Ihr seid nett.

Sie sind nett.


%
% present tense - personal pronouns
%
Ich war nett.

Du warst nett.

Er war nett.

Sie war nett.

Es war nett.

%180
Wir waren nett.

Ihr wart nett.

Sie waren nett.

%
% possessive pronoun structures
%
Mein Freund ist ein Lehrer.

Dein Freund ist ein Arzt.

Sein Freund ist ein Polizist.

Ihre Freundin ist eine Sekretärin.

Seine Tür ist alt.

Unsere Kinder sind klug.

Eure Küche ist neu.

Ihr Auto ist schwer.

%
% nominal predicates
%

%further possessive structrues
Die beiden braunen Hunde eines guten Freunds sind hungrig.

Die drei roten Autos deines alten Freunds sind schön.

2 Hunde deiner reichen Eltern sind weiß.

Ich habe eine kleine Nummer von Kindern gehabt.

%possessive predicate
Das rote Auto ist mein.

Das weiße Auto ist unser.

Jenes Auto ist euer.

Dein ist rot.

Ihre sind schwarz.

%
% reflexive pronouns/adverbs
%200
Ich sehe mich selbst.

Ich habe das Buch für mich selbst gekauft.

Meine Mutter hat diesen Kuchen selbst gekocht.

Meine 7 Jahre alte Tochter hat diesen Kuchen selbst gemacht.

%
% Tenses and modes - Active and passive voice mixed
%

%simple present active
Sie redet.

Ich lese ein Buch.

%simple present with modal verb
Du musst endlich heimgehen.

Ich will ein Eis essen

%simple present active denying
Ich lese nicht ein Buch.

%simple present passive
Das Buch wurde von mir gelesen.

%simple passive present with modal verb
Das Buch muss von mir gelesen werden.

Das Buch muss gelesen werden.

%simple passive present with modal verb
Muss das Buch gleich jetzt gelesen werden?

%simple present passive denying
Sie redet nicht.

Das Buch wird von mir nicht gelesen werden.

%interrogative simple present
Liest du ein Buch?

%interrogative simple present nominal with modal verb
Kann sie hungrig sein?

%interrogative simple present nominal with modal verb and mid-adverb
Kann sie immer hungrig sein?


%interrogative simple present
Wird das Buch von dir gelesen?

%interrogative simple present denying
Wird das Buch von dir nicht gelesen?

Trinkst du Wasser?

Wird das Wasser von dir getrunken?

Liest sie das Buch noch heute?

Wird das Buch von ihr nuch heute gelesen?

Ist das Buch zu lesen auf dem Tisch?


%negative simple present
Sie liest das Buch nicht.

Das buch wird von ihr nicht gelesen.

%interrogative negative simple present
Liest sie nicht das Buch heute?

Wird das Buch von ihr noch heute nicht gelesen?

%present perfect
Ich habe einen Apfel gegessen.

Ich habe ein Buch gelesen.

%negative present perfect
Ich habe den Apfel nicht gegessen.

%passive present perfect
Der Apfel ist gegessen worden.

%passive present perfect with modal
Tiere haben gesehen werden können.

%present perfect with 'von' clause
Der Apfel ist vom Arzt gegessen worden.

%passive present perfect with 'von' clause und modal verb
%*Der Apfel soll vom Arzt nicht gegessen worden sein.

%negative passive present perfect with 'von' clause
Der Apfel ist vom Arzt nicht gegessen worden.

%interrogative present perfect
Hat sie das Buch gelesen?

%interrogative present perfect
Bist du fleißig gewesen?

%interrogative present perfect with mid-adverb
Bist du jemals in Berlin gewesen?

%interrogative negative present perfect with mid-adverb
Bist du nicht jemals in Berlin gewesen?

%interrogative present perfect with two mid-adverbs
Bist du jemals wirklich in Berlin gewesen?

%interrogative passive present perfect with 'von' clause
Ist der Apfel von dem Arzt gegessen worden?

%interrogative negative passive present perfect with 'von' clause
Ist der Apfel von dem Arzt nicht gegessen worden?

%interrogative negative present perfect
Hat sie das Buch gelesen?

%present perfect with adverb
Sie hat den Apfel fast gegessen.

%simple past
Ich kannte dich.

%simple past passive
Das Buch wurde von dir gelesen.

%simple past passive denying
Das Buch wurde von dir nicht gelesen.

%simple passive present with modal verb
Tiere könnten hier gesehen werden.

%simple negative passive present with modal verb
Tiere könnten hier nicht gesehen werden.

%simple passive present with modal verb
Könnten Tiere hier gesehen werden?

%interrogative simple past
Kannte ich dich?

%interrogative simple past with modal verb
Könnte sie hungrig sein?

%interrogative simple past with modal verb and mid-adverb
Könnte sie immer hungrig sein?

%negative simple past
Ich kannte dich nicht.

%negative past passive
Das Buch wurde von dir nicht gelesen.

%interrogative negative simple past
Kannte ich dich nicht?

%simple past with adverb
Ich kannte dich wirklich.

%past perfect
Ich hatte einen Apfel gegessen.

%passive past perfect
Der Apfel war von mir gegessen worden.

%interrogative past perfect
Hatte ich den Apfel gegessen?

%negative past perfect
Ich hatte den Apfel nicht gegessen.

%negative passive past perfect
Der Apfel war von mir nicht gegessen worden.

%interrogative negative past perfect
Hatte ich den Apfel nicht gegessen?

%past perfect with adverb
Ich hatte einen Apfel fast gegessen.

%simple future
Ich werde einen Apfel essen.

%simple passive future
Der Apfel wird von mir gegessen werden.

%interrogative simple future
Werde ich einen Apfel essen?

%negative simple future
Ich werde einen Apfel nicht essen.

%negative simple passive future
Der Apfel wird von mir nicht gegessen werden.

%interrogative negative simple future
Werde ich den Apfel nicht essen?

%simple future with adverb
Ich werde fast einen Apfel essen.

Sie wird sicher einen Apfel essen.

%future perfect
Sie wird sicher einen Apfel gegessen haben.

%interrogative future perfect
Werde ich einen Apfel gegessen haben?

%negative future perfect
Sie wird einen Apfel nicht gegessen haben.

%interrogative negative future perfect
Werde ich einen Apfel nicht gegessen haben?

%future perfect with adverb
Sie wird eigentlich einen Apfel nicht gegessen haben.

%simple future in the past
Ich würde einen Apfel essen.

%interrogative simple future in the past (conditional)
Würde ich einen Apfel essen?

%negative simple future in the past (conditional)
Sie würde den Apfel nicht essen.

%interrogative simple future in the past (conditional)
Würde ich den Apfel nicht essen?

%the same with modal (conditional)
Könnte ich einen Apfel nicht essen?

%simple future in the past (conditional) with adverb
Sie würde einen Apfel sicher essen.

%perfect future in the past
Ich hätte einen Apfel gegessen.

%interrogative perfect future in the past
Hätte ich den Apfel gegessen?

%negative perfect future in the past
Sie würde den Apfel nicht gegessen.

%interrogative negative perfect future in the past
Würde ich den Apfel nicht gegessen?

%perfect future in the past with adverb
Sie würde einen Apfel sichter gegessen.


%modal simple present
Ich kann ein Buch lesen.

Ich darf ein Buch lesen.

Ich muss nach Hause gehen.

%interrogative modal simple present
Sie darf das Buch nicht lesen?

Darf ich einen Apfel essen?

%negative modal simple present
Sie kann ein Buch nicht lesen.

Sie darf das Buch nicht lesen.

%interrogative negative modal simple present
Kann sie das Buch nicht lesen?

%negative modal simple present with adverb
Sie kann immer ein Buch lesen.

%modal present perfect
Ich habe einen Apfel essen können.

Ich habe einen Apfel essen mögen.

Ich habe ein Buch lesen müssen.

%interrogative modal present perfect
Hat sie ein Buch lesen können?

%negative modal present perfect
Sie hat das Buch nicht lesen dürfen.

%interrogative negative modal present perfect
Hat sie das Buch nicht lesen können?

%modal present perfect with adverb after the modal
Sie hat das Buch fast lesen müssen.

Sie hat das Buch nie lesen müssen.

%interrogative modal present perfect with adverb after the auxverb
Has du das Buch jemals lesen können?



%modal simple past
Ich könnte das Buch lesen.

Ich mochte das Buch lesen.

%interrogative modal simple past
Könnte sie das Buch lesen?

%negative modal simple past
Sie könnte das Buch nicht lesen.

%interrogative negative modal simple past
Könnte sie das Buch nicht lesen?

%modal simple past with adverb
Sie könnte das Buch eigentlich lesen.

%modal past perfect
Ich hatte das Buch lesen können.

I hatte den Apfel essen mögen.

%interrogative modal past perfect
Hatte sie das Buch lesen können?

%negative modal past perfect
Sie hatte das Buch nicht lesen können.

%interrogative negative modal past perfect
Hatte sie das Buch nicht lesen können?

%modal past perfect with adverb
Sie hatte das Buch eigentlich nicht lesen können.


%
% verbal arguments
%
Sie mag Kaffee.

Sie hat mir ein Buch gegeben.

Ich hätte gerne einen Kaffee.


%
% interrogatives
%
%wh adverbial interrogatives with inverted word order
%
Warum hat er mir ein Buch gegeben?

Warum isst du einen Apfel?

Wo isst du zwei roten Äpfel?

Wann wirst du zwei roten Äpfel essen?

Wo bist du im Winter gewesen?

Wo wohnst du eigentlich?

Wo bist du?

%
%subject interrogatives with pers=3
%
Wer mag Kaffee?

Was parkt draußen?

Was ist das?

Wer hat das Buch geschrieben?

%
%Interrogatives questioning verb argument
%
Was hast du draußen gesehen?

Wen hast du gestern getroffen?

Welches schöne Mädchen magst du?

Wie fern wohnst du?

An wen hast du den Brief geschrieben?

Wie schnelles Auto hast du?

Wie schnell fährst du?

Wie kurzen Rock hattest du?

Unter welchem Baum bist du gesessen?

Warum bist du so jung?

Warum ist deine Mutter eine Lehrerin?

Wer bin ich?

Wer bin ich gestern gewesen?

Wer bist du?

Was machst du hier?

Was hast du getan?

Was hast du mit mir getan?

Wo ist das bad?

Wo ist Berlin?

Wie bäckt meine Mutter Brot?

Wie viel rote Äpfel siehst du?

Wie lang hast du auf sie gewartet?

Wie viel Geld kostet es?

Wie viel kostet es?

Was kostet es?

Wessen Buch liegt auf dem Tisch?

%
% Nominal verb-arguments
%
Ein schöner Ausflug hängt vom Wetter ab.

Ich bedanke mich für den Apfel.

Wir haben auf das neues Auto geachtet.

Ein Computer besteht aus Teilen.

Ich warte auf ein schönes Mädchen.

Dieses Mädchen gehört zu mir.

Ich freue mich über deine Prüfung.

Julia interessiert sich über nette Männer.

Einige Leute sterben sogar an Grippe.

Ich freue mich über den Ausflug.

%
% subclauses as verbal arguments
%

%infinitive subclause simple
Julia versucht ein Bier zu trinken.

Wer versucht ein Bier zu trinken?

%infinitive subclause with detachable prefix
Es fing zu regnen an.

Hans versucht einen Kuchen mitzubringen.

Wer versucht einen Kuchen mitzubringen?

%infinitive subclause with modal verb
Hans versucht seine Frau mitbringen zu können.

Wer versucht seine Frau mitbringen zu können?

%infinitive perfect
Hans vergisst einen Kuchen mitgebracht zu haben.

Wer vergisst einen Kuchen mitgebracht zu haben?

Es fing an zu regnen.
%
% um zu?????
% OBJ+infinitive????
% other POS+infinitive????
%


%
%nominal clauses
%

%Nominal clause present / direct-inverted
Der Apfel ist süß.

Heute bin ich müde.

%yn interrogative positive/negative nominal clause present
Ist der Apfel noch süß?

Bist du nicht immer nett?

%Nominal clause past / direct-inverted
Der Apfel war gestern noch süß.

Gestern war ich fit.

%yn interrogative nominal clause past positive/negative
War der Apfel gestern noch süß?

Warst du nicht immer nett?

%Nominal clause present perfect positive/negative-direkt/inverted
Du bist immer nett gewesen.

Du bist nicht immer nett gewesen.

Bis heute war ich immer nett.

%nominal with superlative
Sie ist meine beste Freundin.

Sie ist das süßeste Mädchen.

%nominal with comparative
Mein Apfel ist süßer als deine Orange.

Mein Apfel ist weniger süß als deine Orange.

Mein Auto ist schneller als dein Fahrrad.

Mein Auto ist meistens schneller als dein Fahrrad.

Ich laufe schneller als mein Bruder.

Mein Auto ist doppelt so schnell als dein Fahrrad.

%comparative interrogative
Ist mein Apfel wirklich süßer als deine Orange?

Ist mein Apfel weniger süß als deine Orange?

%comparative as a free argument
Ich lief schneller als mein Bruder.

%comparing to a declarative clause
Ich lief schneller als mein Bruder ging.

Ich lief so schnell wie mein Bruder.

%comparing to a short sentence
Ich war so schnell wie ich konnte.

Ich lief so schnell wie ich konnte.

%problem:tenses
Ich backte so viel Brot, wie du willst.

Ich aß so viel wie ich wollte.

Ich aß so viel Eis wie ich wollte.

So schnell wie ich konnte aß ich meinen Apfel.

%
% compound verbs (particles, etc)
%

%a simple verb
Blumen wachsen im Garten.

Ich bin auf dem Land aufgewachsen.

Ich bin heute früh aufgewacht.

Ich wachte heute früh auf.

Schreibst du meine Geschichte auf?

Ich kümmere mich um das Kind.

Ich sehe dich an.

Ich suche meine Julia.

Ich schaute aus dem Fenster.

Ich habe die Erdbeeren schnell verkauft.

Gestern bin ich nach Hamburg zurückgekommen.

Kannst du vielleicht die Tür zumachen?

Machst du vielleicht die Tür zu? 

Kannst du vielleicht das Licht ausmachen?

Machst du vielleicht das Licht aus? 

Ich höre auf zu reden.

Ich freue mich über deine Prüfung.
%
% subclauses as verbal arguments
%
%subclauses arguments with 'that'
%
Ich glaube, dass du ein Haus bauen kannst.

Ich dachte, dass du ein Haus bauen kannst.

Glaubst du, dass ich Auto fahren kann?

Glaubst du wirklich, dass dieses Auto rot ist?

Warum denkst du, dass du Auto fahren kannst?

Warum denkst du, dass du heute so froh bist?

Ich wusste nicht, dass du hier bist.

Ich glaube nicht, dass du das kannst.

Ich wollte, dass du das Auto nimmst.

Ich glaube, dass ich ein Haus bauen kann.

Ich habe gelernt, dass du Musik magst.

Ich hoffe, dass du das Buch liest.

Ich wusste nicht, dass du nach Hause gehst.

Ich glaube nicht, dass du hier warst.


%*Kannst du mir zeigen, wie man Brot backt?

%
% pure infinitive subclauses 
%
%*Ich habe schwimmen gelernt.

Ich lasse dich spazieren gehen.

Ich möchte einen Kaffee trinken.

Ich muss nach Budapest fahren.

Ich lasse dich nach Budapest fahren

%* Ich wollte spazieren gehen.

Ich wollte einen kleinen Spaziergang machen.

%*Ich habe Fahrrad fahren gelernt.

Ich sah dich kommen.

%*Ich sah dich auf die Strasse kommen.

%*Ich habe dich zu deinem Arbeitsplatz gehen gesehen.

%*Ich habe dich dein Buch lesen gesehen 

%*Ich hörte dich ein Lied singen.


%
% zu+infinitive subclauses as arguments
%
Wir haben zugestimmt jetzt zu gehen.

Ich hoffe, Budapest bald wider zu erreichen.

%*Meine Mutter begann schwimmen zu lernen erst heuer.

%*Ich habe micht entschieden, einen Kaffee zu trinken.

Ich hoffe einen Kaffee zu kriegen.

%*Ich beschloss nach Hause zu gehen.

%*Wir haben vereinbart einige Äpfel zu kaufen.

%*Ich habe gelernt wie Brot zu backen.

Hörst du einmal auf schlimme Geschichte zu erzählen?

Würdest du aufhören schlimme Geschichten zu erzählen?

%*Ich liebe es, in Budapest zu leben.

%*Ich liebe es, nette Mädchen zu küssen.

%*Ich liebe es, nette Mädchen küssen zu dürfen.

%
% infinitive subclauses with interrogatives as arguments
%



%
% um zu+infinitive (ohne, anstatt) subclauses as arguments
%
%*Können wir jetzt anhalten um einen Kaffee zu trinken?



% interrogative y/n subclauses as arguments
% (using conjunctives 'if' and/or 'whether')
%
%*Ich weiß nicht, ob du heute glücklich bist.

%*Ich weiß nicht, ob du schön bist.

%*Ich weiß nicht, ob du dieses Buch gelesen hast. 

%*Ich weiß nicht, ob du die Koffee getrunken hast.


% interrogative wh subclauses with linear order as arguments
%
%*Ich weiß nicht, wann ich wieder anfangen kann.

%*Weißt du, wie man Brot backt?

%*Weißt du, wie man Auto fährt?

%*Ich weiß nicht, wer hat dieses Buch geschrieben.

%*Ich wusste, was ich tun könnte.

%*Kannst du sehen, wer singt?

%*Ich kann nicht verstehen, wie sie das tun würde.

%*Ich wusste nicht, wohin sie gegangen ist.

%*Ich mag, when ich früher heimgehen kann.

%*Ich will wissen, was ist das.



%
% compound *phrases* with conjunctions
%

Ich will mit Bus und Bahn fahren.

Die Hose ist schön, aber zu klein.

Er ist klug, aber faul.

%*Er ist zwar klug, aber faul.

%*Ich sollte jetzt lachen oder weinen.

%*Ich weiß nicht, ob ich lachen oder weinen soll.

Meine Freunde und ich wollen ins Kino gehen.

%
% compound sentences / Nebenordnenden Konjunktionen
%
Ich esse Apfel und du trinkst Bier.

Ich esse Apfel, und trinke Bier.

Ich esse Apfel oder trinke Bier.

Ich esse Apfel, oder trinke Bier.

Wir fahren mit Bus oder wir gehen zu Fuß

Wir fahren mit Bus, oder wir gehen zu Fuß

Er war müde und er hat dem Lehrer geholfen.

Wir fahren in die Berge oder wir fahren an die See.

Er war müde, aber er hat dem Lehrer geholfen.

Julia ist 44 Jahre alt, aber sie ist noch hübsch.

Der Vorschlag ist super, aber wir haben keine Zeit.

Die Hose ist schön, aber die ist zu klein.

Mein Freund hat einen Hund, aber ich mag sie nicht.

Er hat nicht gearbeitet, denn er war krank.

Ich weinte, denn ich hatte kein Geld.

Sie ist nicht nur eine Mutter von drei Kindern, sondern sie schreibt auch Bücher.

Das Haus ist nicht alt, sondern du bist zu jung.

Ich möchte keinen Kaffee, sondern ich möchte Eis.

Einerseits könnte ich tanzen, andererseits bin ich müde.

Mal kann der Hund ruhig sein, mal ist er nervös. 

Ich habe ein Auto beziehungsweise meine Frau hat ein Fahrrad.

Ich habe ein Auto, beziehungsweise meine Frau hat ein Fahrrad.

*Ein Auto habe ich, beziehungsweise meine Frau hat auch eins.

%composition with semicolon
Sie ist meine Frau; sie ist auch meine beste Freundin.

%
% complex sentences / Unterordnenden Konjunktionen
%
Als ich Kind war, aß ich viel Äpfel.

Sie war erst 16 Jahre alt, als sie das erste Kind bekommen hat.

Wir sind angezogen, bevor wir in die Bank gegangen sind.

Ich habe ein Bier getrunken, bevor er angekommen ist.

Wir sind zu Hause geblieben, bis er wieder gekommen ist.

Ich wusste, dass du schön singst.

Ich wusste, dass du mich liebst

Ich wusste nicht, ob du verheiratet bist.

Ich habe nicht gesehen, ob du nach Hause gekommen bist.

Ich habe großen Hunger, falls ich nichts esse.

Er hat sich geärgert, weil die Kinder zu laut waren.

Mein Vater ist traurig, weil sein Hund krank ist.

%
% correlated noun phrases
%
Wir müssen mit Zug oder mit Bus fahren.

Wir müssen entweder mit Zug oder mit Bus fahren.

Du kannst weder Bier noch Wein trinken.

Weder Klaus noch Julia fuhr mit Auto.

Sowohl Klaus als auch Julia fuhren mit Auto.

Ich war einerseits in Berlin andererseits in Hamburg.

Ich bin nicht nur in Berlin sondern auch in Hamburg gewesen.

Nicht nur Julia sondern auch Klaus können mit uns nach Berlin fahren.

Entweder du bezahlst dein Essen oder du gehst nach Hause.

%
% correlated sentences - casual sentences
%
Wenn du dein Essen nicht bezahlst, musst du nach Hause gehen.

Ich habe Hunger, wenn ich nicht esse.

Wenn ich nicht esse, habe ich Hunger.

Ich bleibe heute zu Hause, weil ich krank bin.

Ich bin krank, deswegen gehe ich heute nicht zur Arbeit.

Kaum hatten wir das Haus verlassen, ging er zu Arbeit.

Kaum hatte ich die Arbeit beendet, ging er zur Arbeit.

%
% adverbs, addresses as connectives
%
So, er ist ein Arzt.

Hans, bist du der Arzt?

Hans, du bist der Arzt.

Endlich, ich habe die Äpfel verkauft.

Ja, ich bin dein Freund.

Übrigens, wohin gehst du?

%
% short nominal/verbal sentences
%
%*Ja, ich bin.

%*Nein, ich bin nicht.

%*Nein, du bist nicht.

%*Ja, es ist so.

%*Nein, es ist nicht.

%*Nein, ich könnte nicht.

%%%%%However, *we couldn't.

%*Wow, du bist nett.

%*Ich könnte auch.

%*Ich habe auch nicht.

%*Wir hatten auch.

%
% 	Question tags, addressing
%
%%%%I saw you coming in the street, didn't I? Ich habe dich auf der Straße kommen sehen, nicht wahr?

%*Du kannst gut singen, oder?

Hans, warum bist du hier gekommen?

%*Könntest du bitte ruhig sein?

%
% Imperatives
%
%*Sei ruhig!

%*Bitte ruhig bleiben!

%*Iss mehr Apfel.

%*Schalte das Licht aus!

%
% mixed sentences
%
Es geht ihm besser.

Ich fühle mich wohl.

Ich fühle mich besser.

Ich weiß dich gut.

Ich lache gern.
