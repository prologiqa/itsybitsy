%
%:- writeln("user:file_search_path(prolog,'..')").

%:- set_prolog_flag(autoload, false).

user:file_search_path(prolog,'..').

%to denote an instance is belonging to a class
:- op(400,yfx,#).


:- set_prolog_flag(encoding,iso_latin_1).

:- use_module(itsybitsy).

:- initItsyBitsy(en).

%case dependent imports
:- use_module(winniepooh/ontology).
:- use_module(winniePooh/enDict).

%language dependent imports
:- use_module(enParse/enParser).
:- use_module(enParse/enGen).

%application dependent imports
:- use_module(commonsense/semantics).
:- use_module(commonsense/metaDriven).

%general (library) imports
:- use_module(library(debug)).

:- nodebug(parser).
:- debug(semantics).
:- nodebug(metaDriven).


:- load('commonsense/commonsense.owl',model,ontology).


countall:- count(container,commonsense,STAT),
   STAT=.. [s|LIST], LABELS=
   ['subclass relations','sub object property relations',
    'object property instances','sub data property relations',
    'data property instances','subrelations',
    'relation instances','disjunct set declarations',
    'equivalent set declarations','object property declarations',
    'data property declarations','relation declarations',
    'primitive datatype declarations','enumeration datatype declarations',
    'class declarations','container (ontology) declarations'],
   (nth1(NTH,LIST,NR), nth1(NTH,LABELS,LABEL),
    format('~w\t\t~w\n',[NR,LABEL]), fail; true).

:- countall.
