:- module(itsybitsy,[initItsyBitsy/1,
                    sentence2Log/4, sentence2Log/8,
                    tryAnswer/2, tryAnswer/5,
                    chat/0, chat/1]).

%case dependent imports
:- use_module(winniePooh/ontology).
:- use_module(winniePooh/enDict).
:- use_module(winniePooh/dataface).

%application dependent imports
:- use_module(commonsense/semantics).
:- use_module(parser).


%general (library) imports
:- use_module(library(debug)).

:- debug(parser).
:- debug(semantics).

initItsyBitsy(LANGUAGECODE):-
%  atom_concat(LANGUAGECODE,'Parse',DIR),
  atom_concat(LANGUAGECODE,'Parser',PARSER),
  atom_concat(LANGUAGECODE,'Dict',DICT),
  atom_concat(LANGUAGECODE,'Gen',GEN),
  add_import_module(parser,GEN,end), initParser(PARSER,DICT,GEN).


%!sentence2Log(SENTENCE,ANSWER,STRING,X,LOGEXP)
%+SENTENCE: string, a grammatically correct sentence
%-ANSWER: dict, the answer of the sentence/query (if any or var)
%-ANSX:   var, piece of ANSWER that answers the query
%-ANSD:   dict, the grammatical structure that answers the query
%-STEM:   the proper name as a string/lingual ID (lemma)
%-X:      answer object, that is referred by LOGEXP
%-LOGEXP: logical expression, referring to X
% -- for questions, a conjunctive PROLOG query is generated
% -- for declaratives am OWL clause is generated
sentence2Log(SENTENCE,FULLANSWER,ANSX,ANSD,STEM,NUMPERS,X,LOGCALL):-
  parseThisSentence2Dict(SENTENCE,[DICT|_]),
  %in case of queries
  (query2Conjs(DICT,FULLANSWER,ANSX,ANSD,STEM,NUMPERS,X,LOGCALL)->
    debug(semantics,'~w\n~w',[LOGCALL,ANSD]);
  %in case of statements
  sentence2Stmt(DICT,STRING,NUMPERS,X,LOGCALL)->
    debug(semantics,'~w',[LOGCALL]);
  debug(semantics,'No logical expression generated...',[]), fail).

sentence2Log(SENTENCE,STEM,X,LOGCALL):-
  sentence2Log(SENTENCE,_,_,_,STEM,NUMPERS,X,LOGCALL).



%tryAnswer(SENTENCE,ANSWER) is nondet.
%+SENTENCE: a sentence string
%-ANSWER: an answer string, a complete sentence
%
%The main chatbot predicate - tries to answer a sentence
% - if the sentence was a question => answer is given
% - if the sentence was a statement => ANSWER remains var
% - if the sentence was a statement => the information will be stored
%
%Fails: if there is some other problem...
%
%A sentence2Log adjon vissza egy szófajt is, amit
%ami a LOG kiértékelés után segít a mondatba foglalásban
tryAnswer(SENTENCE,FULLANSWER,WHDICT,STEM,X):- string(SENTENCE), !,
  sentence2Log(SENTENCE,FULLANS,ANSX,WHDICT,STEM,NUMPERS,X,LOG),
  (once(functor(LOG,?,1))->
    (LOG->ANSX=WHDICT, STEM=yes, X = + ,
      toEnglish(FULLANS,FULLANSWER),debug(semantics,FULLANSWER,[]);
    ANSX=WHDICT, STEM=no, X = -,
      toEnglish(FULLANS,FULLANSWER),debug(semantics,FULLANSWER,[]));
  once(functor(LOG,^,2);functor(LOG,:=,2))->
    (LOG, embedResultInSentence(ANSX,WHDICT,STEM,NUMPERS,X),
      toEnglish(FULLANS,FULLANSWER),
      debug(semantics,FULLANSWER,[]), debug(semantics,'X=~w',[X]);
%      (nonvar(FULLANS), toEnglish(FULLANS,FULLANSWER),
%         (nonvar(FULLANSWER), debug(semantics,FULLANSWER,[]), fail,
%         nonvar(X), debug(semantics,'X=~w',[X]),fail; true));
    debug(semantics,'No more results...',[]),fail);
  checkAssert(LOG,STEM,X,winniepooh,model,ontology)->true;
  debug(semantics,'I already knew it!',[])).

tryAnswer(SENTENCE,FULLANSWER):- string(SENTENCE), !,
    tryAnswer(SENTENCE,FULLANSWER,_,_,_).


start(QUESTION):-
  tryAnswer(QUESTION,FULLANSWER,ANSWER,STEM,X),
    (nonvar(FULLANSWER)->writeln(FULLANSWER), fail;
    nonvar(ANSWER)->writeln(ANSWER), fail;
    nonvar(STEM)->writeln(STEM), fail;
    nonvar(X)->writeln(X)), fail;
  writeln('This question has no (more) logical answer.').

start:-
  start("Who are Winnie the Pooh's friends?").


chat(YOU):- format('Hi ~w!\n',[YOU]),
  writeln('I am ITSy-Bitsy, the chatbot!'),
  writeln('I can answer some independent questions about Winnie-the-Pooh.'),
  writeln('If you want to finish getting informed, just enter an empty line!'), chat.

chat:- read_line_to_string(current_input,QUERY), string_length(QUERY,QL),
  (QL=<1, !;
  QUERY=="Bye", !;
  start(QUERY), chat).









