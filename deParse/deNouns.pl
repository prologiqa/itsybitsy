:- encoding(iso_latin_1).


compBind(e).
compBind(n).
compBind(r).
compBind(en).
compBind(ens).
compBind(er).
compBind(s).
compBind(es).



proper(bachStreet,street,e,'Bachstra�e').
proper(bahnhofStreet,street,e,'Bahnhofstra�e').
proper(tengStreet,street,e,'Tengstra�e').

proper(agata,human,e,'Agata').
proper(alex,human,r,'Alex').
proper(alexander,human,r,'Alexander').
proper(amira,human,e,'Amira').
proper(andrea,human,e,'Andrea').
proper(andreas,human,r,'Andreas').
proper(anna,human,e,'Anna').
proper(anton,human,r,'Anton').
proper(bella,human,e,'Bella').
proper(caterina,human,e,'Caterina').
proper(charlotte,human,e,'Charlotte').
proper(christina,human,e,'Christina').
proper(claudia,human,e,'Claudia').
proper(conchi,human,e,'Conchi').
proper(dina,human,e,'Dina').
proper(eva,human,e,'Eva').
proper(felix,human,r,'Felix').
proper(florian,human,r,'Florian').
proper(gomez,human,r,'Gomez').
proper(gonzalez,human,r,'Gonz�lez').
proper(hannes,human,r,'Hannes').
proper(hans,human,r,'Hans').
proper(hector,human,r,'Hector').
proper(ibrahim,human,r,'Ibrahim').
proper(inge,human,e,'Inge').
proper(jakob,human,r,'Jakob').
proper(jana,human,e,'Jana').
proper(jenny,human,e,'Jenny').
proper(john,human,r,'John').
proper(jose,human,r,'Jos�').
proper(juan,human,r,'Juan').
proper(judith,human,e,'Judith').
proper(julia,human,e,'Julia').
proper(klaus,human,r,'Klaus').
proper(lara,human,e,'Lara').
proper(laura,human,e,'Laura').
proper(lisa,human,e,'Lisa').
proper(lena,human,e,'Lena').
proper(lotte,human,e,'Lotte').
proper(lucia,human,e,'Lucia').
proper(luis,human,r,'Luis').
proper(maier,human,e,'Maier').
proper(maria,human,e,'Maria').
proper(max,human,r,'Max').
proper(michael,human,r,'Michael').
proper(michele,human,e,'Michele').
proper(miriam,human,e,'Miriam').
proper(molly,human,e,'Molly').
proper(nadine,human,e,'Nadine').
proper(nessrin,human,e,'Nessrin').
proper(nich,human,r,'Nick').
proper(oliver,human,r,'Oliver').
proper(oskar,human,r,'Oskar').
proper(pamela,human,e,'Pamela').
proper(paul,human,r,'Paul').
proper(peter,human,r,'Peter').
proper(ricardo,human,r,'Ricardo').
proper(rico,human,r,'Rico').
proper(rita,human,e,'Rita').
proper(romeo,human,r,'Romeo').
proper(robert,human,r,'Robert').
proper(sarah,human,e,'Sarah').
proper(silvana,human,e,'Silvana').
proper(sonja,human,e,'Sonja').
proper(susanne,human,e,'Susanne').
proper(stefan,human,r,'Stefan').
proper(steffi,human,e,'Steffi').
proper(tim,human,r,'Tim').
proper(thomas,human,r,'Thomas').
proper(tom,human,r,'Tom').
proper(veronika,human,e,'Veronika').
proper(yannick,human,r,'Yannick').
proper(yin,human,r,'Yin').

proper(albertEinstein,scientist,'Einstein').

proper(bast,family,'Bast').
proper(bauer,family,'Bauer').
proper(becker,family,'Becker').
proper(berger,family,'Berger').
proper(brandner,family,'Brandner').
proper(georgi,family,'Georgi').
proper(klein,family,'Klein').
proper(k�nig,family,'K�nig').
proper(kunz,family,'Kunz').
proper(kurz,family,'Kurz').
proper(linke,family,'Linke').
proper(meier,family,'Meier').
proper(m�ller,family,'M�ller').
proper(rausch,family,'Rausch').
proper(schmidt,family,'Schmidt').
proper(schmitt,family,'Schmitt').
proper(schneider,family,'Schneider').
proper(wei�,family,'Wei�').
proper(watanabe,family,'Watanabe').

proper(jamesBond,human,r,'James'-'Bond').

proper(picasso,painter,r,'Picasso').
proper(rammstein,musician,r,'Rammstein').
proper(goethe,poet,r,'Goethe').


proper(january,month,r(st),'Januar').
proper(january,month,r(st),'J�nner').
proper(february,month,r(st),'Februar').
proper(february,month,r(st),'Feber').
proper(march,month,r(st),'M�rz').
proper(april,month,r(st),'April').
proper(may,month,r(st),'Mai').
proper(june,month,r(st),'Juni').
proper(july,month,r(st),'Juli').
proper(august,month,r(st),'August').
proper(september,month,r(st),'September').
proper(october,month,r(st),'Oktober').
proper(november,month,r(st),'November').
proper(december,month,r(st),'Dezember').

proper(spring,season,r(st),'','Fr�hling').
proper(spring,season,s,'','Fr�hjahr').
proper(summer,season,r(st),'','Sommer').
proper(autumn,season,r(st),'','Herbst').
proper(winter,season,r(st),'','Winter').

%proper(east,'East').
%proper(north,'North').
%proper(south,'South').
%proper(west,'West').

%am Montag-->adv
%montags-->adv
proper(monday,weekday,r(st),e,'Montag').
proper(tuesday,weekday,r(st),e,'Dienstag').
proper(wednesday,weekday,r(st),e,'Mittwoch').
proper(thursday,weekday,r(st),e,'Donnerstag').
proper(friday,weekday,r(st),e,'Freitag').
proper(saturday,weekday,r(st),e,'Samstag').
proper(saturday,weekday,r(st),e,'Sonnabend').
proper(sunday,weekday,r(st),e,'Sonntag').

proper(newyear,feast,s,neujahr).
proper(newyeareve,feast,r(st),silvester).
proper(christmas,feast,s,weihnachten).
proper(whitsun,feast,s,pfingsten).
proper(easter,feast,s,ostern).


proper(africa,continent,'Africa').

proper(america,continent,'America').

proper(canada,country,'Kanada').
proper(mexico,country,'Mexico').
proper(brazil,country,'Brasilien').
proper(us,country,'US').


proper(alaska,state,'Alaska').
proper(alabama,state,'Alabama').
proper(arizona,state,'Arizona').
proper(arkansas,state,'Arkansas').
proper(california,state,'California').
proper(colorado,state,'Colorado').
proper(connecticut,state,'Connecticut').
proper(delaware,state,'Delaware').
proper(georgia,state,'Georgia').
proper(florida,state,'Florida').
proper(hawaii,state,'Hawaii').
proper(illinois,state,'Illinois').
proper(indiana,state,'Indiana').
proper(kansas,state,'Kansas').
proper(kentucky,state,'Kentucky').
proper(louisana,state,'Louisana').
proper(maine,state,'Maine').
proper(maryland,state,'Maryland').
proper(massachusetts,state,'Massachusetts').
proper(michigan,state,'Michigan').
proper(minnesota,state,'Minnesota').
proper(nebraska,state,'Nebraska').
proper(newHampshire,state,['New','Hampshire']).
proper(newJersey,state,['New','Jersey']).
proper(newMexico,state,['New','Mexico']).
proper(newSouthWales,state,['New','South','Wales']).
proper(newYork,state,['New','York']).
proper(newYorkCity,city,['New','York']).
proper(northCarolina,state,['North','Carolina']).
proper(northDakota,state,['North','Dakota']).
proper(oklahoma,state,'Oklahoma').
proper(oregon,state,'Oregon').
proper(pennsylvania,state,'Pennsylvania').
proper(rhodeIsland,state,['Rhode','Island']).
proper(southCarolina,state,['South','Carolina']).
%a noun with an indefinite numeral, requiring plural
proper(southDakota,state,['South','Dakota']).
proper(tennessee,state,'Tennessee').
proper(texas,state,'Texas').
proper(vermont,state,'Vermont').
proper(virginia,state,'Virginia').
proper(washington,state,'Washington').
proper(westVirginia,state,['West','Virginia']).
proper(wisconsin,state,'Wisconsin').
proper(wyoming,state,'Wyoming').

proper(mississippi,river,'Mississippi').
proper(missouri,river,'Missouri').


proper(asia,continent,'Asia').
proper(mountEverest,peak,['Mount','Everest']).

proper(china,country,'China').

proper(australia,continent,'Australia').

proper(europeanUnion,federation,e,['Europ�ische','Union']). %DE
proper(europe,continent,'Europa').                          %DE
proper(russia,country,'Russia').

proper(england,country,'England').
proper(london,city,'London').                               %DE
proper(manchester,city,'Manchester').
proper(english,'English').

proper(hertie,company,'Hertie').                            %DE
proper(mercedes,company,'Mercedes').                        %DE
proper(siemens,company,'Siemens').                          %DE

proper(germany,country,'Deutschland').                      %DE
proper(bavaria,state,'Bayern').                             %DE
proper(basel,city,'Basel').                                 %DE
proper(berlin,city,'Berlin').                               %DE
proper(bremen,city,'Bremen').                               %DE
proper(cologne,city,'K�ln').                                %DE
proper(dortmund,city,'Dortmund').                           %DE
proper(dresden,city,'Dresden').                             %DE
proper(frankfurt,city,'Frankfurt').                         %DE
proper(freiburg,city,'Freiburg').                           %DE
proper(hamburg,city,'Hamburg').                             %DE
proper(heidelberg,city,'Heidelberg').                       %DE
proper(kiel,city,'Kiel').                                   %DE
proper(mainz,city,'Mainz').                                 %DE
proper(mannheim,city,'Mannheim').                           %DE
proper(munich,city,'M�nchen').                              %DE
proper(neuschwanstein,city,'Neuschwanstein').               %DE
proper(offenbach,city,'Offenbach').                         %DE
proper(r�desheim,city,'R�desheim').                         %DE
proper(saarbr�cken,city,'Saarbr�cken').                     %DE
proper(stuttgart,city,'Stuttgart').                         %DE
proper(wiesbaden,city,'Wiesbaden').                         %DE

proper(zugspitze,peak,'Zugspitze').                         %DE

proper(balticSea,sea,'Ostsee').                             %DE
proper(northSea,sea,'Nordsee').                             %DE

proper(main,river,'Main').                                  %DE
proper(rhein,river,'Rhein').                                %DE

proper(bodensee,lake,'Bodensee').                           %DE

proper(r�gen,island,'R�gen').                               %DE

proper(hungary,country,'Ungarn').                           %DE
proper(budapest,city,'Budapest').                           %DE

proper(croatia,country,'Croatia').                          %DE
proper(zagreb,city,'Zagreb').                               %DE

proper(austria,country,s,'�sterreich').                     %DE
proper(vienna,city,'Wien').                                 %DE
proper(salzburg,city,'Salzburg').                           %DE

proper(switzerland,country,e,'Schweiz').                    %DE

proper(france,country,s,'Frankreich').                      %DE
proper(paris,city,'Paris').                                 %DE

proper(greece,country,s,'Griechenland').                    %DE

proper(turkey,country,e,'T�rkei').                          %DE

proper(ukraine,country,e,'Ukraine').                        %DE

proper(italy,country,'Italien').                            %DE

proper(spain,country,'Spanien').                            %DE

%proper(valentinesDay,feast,'Valentine\'s Day').

proper(europeanParliament,parliament,s,
       ['Europ�ische','Parliament']).                       %DE

proper(jamesBondFilm,film,'James'-'Bond'-'Film').           %DE

proper(monopoly,boardgame,s,'Monopoly').                    %DE

proper(aspirin,medikament,s,'Aspirin').                     %DE


%noun(SEM,GENDER,STEM,PLNOM).
%noun(SEM,GENDER,STEM,SGGEN,PLNOM).
noun(accident,r(st),:,e,unfall).
noun(accomodation,e,:,e,unterkunft).
noun(account,s,en,konto).
noun(ache,e,e,weh).
%noun(act,e,en,handlung).
%noun(acid,acid).
noun(acquaintance,r(st),n,bekannter).
noun(acquaintancein,e,n,bekannte).
noun(act,e,en,tat).
noun(action,e,en,aktion).
%noun(activity,e,en,t�tigkeit).
%noun(actor,r(st),schauspieler).
noun(accumulator,r(st),s,akku).
noun(accumulator,r(st),s,akkumulator).
%noun(ad,e,n,anzeige).
%noun(addition,addition).
%noun(adjustment,adjustment).
noun(address,e,n,adresse).
noun(adult,r(n),n,erwachsene).
noun(adventure,s,'',abenteuer).
noun(advertisement,e,n,anzeige).
noun(advertisement,e,n,annonce).
%noun(advert,s,e,inserat).
noun(advice,r(st),:,e,vorschlag).
noun(afternoon,r(st),e,nachmittag).
noun(age,s,'',alter).
%noun(agent,agent).
%noun(agreement,agreement).
%noun(aid,aid).
noun(aim,r(st),e,ziel).
%noun(air,e,luft). %only sing
noun(aircraft,s,e,flugzeug).
noun(airport,r(st),'',flughafen).
noun(alarm,r(st),e,alarm).
noun(alarmclock,[alarm,clock]).
%noun(album,album).
noun(alcohol,r(st),'',alkohol).
%noun(allowance,allowance).
noun(alphabet,s,e,alphabet).
noun(alternative,e,n,alternative).
%noun(ambulance,ambulance).
noun(amount,e,n,menge).
%noun(amusement,amusement).
%noun(analysis,analysis).
%noun(angle,angle).
noun(animal,s,e,tier).
noun(announcement,e,n,ansage).
noun(announcement,e,n,durchsage).
noun(answer,e,en,antwort).
noun(answeringMachine,r(st),'',anrufbeantworter).
noun(answerSheet,r(st),:,antwortbogen).
%noun(ant,ant).
noun(apartment,e,en,wohnung).
noun(apology,e,en,entschuldigung).
%noun(app,app).
%noun(apparatus,apparatus).
noun(appetite,r,n,appetit).
noun(apple,r(st),::,'',apfel). %last but one vowel is umlauted
noun(appleJuice,apfel+saft).
noun(appleTree,apfel+baum).
noun(application,e,en,bewerbung).
noun(appointment,r(st),e,termin).
%noun(approval,approval).
%noun(arch,arch).
noun(area,r(st),'',bereich).
%noun(argument,argument).
noun(arm,r(st),e,arm).
%noun(armament,armament).
%noun(armchair,r(st),sessel).
noun(arrival,e,'',ankunft).
%noun(arrow,arrow).
noun(art,e,:,e,kunst).
noun(artMuseum,kunst+museum).
noun(article,r,'',artikel).
noun(artist,r(st),'',k�nstler).
noun(artistin,e,nen,k�nstlerin).
%noun(army,army).
%noun(assembly,assembly).
%noun(assistant,r(st),'?',assistent).
noun(attachment,e,n,anlage).
%noun(attack,attack).
%noun(attempt,attempt).
%noun(attention,attention).
%noun(attitude,attitude).
%noun(attraction,attraction).
%noun(audience,audience).
noun(aunt,e,n,tante).
noun(austrian,r(st),'',�sterreicher).
noun(austrianin,e,nen,�sterreicherin).
noun(vendingMachine,r(en),en,automat).
%noun(authority,authority).
%noun(average,average).


noun(baby,s,s,baby).
noun(babysitter,r(st),'',babysitter).
noun(babysitter,e,nen,babysitterin).
noun(back,r(st),'',r�cken).
noun(backpack,r(st),:,e,rucksack).
%noun(badminton,badminton).
noun(bag,e,n,tasche).
noun(baggage,s,gep�ck). %DE only sing
noun(bakery,e,en,b�ckerei).
noun(balcony,r(st),e,balkon).
%noun(balance,balance).
noun(ball,r(st),:,e,ball).
noun(ballPen,r,'',kugelschreiber).
%noun(balloon,luftballoon).
noun(banana,e,n,banane).
noun(band,e,s,band).
noun(bank,e,:,e,bank).
%noun(barbecue,r(st),s,grill).
%noun(base,base). %alapzat, l�g
noun(basement,s,'UG').     %DE only sing
noun(basement,s,e,'untergescho�').
%noun(baseball,baseball).
%noun(basin,basin).
%noun(basket,basket).
noun(basketball,r(st),:,e,basketball).
%noun(bat,bat). %HU:denev�r
noun(bath,s,:,er,bad).
noun(bathsuit,[bathing,suit]).
noun(bathroom,(bad|e)+zimmer).
%noun(bathtube,badewanne).
%noun(battery,batterie).
noun(beach,r(st),:,e,strand).
noun(bean,e,n,bohne).
noun(bear,r(st),en,b�r).
%noun(beard,r(st),bart).
noun(bed,s,en,bett).
noun(bedroom,s,'',schlafzimmer).
noun(bee,e,n,biene).
noun(beer,s,e,bier).
%noun(beginner,anf�nger).
noun(beginning,r(st),:,e,anfang).
%noun(behaviour,behaviour).
%noun(belt,r(st),'',g�rtel).
%noun(belief,belief).
%noun(bell,bell).
noun(belly,r(st),:,e,bauch).
noun(bellyPain,bauch+schmerz).
%noun(berry,berry).
noun(bicycle,s,:,er,fahrrad).
noun(bike,s,:,er,rad).
noun(bike,s,s,bike).
noun(bikini,r(st),s,bikini).
noun(bill,e,en,rechnung).
%noun(bin,r(st),'',beh�lter).
noun(biology,e,'',biologie).
noun(bird,r(st),:,'',vogel).
noun(birth,e,en,geburt).
noun(birthday,r(st),e,geburtstag).
noun(birthYear,s,e,geburtsjahr).
noun(birthPlace,r(st),e,geburtsort).
noun(bit,s,s,bit).
%noun(bite,bite).
%noun(blade,blade).
%noun(black,s,'',schwarz).
%%%noun(blackboard,e,n,tafel). board->Tafel
%noun(blanket,e,en,decke).
%noun(block,block).
%noun(blockage,blockage).
noun(blog,r(st),s,blog).
%noun(blood,s,s,blut).
noun(blouse,e,n,bluse).
%noun(blow,blow).
noun(blue,s,s,blau).
noun(board,e,n,tafel).
noun(board,s,er,brett).
noun(boardGame,brett+spiel).
noun(boat,s,s,boot).
noun(body,r(st),'',k�rper).
%noun(bomb,bomb).
%noun(bone,bone).
noun(book,s,:,er,buch).
noun(book,s,s,book).
noun(bookFair,buch+messe).
%noun(bookcase,s,e,b�cherregal).
%noun(bookshelf,s,e,b�cherregal).
%noun(bookstore,e,en,buchhandlung).
noun(boot,r(st),'',stiefel).
noun(boss,r(st),s,chef).
noun(bossin,e,nen,chefin).
noun(bottle,e,n,flasche).
noun(bottom,e,n,unterseite).
%noun(bowl,e,n,sch�ssel).
noun(box,r(st),::,kasten).
noun(boy,r(n),n,junge).
noun(boyfriend,r(st),e,freund).
noun(bracelet,r(st),:,er,armband).
noun(brain,s,e,gehirn).
%noun(brake,brake).
%noun(branch,branch).
%noun(brass,brass).
noun(bread,s,e,brot).
noun(break,e,n,pause).
noun(breakfast,s,fr�hst�ck). %DE only sing
%noun(breath,breath).
%noun(brick,brick).
noun(bridge,e,n,br�cke).
noun(brother,r(st),::,'',bruder).
%noun(brush,e,n,b�rste).
%noun(bubble,bubble).
%noun(bucket,bucket).
noun(building,s,'',geb�ude).
%noun(bulb,bulb).
noun(bun,s,'',br�tchen).
%noun(burger,burger).
%noun(burst,burst).
noun(bus,r(st),se,bus).
%noun(businessman,businessman).
%noun(businesswoman,businesswoman).
%noun(businessPerson,[business,person]).
noun(busStation,[bus,station]).
noun(busStop,e,n,haltestelle).
noun(businessTrip,(gesch�ft|s)+reise).
noun(butter,e,butter). %DE no plural
noun(butterCream,butter+creme).
%noun(butterfly,butterfly).
%noun(button,button).


%noun(cabinet,cabinet).
%noun(cafe,cafe).
noun(cafeteria,e,en,cafeteria).
noun(cafeteria,s,s,caf�).
noun(cake,r(st),'',kuchen).
noun(calendar,r(st),'',kalender).
%noun(call,call).
noun(camera,r(st),e,fotoapparat).
noun(camera,e,s,kamera).
noun(camp,s,s,camping).
noun(camping,camping).
noun(campsite,campsite).
noun(can,e,n,kanne).
%noun(cancer,cancer).
%noun(candy,candy).
%noun(canvas,canvas).
noun(cap,e,n,m�tze).
noun(capital,e,:,e,hauptstadt).
noun(car,s,s,auto).
noun(car,r(st),s,pkw).
noun(car,r(st),'',wagen).
noun(car,r(st),'',personenkraftwagen).
noun(card,e,n,karte).
noun(cardGame,(karte|n)+spiel).
%noun(care,care).
%noun(career,career).
noun(caretaker,r(st),e,abwart).
noun(caretaker,r(st),e,hausmeister).
noun(caretakerin,e,nen,abwartin).
noun(caretakerin,e,nen,hausmeisterin).
noun(carPark,[car,park]).
%noun(carpet,carpet).
%noun(carriage,carriage).
%noun(carrot,carrot).
%noun(cart,cart). %HU: k�zikocsi
noun(card,e,n,karte).
%noun(cartoon,cartoon).
noun(case,r,:,e,fall).   %nicht in A2
%noun(cash,cash).
noun(cashiersDesk,e,n,kasse).
noun(casting,e,s,castingshow).
noun(castle,s,:,er,schloss).
noun(cat,e,n,katze).
noun(cattle,s,er,rind).
%noun(cause,cause). %HU:ok
%noun(caution,caution).
noun(cd,e,s,'CD').
noun(cdPlayer,'CD'-player).
noun(cdPlayer,['CD',player]).
%noun(ceiling,ceiling).
noun(celebration,e,n,feier).
noun(cellar,r(st),'',keller).
noun(celebration,e,n,feier).
%noun(cent,cent).
noun(center,s,en,zentrum).
%noun(center,centre).
%noun(centimeter,centimeter).
noun(century,s,e,jahrhundert).
%noun(cereal,cereal).
noun(certificate,s,se,zeugnis).
noun(chain,e,n,kette).
noun(chair,r(st),:,e,stuhl).
%noun(chalk,chalk).
%noun(champion,champion).
%noun(chance,chance).
noun(change,e,en,�nderung).
%noun(channel,channel).
noun(chancellor,r(st),'',bundeskanzler).
noun(chancellorin,e,nen,bundeskanzlerin).
%noun(chat,chat).
noun(chatroom,r(st),s,chatroom).
noun(cheese,r(n),k�se).   %DE: only sing
noun(cheeseSandwich,k�se+br�tchen).
%noun(chef,chef).
%noun(chemist,chemist).
noun(chemistry,e,'',chemie).
%noun(cheque,cheque).
%noun(chess,chess).
%noun(chest,chest).
noun(chicken,s,'',h�hnchen).
%noun(chief,chief).
noun(child,s,er,kind).
%noun(chilli,chilli).
%noun(chin,chin).
noun(chineseLanguage,s,chinesisch). %just singular
noun(chip,r(st),s,chip).
%noun(chips,chips).
noun(circus,r(st),se,zirkus).
noun(chocolate,e,schokolade).   %DE only sing
noun(chocolateCake,(schokolade|n)+kuchen).
%noun(choice,choice).
%noun(christmas,christmas).
noun(church,e,n,kirche).
noun(cinema,s,s,kino).
noun(cinemaProgram,kino+programm).
%noun(circle,circle).
%noun(circus,circus).
noun(city,e,s,city).
noun(cityCenter,stadt+zentrum).
noun(cityMap,stadt+plan).
noun(cityHall,s,:,er,rathaus).
noun(cityTour,stadt+tour).
noun(class,e,n,klasse).
%noun(classmate,classmate).
noun(classParty,(klasse|n)+party).
%noun(classroom,classroom).
noun(cleaner,e,en,reinigung).
%noun(clearance,clearance). %space between two objects
%noun(click,click).
noun(climbing,s,steigen).
noun(clock,e,en,uhr).
%noun(closet,closet).
noun(closing,r(st),:,e,schluss).
noun(cloud,e,n,wolke).
%noun(clown,clown).
noun(club,r(st),s,club).
noun(club,r(st),s,klub).
%noun(coach,coach).
%noun(coal,coal).
noun(coat,r,:,'',mantel). %NOT IN A2!!!
%noun(code,code).
noun(coffee,r,s,kaffee).
noun(coffeeShop,s,s,caf�).
noun(coffeeShop,e,s,cafeteria).
%noun(coin,coin).
noun(cola,e,s,cola).
noun(cola,s,s,cola).
%noun(cold,cold).
%noun(collar,collar).
noun(colleague,r(n),n,kollege).
noun(colleaguein,e,nen,kollegin).
%noun(college,college).
noun(colorPencil,r(st),e,farbstift).
noun(color,e,n,farbe).
%noun(comb,comb).
%noun(comedy,comedy).
noun(comic,r(st),s,comic).
noun(comic,s,s,comic).
%noun(comfort,comfort).
%noun(command,command).
noun(comment,r(st),e,kommentar).
%noun(committee,committee).
%noun(communication,communication).
noun(company,e,en,firma).
%noun(comparison,comparison).
noun(competition,r(st),e,wettbewerb).
noun(completion,r(st),:,e,abschluss).
noun(computer,r(st),'',computer).
noun(computerKnowledge,computer+kenntnisse).
noun(concert,s,e,konzert).
%noun(condition,condition).
noun(confirming,s,'',best�tigen).
noun(congratulation,r(st),:,e,gl�ckwunsch).
noun(connection,r(st),:,e,anschluss).
noun(constructionSite,e,n,baustelle).
noun(consultationHour,e,n,sprechstunde).
noun(contact,r(st),e,kontakt).
noun(contactData,kontakt+daten).
%noun(container,container).
%noun(contents,contents).
%noun(continent,continent).
%noun(contour,contour).
noun(contract,r(st),:,e,vertrag).
%noun(control,control).
noun(conversation,s,e,gespr�ch).
%noun(cook,cook).
%noun(cooker,cooker).
%noun(cookie,cookie).
noun(cooking,s,'',kochen).
%noun(copper,copper).
%noun(copy,copy).
%noun(cord,cord).
%noun(cork,cork).
noun(corner,e,n,ecke).
%noun(correction,correction).
noun(cosmetics,e,kosmetik). %DE only sing
%noun(cost,cost).
%noun(costume,costume).
%noun(cotton,cotton).
%noun(cottage,cottage).
%noun(cough,cough).
noun(country,s,:,er,land).
noun(countryside,countryside).
noun(court,s,e,gericht).
noun(course,r(st),e,kurs).
noun(courseDay,kurs+tag).
noun(courseBook,kurs+buch).
%noun(cousin,cousin).
%noun(cover,cover).
noun(cow,e,:,e,kuh).
%noun(crack,crack).
noun(credit,r(st),e,kredit).
%noun(cream,e,s,creme).
noun(cream,e,n,creme).
noun(creditCard,kredit+karte).
noun(cricket,cricket).
%noun(crime,crime).
noun(crimeScene,tat+ort).
noun(crocodile,s,e,krokodil).
noun(cross,e,en,kreuzung).
noun(crossing,crossing).
%noun(crowd,crowd).
%noun(crush,crush).
%noun(cry,cry).
noun(culture,e,en,kultur).
noun(cup,e,n,tasse).
noun(cupboard,cupboard).
%noun(current,current).
%noun(curry,curry).
%noun(curtain,curtain).
%noun(curve,curve).
%noun(cushion,cushion). %HU: díszpárna
noun(customer,r(n),n,kunde).
noun(customerin,e,nen,kundin).
%noun(cycle,cycle).
%noun(cycling,cycling).


%noun(dad,dad).
%noun(damage,damage).
%noun(dance,dance).
%noun(dancer,dancer).
noun(danceCourse,tanz+kurs).
noun(dancing,s,'',tanzen).
%noun(danger,danger).
%noun(data,data).
noun(date,s,en,datum).
noun(daughter,e,:,'',tochter).
%noun(dawn,dawn).
noun(day,r(st),e,tag).
%noun(deaf,deaf).
%noun(death,death).
%noun(debt,debt).
noun(decade,s,e,jahrzehnt).
%noun(decision,decision).
%noun(defect,defect).
%noun(degree,degree).
noun(delay,e,en,versp�tung).
noun(demokracy,e,n,demokratie).
%noun(dent,dent).
noun(dentist,zahn+arzt).
noun(dentistin,zahn+�rztin).
noun(dentistVisit,zahn+arzt+termin).
noun(department,e,en,abteilung).
noun(departmentStore,s,:,er,kaufhaus).
%noun(depth,depth).
%noun(desert,desert).
%noun(design,design).
%noun(desire,desire).
%noun(desk,desk).
%noun(dessert,dessert).
%noun(destruction,destruction).
%noun(detail,detail).
noun(detergent,wasch+mittel).
noun(device,r(st),e,apparat).
%noun(development,development).
%noun(diary,diary).
noun(dictionary,w�rter+buch).
noun(difference,r(st),e,unterschied).
%noun(digestion,digestion).
noun(digitalCamera,digital+camera).
%noun(dimension,dimension).
noun(diningRoom,[dining,room]).
noun(dinner,abend+essen).
%noun(dinosaur,dinosaur).
%noun(diploma,diploma).
%noun(direction,direction).
%noun(disagreement,disagreement).
noun(disco,e,s,disco).
noun(disco,e,s,disko).
noun(discount,e,en,erm��igung).
%noun(discovery,discovery).
%noun(discussion,discussion).
%noun(disease,disease).
%noun(disgust,disgust).
noun(dish,s,e,geschirr).
%noun(display,display).
%noun(distance,distance).
%noun(distribution,distribution).
%noun(division,division).
%noun(divorce,divorce).
noun(doctor,r,:,e,arzt).
noun(doctorin,e,nen,�rztin).
noun(doctor,r,en,doktor).
noun(doctorsVisit,arzt+termin).
%noun(document,document).
noun(dog,r(st),e,hund).
%noun(doll,doll).
%noun(dollar,dollar).
%noun(dolphin,dolphin).
noun(door,e,en,t�r).
%noun(dorm,dorm).
%noun(dot,dot).
noun(doubleRoom,s,'',doppelzimmer).
%noun(download,download).
%noun(doubt,doubt).
%noun(drain,drain).
%noun(drawer,drawer).
noun(dream,r(st),:,e,traum).
noun(dress,s,er,kleid).
noun(dress,e,en,kleidung).
%noun(dresser,dresser).
%noun(drift,drift).
noun(drink,s,e,getr�nk).
noun(drinkMenu,(getr�nk|e)+karte).
%noun(drive,drive).
%noun(driver,driver).
%noun(driving,driving).
noun(drivingLicense,r(st),e,f�hrerschein).
noun(drivingExam,f�hrerschein+pr�fung).
%noun(drop,drop).
noun(drugstore,drugstore).
%noun(drum,drum).
noun(duck,e,n,ente).
noun(dustbin,r(st),'',abfalleimer).
noun(dvd,e,s,'DVD').
noun(dvdPlayer,['DVD',player]).


noun(ear,s,en,ohr).
noun(earPain,(ohr|en)+schmerz).
noun(earlyMorning,r(st),'',morgen).
%noun(earring,earring).
%noun(earth,earth).
noun(east,r(st),'',osten).
noun(eBike,e-bike).
noun(eBook,e-book).
noun(education,e,en,ausbildung).
%noun(edge,edge).
%noun(education,education).
%noun(effect,effect).
noun(egg,s,er,ei).
%noun(electricity,electricity).
noun(electro,elektro). %substantiv ohne gender and article
noun(electroEquipment,elektro+ger�t).
%noun(electroEquipment,s,e,elektroger�t).
%noun(electronics,electronics).
noun(elephant,r(st),en,elefant).
%noun(elevator,elevator).
noun(email,e,s,e-mail).
noun(employee,r,'',mitarbeiter).
%noun(emergency,emergency).
noun(end,s,n,ende).
noun(endOfWork,r,e,feierabend).
%noun(energy,energy).
noun(engine,r(st),en,motor).
%noun(engineer,engineer).
noun(englishLanguage,s,englisch). %just singular
%noun(enter,enter).
noun(entrance,r(st),:,e,eingang).
noun(entry,r(st),e,eintritt).
%noun(envelope,envelope).
noun(equipment,s,e,ger�t).
%noun(eraser,eraser).
noun(error,r(st),'',fehler).
noun(etage,s,e,stockwerk).
noun(european,r(st),'',europ�er).
noun(europeanin,e,nen,europ�erin).
noun(euro,s,'',euro).
noun(evening,r(st),e,abend).
noun(event,e,en,veranstaltung).
noun(everydayLife,r(st),'',alltag).
noun(exam,e,en,pr�fung).
noun(example,s,e,beispiel).
%noun(exchange,exchange).
noun(excursion,r(st),:,e,ausflug).
%noun(excuse,excuse).
noun(exercise,e,en,�bung).
noun(exhaust,e,abgase).
noun(exhibition,e,en,ausstellung).
%noun(existence,existence).
noun(exit,r(st),:,e,ausgang).
%noun(expansion,expansion).
noun(experience,e,en,erfahrung).
%noun(expert,expert).
%noun(explanation,explanation).
%noun(explorer,explorer).
%noun(explosion,explosion).
%noun(extension,extension).
noun(eye,s,n,auge).


noun(face,s,er,gesicht).
%noun(fact,fact).
%noun(factory,factory).
%noun(failure,failure).
noun(fair,e,n,messe).
noun(fairGround,messe+platz).
%noun(fall,fall).
noun(family,e,n,familie).
noun(fan,r(st),s,fan).
noun(fancyCake,e,n,torte).
%noun(farm,farm).
%noun(farmer,farmer).
noun(fashion,e,n,mode).
noun(fastFood,[fast,food]).
noun(father,r(st),:,'',vater).
noun(favouriteAnimal,(liebling|s)+tier).
noun(favouriteBand,(liebling|s)+band).
noun(favouriteColor,(liebling|s)+farbe).
noun(favouriteFood,(liebling|s)+gericht).
noun(favouriteDrink,(liebling|s)+getr�nk).
noun(favouriteMeal,(liebling|s)+essen).
noun(favouriteSinger,(liebling|s)+s�nger).
noun(favouriteSubject,(liebling|s)+fach).
noun(favouriteTeam,(liebling|s)+mannschaft).
noun(favouriteVeg,(liebling|s)+gem�se).
noun(fax,s,e,fax).
noun(fear,e,:,e,angst).
noun(feast,s,e,fest).
%noun(feather,feather).
noun(federalPresident,r(st),'',bundespresident).
noun(federalPresidentin,e,nen,bundespresidentin).
noun(feedback,e,en,r�ckmeldung).
%noun(feeling,feeling).
noun(festival,s,s,festival).
%noun(female,female).
noun(fever,s,fieber).  %DE, only sing
%noun(fiction,fiction).
%noun(field,field).
%noun(fight,fight).
noun(file,e,en,datei).
noun(film,r,e,film).
%noun(filter,filter).
%noun(finger,finger).
%noun(finish,finish).
%noun(fire,fire).
%noun(fireman,fireman,firemen).
noun(firstFloor,s,'EG').
noun(firstFloor,s,'',erdgeschoss).
noun(firstName,r(st),n,vorname).
noun(fish,r(st),e,fisch).
%noun(fishing,fishing).
noun(fitness,e,'',fitness).
noun(fitnessStudio,fitness-studio).
%noun(flag,flag).
%noun(flame,flame).
noun(flat,e,en,wohnung).
noun(fleaMarket,r(st),:,e,flohmarkt).
noun(flight,r(st),:,e,flug).
noun(floor,r(st),:,e,stock).
%noun(flow,flow).
noun(flower,e,n,blume).
noun(flu,e,grippe). %DE just sing
%noun(fluid,fluid).
noun(fly,e,n,fliege).
%noun(focus,focus).
%noun(fog,fog).
%noun(folder,folder).
%noun(fold,fold).
noun(folie,e,n,folie).
noun(food,s,'',essen).
noun(foodStuff,lebens+mittel).
noun(foot,r(st),:,e,fu�).
noun(football,r(st),:,e,fu�ball).
noun(footballFan,fu�ball+fan).
noun(footballGame,fu�ball+spiel).
noun(footballPlayer,fu�ball+spieler).
noun(footballTeam,fu�ball+team).
noun(footballTraining,fu�ball+training).
%noun(force,force).
%noun(forecast,forecast).
noun(foreignCountry,s,ausland). %just singular
%noun(forest,forest).
noun(fork,e,n,gabel).
noun(form,s,e,formular).
noun(foundThing,e,n,fundsache).
%noun(fowl,fowl). %baromfi
%noun(fox,fox).
%noun(fraction,fraction).
%noun(fracturecrack,fracture).
%noun(frame,frame).
%noun(freedom,freedom).
%noun(freezer,freezer).
noun(frenchLanguage,s,franz�sisch). %just singular
%noun(frequency,frequency).
%noun(fridge,fridge).
noun(friedSausages,brat+wurst).
noun(friend,r(st),e,freund).
noun(friendin,e,nen,freundin).
%noun(friction,friction).
%noun(front,front).
noun(frontDoor,haus+t�r).
noun(fruit,s,obst).    %DE just sing
%noun(fuel,fuel).
noun(fun,r(st),spa�).      %DE just sing
%noun(function,function).
noun(furniture,s,'',m�bel).
%noun(future,future).


noun(game,s,e,spiel).
noun(garage,e,n,garage).
noun(garden,r(st),'',garten).
%noun(garlic,garlic).
%noun(gas,gas).
noun(gasStation,[gas,station]).
%noun(gate,gate).
%noun(geometry,geometry).
noun(geography,e,'',geografie).
noun(german,r(n),n,deutsche).
noun(germanin,e,n,deutsche).
noun(germanKnowledge,deutsch+kenntnisse).
noun(germanLanguage,s,deutsch). %just singular
noun(germanExercise,deutsch+�bung).
noun(germanCourse,deutsch+kurs).
noun(gettingOff,aus+steigen).
%noun(gift,gift).
noun(giraffe,e,n,giraffe).
noun(girl,s,'',m�dchen).
noun(girlfriend,girlfriend).
noun(glass,s,:,er,glas).
noun(glasses,e,n,brille).
%noun(glove,glove).
%noun(goal,goal).
%noun(goat,goat).
%noun(god,god).
noun(going,r,gang).
%noun(gold,gold).
%noun(golf,golf).
noun(good,s,:,er,gut). %NOT IN A2
noun(goody,r,gute). %only in singular
noun(government,e,en,regierung).
noun(grade,e,n,note).
%noun(grain,grain). %gabonaszem
%noun(gram,gram).
%noun(gram,gramme).
noun(grammar,e,en,grammatik).
noun(grammarExercise,grammatik+�bung).
noun(grandchild,enkel+kind).
%noun(granddaughter,granddaughter).
noun(grandfather,gro�+vater).
noun(grandmother,gro�+mutter).
noun(grandma,e,'',oma).
noun(grandpa,r,'',opa).
noun(grandparent,gro�+eltern).
%noun(grandson,grandson).
%noun(granny,granny).
%noun(grape,grape).
%noun(graph,graph).
%noun(grass,grass).
%noun(gravity,gravity).
%noun(grease,grease).
noun(greek,r(st),n,grieche).
noun(greekin,e,nen,griechin).
noun(greeting,e,n,anrede).
noun(greeting,r(st),:,e,gru�).
%noun(grey,grey).
%noun(grip,grip).
noun(groceryStore,[grocery,store]).
%noun(groove,groove).
noun(ground,r,:,'',boden).  %NOT IN A2
noun(group,e,n,gruppe).
%noun(growth,growth).
%noun(guard,guard).
%noun(guess,guess).
noun(guest,r(st),:,e,gast).
noun(guestHouse,guest-house).
noun(guidance,e,en,f�hrung).
%noun(guide,guide).
%noun(guidebook,guidebook).
noun(guitar,e,n,gitarre).
%noun(gun,gun).
%noun(guy,guy).
%noun(gym,gym).


noun(hair,s,e,haar).
%noun(half,half).
noun(hall,e,n,halle).
noun(hamburger,r(st),'',hamburger).
%noun(hammer,hammer).
noun(hand,e,:,e,hand).
%noun(handbag,handbag).
%noun(harbour,harbour).
%noun(harmony,harmony).
noun(hat,r,:,e,hut).
noun(head,r(st),:,e,kopf).
noun(headache,kopf+schmerz).
noun(headteacher,headteacher).
noun(health,e,gesundheit).  %DE only sing
noun(healtInsurance,e,n,krankenkasse).
%noun(hear,hear).
noun(hearAgain,s,'',wiederh�ren).
noun(hearing,s,'',h�ren).
%noun(heart,heart).
%noun(heartAttack,[heart,attack]).
%noun(heartburn,heartburn).
%noun(heat,heat).
noun(heating,e,en,heizung).
%noun(height,height).
%noun(helicopter,helicopter).
noun(help,e,hilfe).     %DE only sing
noun(highway,e,en,autobahn).
%noun(hike,hike).
noun(hill,r(st),e,berg).
noun(hiphop,[hip,hop]).
noun(history,e,n,geschichte).
noun(hit,r(st),s,hit).
noun(hobby,s,s,hobby).
%noun(hockey,hockey).
%noun(hole,hole).
noun(holiday,feier+tag).
noun(holidayPhoto,(urlaub|s)+foto).
noun(holidayTravel,(urlaub|s)+reise).
%noun(home,home).
noun(homeland,e,heimat). %DE: only sing
noun(homeland,heimat+land). %DE: only sing
noun(homepage,e,s,homepage).
noun(homework,haus+aufgabe).
%noun(honey,honey).
%noun(hook,hook).
%noun(hope,hope).
%noun(horizon,horizon).
%noun(horn,horn).
noun(horse,s,e,pferd).
noun(hospital,(kranke|n)+haus).
noun(hotel,s,s,hotel).
%noun(hotdog,hotdog).
noun(hour,e,n,stunde).
noun(house,s,::,er,haus).
nounDecl(haus,sg,dat,haus,e).
noun(household,r(st),e,haushalt).
noun(houseNumber,haus+nummer).
noun(housewife,housewife).
%noun(humour,humour).
noun(human,r(en),en,mensch).
noun(hunger,r(st),hunger).  %DE only sing
%noun(hurt,hurt).
noun(husband,r(st),:,er,ehemann).


noun(ice,s,eis). %DE only sing
noun('IC',r(st),'ICE').
noun(iceCream,[ice,cream]). %can be composed from ice and cram
%noun(iceSkating,[ice,skating]).
%noun(id,'ID').
noun(idCard,r(st),e,ausweis).
noun(idea,e,'',ahnung).
noun(idea,e,n,idee).
noun(identification,identification).
%noun(identity,identity).
noun(illustration,e,en,abbildung).
%noun(impact,impact).
%noun(impulse,impulse).
%noun(incident,incident).
%noun(increment,increment).
%noun(indication,indication).
%noun(industry,industry).
noun(information,e,:,e,auskunft).
noun(information,e,en,information).
noun(information,e,s,info).
noun(informationDesk,e,:,e,auskunft).
noun(informationBoard,(information|s)+tafel).
noun(informationBoard,info+tafel). %%%% INFO
%noun(inhibition,inhibition).
%noun(injury,injury).
%noun(ink,ink).
%noun(input,input).
noun(insect,s,en,insekt).
%noun(inside,inside).
%noun(inspection,inspection).
%noun(instability,instability).
%noun(installation,installation).
%noun(instruction,instruction).
noun(instrument,s,e,instrument).
%noun(insulation,insulation).
%noun(insurance,insurance).
%noun(intensity,intensity).
noun(intention,e,en,absicht).
noun(interest,s,n,interesse).
%noun(interface,interface).
%noun(interference,interference).
noun(internet,s,internet).  %DE only sing
noun(internship,s,a,praktikum).
%noun(interpreter,interpreter).
noun(interview,s,s,interview).
noun(introduction,e,en,einleitung).
%noun(invention,invention).
noun(invitation,e,en,einladung).
%noun(iron,iron).
noun(island,e,n,insel).
%noun(it,'IT').
noun(italian,r,'', italiener).
noun(italianin,e,nen, italienerin).
%noun(item,item).
%
%
%noun(jack,jack).
noun(jacket,e,n,jacke).
%noun(jail,jail).
%noun(jam,jam).
noun(jazz,r(st),'',jazz).
noun(jeans,e,'',jeans).
%noun(jelly,jelly).
%noun(jewel,jewel).
%noun(jewellery,jewellery).
noun(job,r(st),s,job).
noun(job,r(st),e,beruf).
noun(jobOpening,(stelle|n)+angebot).
noun(jogging,s,joggen).
%noun(joint,joint).
noun(joke,r(st),e,witz).
noun(journalist,journalist).
noun(journey,e,en,fahrt).
%noun(judge,judge).
noun(juice,r(st),:,e,saft).
%noun(jump,jump).
%noun(jumper,jumper).


%noun(kettle,kettle).
noun(ketchup,r(st),s,ketchup).
noun(ketchup,s,s,ketschup).
noun(key,r(st),'',schl�ssel).
%noun(keyboard,keyboard).
noun(motorVehicle,s,s,kfz).
%noun(kick,kick).
%noun(kid,kid).
noun(killer,r(st),'',killer).
noun(killerin,e,nen,killerin).
noun(kilogramm,r,s,kilo).
noun(kilogramm,s,'',kilogramm).
noun(kilometer,r,'',kilometer).
%noun(kind,kind).
noun(kindergarten,r(st),:,'',kindergarten).
noun(king,r,e,k�nig).
noun(kiosk,r,e,kiosk).
%noun(kiss,kiss).
%noun(kit,kit).
noun(kitchen,e,n,k�che).
%noun(kite,kite).
%noun(knee,knee).
noun(knife,s,'',messer).
%noun(knot,knot).
noun(knowledge,e,se,kenntnis).


%noun(label,label).
noun(lady,e,n,dame).
noun(lake,r(st),n,see).
noun(landlord,r(st),'',vermieter).
noun(landlady,e,nen,vermieterin).
%noun(lamination,lamination).
noun(lamp,e,n,lampe).
%noun(land,land).
%noun(landing,landing).
noun(landscape,e,en,landschaft).
noun(language,e,n,sprache).
noun(laptop,r(st),s,laptop).
noun(laptopComputer,[laptop,computer]).
%noun(laugh,laugh).
noun(loundry,e,n,w�sche).
%noun(layer,layer).
%noun(law,law).
%noun(lawyer,lawyer).
%noun(lead,lead).
noun(leaf,s,:,er,blatt).
%noun(leak,leak).
%noun(leakage,leakage).
%noun(learning,learning).
%noun(leather,leather).
%noun(leave,leave).
noun(leaving,e,en,abfahrt).
%noun(left,left).
noun(leg,s,e,bein).
noun(leisure,e,freizeit). %DE only sing
noun(lemon,e,n,zitrone).
noun(lemonIcecream,(zitrone|n)+eis).
%noun(lemonade,lemonade).
%noun(length,length).
%noun(lesson,lesson).
noun(letterPost,r(st),e,brief).
noun(letterChar,r(n),n,buchstabe).
%noun(lettuce,lettuce).
%noun(level,level).
noun(library,e,en,bibliothek).
noun(licence,licence).
noun(lie,e,n,l�ge).
noun(life,s,'',leben).
noun(lift,r(st),:,e,aufzug).
noun(light,s,er,licht).
%noun(lightning,lightning).
noun(lightSwitch,licht+schalter).
%noun(limit,limit).
noun(line,e,n,linie).
noun(link,r(st),s,link).
noun(lion,r(n),n,l�we).
%noun(lip,lip).
%noun(liquid,liquid).
noun(list,e,n,liste).
%noun(liter,liter).
%noun(liter,litre).
%noun(literacy,literacy).
noun(livingRoom,s,'',wohnzimmer).
noun(livingRoomTable,wohnzimmer+tisch).
noun(truck,s,s,lkw).
noun(local,s,e,lokal).
noun(localCommunity,e,n,gemeinde).
%noun(lock,lock).
%noun(look,look).
%noun(loop,loop).
%noun(lorry,lorry).
%noun(loss,loss). %HU:veszteseeg
%noun(love,love).
%noun(lubricant,lubricant).
noun(luck,s,gl�ck).     %DE just sing
%noun(luggage,luggage).
noun(lunch,mittag+essen).
noun(lunchBreak,(mittag|s)+pause).
noun(lunchtime,lunchtime).
%noun(lung,lung).
noun(lust,e,:,e,lust).


noun(machine,e,n,maschine). %NOT IN A2
noun(magazine,e,en,zeitschrift).
%noun(magic,magic,magic).
%noun(magnet,magnet).
noun(mail,s,s,mail).
noun(mail,e,s,mail).
noun(mailbox,e,en,mailbox).
%noun(mailman,mailman,mailmen).
noun(mainCourse,[main,course]).
noun(mainStation,haupt+bahnhof).
%noun(maintenance,maintenance).
%noun(makeup,make-up).
%noun(male,male).
noun(man,r(st),:,er,mann).
noun(manager,r(st),'',manager).
noun(managerin,e,nen,managerin).
%noun(mango,mango).
noun(manual,e,en,anleitung).
noun(manual,s,:,er,handbuch).
noun(map,e,:,e,plan).
%noun(mark,mark).
noun(market,r,:,e,markt).
%noun(marriage,marriage).
%noun(mass,mass).
%noun(match,match).
%noun(material,material).
%noun(maths,maths).
noun(maths,e,'',mathematik).
noun(matter,e,n,sache).
noun(maturaExam,s,'',abitur).
noun(maturaExam,s,'',matura).
%noun(maximum,maximum).
noun(mayor,r(st),'',b�rgermeister).
noun(mayorin,e,nen,b�rgermeisterin).
noun(means,s,'',mittel).
%noun(meal,meal).
%noun(measure,measure).
noun(meat,s,fleisch). %DE only sing
noun(mechanic,r,'',mechaniker).
%noun(mechanism,mechanism).
noun(medicine,s,e,medikament).
%noun(meeting,meeting).
%noun(melon,melon).
%noun(member,member).
%noun(memory,memory).
noun(menu,e,n,speisekarte).
noun(message,e,en,nachricht).
%noun(metal,metal).
noun(meter,r,'',meter).
%noun(meter,metre).
%noun(method,method).
noun(midday,r,e,mittag).
noun(middle,e,n,mitte).
noun(midnight,e,'',mitternacht).
%noun(mile,mile).
%noun(military,military).
noun(milk,e,e,milch).
noun(millenium,s,e,jahrtausend).
%noun(million,million).
%noun(mind,mind).
%noun(mine,mine).
noun(mineralWater,mineral+wasser).
%noun(minimum,minimum).
noun(minister,r(st),'',minister).
noun(ministerin,e,nen,ministerin).
noun(minute,e,n,minute).
%noun(mirror,mirror).
%noun(miss,'Miss').
%noun(mist,mist).
%noun(mistake,mistake).
noun(mister,r(n),en,herr).
%noun(mixture,mixture).
noun(mobilbox,e,en,mobilbox). %???HU
noun(mobilePhone,s,s,handy).
noun(mobileNumber,handy+nummer).
%noun(mode,mode).
%noun(model,model).
noun(moderator,r(en),en,moderator).
noun(moderatorin,e,nen,moderatorin).
%noun(modification,modification).
noun(modul,s,e,modul).
noun(mom,mom).
noun(moment,r(st),e,moment).
noun(money,s,geld).  %DE only sing
noun(monkey,r(st),n,affe).
noun(month,r(st),e,monat).
noun(monthEnd,(monat|s)+ende).
%noun(moon,moon).
noun(morning,r(st),e,vormittag).
noun(mosquito,e,n,m�cke).
noun(mother,e,:,'',mutter).
%noun(motion,motion).
%noun(motorbike,motorbike).
%noun(motorcycle,motorcycle).
noun(motorVehicle,s,e,kraftfahrzeug).
%noun(motorway,motorway).
%noun(mount,mount).
%noun(mountain,mountain).
noun(mountainBike,s,s,mountainbike).
noun(mouse,e,:,e,maus).
noun(mouth,r(st),:,er,mund).
%noun(movement,movement).
noun(movieStar,[movie,star]).
noun(movieTheater,[movie,theater]).
noun(movingHouse,r(st),:,e,umzug).
noun(mp3Player,['MP3',player]).
%noun(mr,r(st),en,herr).
%noun(mrs,'Mrs').
%noun(ms,'Ms').
%noun(multiplication,multiplication).
%noun(mum,mum).
%noun(muscle,muscle).
noun(museum,s,een,museum).
%noun(mushroom,mushroom).
noun(music,e,en,musik).
%noun(musician,musician).


%noun(nail,nail).
noun(name,r(n),n,name).
%noun(nation,nation).
%noun(nationality,nationality).
noun(nature,e,natur).    %DE only sing
noun(neck,r(st),:,e,hals).
noun(neckPain,hals+schmerz).
%noun(necklace,necklace).
%noun(need,need).
%noun(needle,needle).
noun(neighbour,r(n),n,nachbar).
noun(neighbourin,e,nen,nachbarin).
%noun(nephew,nephew).
%noun(nerve,nerve).
%noun(net,net).
%noun(news,news).
noun(newspaper,e,en,zeitung).
%noun(niece,niece).
noun(night,e,:,e,nacht).
%noun(nod,nod).
%noun(noise,noise).
noun(noodle,e,n,nudel).
noun(noon,r,e,mittag).
noun(north,r(st),'',norden).
%noun(nose,nose).
%noun(notch,notch).
noun(note,e,n,notiz).
noun(noteSheet,notiz+zettel).
noun(notebook,s,e,heft).
noun(notice,r(st),'',bescheid).
noun(number,e,n,nummer).
noun(number,e,en,zahl).
noun(nurse,(kranke|n)+schwester).
noun(nurseMaleLimited,r,'',krankenpfleger).
noun(nurseFemaleLimited,e,nen,krankenpflegerin).
%noun(nut,nut).


%noun(object,object).
%noun(observation,observation).
%noun(ocean,ocean).
noun(oClock,[of,the,clock]).
noun(occupation,occupation).
noun(offer,s,e,angebot).
noun(office,s,:,e,amt).
noun(office,s,s,b�ro).
noun(oil,s,e,�l).
%noun(omelette,omelette).
%noun(onion,onion).
%noun(opening,opening).
%noun(opera,opera).
%noun(operation,operation).
%noun(operator,operator).
noun(opinion,e,en,meinung).
noun(opposite,r(st),e,gegenteil).
noun(orange,e,n,orange).
noun(order,e,en,ordnung).
%noun(organization,organization).
%noun(ornament,ornament).
%noun(output,output).
%noun(outside,outside).
%noun(oven,oven).
%noun(overlap,overlap).
%noun(owner,owner).


noun(package,s,e,paket).
noun(page,e,n,seite).
noun(pain,r(st),en,schmerz).
%noun(paint,paint).
noun(painter,r,'',maler).
noun(painting,painting).
noun(pair,s,e,paar).
noun(pan,e,en,pfanne).
noun(paper,s,e,papier).
noun(paragraph,paragraph).
%noun(parcel,parcel).
noun(park,r(st),s,park).
noun(parking,s,'',parken).
noun(parkingSpot,park+platz).
noun(parliament,s,'',parlament).
noun(part,r,e,teil).
noun(part,s,e,teil).
noun(partner,r(st),'',partner).
noun(partnerin,e,nen,partnerin).
noun(partyPol,e,en,partei).
noun(party,e,s,party).
%noun(part,part).
%noun(particle,particle).
noun(passenger,passenger).
noun(passport,r(st),:,e,pass).
noun(password,s,:,er,passwort).
%noun(past,past).
%noun(pasta,pasta).
%noun(patch,patch).
%noun(path,path).
noun(pc,r(st),s,'PC').
%noun(payment,payment).
%noun(peace,peace).
%noun(peach,peach).
noun(pear,e,n,birne).
noun(pen,r(st),e,stift).
%noun(pence,pence).
noun(pencil,r(st),e,bleistift).
noun(pencilCase,[pencil,case]).
%noun(penfriend,penfriend).
noun(penguin,r(st),e,pinguin).
%noun(penny,penny).
noun(pensioner,r(st),'',rentner).
noun(pensionerin,e,nen,rentnerin).
%noun(pepper,pepper).
%noun(performance,performance).
noun(perfume,s,s,parfum).
noun(permission,e,se,erlaubnis). %DE: only sing
noun(person,e,en,person).
%noun(pet,pet).
%noun(petrol,petrol).
noun(petrolStation,[petrol,station]).
noun(pharmacy,e,n,apotheke).
noun(philosophy,e,'',philosophie).
%noun(phone,phone).
noun(phonecall,r(st),e,anruf).
noun(phonenumber,[phone,number]).
noun(photo,s,s,foto).
noun(photograph,photograph).
noun(photographer,photographer).
noun(photography,photography).
noun(physics,e,'',physik).
noun(piano,s,e,klavier).
noun(picnic,picnic).
noun(picture,s,er,bild).
%noun(pie,pie).
noun(piece,s,e,st�ck).
noun(pig,s,e,schwein).
noun(pill,e,n,tablette).
%noun(pillow,pillow).
%noun(pilot,pilot).
%noun(pin,pin).
%noun(pipe,pipe).
%noun(pity,pity).
noun(pizza,e,s,pizza).
noun(place,r(st),e,ort).
noun(place,r(st),:,e,platz).
%noun(plan,map).
%noun(plane,plane).
noun(plant,e,n,pflanze).
noun(plastic,plastic).
noun(plate,r(st),'',teller).
noun(platform,e,en,plattform).
noun(platform,r(st),e,bahnsteig).
%noun(play,play).
noun(player,r,'',player).
%noun(playground,playground).
%noun(pleasure,pleasure).
%noun(plough,plough).
noun(pocket,e,n,tasche).
noun(pocketMoney,s,taschengeld).  %DE just sing
%noun(poem,poem).
%noun(poet,poet).
noun(point,r(st),e,punkt).
%noun(poison,poison).
noun(police,e,polizei). %DE only sing
noun(policeCar,[police,car]).
noun(policeman,r(en),en,polizist).
noun(policewoman,e,nen,polizistin).
noun(policeOfficer,[police,officer]).
noun(policeStation,[police,station]).
%noun(policy,policy).
%noun(pool,pool).
%noun(pop,pop).
noun(pork,(schwein|e)+fleisch).
%noun(port,port).
%noun(porter,porter).
noun(portion,e,en,portion).
%noun(position,position).
noun(post,e,post). %DE just sing
noun(postcard,e,n,postkarte).
noun(postcode,e,n,postleitzahl).
noun(poster,s,'',poster).
noun(poster,s,e,plakat).
noun(postOffice,post+amt).
noun(pot,r(st),:,e,topf).
noun(potato,e,n,kartoffel).
%noun(pound,pound).
%noun(powder,powder).
%noun(power,power).
noun(practice,e,en,praxis).
%noun(president,r(en),en,president).
%noun(precaution,precaution).
%noun(precision,precision).
%noun(preparation,preparation).
%noun(preservation,preservation).
noun(present,s,e,geschenk).
noun(presentation,e,en,pr�sentation).
%noun(pressure,pressure).
noun(price,r(st),e,preis).
%noun(print,print).
noun(printer,r,'',drucker).
%noun(prison,prison).
%noun(prize,prize).
noun(problem,s,e,problem).
%noun(process,process).
%noun(procedure,procedure).
noun(product,s,e,produkt).
%noun(professional,professional).
%noun(profit,profit).
noun(program,e,en,sendung).
noun(programme,s,e,programm).
noun(project,s,e,projekt).
%noun(projection,projection).
%noun(property,property).
%noun(proportion,proportion).
noun(proposal,r(st),:,e,vorschlag).
noun(prospect,r(st),e,prospekt).
%noun(protection,protection).
%noun(protest,protest).
noun(provider,r(st),'',anbieter).
noun(proximity,e,n�he).   %DE only sing
%noun(pull,pull).
noun(pullover,r(st),'',pullover).
%noun(pump,pump).
%noun(punishment,punishment).
noun(pupil,r(st),'',sch�ler).
noun(pupilin,e,nen,sch�lerin).
%noun(push,push).
%noun(purpose,purpose).
noun(purse,e,n,geldb�rse).
%noun(psychology,psychology).
noun(puzzle,s,s,puzzle).


noun(quality,e,en,qualit�t).
%noun(quantity,quantity).
%noun(quarter,quarter).
%noun(queen,queen).
noun(question,e,n,frage).
%noun(queue,queue).
noun(quiet,e,ruhe).     %DE just sing
noun(quiz,s,quiz).      %DE just sing
noun(quizBroadcast,quiz+sendung).


noun(rabbit,r(st),n,hase).
%noun(race,race).
%noun(racket,racket).
noun(radio,s,s,radio).
%noun(rail,rail).
noun(railway,e,en,bahn).
noun(railwayStation,r(st),:,e,bahnhof).
noun(rain,r(st),regen).   %DE just sing
%noun(raincoat,raincoat).
%noun(range,range).
%noun(rap,rap).
%noun(rat,rat).
%noun(rate,rate).
%noun(ray,ray).
%noun(reaction,reaction).
noun(reading,s,'',lesen).
%noun(reason,reason).
%noun(receipt,receipt).
noun(receiver,r(st),'',empf�nger).
noun(reception,e,en,rezeption).
%noun(receptionist,receptionist).
noun(recipe,s,e,rezept).
%noun(record,record).
noun(red,s,s,rot).
%noun(reflection,reflection).
noun(refridgerator,r(st),:,e,k�hlschrank).
noun(registration,e,en,anmeldung).
%noun(regret,regret).
%noun(relation,relation).
noun(relative,r(n),n,verwandte).
%noun(relax,relax).
%noun(relaxation,relaxation).
%noun(release,release).
%noun(religion,religion).
noun(remain,r(st),e,rest).
%noun(removal,removal).
noun(rentalFee,e,n,miete).
noun(repair,e,en,reparatur).
%noun(representative,representative).
%noun(replacement,replacement).solution
%noun(reply,reply).
%noun(report,report).
noun(request,e,n,bitte).
%noun(research,research).
noun(residenzePlace,wohn+ort).
%noun(respect,respect).
%noun(result,result).
%noun(rest,rest).
noun(restaurant,s,s,restaurant).
noun(retirementHome,s,e,altersheim).
noun(retirementHome,s,e,altenheim).
%noun(retraction,retraction).
%noun(return,return).
%noun(reward,reward).
%noun(rhino,rhino).
%noun(rhythm,rhythm).
noun(rice,r(st),reis).     %DE only sing
%noun(ride,ride).
noun(riddle,s,'',r�tsel).
%noun(right,right).
noun(ring,r(st),e,ring).
%noun(risk,risk).
noun(river,r(st),:,e,fluss).
%noun(road,road).
%noun(rock,rock).
%noun(rod,rod).
%noun(roll,roll).
%noun(roller,roller).
%noun(roof,roof).
noun(room,r(st),:,e,raum).
noun(room,s,'',zimmer).
noun(roomKey,zimmer+schl�ssel).
%noun(rooster,rooster).
%noun(root,root).
%noun(rope,rope).
noun(rose,e,n,rose).
%noun(roundabout,roundabout).
noun(roundtour,r(st),:,e,rundgang).
noun(roundtrip,rund+reise).
%noun(route,route).
noun(row,e,n,reihe).
%noun(rubber,rubber).
noun(rubbish,r(st),m�ll). %DE only sing
%noun(rugby,rugby).
%noun(rule,rule).
%noun(ruler,ruler).
%noun(run,run).
%noun(runner,runner).
%noun(running,running).
noun(russianLanguage,s,russisch). %just singular
%
% SSSSSSSSSSSSSSSSSSSS
%
%noun(safety,safety).
%noun(sail,sail).
%noun(sailing,sailing).
noun(salad,r(st),e,salat).
noun(salary,s,:,er,gehalt).
%noun(sale,sale).
noun(salesman,r(st),s,verk�ufer).
noun(saleswoman,e,nen,verk�uferin).
noun(salt,r(st),:,e,salz).
%noun(sample,sample).
%noun(sand,sand).
noun(sandwich,s,es,sandwich).
%noun(sauce,sauce).
noun(sausage,e,:,e,wurst).
%noun(saw,saw).
%noun(scale,scale).
%noun(scan,scan).
%noun(scare,scare). %HU ijedelem, r�m�let
%noun(scarf,scarf).
noun(school,e,n,schule).
noun(scholarship,s,en,stipendium).
%noun(science,science).
noun(scissors,e,n,schere).
%noun(scratch,scratch).
%noun(screw,screw).
noun(scooter,r(st),'',motorroller).
%noun(screen,screen).
noun(sea,s,e,meer).
noun(sea,e,n,see).
%noun(seal,seal).
%noun(season,season).
%noun(seat,seat).
noun(second,e,n,sekunde).
%noun(secret,secret).
noun(secretary,e,nen,sekret�rin).
noun(section,r(st),e,abschnitt).
%noun(security,security).
noun(seeAgain,s,'',wiedersehen).
%noun(seed,seed).
%noun(selection,selection).
noun(sender,r(st),'',absender).
noun(senderin,e,nen,absenderin).
%noun(sense,sense).
%noun(senior,senior).
noun(sentence,r(st),:,e,satz).
%noun(separation,separation).
%noun(sequence,sequence).
%noun(servant,servant).
noun(service,r(st),service).    %DE just sing
%noun(set,set).
%noun(sex,sex).
%noun(silk,silk).
noun(signature,e,en,unterschrift).
noun(sister,e,n,schwester).
%noun(shade,shade).
%noun(shake,shake).
%noun(shame,shame).
%noun(shampoo,shampoo).
%noun(shape,shape).
noun(sharedApartment,e,s,'WG').
noun(sheep,s,e,schaf).
%noun(sheet,sheet).
noun(shelf,s,e,regal).
noun(shield,s,er,schild).
noun(ship,s,e,schiff).
noun(shirt,s,en,hemd).
noun(shir,s,s,shirt).
%noun(shock,shock).
noun(shoe,r(st),e,schuh).
noun(shop,s,e,gesch�ft).
noun(shop,r(st),'',laden).
noun(shopAssistant,[shop,assistant]).
noun(shopping,shopping).
noun(shoppingMall,s,en,einkaufszentrum).
%noun(shoulder,shoulder).
noun(show,e,s,show).
noun(shower,e,'',dusche).
noun(sibling,s,'',geschwister).
noun(sick,e,n,kranke). %not in A2
noun(sickness,e,n,krankheit). %not in A2
%noun(side,side).
%noun(sightseeing,sightseeing).
%noun(sign,sign).
%noun(signature,signature).
noun(sight,e,en,sehensw�rdigkeit).
noun(singleRoom,einzel+zimmer).
noun(singleChild,einzel+kind).
%noun(silk,silk).
noun(silver,silver).
noun(singer,r,'',s�nger).
noun(singerin,e,nen,s�ngerin).
%noun(singing,singing).
%noun(sink,sink).
%noun(sister,sister).
%noun(site,site).
noun(situation,e,en,situation).
noun(sittingRoom,[sitting,room]).
noun(size,e,n,gr��e).
%noun(skateboard,skateboard).
%noun(skateboarding,skateboarding).
%noun(skating,skating).
noun(ski,r(st),'',ski).
%noun(skiing,skiing).
%noun(skin,skin).
noun(skirt,r(st),:,e,rock).
noun(sky,r(st),himmel).       %DE only sing
%noun(sleep,sleep).
%noun(slice,slice).
%noun(slip,slip).
noun(slipPaper,r(st),'',zettel).
%noun(slope,slope).
%noun(slot,slot).
noun(smartPhone,s,s,smartphone).
%noun(smash,smash).
%noun(smell,smell).
%noun(smile,smile).
%noun(smoke,smoke).
noun(smoking,s,rauchen).
noun(snack,r(st),s,snack).
noun(snake,e,n,schlange).
noun(snow,r(st),schnee).       %DE only sing
%noun(snowboard,snowboard).
%noun(snowboarding,snowboarding).
%noun(sneeze,sneeze).
noun(soap,e,n,seife).
%noun(soccer,soccer).
noun(society,r(st),e,verein).
%noun(sock,sock).
%noun(soda,soda).
noun(sofa,s,s,sofa).
noun(software,e,s,software).
noun(solution,e,en,l�sung).
noun(son,r,:,e,sohn).
noun(song,s,er,lied).
%%noun(sort,sort).
%noun(sound,sound).
noun(soup,e,n,suppe).
%noun(source,source).
noun(south,r(st),'',s�den).
%noun(space,space).
%noun(spade,spade).
%noun(span,span).
noun(spanish,r(st),'',spanier).
noun(spanishin,e,nen,spanierin).
noun(spanishLanguage,s,spanisch). %no plural
%noun(spark,spark).
%noun(speaker,speaker).
noun(speaking,s,'',sprechen).
noun(specification,e,n,angabe).
%noun(speed,speed).
%noun(spelling,spelling).
%noun(sphere,sphere).
%noun(sponge,sponge).
noun(spoon,r(st),'',l�ffel).
noun(sport,r(st),sport).       %DE just sing
noun(sportsCenter,[sports,centre]).
noun(sportClub,sport+verein).
noun(sportsGround,r(st),:,e,sportplatz).
noun(sportHall,sport+halle).
noun(sportBroadcast,sport+sendung).
noun(spot,r(st),s,spot).
noun(spouse,r(st),'',ehepartner).
%noun(spray,spray).
%noun(square,square).
%noun(squirrel,squirrel).
%noun(stadium,stadium).
%noun(staff,staff).
%noun(stage,stage).
noun(stair,r(st),n,treppe).
noun(stamp,e,n,briefmarke).
%noun(stand,stand).
%noun(standard,standard).
noun(starPerson,r(st),s,star).
noun(star,s,e,stern).
%noun(start,start).
noun(state,r(st),en,staat).
noun(statement,e,n,aussage).
%noun(station,station).
noun(steak,s,s,steak).
%noun(steam,steam).
%noun(steel,steel).
%noun(stem,stem).
%noun(step,step).
%noun(stereotype,stereotype).
%noun(stick,stick).
%noun(stitch,stitch).
%noun(stocking,stocking).
noun(stomach,r,::,'',magen).
noun(stomachPain,magen+schmerz).
%noun(stone,stone).
%noun(stop,stop).
%noun(store,store).
%noun(storm,storm).
%noun(story,story).
%noun(storytelling,storytelling).
noun(stove,r(st),e,herd).
%noun(strategy,strategy).
%noun(strawberry,strawberry).
noun(street,e,n,stra�e).
%noun(strength,strength).
noun(stress,r(st),stress).     %DE just sing
%noun(stretch,stretch).
%noun(strip,strip).
%noun(stripe,stripe).
noun(structure,e,en,struktur).
noun(student,r(en),en,student).
noun(studentin,e,nen,studentin).
noun(studio,s,s,studio).
noun(study,s,studium).     %DE just sing
%noun(stuff,stuff).
noun(subject,s,:,er,fach).
noun(subscription,s,s,abo).
noun(subscription,s,e,abonnement).
%noun(substance,substance).
%noun(success,success).
noun(sugar,r(st),zucker).  %DE just sing
%noun(suggestion,suggestion).
noun(suit,r(st),:,e,anzug).
noun(suitcase,r(st),'',koffer).
%noun(sum,sum).
noun(sun,e,n,sonne).
%noun(sunglasses,sunglasses).
%noun(supply,supply).
noun(supermarket,r(st),:,e,supermarkt).
%noun(supper,supper).
%noun(support,support).
%noun(surfboard,surfboard).
%noun(surface,surface).
%noun(surfing,surfing).
noun(surname,r(st),n,familienname).
%noun(surprise,surprise).
%noun(sweater,sweater).
noun(sweet,e,en,s��igkeit).
%noun(swim,swim).
noun(swimming,s,schwimmen).
noun(swimmingCostume,[swimming,costume]).
noun(swimmingPool,r(st),s,swimmingpool).
noun(swimmingPool,schwimm+bad).
noun(swimsuit,swimsuit).
noun(swiss,r(st),'',schweizer).
noun(swissin,e,nen,schweizerin).
noun(switch,r(st),'',schalter).
%noun(symbol,symbol).
%noun(symmetry,symmetry).
%noun(symptom,symptom).
noun(system,e,n,anlage).


noun(table,r(st),e,tisch).
noun(tableTennis,s,tischtennis). %DE just sing
noun(tablet,s,s,tablet).
%noun(tail,tail).
%noun(tag,tag).
noun(tale,s,'',m�rchen).
%noun(talk,talk).
%noun(tape,tape).
noun(task,e,n,aufgabe).
%noun(taste,taste).
%noun(tax,tax).
noun(taxi,s,s,taxi).
noun(tea,r(st),s,tee).
noun(teacher,r,'',lehrer).
noun(teacherin,e,nen,lehrerin).
noun(teaching,r(st),unterricht).  %DE just sing
noun(team,s,s,team).
noun(team,e,en,mannschaft).
%noun(tear,tear).
noun(teenager,teenager).
noun(telephone,s,e,telefon).
noun(telephoneNumber,telefon+nummer).
noun(television,television).
noun(temperature,temperature).
noun(tennis,s,tennis).   %DE: just sing
%noun(tendency,tendency).
%noun(tension,tension).
noun(tent,s,e,zelt).
%noun(term,term).
noun(terminal,r(st),s,terminal).
%noun(test,test).
noun(text,r(st),e,text).
%noun(textbook,textbook).
noun(textMessage,[text,message]).
noun(thank,r(st),dank). %DE no plu
noun(theatre,s,'',theater). %AmEng
noun(theme,s,en,thema).
%noun(theory,theory).
%noun(thickness,thickness).
noun(thing,s,e,ding).
%noun(thinking,thinking).
noun(thirst,r(st),durst). %DE only sing
%noun(thought,thought).
%noun(thread,thread).
noun(thriller,r(st),s,krimi).
%noun(throat,throat).
%noun(thumb,thumb).
%noun(thunder,thunder).
noun(thunderstorm,s,'',gewitter).
noun(ticket,s,s,ticket).
noun(ticket,e,n,fahrkarte).
%noun(tie,tie).
%noun(tiger,tiger).
%noun(tightness,tightness).
noun(time,e,en,zeit).
noun(timeCase,s,e,mal).
noun(timetable,r(st),:,e,fahrplan).
%noun(tin,tin).
noun(tip,r(st),s,tipp).
noun(title,s,'',titel).
noun(toast,toast).
%noun(today,today).
%noun(toe,toe).
noun(toilet,e,n,toilette).
%noun(tolerance,tolerance).
noun(tomato,e,n,tomate).
noun(tomatoJuice,(tomate|n)+saft).
%noun(tomorrow,tomorrow).
%noun(tongue,tongue).
%noun(tonight,tonight).
noun(tool,s,'',hilfsmittel).
noun(tooth,r(st),:,e,zahn).
%noun(toothache,toothache).
%noun(toothbrush,toothbrush).
%noun(toothpaste,toothpaste).
%noun(top,top).
%noun(total,total).
%noun(touch,touch).
noun(tour,e,en,tour).
noun(tourGuide,[tour,guide]).
noun(tourist,r(st),en,tourist).
noun(touristin,e,nen,touristin).
noun(touristInfoCenter,[tourist,information,centre]).
noun(towel,s,:,er,handtuch).
noun(town,e,:,e,stadt).
%noun(toy,toy).
noun(track,s,e,gleis).
%noun(trade,trade).
noun(traffic,r(st),verkehr). %DE only sing
noun(trafficLight,e,n,ampel).
noun(train,r(st),:,e,zug).
noun(trainRestaurant,zug+restaurant).
noun(trainee,r(st),s,azubi).
noun(traineein,e,s,azubi).
noun(trainee,r(st),n,auszubildende).
noun(traineein,e,n,auszubildende).
%noun(trainer,trainer).
noun(training,r(st),s,training).
noun(tram,e,en,stra�enbahn).
noun(translation,e,en,�bersetzung).
%noun(transport,transport).
noun(transportMeans,s,'',verkehrsmittel).
%noun(trash,trash).
%noun(tray,tray).
noun(travel,e,n,reise).
noun(travelAgency,s,s,reiseb�ro).
noun(travelGuide,r(st),'',reisef�hrer).
noun(travelTicket,e,n,fahrkarte).
noun(travelTicketMachine,(fahrkarte|n)+automat).
noun(tree,r(st),::,e,baum).
nounDecl(baum,sg,gen,baum,es).
noun(trend,r(st),s,trend).
%noun(trick,trick).
noun(trip,e,en,tour).
%noun(trouble,trouble).
noun(trouser,e,n,hose).
noun(truck,s,'',lastkraftwagen).
%noun(try,try).
noun(tshirt,t-shirt).
%noun(turkey,turkey).
noun(turk,r(st),n,turke).
noun(turkin,e,nen,turkin).
%noun(turn,turn).
noun(turtle,e,n,schildkr�te).
noun(tv,s,'','TV').
noun(television,s,fernsehen). %no plural
noun(television,r(st),'',fernseher).
%noun(twist,twist).
%noun(type,type).
noun(tire,r(st),'',reifen).


noun(umbrella,r(st),e,regenschirm).
noun(umbrella,r(st),e,schirm).
noun(ukrainian,r(st),'',ukrainer).
noun(ukrainianin,e,nen,ukrainerin).
noun(uncle,r,'',onkel).
noun(underground,e,en,u-bahn).
%noun(unit,unit).
%noun(uniform,uniform).
noun(university,e,en,universit�t).
%noun(use,use).
noun(user,r(st),'',user).
noun(userin,e,nen,userin).


noun(vacation,r(st),e,urlaub).
noun(vacationJob,ferien+job).
%noun(valentine,'Valentine').
%noun(value,value).
noun(vegetable,s,gem�se). %DE only sing
%noun(verse,verse).
%noun(vessel,vessel).
%noun(vibration,vibration).
noun(video,video).
noun(videoGame,[video,game]).
%noun(view,view).
noun(village,s,:,er,dorf).
noun(violin,violin).
noun(visit,r(st),e,besuch).
%noun(visitor,visitor).
%noun(voice,voice).
%noun(vocabulary,vocabulary).
noun(volleyball,r(st),:,e,volleyball).
%noun(volume,volume).


%noun(waiter,waiter).
%noun(waitress,waitress).
noun(walk,spazier+gang).
noun(wall,e,:,e,wand).
%noun(wallet,wallet).
%noun(war,war).
noun(wardrobe,r(st),:,e,schrank).
%noun(warning,warning).
%noun(wash,wash).
noun(washingMachine,wasch+maschine). %NOT IN A2
noun(washingMachineConnection,((wasch+maschine)|n)+anschluss). %NOT IN A2
noun(washingUp,[washing,up]).
noun(waste,r(st),:,e,abfall).
noun(watch,watch).
noun(water,s,wasser).    %DE just sing
%noun(wave,wave).
%noun(wax,wax).
noun(way,r(st),e,weg).
noun(wc,s,wc).
noun(weather,s,wetter).   %DE just sing
noun(web,web).
noun(webPage,web+seite).
noun(website,e,n,website).
noun(wedding,e,en,hochzeit).
noun(wednesEve,mittwoch+abend).
noun(week,e,n,woche).
noun(weekday,r(st),e,wochentag).
noun(weekend,s,n,wochenende).
%noun(weight,weight).
noun(west,r(st),'',westen).
noun(whale,whale).
noun(wheel,wheel).
%noun(wheelchair,wheelchair).
%noun(whip,whip).
%noun(whistle,whistle).
%noun(white,white).
noun(whiteBread,wei�+brot).
noun(whole,whole).
%noun(width,width).
noun(wife,e,en,ehefrau).
noun(wifi,wifi).
noun(wind,r(st),e,wind).
noun(window,s,'',fenster).
noun(windsurfing,windsurfing).
noun(wine,r(st),e,wein).
%noun(wing,wing).
noun(winner,winner).
%noun(wire,wire).
noun(wish,s,:,e,wunsch).
noun(woman,e,en,frau).
noun(wood,r(st),:,er,wald).
noun(wool,wool).
noun(word,s,:,er,wort).
noun(work,e,en,arbeit).
noun(worker,worker).
noun(workingPermission,(arbeit|s)+erlaubnis).
noun(workplace,e,en,stelle).
noun(workshop,e,:,en,werkstatt).
noun(workshop,r(st),'',workshop).
noun(world,e,en,welt).
%noun(worm,worm).
%noun(wound,wound).
noun(writer,writer).
noun(writing,s,'',schreiben).


noun(year,s,e,jahr).
noun(yoghurt,yoghurt).
noun(yoghurt,yogurt).
noun(youth,r(st),n,jugendliche).
noun(youthin,e,n,jugendliche).
noun(youthHostel,e,n,jugendherberge).


%noun(zebra,zebra).
noun(zero,e,en,null).
noun(zoo,r(st),s,zoo).

plnoun(data,daten).                  %DE
plnoun(frenchFries,[pommes,frites]). %DE
plnoun(frenchFries,pommes).          %DE
plnoun(parents,eltern).              %DE
plnoun(people,leute).                %DE
plnoun(vacation,ferien).             %DE




